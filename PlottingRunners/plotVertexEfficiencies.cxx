#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/TriggerVariables.h"
#include "PlottingPackage/EfficiencyHistograms.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/SystHistograms.h"
#include "PlottingPackage/HistTools.h"

#include "TVector3.h"
//#include "PlottingPackage/PlottingUtils.h"
//#include "CommonUtils/MathUtils.h"
#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLorentzVector.h"
#include <TString.h>

using namespace std;
using namespace plotVertexParameters;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;

}
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

int isSignal; int isData;
double jetSliceWeights[13];
TStopwatch timer;
int nEvt_passedNoIsoOffIso;
int nEvt_passedIso;
int nEvt_passedNoIsoOffIso_Barrel;
int nEvt_passedIso_Barrel;
int nEvt_passedNoIsoOffIso_EndCaps;
int nEvt_passedIso_EndCaps;
//
double nMSVx;
double nBMSVx;
double nEMSVx;
double nMSVx_passHitQual;
double nMSVx_passJetIso;
double nMSVx_passTrkIso;
double nMSVx_passGVC;
double nBMSVx_passJetIso03;
double nBMSVx_passJetIso06;
double nBMSVx_passJetIso10;
double nEMSVx_passJetIso03;
double nEMSVx_passJetIso06;
double nEMSVx_passJetIso10;
//
int nEvt_MoreThanOneVtx;
int nEvt_MoreThanOneVtx_noiso;

double nEventsPassTrigger;

int main(int argc, char **argv){


    double nBarrel_ABCD_1vx_events = 0;
    double nEndcap_ABCD_1vx_events = 0;

    std::cout << "running program!" << std::endl;

    TChain *chain = new TChain("recoTree");

    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    std::cout << "Output File name is: " << argv[3] << std::endl;
    bool applyVertexSF = (TString(argv[4]) == "true") ? true : false;
    std::cout << "Apply scale factor? " << argv[4] << " (" << applyVertexSF << ")" << std::endl;
    int SYST_VAL = atoi(argv[5]);
    std::cout << "Systematics are -1/0/+1? " << argv[5] << "( " << SYST_VAL << ")" << std::endl;
    bool applyTriggerSF = (TString(argv[6]) == "true") ? true : false;
    std::cout << "Apply trigger scale factor? " << argv[6] << " (" << applyTriggerSF << ")" << std::endl;
    bool doPDFSystematics = (TString(argv[7]) == "true") ? true : false;
    std::cout << "PDF Systematics are: " << argv[7] << " ("<< doPDFSystematics << ")" << std::endl;
    int nPDFs = doPDFSystematics ? 101 : 0;
    bool doPileupSystematics = (TString(argv[8]) == "true") ? true : false;
    std::cout << "Pileup systematics are: " << argv[8] << " (" << doPileupSystematics << ")" << std::endl;

    InputFiles::AddFilesToChain(TString(argv[1]), chain);
    std::cout << " running on: " << chain->GetEntries() << " events" << std::endl;

    //Directory the plots will go in

    TString plotDirectory = TString(argv[2]);
    TFile *outputFile = new TFile(plotDirectory+"/"+TString(argv[3]),"RECREATE");

    TriggerVariables *trig = new TriggerVariables;
    VertexVariables *msvx = new VertexVariables;
    CommonVariables *commonVar = new CommonVariables;
    LLPVariables *llpVar = new LLPVariables;

    TChain *vxChain = new TChain("recoTree");
    std::map<ULong64_t, int> msvxMap;
    ULong64_t eventNumber = 0;
    if(applyVertexSF){
        InputFiles::AddVertexRerunFiles(TString(argv[1]), vxChain);
        vxChain->SetBranchStatus("*",0);
        msvxMap = CreateMap(vxChain);
        //have to have the next few lines here or it segfaults, because it's used in CreateMap.
        //I don't fully understand why SetBranchStatus("*",0) doesn't get rid of it.
        vxChain->SetBranchStatus("*",1);
        vxChain->SetBranchAddress("EventNumber", &eventNumber);
    }

    EfficiencyHistograms *effHistMSVx = new EfficiencyHistograms;
    EfficiencyHistograms *effHistMSTrig = new EfficiencyHistograms;
    EfficiencyHistograms *effHistLLPJet = new EfficiencyHistograms;

    setPrettyCanvasStyle();

    std::cout << "set canvas style" << std::endl;

    TH1::SetDefaultSumw2();

    isSignal = -1;
    chain->SetBranchStatus("*",1);
    trig->setZeroVectors();
    trig->setBranchAdresses(chain,applyTriggerSF);
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain, true, false, false ,true, doPDFSystematics);
    llpVar->setZeroVectors();
    llpVar->setBranchAddresses(chain);
    msvx->setZeroVectors(applyVertexSF);
    msvx->setBranchAdresses(applyVertexSF ? vxChain : chain, applyVertexSF, true);
//    msvx->setBranchAdresses(chain, applyVertexSF,true);

    //
    effHistMSVx->addHist("MSVxTrig_Barrel_Lxy",40,0,10);
    effHistMSVx->addHist("MSVxTrig_1B_Lxy",40,0,10);
    effHistMSVx->addHist("MSVxTrig_Endcap_Lz",60,0,15);
    effHistMSVx->addHist("MSVxTrig_1E_Lz",60,0,15);
    effHistMSVx->addHist("MSVxTrig_BB_Lxy",10,0,10);
    effHistMSVx->addHist("MSVxTrig_BE_Lxy",10,0,10);
    effHistMSVx->addHist("MSVxTrig_EB_Lz",15,0,15);
    effHistMSVx->addHist("MSVxTrig_EE_Lz",15,0,15);


    effHistMSVx->addEffHist("MSVxTrig_BB",40,0,10);
    effHistMSVx->addEffHist("MSVxTrig_BE",40,0,10);
    effHistMSVx->addEffHist("MSVxTrig_EB",60,0,15);
    effHistMSVx->addEffHist("MSVxTrig_EE",60,0,15);

    effHistMSVx->addHist("MSVxABCD_BB_Lxy",10,0,10);
    effHistMSVx->addHist("MSVxABCD_BE_Lxy",10,0,10);
    effHistMSVx->addHist("MSVxABCD_EB_Lz",15,0,15);
    effHistMSVx->addHist("MSVxABCD_EE_Lz",15,0,15);

    effHistMSVx->addHist("MSVxABCD_1B_Lxy",10,0,10);
    effHistMSVx->addHist("MSVxABCD_1E_Lz",15,0,15);

    effHistMSVx->addHist("MSVx_eta",100,-2.5,2.5);

    effHistMSVx->addHist("MSVx_Barrel_Lxy",40,0,10);
    effHistMSVx->addHist("MSVx_1B_Lxy",40,0,10);
    effHistMSVx->addHist("MSVx_BB_Lxy",40,0,10);
    effHistMSVx->addHist("MSVx_BE_Lxy",40,0,10);
    effHistMSVx->addHist("MSVx_Endcap_Lz",60,0,15);
    effHistMSVx->addHist("MSVx_1E_Lz",60,0,15);
    effHistMSVx->addHist("MSVx_EB_Lz",60,0,15);
    effHistMSVx->addHist("MSVx_EE_Lz",60,0,15);

    effHistMSVx->addHist("MSVx_Barrel_pT_A",50,0,2500);
    effHistMSVx->addHist("MSVx_Endcap_pT_A",50,0,2500);

    effHistMSVx->addHist("MSVx_Barrel_pT_B",50,0,2500);
    effHistMSVx->addHist("MSVx_Endcap_pT_B",50,0,2500);

    effHistMSVx->addHist("MSVx_Barrel_pTAll",30,0,300);
    effHistMSVx->addHist("MSVx_Endcap_pTAll",30,0,300);

    effHistMSTrig->addHist("MSTrig_1B_Lxy",40,0,10);
    effHistMSTrig->addHist("MSTrig_BID_Lxy",40,0,10);
    effHistMSTrig->addHist("MSTrig_EID_Lz",60,0,15);
    effHistMSTrig->addHist("MSTrig_1E_Lz",60,0,15);

    effHistMSTrig->addHist("MSTrig_BB",40,0,10,40,0,10);
    effHistMSTrig->addHist("MSTrig_BE",60,0,15,40,0,10);
    effHistMSTrig->addHist("MSTrig_EE",60,0,15,60,0,15);

    effHistLLPJet->addHist("LLPJetEff",50,0,5,50,0,5);
    effHistLLPJet->addHist("LLPJetEvent_Eff",100,0,10,100,0,10);

    Double_t pzbinsb[30] = {0,50,100,150,200,250,300,350,400,450,500,550,600,650,700,800,900,1000,1100,1200,1300,1400,1500,1750,2000,2500,3000,3500,4000,5000};

    effHistLLPJet->addHist("LLPJetEff_pT",100,0,1000);
    effHistLLPJet->addHist("LLPJetEff_pz",29, pzbinsb);
    effHistLLPJet->addHist("LLPJetEff_eta",64,-3.2,3.2);
    effHistLLPJet->addHist("LLPJetEff_E",100,0,1000);
    effHistLLPJet->addHist("LLPJetEff_Lxy",40,0,4);
    effHistLLPJet->addHist("LLPJetEff_Lz",40,0,10);
    effHistLLPJet->addHist("LLPJetProb_Lxy",40,0,4);
    effHistLLPJet->addHist("LLPJetEff_pT_Lxy",100,0,10,100,0,1000);
    Double_t ptbins[10] = {0,50,100,150,200,250,300,350,400,500};
    Double_t pzbins[10] = {0,100,200,300,400,500,600,800,1000,1200};
    Double_t Lxbins[10] = {0,0.5,1,1.5,2,2.5,3,3.5,4,5};
    Double_t Lzbins[11] = {0,0.5,1,1.5,2,2.5,3,3.5,4,6,8};
    effHistLLPJet->addHist("LLPJetEff2_pT_Lxy",9,Lxbins,9, ptbins);
    effHistLLPJet->addHist("LLPJetEff_pz_Lz",100,0,20,100,0,1000);
    effHistLLPJet->addHist("LLPJetEff2_pz_Lz",10,Lzbins,9,pzbins);

    effHistLLPJet->addHist("LLPJetEff2E_pT_Lxy",9,Lxbins,9, ptbins);
    effHistLLPJet->addHist("LLPJetEff2E_pz_Lz",10,Lzbins,9,pzbins);

    char name[40];
    if(doPDFSystematics){
        for(int i=0;i < nPDFs; i++){
            sprintf(name,"%s%d","MSVx_Barrel_Lxy_",i);
            effHistMSVx->addHist(TString(name),40,0,10);
            sprintf(name,"%s%d","MSVx_Endcap_Lz_",i);
            effHistMSVx->addHist(name,60,0,15);
        }
    }
    if(doPileupSystematics){
        effHistMSVx->addHist("MSVx_Barrel_Lxy_PRW_1up",40,0,10);
        effHistMSVx->addHist("MSVx_Barrel_Lxy_PRW_1down",40,0,10);
        effHistMSVx->addHist("MSVx_Endcap_Lz_PRW_1up",60,0,15);
        effHistMSVx->addHist("MSVx_Endcap_Lz_PRW_1down",60,0,15);
    }
    HistTools *hists = new HistTools;
    hists->addHist("LLP_trig_dR_B",200,0,2);
    hists->addHist("LLP_trig_dR_E",200,0,2);
    hists->addHist("Trig_jet_dR_B",200,0,5);
    hists->addHist("Trig_jet_dR_E",200,0,5);
    hists->addHist("Trig_trk_dR_B",200,0,5);
    hists->addHist("Trig_trk_dR_E",200,0,5);
    hists->addHist("LLP_closestJet_dR_Lxy",50,0,5,200,0,1);

    nEvt_passedNoIsoOffIso=0;
    nEvt_passedIso=0;
    nEvt_passedNoIsoOffIso_Barrel=0;
    nEvt_passedIso_Barrel=0;
    nEvt_passedNoIsoOffIso_EndCaps=0;
    nEvt_passedIso_EndCaps=0;
    //
    nMSVx=0;
    nBMSVx=0;
    nEMSVx=0;
    nMSVx_passHitQual=0;
    nMSVx_passJetIso=0;
    nMSVx_passTrkIso=0;
    nMSVx_passGVC=0;
    nBMSVx_passJetIso03=0;
    nBMSVx_passJetIso06=0;
    nBMSVx_passJetIso10=0;
    nEMSVx_passJetIso03=0;
    nEMSVx_passJetIso06=0;
    nEMSVx_passJetIso10=0;

    double nBB_2MSVx=0;
    double nBE_2MSVx=0;
    double nEE_2MSVx=0;

    //
    nEvt_MoreThanOneVtx=0;
    nEvt_MoreThanOneVtx_noiso=0;

    nEventsPassTrigger=0;

    timer.Start();

    setJetSliceWeights(jetSliceWeights);

    int jetSlice = -1;
    double finalWeight = 0;
    bool debug = true;

    for (int l=0;l<chain->GetEntries();l++)
    {

        if(l % 50000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        if(l == 74424 && TString(argv[1]) == "mH100mS8lt5") continue;
        if(debug) std::cout << "Now getting entry..." << std::endl;
        chain->GetEntry(l);
        if(debug) std::cout << "Got entry " << l << std::endl;
        if(applyVertexSF) {
            std::cout << "Uh oh! applyVertexSF = true" << std::endl;
            vxChain->GetEntry(msvxMap[commonVar->eventNumber]);
        }
        if(debug) std::cout << "Past if statement..." << std::endl;
        //commonVar->fixJetET();
//        if(debug) std::cout << "does this event have vertices? " << msvx->eta->size() << std::endl;
        //if(MSVertex_eta->size() == 0) continue;

        TString inputFile = TString(chain->GetCurrentFile()->GetName());
        if(debug) std::cout << "Got name of input file" << std::endl;

        //TString inputFile = chain->GetFile()->GetName();
        //if( l % 100000 == 0 ) std::cout << "running over file: " << inputFile << std::endl;
        if(   inputFile.Contains("JZ")    ){
            isSignal = 0; isData = 0;
            if(inputFile.Contains("JZ0W")) jetSlice = 0;
            if(inputFile.Contains("JZ1W")) jetSlice = 1;
            if(inputFile.Contains("JZ2W")) jetSlice = 2;
            if(inputFile.Contains("JZ3W")) jetSlice = 3;
            if(inputFile.Contains("JZ4W")) jetSlice = 4;
            if(inputFile.Contains("JZ5W")) jetSlice = 5;
            if(inputFile.Contains("JZ6W")) jetSlice = 6;
            if(inputFile.Contains("JZ7W")) jetSlice = 7;
            if(inputFile.Contains("JZ8W")) jetSlice = 8;
            if(inputFile.Contains("JZ9W")) jetSlice = 9;
            if(inputFile.Contains("JZ10W")) jetSlice = 10;
            if(inputFile.Contains("JZ11W")) jetSlice = 11;
            if(inputFile.Contains("JZ12W")) jetSlice = 12;
        }
        if( inputFile.Contains("LongLived") || inputFile.Contains("_LLP")|| inputFile.Contains("ChiChi")  || inputFile.Contains("Stealth")){ isSignal = 1; isData = 0; }
        if( inputFile.Contains("data15_13TeV")){ isSignal=0; isData = 1;}
        if(!isData){
            if(isSignal == 0 ) finalWeight = jetSliceWeights[jetSlice] * commonVar->eventWeight;
            else if( isSignal == 1 ){ 
//                finalWeight = commonVar->pileupEventWeight;
                finalWeight = 1;
            }
            else std::cout << "what on earth are you running on? Input file is " << inputFile << std::endl;
            if(l==0) std::cout << "isSignal: " << isSignal << std::endl;
        }
        else if(isData) finalWeight = 1.0;
    
        if(debug) std::cout << "Event weight is: " << finalWeight << std::endl;
        if(debug) std::cout << "Finishing parsing input file." << std::endl;
        //if(l == 74424 ) std::cout << "made it to : " << __LINE__ << std::endl;
        int nBarrel=0; int nEndcap = 0; int nID = 0;
        int bIndex = -1; int eIndex = -1;
        std::vector<int> llpMatching;

        if(debug) std::cout << "Now filling denominators" << std::endl;
        //fill denominators
        int nInCone[2][40]; for(int i=0;i<40;i++){ nInCone[0][i]=0;nInCone[1][i]=0;}
        //int nEtaSlice[2]; nEtaSlice[0] = -1; nEtaSlice[1] = -1;
        if(debug) std::cout << "Filled denominators." << std::endl;
        double aa = 0.05;

        bool is2J150Event = false;
        bool is2J250Event = false;

        bool eventHasLT40_Tracklets = true;
        if(applyVertexSF){
            if(msvx->countScaledTracklets() > 40) eventHasLT40_Tracklets = false;
        } else if(msvx->tracklet_eta->size() > 40){
             eventHasLT40_Tracklets = false;
            if(debug) std::cout << "Enforcing cut on number of tracklets." << std::endl;
        }
        if(debug) std::cout << "Went through some variable declarations." << std::endl;
        bool passesEventQualityCriteria = commonVar->isQualityEvent(isData);
        if(debug) std::cout << "Checked commonVar quality event" << std::endl;
        bool LLPsAreSeparated = false;
        double trig_eta =-99;
        double trig_phi = -99;//trig->phi->at(0);
        if(applyTriggerSF && commonVar->passMuvtx_noiso){
            //make the scale-factored clusters!
            trig->reclusterRoIs();
            //std::cout << "number of new clusters: " << trig->cluSyst->size() << std::endl;

        if(debug) std::cout << "Entering loop on trig->cluEta" << std::endl;
            for(unsigned int i=0; i<trig->cluEta->size(); i++){
                if(trig->cluSyst->at(i) == SYST_VAL){
                    trig_eta = trig->cluEta->at(i);
                    trig_phi = trig->cluPhi->at(i);
                    break; //pick out the FIRST in the list - what the trigger sees....
                }
            }
        if(debug) std::cout << "Assigned trigger eta and phi" << std::endl;

            if(trig_eta > -90){
                commonVar->passMuvtx_noiso = true;
            } else{
                commonVar->passMuvtx_noiso = false;
            }
        } else{
            if(debug) std::cout << "Look at trigger eta and phi" << std::endl;
            if(trig->mu_roi_eta->size() > 0){
                trig_eta=  trig->mu_roi_eta->at(0);
                trig_phi=  trig->mu_roi_phi->at(0);
            }
        }
        
        bool triggerMatch = false;
        if(isSignal){
            if(debug) std::cout << "Now looking for trigger matching" << std::endl;
            llpVar->set_pz();

            double deltaR2LLPs = DeltaR2(llpVar->phi->at(0),llpVar->phi->at(1),llpVar->eta->at(0),llpVar->eta->at(1));
            LLPsAreSeparated = (deltaR2LLPs > 1.0) ? true : false;
            if(debug) std::cout << "LLP dR: " << sqrt(deltaR2LLPs) << std::endl;
            if(llpVar->Lxy->at(0) > llpVar->Lxy->at(1) ){
                effHistLLPJet->fill("LLPJetEvent_Eff_denom",llpVar->Lxy->at(0)*0.001,llpVar->Lxy->at(1)*0.001,finalWeight);
            } else{
                effHistLLPJet->fill("LLPJetEvent_Eff_denom",llpVar->Lxy->at(1)*0.001,llpVar->Lxy->at(0)*0.001,finalWeight);
            }
            for(unsigned int j=0; j<llpVar->eta->size(); j++){
                double dR2_trig_llp = DeltaR2(llpVar->phi->at(j),trig_phi,llpVar->eta->at(j),trig_eta);
                if (dR2_trig_llp < 0.16) triggerMatch = true;
                double closestJetdR = 99;
                for(unsigned int iJ = 0; iJ < commonVar->Jet_eta->size(); iJ++){
                    if( !commonVar->Jet_passJVT->at(iJ) ) continue;
                    if( !commonVar->Jet_isGoodStand->at(iJ) ) continue;
                    if( commonVar->Jet_ET->at(iJ) < 100. ) continue;
                    double dr2 = DeltaR2(commonVar->Jet_phi->at(iJ), llpVar->phi->at(j),commonVar->Jet_eta->at(iJ), llpVar->eta->at(j));
                    if(dr2 < closestJetdR) closestJetdR = dr2;
                }
                hists->fill("LLP_closestJet_dR_Lxy",llpVar->Lxy->at(j)*0.001, sqrt(closestJetdR),finalWeight);

                llpMatching.push_back(0);
                int isBarrel = 0; int isEndcap = 0;
                if( (fabs(llpVar->eta->at(j)) < 0.7) && llpVar->Lxy->at(j) > 3000. && llpVar->Lxy->at(j) < 8000.) isBarrel = 1;
                else if( (fabs(llpVar->eta->at(j)) > 1.3) &&(fabs(llpVar->eta->at(j))< 2.5)&& llpVar->Lz->at(j) > 5000. && llpVar->Lz->at(j) < 15000. && llpVar->Lxy->at(j) < 10000.) isEndcap = 1;
                else if( fabs(llpVar->eta->at(j)) < 2.5 && llpVar->Lxy->at(j) < 275. && llpVar->Lz->at(j) < 840.) nID++;

                //no eta cuts, for eta efficiency
                if((llpVar->Lz->at(j) > 5000. && llpVar->Lz->at(j) < 15000. && llpVar->Lxy->at(j) < 10000.) ||( llpVar->Lxy->at(j) > 3000. && llpVar->Lxy->at(j) < 8000.) ){
                    effHistMSVx->fill("MSVx_eta_denom",llpVar->eta->at(j),finalWeight);
                }

                if(isBarrel || isEndcap){
                    /*if((fabs(llpVar->eta->at(j)) < 0.7)) nEtaSlice[j] = 0;
                    else if((fabs(llpVar->eta->at(j)) < 1.0)) nEtaSlice[j] = 1;
                    else if((fabs(llpVar->eta->at(j)) < 1.5)) nEtaSlice[j] = 2;
                    else if((fabs(llpVar->eta->at(j)) < 1.7)) nEtaSlice[j] = 3;
                    else if((fabs(llpVar->eta->at(j)) < 2.7)) nEtaSlice[j] = 4;*/

                    for(size_t tt = 0; tt < msvx->tracklet_eta->size(); tt++){
                        double eta = msvx->tracklet_eta->at(tt);
                        double phi = msvx->tracklet_phi->at(tt);
                        double dR_llp = DeltaR(phi,llpVar->phi->at(j),eta,llpVar->eta->at(j));
                        for(int i=1;i<41;i++){
                            if( dR_llp < aa * double(i) ) nInCone[j][i-1]++;
                        }
                    }

                }
                effHistLLPJet->fill("LLPJetEff_denom",llpVar->Lxy->at(j)*0.001,llpVar->Lz->at(j)*0.001,finalWeight);
                if(TMath::Abs(llpVar->eta->at(j)) < 3.2){
                    effHistLLPJet->fill("LLPJetProb_Lxy_denom",llpVar->Lxy->at(j)*0.001,finalWeight);
                    effHistLLPJet->fill("LLPJetEff_pT_Lxy_denom",llpVar->Lxy->at(j)*0.001,llpVar->pT->at(j)*0.001,finalWeight);
                }
                if(TMath::Abs(llpVar->eta->at(j)) < 1.5){ effHistLLPJet->fill("LLPJetEff_Lxy_denom",llpVar->Lxy->at(j)*0.001,finalWeight);}
                if(TMath::Abs(llpVar->eta->at(j)) < 3.2 && TMath::Abs(llpVar->eta->at(j)) > 1.5 ){ effHistLLPJet->fill("LLPJetEff_Lz_denom",llpVar->Lz->at(j)*0.001,finalWeight);}

                if(llpVar->Lxy->at(j) < 2000.) { effHistLLPJet->fill("LLPJetEff_eta_denom",llpVar->eta->at(j),finalWeight);}
                if(TMath::Abs(llpVar->eta->at(j)) < 3.2 && llpVar->Lxy->at(j) < 2000. ){effHistLLPJet->fill("LLPJetEff_E_denom",llpVar->E->at(j)*0.001,finalWeight);}
                if(TMath::Abs(llpVar->eta->at(j)) < 1.5 && llpVar->Lxy->at(j) < 2000. ) {effHistLLPJet->fill("LLPJetEff_pT_denom",llpVar->pT->at(j)*0.001,finalWeight);}
                if(TMath::Abs(llpVar->eta->at(j)) < 3.3 && TMath::Abs(llpVar->eta->at(j)) > 1.5 && llpVar->Lz->at(j) < 4300. ) {effHistLLPJet->fill("LLPJetEff_pz_denom",TMath::Abs(llpVar->pz->at(j))*0.001,finalWeight);}
                if(isBarrel){
                    nBarrel++; bIndex = j;
                    effHistMSVx->fill("MSVx_Barrel_pTAll_denom",llpVar->pT->at(j)*0.001,finalWeight);
                    if(llpVar->Lxy->at(j) < 3800){ effHistMSVx->fill("MSVx_Barrel_pT_A_denom",llpVar->pT->at(j)*0.001,finalWeight);}
                    else { effHistMSVx->fill("MSVx_Barrel_pT_B_denom",llpVar->pT->at(j)*0.001,finalWeight);}
                    if(doPDFSystematics) {
                        for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                            sprintf(name,"%s%d%s","MSVx_Barrel_Lxy_",i_pdf,"_denom");
                            effHistMSVx->fill(TString(name),llpVar->Lxy->at(j)*0.001,finalWeight*commonVar->pdfWeights->at(i_pdf));
                        }
                    }
                    if(doPileupSystematics){
                        effHistMSVx->fill("MSVx_Barrel_Lxy_PRW_1up_denom", llpVar->Lxy->at(j)*0.001,commonVar->prw_1up);
                        effHistMSVx->fill("MSVx_Barrel_Lxy_PRW_1down_denom", llpVar->Lxy->at(j)*0.001,commonVar->prw_1down);
                    }
                    effHistMSVx->fill("MSVx_Barrel_Lxy_denom",llpVar->Lxy->at(j)*0.001,finalWeight);
                    effHistMSVx->fill("MSVxTrig_Barrel_Lxy_denom",llpVar->Lxy->at(j)*0.001,finalWeight);
                }
                else if(isEndcap){
                    effHistMSVx->fill("MSVx_Endcap_pTAll_denom",llpVar->pT->at(j)*0.001,finalWeight);
                    if(llpVar->Lz->at(j) < 6000){ effHistMSVx->fill("MSVx_Endcap_pT_A_denom",llpVar->pT->at(j)*0.001,finalWeight);}
                    else { effHistMSVx->fill("MSVx_Endcap_pT_B_denom",llpVar->pT->at(j)*0.001,finalWeight);}
                    effHistMSVx->fill("MSVx_Endcap_Lz_denom",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    effHistMSVx->fill("MSVxTrig_Endcap_Lz_denom",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    if(doPDFSystematics) {
                        for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                            sprintf(name,"%s%d%s","MSVx_Endcap_Lz_",i_pdf,"_denom");
                            effHistMSVx->fill(TString(name),fabs(llpVar->Lz->at(j)*0.001),finalWeight*commonVar->pdfWeights->at(i_pdf));
                        }
                    }
                    if(doPileupSystematics){
                        effHistMSVx->fill("MSVx_Endcap_Lz_PRW_1up_denom",fabs(llpVar->Lz->at(j)*0.001),commonVar->prw_1up);
                        effHistMSVx->fill("MSVx_Endcap_Lz_PRW_1down_denom",fabs(llpVar->Lz->at(j)*0.001),commonVar->prw_1down);
                    }
                    nEndcap++; eIndex=j;
                }
            }
            //std::cout << "made it through llp loop" << std::endl;
            //if(Lxy->size() == 2){
            if(nBarrel == 1 && nEndcap ==0 ){
                effHistMSVx->fill("MSVx_1B_Lxy_denom",llpVar->Lxy->at(bIndex)*0.001,finalWeight);
                effHistMSVx->fill("MSVxTrig_1B_Lxy_denom",llpVar->Lxy->at(bIndex)*0.001,finalWeight);
                if(commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria) {
                    effHistMSVx->fill("MSVxABCD_1B_Lxy_denom",llpVar->Lxy->at(bIndex)*0.001, finalWeight);
                }
            }
            else if(nBarrel == 1 && nEndcap == 1){
                if(commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria){
                    if(LLPsAreSeparated){ 
                        effHistMSVx->fill("MSVxTrig_BE_Lxy_denom",llpVar->Lxy->at(bIndex)*0.001,finalWeight);
                        effHistMSVx->fill("MSVxTrig_EB_Lz_denom",fabs(llpVar->Lz->at(eIndex))*0.001,finalWeight);
                    }
                    effHistMSVx->fill("MSVxABCD_BE_Lxy_denom",llpVar->Lxy->at(bIndex)*0.001,finalWeight);
                    effHistMSVx->fill("MSVxABCD_EB_Lz_denom",fabs(llpVar->Lz->at(eIndex))*0.001,finalWeight);
                }
                effHistMSVx->fill("MSVx_BE_Lxy_denom",llpVar->Lxy->at(bIndex)*0.001,finalWeight);
                effHistMSVx->fill("MSVx_EB_Lz_denom",fabs(llpVar->Lz->at(eIndex))*0.001,finalWeight);

            }
            else if(nBarrel == 2 ){
                if(commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria){
                    if(LLPsAreSeparated){
                        effHistMSVx->fill("MSVxTrig_BB_Lxy_denom",llpVar->Lxy->at(0)*0.001,finalWeight);
                        effHistMSVx->fill("MSVxTrig_BB_Lxy_denom",llpVar->Lxy->at(1)*0.001,finalWeight);
                    }
                    effHistMSVx->fill("MSVxABCD_BB_Lxy_denom",llpVar->Lxy->at(0)*0.001,finalWeight);
                    effHistMSVx->fill("MSVxABCD_BB_Lxy_denom",llpVar->Lxy->at(1)*0.001,finalWeight);
                }
                effHistMSVx->fill("MSVx_BB_Lxy_denom",llpVar->Lxy->at(0)*0.001,finalWeight);
                effHistMSVx->fill("MSVx_BB_Lxy_denom",llpVar->Lxy->at(1)*0.001,finalWeight);
            }
            else if(nBarrel == 0 && nEndcap ==1){
                effHistMSVx->fill("MSVx_1E_Lz_denom",fabs(llpVar->Lz->at(eIndex))*0.001,finalWeight);
                effHistMSVx->fill("MSVxTrig_1E_Lz_denom",fabs(llpVar->Lz->at(eIndex))*0.001,finalWeight);
                if(commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria){
                    effHistMSVx->fill("MSVxABCD_1E_Lz_denom",fabs(llpVar->Lz->at(eIndex))*0.001,finalWeight);
                }
            }
            else if(nEndcap == 2){
                if(commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria){
                    if(LLPsAreSeparated){
                        effHistMSVx->fill("MSVxTrig_EE_Lz_denom",fabs(llpVar->Lz->at(0))*0.001,finalWeight);
                        effHistMSVx->fill("MSVxTrig_EE_Lz_denom",fabs(llpVar->Lz->at(1))*0.001,finalWeight);
                    }
                    effHistMSVx->fill("MSVxABCD_EE_Lz_denom",fabs(llpVar->Lz->at(0))*0.001,finalWeight);
                    effHistMSVx->fill("MSVxABCD_EE_Lz_denom",fabs(llpVar->Lz->at(1))*0.001,finalWeight);      
                }
                effHistMSVx->fill("MSVx_EE_Lz_denom",fabs(llpVar->Lz->at(0))*0.001,finalWeight);
                effHistMSVx->fill("MSVx_EE_Lz_denom",fabs(llpVar->Lz->at(1))*0.001,finalWeight);

            }

        }

        //std::cout << " filled denominators " << std::endl;
        int nMSVx_evt=0;
        int nMSVx_noiso=0;
        if(debug) std::cout << "there are: " << msvx->eta->size() << " vertices" << std::endl;
        if(applyVertexSF && msvx->eta->size() > 0){
            msvx->performJetIsolation(commonVar->Jet_ET,commonVar->Jet_eta,commonVar->Jet_phi, commonVar->Jet_logRatio, commonVar->Jet_passJVT);
            msvx->performTrackIsolation(commonVar->Track_pT,commonVar->Track_eta,commonVar->Track_phi);
            msvx->testHitThresholds();
            msvx->testTruthMatch(llpVar->eta, llpVar->phi);
            msvx->testRoIMatch(trig->mu_roi_eta,trig->mu_roi_phi);
        } 
        if(msvx->eta->size() > 0){
            msvx->testIsGood(applyVertexSF);
        }
        if(debug) std::cout << "performed isolation" << std::endl;
        int nbbvx=0; int nbevx=0; int neevx=0;

        //find a better way to do all this bookkeeping.
        int nABCD_SRvx = 0; int nABCD_SRvx_matched = 0; int ABCD_SRvx_matched_index = -1; int vx_index_ABCDMatched = -1;
        /*int nB_SRvx = 0; int nB_SRvx_matched = 0; int B_SRvx_matched_index = -1; int vx_index_BMatched = -1;
        int nC_SRvx = 0; int nC_SRvx_matched = 0; int C_SRvx_matched_index = -1; int vx_index_CMatched = -1;
        int nD_SRvx = 0; int nD_SRvx_matched = 0; int D_SRvx_matched_index = -1; int vx_index_DMatched = -1;*/

        std::vector<double> vx_abcd_eta; std::vector<double> vx_abcd_phi; std::vector<int> vx_abcd_region;
        for (size_t i_vx=0;i_vx<msvx->eta->size();i_vx++)
        {
            if(applyVertexSF){
                if(msvx->syst->at(i_vx) != SYST_VAL){
                    continue;
                }
                if(!msvx->passReco->at(i_vx)) continue; //chi2 cut, minimum hit cut internal to algorithm
            }
            if(msvx->nMDT->at(i_vx)==0) continue; //skip dummy vertices (when too many tracklets in event)

            if(debug) std::cout << "vertex: " << i_vx << std::endl;

            double msvx_eta = msvx->eta->at(i_vx);
            double msvx_phi = msvx->phi->at(i_vx);

            bool isGood = (msvx->isGood->at(i_vx) == 1) ? true : false;

            nMSVx+=finalWeight;
            nMSVx_noiso++;
            int isEndcap = 0;
            int isBarrel = 0;
            int isGoodEta = 1;
            if ( fabs(msvx_eta)<0.7 ){
                isBarrel = 1;
                nBMSVx+=finalWeight;
                //if(msvx->nTrks->at(i_vx) < 6) isGood = false;
                if(debug) std::cout << "vertex in barrel! " <<std::endl;
            }
            else if ( fabs(msvx_eta)> 1.3 && fabs(msvx_eta) < 2.5 ){
                nEMSVx+=finalWeight;
                isEndcap = 1;// for endcaps
                //if(msvx->nTrks->at(i_vx) < 5) isGood = false;
                if(debug) std::cout << "Vertex in endcaps!" << std::endl;
            }
            else{
                if(debug) std::cout << "Vertex found, but in neither barrel nor endcaps" << std::endl;
                isGoodEta = 0;
            }
            if(!isGoodEta) isGood = false;
            //
            if(isGood) nMSVx_evt++;
            bool goodABCDHits = false;
            if(isEndcap && ( msvx->nMDT->at(i_vx) + msvx->nTGC->at(i_vx) )> 2500 && msvx->nMDT->at(i_vx) < 3000) goodABCDHits= true;
            else if(isBarrel && ( msvx->nMDT->at(i_vx) + msvx->nRPC->at(i_vx) ) > 2000 &&  msvx->nMDT->at(i_vx) < 3000) goodABCDHits= true;

            bool isGoodABCD = false;
            if(isBarrel) isGoodABCD = (msvx->closestdR->at(i_vx) > 0.3 && msvx->indexTrigRoI->at(i_vx) > -1 && goodABCDHits);
            else if(isEndcap) isGoodABCD = (msvx->closestdR->at(i_vx) > 0.6 && msvx->indexTrigRoI->at(i_vx) > -1 && goodABCDHits);

            //will use these eventually, if we need to do simulatneous likelihood fit.
            /*bool isGoodB = false;
            if(isBarrel) isGoodB = (msvx->closestdR->at(i_vx) > 0.3 && msvx->indexTrigRoI->at(i_vx) > -1 && !goodABCDHits);
            bool isGoodC = false;
            if(isBarrel) isGoodC = (!(msvx->closestdR->at(i_vx) > 0.3) && msvx->indexTrigRoI->at(i_vx) > -1 && goodABCDHits);
            bool isGoodD = false;
            if(isBarrel) isGoodD = (!(msvx->closestdR->at(i_vx) > 0.3) && msvx->indexTrigRoI->at(i_vx) > -1 && !goodABCDHits);*/

            if(isGoodABCD){
                vx_abcd_eta.push_back(msvx_eta);
                vx_abcd_phi.push_back(msvx_phi);
                vx_abcd_region.push_back((isBarrel? 1 : 2));
                nABCD_SRvx++;
            } 
            //if(l == 74424 ) std::cout << "made it to : " << __LINE__ << std::endl;

            int nVertexLLPMatches = 0;
            if(debug) {
                std::cout << "is good for ABCD? " << isGoodABCD << std::endl;
            }
            for(unsigned int j=0; j<llpVar->Lxy->size(); j++){
                double dR2_vx_llp = DeltaR2(llpVar->phi->at(j),msvx_phi,llpVar->eta->at(j),msvx_eta);
                if (dR2_vx_llp > 0.16) continue;
                if(llpMatching.at(j) ){
                    if(debug){
                        std::cout << "This LLP is already matched to a vertex - ignore the 2nd time (hist already filled)" << std::endl;

                        for (size_t l2=0;l2<msvx->eta->size();l2++){ std::cout << "Vertex " << l2 << "(R,z,eta,phi): (" <<
                            msvx->R->at(l2)*0.001 << ", " <<msvx->z->at(l2)*0.001 << ", " <<
                            msvx->eta->at(l2) << ", " << msvx->phi->at(l2) << ")" << std::endl;
                        }
                    }
                    continue;
                }
                llpMatching.at(j)++;
                if(isGoodABCD){
                    nABCD_SRvx_matched++;
                    ABCD_SRvx_matched_index = j;
                    vx_index_ABCDMatched = i_vx;
                }
                nVertexLLPMatches++;
                bool isllpBarrel = false; bool isllpEndcap = false;
                bool isllpBarrel_A = false; bool isllpEndcap_A = false;
                bool isllpBarrel_B = false; bool isllpEndcap_B = false;
                if((fabs(llpVar->eta->at(j)) < 0.7) && llpVar->Lxy->at(j) > 3000. && llpVar->Lxy->at(j) < 8000.){
                    isllpBarrel=true;
                    if(llpVar->Lxy->at(j) < 3800){isllpBarrel_A = true;} //regions A and B for pT dependence
                    else { isllpBarrel_B = true;}
                }
                if( (fabs(llpVar->eta->at(j))> 1.3) &&(fabs(llpVar->eta->at(j))< 2.5) && llpVar->Lz->at(j)>5000.
                        && llpVar->Lz->at(j)<15000. && llpVar->Lxy->at(j)<10000.){
                    isllpEndcap=true;
                    if(llpVar->Lz->at(j) < 6000){ isllpEndcap_A = true;}
                    else { isllpEndcap_B = true;}
                }
                if(isGood){
                    effHistMSVx->fill("MSVx_eta",llpVar->eta->at(j), finalWeight);
                }
                //if(l == 74424 ) std::cout << "made it to : " << __LINE__ << std::endl;

                if(isGoodEta && isllpBarrel){
                    effHistMSVx->fill("MSVx_Barrel_Lxy_ALL",llpVar->Lxy->at(j)*0.001,finalWeight);
                    if(isGood){
                        if(doPDFSystematics) {
                            for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                                sprintf(name,"%s%d","MSVx_Barrel_Lxy_",i_pdf);
                                effHistMSVx->fill(TString(name),llpVar->Lxy->at(j)*0.001,finalWeight*commonVar->pdfWeights->at(i_pdf));
                            }
                        }
                        if(doPileupSystematics){
                            effHistMSVx->fill("MSVx_Barrel_Lxy_PRW_1up", llpVar->Lxy->at(j)*0.001,commonVar->prw_1up);
                            effHistMSVx->fill("MSVx_Barrel_Lxy_PRW_1down", llpVar->Lxy->at(j)*0.001,commonVar->prw_1down);
                        }
                        effHistMSVx->fill("MSVx_Barrel_Lxy",llpVar->Lxy->at(j)*0.001,finalWeight);
                        effHistMSVx->fill("MSVx_Barrel_pTAll",llpVar->pT->at(j)*0.001,finalWeight);
                        if(isllpBarrel_A){
                            effHistMSVx->fill("MSVx_Barrel_pT_A",llpVar->pT->at(j)*0.001,finalWeight);
                        } else if(isllpBarrel_B) {
                            effHistMSVx->fill("MSVx_Barrel_pT_B",llpVar->pT->at(j)*0.001,finalWeight);
                        }
                    }
                    if(isGoodABCD) effHistMSVx->fill("MSVxTrig_Barrel_Lxy_ALL",llpVar->Lxy->at(j)*0.001,finalWeight);
                    if(isGoodABCD && is2J150Event) effHistMSVx->fill("MSVxTrig_Barrel_Lxy",llpVar->Lxy->at(j)*0.001,finalWeight);
                }
                if(isGoodEta && isllpEndcap){
                    effHistMSVx->fill("MSVx_Endcap_Lz_ALL",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    if(isGood){
                        if(doPDFSystematics){
                            for(int i_pdf=0;i_pdf < nPDFs; i_pdf++){
                                sprintf(name,"%s%d","MSVx_Endcap_Lz_",i_pdf);
                                effHistMSVx->fill(TString(name),fabs(llpVar->Lz->at(j)*0.001),finalWeight*commonVar->pdfWeights->at(i_pdf));
                            }
                        }
                        if(doPileupSystematics){
                            effHistMSVx->fill("MSVx_Endcap_Lz_PRW_1up",fabs(llpVar->Lz->at(j)*0.001),commonVar->prw_1up);
                            effHistMSVx->fill("MSVx_Endcap_Lz_PRW_1down",fabs(llpVar->Lz->at(j)*0.001),commonVar->prw_1down);
                        }
                        effHistMSVx->fill("MSVx_Endcap_Lz",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                        effHistMSVx->fill("MSVx_Endcap_pTAll",llpVar->pT->at(j)*0.001,finalWeight);
                        if(isllpEndcap_A){
                            effHistMSVx->fill("MSVx_Endcap_pT_A",llpVar->pT->at(j)*0.001,finalWeight);
                        } else if(isllpEndcap_B) {
                            effHistMSVx->fill("MSVx_Endcap_pT_B",llpVar->pT->at(j)*0.001,finalWeight);
                        }
                    }
                    if(isGoodABCD) effHistMSVx->fill("MSVxTrig_Endcap_Lz_ALL",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    if(isGoodABCD && is2J150Event) effHistMSVx->fill("MSVxTrig_Endcap_Lz",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                }

                if(isGoodEta && nBarrel == 1 && nEndcap == 0 && isllpBarrel){
                    effHistMSVx->fill("MSVx_1B_Lxy_ALL",llpVar->Lxy->at(j)*0.001,finalWeight);
                    if(isGood) effHistMSVx->fill("MSVx_1B_Lxy",llpVar->Lxy->at(j)*0.001,finalWeight);
                    if(isGoodABCD) effHistMSVx->fill("MSVxTrig_1B_Lxy_ALL",llpVar->Lxy->at(j)*0.001,finalWeight);
                    if(isGoodABCD && is2J150Event) effHistMSVx->fill("MSVxTrig_1B_Lxy",llpVar->Lxy->at(j)*0.001,finalWeight);
                }
                else if(isGoodEta &&  nBarrel == 1 && nEndcap == 1){
                    if(isllpBarrel){
                        effHistMSVx->fill("MSVx_BE_Lxy_ALL",llpVar->Lxy->at(j)*0.001,finalWeight);
                        if(isGood) effHistMSVx->fill("MSVx_BE_Lxy",llpVar->Lxy->at(j)*0.001,finalWeight);
                        if(LLPsAreSeparated && isGood && commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria){
                            nbevx++;
                            effHistMSVx->fill("MSVxTrig_BE_Lxy",llpVar->Lxy->at(j)*0.001,finalWeight);
                        }
                        //if(isGoodABCD) effHistMSVx->fill("MSVxTrig_BE_Lxy",llpVar->Lxy->at(j)*0.001,finalWeight);
                    }
                    else if (isllpEndcap) {
                        effHistMSVx->fill("MSVx_EB_Lz_ALL",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                        if(isGood) effHistMSVx->fill("MSVx_EB_Lz",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                        if(LLPsAreSeparated && isGood && commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria){
                            nbevx++;
                            effHistMSVx->fill("MSVxTrig_EB_Lz",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                        }
                        //if(isGoodABCD) effHistMSVx->fill("MSVxTrig_EB_Lz",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    }
                }
                else if(isGoodEta &&  nBarrel == 2 && isllpBarrel){
                    effHistMSVx->fill("MSVx_BB_Lxy_ALL",llpVar->Lxy->at(j)*0.001,finalWeight);
                    if(isGood) effHistMSVx->fill("MSVx_BB_Lxy",llpVar->Lxy->at(j)*0.001,finalWeight);
                    if(LLPsAreSeparated && isGood && commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria){
                        nbbvx++;
                        effHistMSVx->fill("MSVxTrig_BB_Lxy",llpVar->Lxy->at(j)*0.001,finalWeight);
                    }

                    //if(isGoodABCD) effHistMSVx->fill("MSVxTrig_BB_Lxy",llpVar->Lxy->at(j)*0.001,finalWeight);
                }
                else if(isGoodEta &&  nBarrel == 0 && nEndcap ==1 && isllpEndcap){
                    effHistMSVx->fill("MSVx_1E_Lz_ALL",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    if(isGood) effHistMSVx->fill("MSVx_1E_Lz",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    if(isGoodABCD && is2J150Event) effHistMSVx->fill("MSVxTrig_1E_Lz",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                }
                else if(isGoodEta &&  nEndcap == 2 && isllpEndcap){
                    effHistMSVx->fill("MSVx_EE_Lz_ALL",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    if(isGood) effHistMSVx->fill("MSVx_EE_Lz",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    if(LLPsAreSeparated && isGood && commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria){
                        neevx++;
                        effHistMSVx->fill("MSVxTrig_EE_Lz",fabs(llpVar->Lz->at(j))*0.001,finalWeight);
                    }
                }
            }
            if(debug && nVertexLLPMatches > 1){
                std::cout << " this vertex matched an LLP : " << nVertexLLPMatches << " times " << std::endl;
                std::cout << " vertex eta,phi: " << msvx_eta << ", " << msvx_phi << ")" << std::endl;
                std::cout << " LLP 1 eta,phi: " << llpVar->eta->at(0) << ", " << llpVar->phi->at(0) << ")" << std::endl;
                std::cout << " LLP 2 eta,phi: " << llpVar->eta->at(0) << ", " << llpVar->phi->at(1) << ")" << std::endl;
            }
        }
        if(debug) std::cout << " done vertices" << std::endl;

        if(nbbvx == 2 ) nBB_2MSVx+=finalWeight;
        if(nbevx == 2 ) nBE_2MSVx+=finalWeight;
        if(neevx == 2 ) nEE_2MSVx+=finalWeight;
        //if(l == 74424 ) std::cout << "made it to : " << __LINE__ << std::endl;

        //now using tefficiency, as a test!
        for(unsigned int j=0; j<llpVar->Lxy->size(); j++){
            if((fabs(llpVar->eta->at(j)) < 0.7) && llpVar->Lxy->at(j) > 3000. && llpVar->Lxy->at(j) < 8000.){
                if(nBarrel == 2){
                    effHistMSVx->fillEff("MSVxTrig_BB", llpVar->Lxy->at(j)*0.001, finalWeight,llpMatching.at(j));
                } else if(nBarrel == 1 && nEndcap == 1){
                    effHistMSVx->fillEff("MSVxTrig_BE", llpVar->Lxy->at(j)*0.001, finalWeight,llpMatching.at(j));
                }
            }
            if( (fabs(llpVar->eta->at(j))> 1.3) &&(fabs(llpVar->eta->at(j))< 2.5) && llpVar->Lz->at(j)>5000.
                    && llpVar->Lz->at(j)<15000. && llpVar->Lxy->at(j)<10000.){
                if(nEndcap == 2){
                    effHistMSVx->fillEff("MSVxTrig_EE", llpVar->Lz->at(j)*0.001, finalWeight, llpMatching.at(j));
                } else if(nBarrel == 1 && nEndcap == 1){
                    effHistMSVx->fillEff("MSVxTrig_EB", llpVar->Lz->at(j)*0.001, finalWeight, llpMatching.at(j));
                }
            }

        }
        if(isSignal && commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria){
            if(nABCD_SRvx == 2 && nABCD_SRvx_matched == 2){
                if(nBarrel == 2){
                    effHistMSVx->fill("MSVxABCD_BB_Lxy",llpVar->Lxy->at(0)*0.001, finalWeight);
                    effHistMSVx->fill("MSVxABCD_BB_Lxy",llpVar->Lxy->at(1)*0.001, finalWeight);
                }else if(nEndcap == 2){
                    effHistMSVx->fill("MSVxABCD_EE_Lz",llpVar->Lz->at(0)*0.001, finalWeight);
                    effHistMSVx->fill("MSVxABCD_EE_Lz",llpVar->Lz->at(1)*0.001, finalWeight);
                } else if(nEndcap == 1 && nBarrel == 1){
                    if(fabs(llpVar->eta->at(0)) < 1.0){
                        effHistMSVx->fill("MSVxABCD_BE_Lxy",llpVar->Lxy->at(0)*0.001, finalWeight);
                        effHistMSVx->fill("MSVxABCD_EB_Lz",llpVar->Lz->at(1)*0.001, finalWeight);
                    } else {
                        effHistMSVx->fill("MSVxABCD_BE_Lxy",llpVar->Lxy->at(1)*0.001, finalWeight);
                        effHistMSVx->fill("MSVxABCD_EB_Lz",llpVar->Lz->at(0)*0.001, finalWeight);
                    }
                }
            }
        }
        //all that ABCD stuff...
        //if(l == 74424 ) std::cout << "made it to : " << __LINE__ << std::endl;

        if(isSignal){

            if(nABCD_SRvx == 1 && nABCD_SRvx_matched == 1  && commonVar->passMuvtx_noiso && eventHasLT40_Tracklets && triggerMatch && passesEventQualityCriteria){
                commonVar->setGoodJetEventFlags();
                is2J150Event =  commonVar->eventIs2j150(msvx->eta->at(vx_index_ABCDMatched), msvx->phi->at(vx_index_ABCDMatched));
                is2J250Event =  commonVar->eventIs2j250(msvx->eta->at(vx_index_ABCDMatched), msvx->phi->at(vx_index_ABCDMatched));
                if(debug) std::cout << "is this a 2J150 event? " << is2J150Event << std::endl;

                if(nBarrel == 1 && nEndcap == 0 && (bIndex == ABCD_SRvx_matched_index)){
                    nBarrel_ABCD_1vx_events+=finalWeight;
                    effHistMSVx->fill("MSVxABCD_1B_Lxy",llpVar->Lxy->at(ABCD_SRvx_matched_index)*0.001, finalWeight);
                } else if(nBarrel == 0 && nEndcap == 1 && (eIndex == ABCD_SRvx_matched_index)){
                    nEndcap_ABCD_1vx_events+=finalWeight;

                    effHistMSVx->fill("MSVxABCD_1E_Lz",llpVar->Lz->at(ABCD_SRvx_matched_index)*0.001, finalWeight);
                } else if(nBarrel == 2){
                    //nBarrel_ABCD_1vx_events+=finalWeight;
                    effHistMSVx->fill("MSVxABCD_BB_Lxy",llpVar->Lxy->at(ABCD_SRvx_matched_index)*0.001, finalWeight);
                } else if(nEndcap == 2){
                    //nEndcap_ABCD_1vx_events+=finalWeight;

                    effHistMSVx->fill("MSVxABCD_EE_Lz",llpVar->Lz->at(ABCD_SRvx_matched_index)*0.001, finalWeight);
                } else if(nEndcap == 1 && nBarrel == 1){
                    if(fabs(llpVar->eta->at(ABCD_SRvx_matched_index)) < 1.0 && (bIndex == ABCD_SRvx_matched_index)){
                        // nBarrel_ABCD_1vx_events+=finalWeight;

                        effHistMSVx->fill("MSVxABCD_BE_Lxy",llpVar->Lxy->at(ABCD_SRvx_matched_index)*0.001, finalWeight);
                    } else if ((eIndex == ABCD_SRvx_matched_index) && fabs(llpVar->eta->at(ABCD_SRvx_matched_index)) > 1.0) {
                        // nEndcap_ABCD_1vx_events+=finalWeight;

                        effHistMSVx->fill("MSVxABCD_EB_Lz",llpVar->Lz->at(ABCD_SRvx_matched_index)*0.001, finalWeight);
                    }
                }
                //if(l == 74424 ) std::cout << "made it to : " << __LINE__ << std::endl;

                int indexNonVxMatch = -1;
                if(ABCD_SRvx_matched_index == 0){ indexNonVxMatch = 1;}
                else{ indexNonVxMatch = 0;}
                if(TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 1.5 ) {
                    double lxy = (llpVar->Lxy->at(indexNonVxMatch)*0.001 < 5) ? llpVar->Lxy->at(indexNonVxMatch)*0.001 : 4.5; //fill all overflow into last bin
                    double llppt = (llpVar->pT->at(indexNonVxMatch)*0.001 < 500) ? llpVar->pT->at(indexNonVxMatch)*0.001 : 450.;
                    effHistLLPJet->fill("LLPJetEff2_pT_Lxy_denom",lxy,llppt, finalWeight );
                } else if (TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 3.2)  {
                    double lz = (llpVar->Lz->at(indexNonVxMatch)*0.001 < 8) ? llpVar->Lz->at(indexNonVxMatch)*0.001 : 7.;
                    double llppz = (TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 < 1200) ? TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 : 1100.;
                    effHistLLPJet->fill("LLPJetEff2_pz_Lz_denom",lz,llppz, finalWeight );
                }
                if(is2J150Event){
                    if(TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 1.5) {
                        double lxy = (llpVar->Lxy->at(indexNonVxMatch)*0.001 < 5) ? llpVar->Lxy->at(indexNonVxMatch)*0.001 : 4.5; //fill all overflow into last bin
                        double llppt = (llpVar->pT->at(indexNonVxMatch)*0.001 < 500) ? llpVar->pT->at(indexNonVxMatch)*0.001 : 450.;
                        effHistLLPJet->fill("LLPJetEff2_pT_Lxy",lxy, llppt, finalWeight );
                    } else if (TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 3.2)  {
                        double lz = (llpVar->Lz->at(indexNonVxMatch)*0.001 < 8) ? llpVar->Lz->at(indexNonVxMatch)*0.001 : 7.;
                        double llppz = (TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 < 1200) ? TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 : 1100.;
                        effHistLLPJet->fill("LLPJetEff2_pz_Lz",lz,llppz, finalWeight );
                    }
                }
                if(TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 1.5 ) {
                    double lxy = (llpVar->Lxy->at(indexNonVxMatch)*0.001 < 5) ? llpVar->Lxy->at(indexNonVxMatch)*0.001 : 4.5; //fill all overflow into last bin
                    double llppt = (llpVar->pT->at(indexNonVxMatch)*0.001 < 500) ? llpVar->pT->at(indexNonVxMatch)*0.001 : 450.;
                    effHistLLPJet->fill("LLPJetEff2E_pT_Lxy_denom",lxy,llppt, finalWeight );
                } else if (TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 3.2)  {
                    double lz = (llpVar->Lz->at(indexNonVxMatch)*0.001 < 8) ? llpVar->Lz->at(indexNonVxMatch)*0.001 : 7.;
                    double llppz = (TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 < 1200) ? TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 : 1100.;
                    effHistLLPJet->fill("LLPJetEff2E_pz_Lz_denom",lz,llppz, finalWeight );
                }
                if(is2J250Event){
                    if(TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 1.5) {
                        double lxy = (llpVar->Lxy->at(indexNonVxMatch)*0.001 < 5) ? llpVar->Lxy->at(indexNonVxMatch)*0.001 : 4.5; //fill all overflow into last bin
                        double llppt = (llpVar->pT->at(indexNonVxMatch)*0.001 < 500) ? llpVar->pT->at(indexNonVxMatch)*0.001 : 450.;
                        effHistLLPJet->fill("LLPJetEff2E_pT_Lxy",lxy, llppt, finalWeight );
                    } else if (TMath::Abs(llpVar->eta->at(indexNonVxMatch)) < 3.2)  {
                        double lz = (llpVar->Lz->at(indexNonVxMatch)*0.001 < 8) ? llpVar->Lz->at(indexNonVxMatch)*0.001 : 7.;
                        double llppz = (TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 < 1200) ? TMath::Abs(llpVar->pz->at(indexNonVxMatch))*0.001 : 1100.;
                        effHistLLPJet->fill("LLPJetEff2E_pz_Lz",lz,llppz, finalWeight );
                    }
                }

            }
            //if(l == 74424 ) std::cout << "made it to : " << __LINE__ << std::endl;

            for(unsigned int iJet=0; iJet < commonVar->Jet_pT->size(); iJet++){
                //std::cout << "jet " << iJet << "/" << commonVar->Jet_pT->size() << std::endl;
                if( TMath::Abs(commonVar->Jet_eta->at(iJet) > 3.2) ) continue;
                if( !commonVar->Jet_isGoodStand->at(iJet)) continue;
                if(commonVar->Jet_pT->at(iJet) < 60. && TMath::Abs(commonVar->Jet_eta->at(iJet)) < 2.4 && commonVar->Jet_jvt->at(iJet) < 0.59 ) continue;
                int iLLP = commonVar->Jet_indexLLP->at(iJet);
                if( iLLP < 0 ) continue;
                //std::cout << "have a jet-llp match: " << iLLP << std::endl;
                if( commonVar->Jet_pT->at(iJet) < 150. ) continue;
                if(TMath::Abs(llpVar->eta->at(iLLP)) < 3.2 ){ effHistLLPJet->fill("LLPJetProb_Lxy",llpVar->Lxy->at(iLLP)*0.001,finalWeight); }
                effHistLLPJet->fill("LLPJetEff_pT_Lxy",llpVar->Lxy->at(iLLP)*0.001,llpVar->pT->at(iLLP)*0.001,finalWeight);
                effHistLLPJet->fill("LLPJetEff",llpVar->Lxy->at(iLLP)*0.001, llpVar->Lz->at(iLLP)*0.001, finalWeight);
                if(TMath::Abs(llpVar->eta->at(iLLP)) < 1.5 ){ effHistLLPJet->fill("LLPJetEff_Lxy",llpVar->Lxy->at(iLLP)*0.001,finalWeight); }
                if(TMath::Abs(llpVar->eta->at(iLLP)) < 3.2 && TMath::Abs(llpVar->eta->at(iLLP)) > 1.5  ){ effHistLLPJet->fill("LLPJetEff_Lz",llpVar->Lz->at(iLLP)*0.001,finalWeight); }
                if( llpVar->Lxy->at(iLLP) < 2000. ){ effHistLLPJet->fill("LLPJetEff_eta",llpVar->eta->at(iLLP),finalWeight); }
                if(TMath::Abs(llpVar->eta->at(iLLP)) < 3.2 && llpVar->Lxy->at(iLLP) < 2000. ){ effHistLLPJet->fill("LLPJetEff_E",llpVar->E->at(iLLP)*0.001,finalWeight);}
                if(TMath::Abs(llpVar->eta->at(iLLP)) < 1.5 && llpVar->Lxy->at(iLLP) < 2000. ){ effHistLLPJet->fill("LLPJetEff_pT",llpVar->pT->at(iLLP)*0.001,finalWeight);}
                if(TMath::Abs(llpVar->eta->at(iLLP)) < 3.2 && TMath::Abs(llpVar->eta->at(iLLP)) > 1.5 && llpVar->Lz->at(iLLP) < 4300. ){ effHistLLPJet->fill("LLPJetEff_pz",TMath::Abs(llpVar->pz->at(iLLP))*0.001,finalWeight);}

            }
            if(is2J150Event) {
                if(llpVar->Lxy->at(0)*0.001 > llpVar->Lxy->at(1)*0.001){
                    effHistLLPJet->fill("LLPJetEvent_Eff",llpVar->Lxy->at(0)*0.001, llpVar->Lxy->at(1)*0.001, finalWeight);
                } else{
                    effHistLLPJet->fill("LLPJetEvent_Eff",llpVar->Lxy->at(1)*0.001, llpVar->Lxy->at(0)*0.001, finalWeight);
                }
            }
        }
        //if(l == 74424 ) std::cout << "made it to : " << __LINE__ << std::endl;

        //std::cout << "done jets" << std::endl;
        // ** Fill variable for counting number of events with more than one vertex **
        if ( nMSVx_evt>1)       nEvt_MoreThanOneVtx++;
        if ( nMSVx_noiso>1) nEvt_MoreThanOneVtx_noiso++;

        msvx->clearAllVectors(true,applyVertexSF);
        commonVar->clearAllVectors(true, true, doPDFSystematics);
        llpVar->clearAllVectors();
        trig->clearAllVectors();
        //std::cout << "vectors are cleared" << std::endl;
        //if(l == 74424 ) std::cout << "made it to : " << __LINE__ << std::endl;

    }


    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;

    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;
    cout << "" << endl;
    cout << " nEventsPassTrigger = " << nEventsPassTrigger << endl;
    cout << "" << endl;
    cout << " nMS in total, pre any criteria  (b,e)    = " << nMSVx <<
            ", (" << nBMSVx << ", " << nEMSVx << ") " << endl;
    cout << " nMS passing all criteria      = " << nMSVx_passGVC << endl;
    cout << " nMS passing ~track isolation      = " << nMSVx_passTrkIso << endl;
    cout << " nMS passing jet isolation     = " << nMSVx_passJetIso << endl;
    cout << " number of ms vertices passing variable jet isolation, barrel: " << endl;
    cout << " dR = 0.3: " << nBMSVx_passJetIso03 << "/" << nBMSVx << ", dR = 0.6: "<< nBMSVx_passJetIso06 << "/" << nBMSVx << ", dR = 1.0: "<< nBMSVx_passJetIso10 << "/" << nBMSVx << endl;
    cout << " number of ms vertices passing variable jet isolation, endcaps: " << endl;
    cout << " dR = 0.3: " << nEMSVx_passJetIso03 << "/" << nEMSVx << ", dR = 0.6: "<< nEMSVx_passJetIso06 << "/" << nEMSVx << ", dR = 1.0: "<< nEMSVx_passJetIso10 << "/" << nEMSVx << endl;
    cout << " nMS passing Hit quality = " << nMSVx_passHitQual              << endl;
    //
    cout << " nEvt with > 1 vtx                = " << nEvt_MoreThanOneVtx       << endl;
    cout << " nEvt with > 1 vtx (no isolation) = " << nEvt_MoreThanOneVtx_noiso << endl;
    std::cout << " Number of 2vx events, full selection: " << nBB_2MSVx << ", " << nBE_2MSVx << ", " << nEE_2MSVx << std::endl;
    std::cout << "barrel, endcap ABCD 1vx events: " << nBarrel_ABCD_1vx_events << ", " << nEndcap_ABCD_1vx_events << std::endl;

    // ** Make plot **
    //

    for(unsigned int i=0; i < effHistMSVx->t_eff.size(); i++){
        effHistMSVx->t_eff.at(i)->Write();
    }

    effHistLLPJet->plotEfficiencies(plotDirectory);
    effHistMSVx->plotEfficiencies(plotDirectory);
    effHistMSTrig->plotEfficiencies(plotDirectory);

    outputFile->Write();
}
