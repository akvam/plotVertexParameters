//
//  plotParamsFromTTree.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/VertexHistograms.h"
#include "PlottingPackage/TriggerVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/HistTools.h"
#include "PlottingPackage/EventProperties.h"

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>

using namespace std;
using namespace plotVertexParameters;
using namespace PlottingUtils;
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
TStopwatch timer;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;

}

int main(int argc, char **argv){

    std::cout << "running program!" << std::endl;

    //TFile *outFile = new TFile("outputHistograms.root","RECREATE");

    TChain *chain = new TChain("recoTree");

    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    std::cout << "Output file name : " << argv[3] << std::endl;
    int doLate = 0;
    if(TString(argv[4])== "EMPTY" ) doLate = 1;
    else if(TString(argv[4]) == "UNISO" || TString(argv[4]) == "UNPAIRED") doLate = 2;
    else std::cout << "You asked to do a late stream analysis on " << argv[4] << ", that's a little strange..." << std::endl;
    //Directory the plots will go in

    InputFiles::AddFilesToChain(TString(argv[1]), chain);

    
    TString plotDirectory = TString(argv[2]);

    TFile *outputFile = new TFile(plotDirectory+"/"+TString(argv[3]),"RECREATE");

    std::cout << "added file!, will be saved in directory: " << plotDirectory << std::endl;
    chain->SetBranchStatus("*",0);

    VertexHistograms *vxIso = new VertexHistograms;
    vxIso->initializeHistograms("vxVar","iso");

    HistTools *histIso = new HistTools;
    histIso->addHist("trig_eta",100,-2.5,2.5);
    histIso->addHist("trig_phi",64,-3.2,3.2);

    VertexVariables *vxVar = new VertexVariables;
    TriggerVariables *trigVar = new TriggerVariables;
    CommonVariables *commonVar = new CommonVariables;

    vxVar->setZeroVectors(false);
    vxVar->setBranchAdresses(chain, false, false);
    std::cout << "got vertex variables" << std::endl;
    trigVar->setZeroVectors();
    trigVar->setBranchAdresses(chain, true);
    std::cout << "got trigger variables" << std::endl;
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain,true,false,true,false,false, doLate);

    //    void CommonVariables::setBranchAdresses(TChain *chain,bool useJets, bool useTruthJets, bool isData, bool useTracks, bool doPDFSystematics){

    std::cout << "got common variables" << std::endl;

    TString inputFile = chain->GetFile()->GetName();
    std::cout << "File name: " << inputFile << std::endl;
    
    EventProperties *evt = new EventProperties;
    evt->setZeroVectors();
    EventProperties *evt_gjet = new EventProperties;
    evt_gjet->setZeroVectors();

    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;

    //one set of histograms for barrel, one for endcaps,
    //one for the full detector together (nice for 2D maps)
    //    vxVarHistGroup* vxVarHists = new vxVarHistGroup;
    //    vxVarHists->initialize();

    setPrettyCanvasStyle();

    TH1::SetDefaultSumw2();

    timer.Start();

    double finalWeight = 1.0;
    for (int l=0;l<chain->GetEntries();l++)
    {

        chain->GetEntry(l);
        commonVar->setGoodJetEventFlags();

        //if(l < 1531522) continue;
        if(l % 500000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        std::vector<double> trigEta;
        std::vector<double> trigPhi;
        std::vector<int> trigRegion;

        if(!commonVar->passMuvtx_noiso){
            vxVar->clearAllVectors(false,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(false, false, false);
            continue;
        }
        evt->addEventToCutflow(EventProperties::Cutflow::PassTrigger, finalWeight);
        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
            evt_gjet->addEventToCutflow(EventProperties::Cutflow::PassTrigger, finalWeight);
        }
        if(!commonVar->isQualityEvent(true)) {
            vxVar->clearAllVectors(false,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(false, false, false);
            continue;
        }
        /*        if(!commonVar->hasGoodPV) {
            vxVar->clearAllVectors(false,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(false, false, false);
            continue;
        }*/ //no PV in late stream....

        //        std::cout << "number of vertices? " << vxVar->eta->size() << std::endl;
        //         std::cout << "triggers? " << commonVar->passMuvtx_noiso << ", " << commonVar->passMuvtx << std::endl;

        //        std::cout << "trig variables size? " << trigEta.size() << std::endl;


        std::vector<double> vxEta;
        std::vector<double> vxPhi;
        std::vector<int> vxRegion;
        std::vector<int> vxIndex;

        trigVar->FindGoodTriggers(trigEta, trigPhi, trigRegion);
        vxVar->FindGoodVertices(vxEta, vxPhi, vxRegion, vxIndex, false);
        for(unsigned int ivx=0;ivx<vxEta.size();ivx++){
            vxIso->h_eta->Fill(vxEta.at(ivx), finalWeight);
            vxIso->h_phi->Fill(vxPhi.at(ivx), finalWeight);
            vxIso->h_nMDT->Fill(vxVar->nMDT->at(vxIndex.at(ivx)),finalWeight );
            if(vxRegion.at(ivx) == 1) vxIso->h_nRPC->Fill(vxVar->nRPC->at(vxIndex.at(ivx)),finalWeight );
            else vxIso->h_nTGC->Fill(vxVar->nTGC->at(vxIndex.at(ivx)),finalWeight );
            vxIso->h_nTrks->Fill(vxVar->nTrks->at(vxIndex.at(ivx)),finalWeight );
            vxIso->h_R->Fill(vxVar->R->at(vxIndex.at(ivx)),finalWeight );
            vxIso->h_z->Fill(vxVar->z->at(vxIndex.at(ivx)),finalWeight );
        }

        if(trigEta.size() == 0){
            //std::cout << "THIS EVENT PASSED MUVTX BUT HAS NO GOOD TRIGGERS!" << std::endl;
            vxVar->clearAllVectors(false,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(false, false, false);
            continue; //no RoI clusters, don't continue (should be impossible - but not anymore, b/c eta cuts)

        }

        int nTriggers = trigEta.size();

        for(int i=0; i< nTriggers; i++){
            histIso->fill("trig_eta", trigEta.at(i), finalWeight);   
            histIso->fill("trig_phi", trigPhi.at(i), finalWeight);   
        }

        if(nTriggers > 2){
            evt->addStrangeEvent(nTriggers,finalWeight);
            vxVar->clearAllVectors(false,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(false, false, false);
            continue; //code can't cope with more than 2 triggers right now. also, signal shouldn't leave > 2.
        }

        if(vxEta.size() == 0){
            if(nTriggers == 1){
                evt->add1TrigEvent(trigRegion.at(0),0,0,finalWeight);
                if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                    evt_gjet->add1TrigEvent(trigRegion.at(0),0,0,finalWeight);
                }
            } else if(nTriggers == 2){
                if(trigRegion.at(0) == 1 && trigRegion.at(1) == 1){
                    evt->add2TrigEvent(1,0,0,finalWeight);
                    if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                        evt_gjet->add2TrigEvent(1,0,0,finalWeight);
                    }
                } else if(trigRegion.at(0) == 2 && trigRegion.at(1) == 2){
                    evt->add2TrigEvent(3,0,0,finalWeight);
                    if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                        evt_gjet->add2TrigEvent(3,0,0,finalWeight);
                    }
                } else{
                    evt->add2TrigEvent(2,0,0,finalWeight);
                    if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                        evt_gjet->add2TrigEvent(2,0,0,finalWeight);
                    }
                }
            }
        } //now events with vertices...

        else if(nTriggers == 1){
            if(vxEta.size() == 1){
                if(DeltaR(trigPhi.at(0) ,vxPhi.at(0),trigEta.at(0) ,vxEta.at(0)) < 0.4){
                    evt->add1TrigEvent(trigRegion.at(0),1,1,finalWeight);
                    if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                        evt_gjet->add1TrigEvent(trigRegion.at(0),1,1,finalWeight);
                    }
                } else { //one vertex doesn't match...
                    evt->add1TrigEvent(trigRegion.at(0),1,0,finalWeight);
                    if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                        evt_gjet->add1TrigEvent(trigRegion.at(0),1,0,finalWeight);
                    }
                }
            } else if(vxEta.size() == 2){
                if(DeltaR(vxPhi.at(0) ,vxPhi.at(1),vxEta.at(0) ,vxEta.at(1)) < 1.0){
                    vxVar->clearAllVectors(false,false);
                    trigVar->clearAllVectors(false);
                    commonVar->clearAllVectors(false, false, false);
                    continue;
                }
                double match1 = DeltaR(trigPhi.at(0) ,vxPhi.at(0),trigEta.at(0) ,vxEta.at(0));
                double match2 = DeltaR(trigPhi.at(0) ,vxPhi.at(1),trigEta.at(0) ,vxEta.at(1));
                //std::cout << "trigger: " << trigPhi.at(0) << ", " << trigEta.at(0) << std::endl;
                std::cout << "Vertices are at: " << std::endl;
                std::cout << "Vertex 1: (eta, phi) = (" << vxEta.at(0) << ", " << vxPhi.at(0) << ")" << std::endl;
                std::cout << "Vertex 2: (eta, phi) = (" << vxEta.at(1) << ", " << vxPhi.at(1) << ")" << std::endl;
                std::cout << "They are separated by dR = " << DeltaR(vxPhi.at(0),vxPhi.at(1),vxEta.at(0),vxEta.at(1)) << std::endl;        
                bool vx1matches = (match1 < 0.4) ? true : false;
                bool vx2matches = (match2 < 0.4) ? true : false;
                evt->add2Vertex1TrigEvent(trigRegion.at(0),vxRegion.at(0),vx1matches,vxRegion.at(1),vx2matches,finalWeight);
                if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                    evt_gjet->add2Vertex1TrigEvent(trigRegion.at(0),vxRegion.at(0),vx1matches,vxRegion.at(1),vx2matches,finalWeight);
                }
            }
        } else if(nTriggers == 2){
            //ugh. ASSUMES that the vertex and trigger, if matched, are in the same region (Barrel/Endcap)

            if(DeltaR(trigPhi.at(0),trigPhi.at(1),trigEta.at(0),trigEta.at(1)) < 1.){
                vxVar->clearAllVectors(false,false);
                trigVar->clearAllVectors(false);
                commonVar->clearAllVectors(false, false, false);
                continue;
            }
            //only look at events where the triggers are at least dR = 1 apart

            if(vxEta.size() == 1){
                std::cout << "Two triggers and one vertex in this event: " << l << std::endl;
                std::cout << "Trigger 1: (eta, phi) = (" << trigEta.at(0) << ", " << trigPhi.at(0) << ")" << std::endl;
                std::cout << "Trigger 2: (eta, phi) = (" << trigEta.at(1) << ", " << trigPhi.at(1) << ")" << std::endl;
                std::cout << "They are separated by dR = " << DeltaR(trigPhi.at(0),trigPhi.at(1),trigEta.at(0),trigEta.at(1)) << std::endl;
                std::cout << "Vertex is at: " << std::endl;
                std::cout << "Vertex 1: (eta, phi) = (" << vxEta.at(0) << ", " << vxPhi.at(0) << ")" << std::endl;

                double dRTrig1 = DeltaR(trigPhi.at(0) ,vxPhi.at(0),trigEta.at(0) ,vxEta.at(0));
                double dRTrig2 = DeltaR(trigPhi.at(1) ,vxPhi.at(0),trigEta.at(1) ,vxEta.at(0));
                if(dRTrig1 >= 0.4 && dRTrig2 >= 0.4){
                    if(trigRegion.at(0) == 1 && trigRegion.at(1) == 1){
                        evt->add2TrigEvent(1,1,0,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2TrigEvent(1,1,0,finalWeight);
                        }
                    } else if(trigRegion.at(0) == 2 && trigRegion.at(1) == 2){
                        evt->add2TrigEvent(3,2,0,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2TrigEvent(3,2,0,finalWeight);
                        }
                    } else{
                        evt->add2TrigEvent(2,vxRegion.at(0),0,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2TrigEvent(2,vxRegion.at(0),0,finalWeight);
                        }
                    }
                } else {
                    if(trigRegion.at(0) == 1 && trigRegion.at(1) == 1){
                        evt->add2TrigEvent(1,1,1,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2TrigEvent(1,1,1,finalWeight);
                        }
                    } else if(trigRegion.at(0) == 2 && trigRegion.at(1) == 2){
                        evt->add2TrigEvent(3,2,1,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2TrigEvent(3,2,1,finalWeight);
                        }
                    } else {
                        evt->add2TrigEvent(2,vxRegion.at(0),1,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2TrigEvent(2,vxRegion.at(0),1,finalWeight);
                        }
                    }
                }
            }//end only one vertex
            else if(vxEta.size() == 2){

                std::cout << "Two triggers and two vertices in this event: " << l << std::endl;
                std::cout << "Trigger 1: (eta, phi) = (" << trigEta.at(0) << ", " << trigPhi.at(0) << ")" << std::endl;
                std::cout << "Trigger 2: (eta, phi) = (" << trigEta.at(1) << ", " << trigPhi.at(1) << ")" << std::endl;
                std::cout << "They are separated by dR = " << DeltaR(trigPhi.at(0),trigPhi.at(1),trigEta.at(0),trigEta.at(1)) << std::endl;
                std::cout << "Vertices are at: " << std::endl;
                std::cout << "Vertex 1: (eta, phi) = (" << vxEta.at(0) << ", " << vxPhi.at(0) << ")" << std::endl;
                std::cout << "Vertex 2: (eta, phi) = (" << vxEta.at(1) << ", " << vxPhi.at(1) << ")" << std::endl;
                std::cout << "They are separated by dR = " << DeltaR(vxPhi.at(0),vxPhi.at(1),vxEta.at(0),vxEta.at(1)) << std::endl;

                //make sure the vertices are separated as well!
                if(DeltaR(vxPhi.at(0),vxPhi.at(1),vxEta.at(0),vxEta.at(1)) < 1.0){
                    vxVar->clearAllVectors(false,false);
                    trigVar->clearAllVectors(false);
                    commonVar->clearAllVectors(false, false, false);
                    continue;
                }
                double dR1Trig1 = DeltaR(trigPhi.at(0) ,vxPhi.at(0),trigEta.at(0) ,vxEta.at(0));
                double dR1Trig2 = DeltaR(trigPhi.at(1) ,vxPhi.at(0),trigEta.at(1) ,vxEta.at(0));
                double dR2Trig1 = DeltaR(trigPhi.at(0) ,vxPhi.at(1),trigEta.at(0) ,vxEta.at(1));
                double dR2Trig2 = DeltaR(trigPhi.at(1) ,vxPhi.at(1),trigEta.at(1) ,vxEta.at(1));

                if( (dR1Trig1 < 0.4 && dR2Trig2 < 0.4) ||  (dR2Trig1 < 0.4 && dR1Trig2 < 0.4)  ){
                    if(trigRegion.at(0) == 1 && trigRegion.at(1) == 1){
                        evt->add2Vertex2TrigEvent(1,1,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2Vertex2TrigEvent(1,1,finalWeight);
                        }
                    } else if(trigRegion.at(0) == 2 && trigRegion.at(1) == 2){
                        evt->add2Vertex2TrigEvent(3,1,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2Vertex2TrigEvent(3,1,finalWeight);
                        }
                    } else{
                        evt->add2Vertex2TrigEvent(2,1,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2Vertex2TrigEvent(2,1,finalWeight);
                        }
                    }
                } else{
                    if(trigRegion.at(0) == 1 && trigRegion.at(1) == 1){
                        evt->add2Vertex2TrigEvent(1,0,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2Vertex2TrigEvent(1,0,finalWeight);
                        }
                    } else if(trigRegion.at(0) == 2 && trigRegion.at(1) == 2){
                        evt->add2Vertex2TrigEvent(3,0,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2Vertex2TrigEvent(3,0,finalWeight);
                        }
                    } else{
                        evt->add2Vertex2TrigEvent(2,0,finalWeight);
                        if(commonVar->isBadEventJetLow == false && commonVar->isBadEventJetHigh == false){
                            evt_gjet->add2Vertex2TrigEvent(2,0,finalWeight);
                        }
                    }
                }
            }//end two vertices
        }

        vxVar->clearAllVectors(false,false);
        trigVar->clearAllVectors(false);
        commonVar->clearAllVectors(false, false, false);
    }

    std::cout << "plotted hists!" << std::endl;

    outputFile->Write();

    evt->printResults(false);
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "*****************************************" << std::endl;
    std::cout << "With good jet event criteria applied: " << std::endl;

    evt_gjet->printResults(false);


    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;

}
