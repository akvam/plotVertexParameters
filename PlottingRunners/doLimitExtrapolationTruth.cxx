//
//  doLimitExtrapolation.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 03/09/16.
//
//

#include <stdio.h>

#include <TH2.h>
#include <TStyle.h>
#include <iostream>
#include "math.h"
#include <TMath.h>
#include <TRandom3.h>
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TEfficiency.h"
#include <sys/time.h>
#include <numeric>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <vector>
#include <string>
#include "TGraphAsymmErrors.h"
#include <fstream>
#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/HistTools.h"
#include "PlottingPackage/SampleDetails.h"
#include "PlottingPackage/ExtrapolationUtils.h"

using namespace ExtrapolationUtils;

#define PI 3.14159265
#define sq(x) ((x)*(x))

const double LUMI = 32864.+3212.96; //in pb-1
//18857.4+3193.68

//these are in metres?
const double LTSTEP = 0.05;
const double LTSTEP2 = 0.5;
const double LTSTEP3 = 0.005;

//trigger scaling
const double delta_MC = 13.262;
const double delta_DATA = 12.171;
const double sigma_MC = 2.084;
const double sigma_DATA = 2.9548;

//speed of light
const double c = 299.7924580;// in mm/ns
const double VERTEX_DR_CUT = 1.0; //(minimum delta R between two vertices; used to count expected signal)
typedef unsigned long long timestamp_t;
static timestamp_t
get_timestamp ()
{
    struct timeval now;
    gettimeofday (&now, NULL);
    return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000;
}
double myLogScale(double xMin, double nPoints, int nBin);
double myLogScale(double xMin, double nPoints, int nBin){
    double a = floor(double(nBin) / nPoints );
    double b = double(nBin % int(nPoints));

    return pow(10, log10(xMin) + a)*(1 + 0.1*b);

}

//Systematic errors switch
const bool addSystematicErrors = true;

//Ratios switch
#define doRatios false
double LUMISYSERR = 0.0;

TH1D *h_BB_Decays, *h_BE_Decays, *h_EE_Decays, *h_1E_Decays, *h_1B_Decays;
TH1D *h_BB_DecaysRW, *h_BE_DecaysRW, *h_EE_DecaysRW, *h_1E_DecaysRW, *h_1B_DecaysRW;

TH1D *h_1B_Beta[9];
TH1D *h_1E_Beta[9];
TH1D *h_BB_Beta[9];
TH1D *h_BE_Beta[9];
TH1D *h_EE_Beta[9];

TH1D *h_1B_BetaRW[9];
TH1D *h_1E_BetaRW[9];
TH1D *h_BB_BetaRW[9];
TH1D *h_BE_BetaRW[9];
TH1D *h_EE_BetaRW[9];

TH1D *h_1B_BetaSL[9];
TH1D *h_1E_BetaSL[9];
TH1D *h_BB_BetaSL[9];
TH1D *h_BE_BetaSL[9];
TH1D *h_EE_BetaSL[9];

TH1D *h_1B_BetaSLRW[9];
TH1D *h_1E_BetaSLRW[9];
TH1D *h_BB_BetaSLRW[9];
TH1D *h_BE_BetaSLRW[9];
TH1D *h_EE_BetaSLRW[9];

TH2D *h2d_1B_Beta[9];
TH2D *h2d_1E_Beta[9];
TH2D *h2d_BB_Beta[9];
TH2D *h2d_BE_Beta[9];
TH2D *h2d_EE_Beta[9];

TH2D *h2d_1B_BetaRW[9];
TH2D *h2d_1E_BetaRW[9];
TH2D *h2d_BB_BetaRW[9];
TH2D *h2d_BE_BetaRW[9];
TH2D *h2d_EE_BetaRW[9];

TH1D *h_1B_ctau[9];
TH1D *h_1E_ctau[9];
TH1D *h_BB_ctau[9];
TH1D *h_BE_ctau[9];
TH1D *h_EE_ctau[9];

TH1D *h_1B_ctauRW[9];
TH1D *h_1E_ctauRW[9];
TH1D *h_BB_ctauRW[9];
TH1D *h_BE_ctauRW[9];
TH1D *h_EE_ctauRW[9];

TH1D *h_1B_ctauSL[9];
TH1D *h_1E_ctauSL[9];
TH1D *h_BB_ctauSL[9];
TH1D *h_BE_ctauSL[9];
TH1D *h_EE_ctauSL[9];

TH1D *h_1B_ctauSLRW[9];
TH1D *h_1E_ctauSLRW[9];
TH1D *h_BB_ctauSLRW[9];
TH1D *h_BE_ctauSLRW[9];
TH1D *h_EE_ctauSLRW[9];

TH2D *h2d_1B_ctau[9];
TH2D *h2d_1E_ctau[9];
TH2D *h2d_BB_ctau[9];
TH2D *h2d_BE_ctau[9];
TH2D *h2d_EE_ctau[9];

TH2D *h2d_1B_ctauRW[9];
TH2D *h2d_1E_ctauRW[9];
TH2D *h2d_BB_ctauRW[9];
TH2D *h2d_BE_ctauRW[9];
TH2D *h2d_EE_ctauRW[9];



timestamp_t t0;

using namespace std;
using namespace plotVertexParameters;


//random number
TRandom3 rnd;

bool debug = false;
double nEvents = 0;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;

}
int main(int argc, char **argv){
    if(addSystematicErrors){
        LUMISYSERR = 0.029; //from https://twiki.cern.ch/twiki/bin/view/Atlas/LuminosityForPhysics#2016_13_TeV_ICHEP_2016
    }

    TChain *chain = new TChain("recoTree");

    std::cout << "running program!" << std::endl;
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over sample: " << argv[1] << std::endl;
    std::cout << "Results aren't put in the folder: " << argv[2] << ". I'm ignoring it. Haha! Instead, they're in signalMC/extrapolation/" << std::endl;
    std::cout << "Output File name is: " << argv[3] << std::endl;

    InputFiles::AddFilesToChain(TString(argv[1]), chain);
    t0 = get_timestamp();

    Double_t xBinValues[541];
    for(int j=0; j<541; j++){
        xBinValues[j] = myLogScale(0.001, 90, j);
    }

    LLPVariables *toyLLP = new LLPVariables;
    toyLLP->setZeroVectors();
    
    std::cout << "I'm running over: "  << chain->GetEntries()<< " events" << std::endl;
    toyLLP->setBranchAddresses(chain);
    bool lDebug=true;

    //set parameters: masses, systematics...

    //string to enum?!?
    SampleDetails::setGlobalVariables(TString(argv[1]));

    std::cout << "Luminosity (not included in total error calculation at end!): " << LUMISYSERR << std::endl;
    std::cout << "Note that these are all relative errors!" << std::endl;
    std::cout << "Proper lifetime of this sample is: " << SampleDetails::sim_ctau << std::endl;

    double gen_ctau = SampleDetails::sim_ctau * 1000.;
    
    TString inputDir = "/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/";

    //////////////////////////////
    //* Histograms declaration *//
    TFile * outputFile = new TFile(inputDir+"/extrapolationTruth/"+TString(argv[3]),"RECREATE");

    h_1B_Decays = new TH1D("n1B_Decays","",540, xBinValues);
    h_1E_Decays = new TH1D("n1E_Decays","",540, xBinValues);
    h_BB_Decays = new TH1D("nBB_Decays","",540, xBinValues);
    h_BE_Decays = new TH1D("nBE_Decays","",540, xBinValues);
    h_EE_Decays = new TH1D("nEE_Decays","",540, xBinValues);
    
    h_1B_DecaysRW = new TH1D("n1B_DecaysRW","",540, xBinValues);
    h_1E_DecaysRW = new TH1D("n1E_DecaysRW","",540, xBinValues);
    h_BB_DecaysRW = new TH1D("nBB_DecaysRW","",540, xBinValues);
    h_BE_DecaysRW = new TH1D("nBE_DecaysRW","",540, xBinValues);
    h_EE_DecaysRW = new TH1D("nEE_DecaysRW","",540, xBinValues);
    
    std::vector<TString> bN = {"0.005","0.01","simct","0.5","1","5","10","100","1000"};
    for(int i=0;i<bN.size(); i++){
        h_1B_Beta[i] = new TH1D("beta_1B_"+bN.at(i),"beta_1B_"+bN.at(i),200,0,1 );
        h_1E_Beta[i] = new TH1D("beta_1E_"+bN.at(i),"beta_1E_"+bN.at(i),200,0,1 );
        h_BB_Beta[i] = new TH1D("beta_BB_"+bN.at(i),"beta_BB_"+bN.at(i),200,0,1 );
        h_BE_Beta[i] = new TH1D("beta_BE_"+bN.at(i),"beta_BE_"+bN.at(i),200,0,1 );
        h_EE_Beta[i] = new TH1D("beta_EE_"+bN.at(i),"beta_EE_"+bN.at(i),200,0,1 );
        h_1B_BetaRW[i] = new TH1D("beta_rw_1B_"+bN.at(i),"beta_rw_1B_"+bN.at(i),200,0,1 );
        h_1E_BetaRW[i] = new TH1D("beta_rw_1E_"+bN.at(i),"beta_rw_1E_"+bN.at(i),200,0,1 );
        h_BB_BetaRW[i] = new TH1D("beta_rw_BB_"+bN.at(i),"beta_rw_BB_"+bN.at(i),200,0,1 );
        h_BE_BetaRW[i] = new TH1D("beta_rw_BE_"+bN.at(i),"beta_rw_BE_"+bN.at(i),200,0,1 );
        h_EE_BetaRW[i] = new TH1D("beta_rw_EE_"+bN.at(i),"beta_rw_EE_"+bN.at(i),200,0,1 );

        h_1B_BetaSL[i] = new TH1D("beta_sl_1B_"+bN.at(i),"beta_sl_1B_"+bN.at(i),200,0,1 );
        h_1E_BetaSL[i] = new TH1D("beta_sl_1E_"+bN.at(i),"beta_sl_1E_"+bN.at(i),200,0,1 );
        h_BB_BetaSL[i] = new TH1D("beta_sl_BB_"+bN.at(i),"beta_sl_BB_"+bN.at(i),200,0,1 );
        h_BE_BetaSL[i] = new TH1D("beta_sl_BE_"+bN.at(i),"beta_sl_BE_"+bN.at(i),200,0,1 );
        h_EE_BetaSL[i] = new TH1D("beta_sl_EE_"+bN.at(i),"beta_sl_EE_"+bN.at(i),200,0,1 );
        h_1B_BetaSLRW[i] = new TH1D("beta_sl_rw_1B_"+bN.at(i),"beta_sl_rw_1B_"+bN.at(i),200,0,1 );
        h_1E_BetaSLRW[i] = new TH1D("beta_sl_rw_1E_"+bN.at(i),"beta_sl_rw_1E_"+bN.at(i),200,0,1 );
        h_BB_BetaSLRW[i] = new TH1D("beta_sl_rw_BB_"+bN.at(i),"beta_sl_rw_BB_"+bN.at(i),200,0,1 );
        h_BE_BetaSLRW[i] = new TH1D("beta_sl_rw_BE_"+bN.at(i),"beta_sl_rw_BE_"+bN.at(i),200,0,1 );
        h_EE_BetaSLRW[i] = new TH1D("beta_sl_rw_EE_"+bN.at(i),"beta_sl_rw_EE_"+bN.at(i),200,0,1 );

        h2d_1B_Beta[i] = new TH2D("beta_2d_1B_"+bN.at(i),"beta_2d_1B_"+bN.at(i),50,0,1,50,0,1 );
        h2d_1E_Beta[i] = new TH2D("beta_2d_1E_"+bN.at(i),"beta_2d_1E_"+bN.at(i),50,0,1,50,0,1 );
        h2d_BB_Beta[i] = new TH2D("beta_2d_BB_"+bN.at(i),"beta_2d_BB_"+bN.at(i),50,0,1,50,0,1 );
        h2d_BE_Beta[i] = new TH2D("beta_2d_BE_"+bN.at(i),"beta_2d_BE_"+bN.at(i),50,0,1,50,0,1 );
        h2d_EE_Beta[i] = new TH2D("beta_2d_EE_"+bN.at(i),"beta_2d_EE_"+bN.at(i),50,0,1,50,0,1 );
        h2d_1B_BetaRW[i] = new TH2D("beta_2d_rw_1B_"+bN.at(i),"beta_2d_rw_1B_"+bN.at(i),50,0,1,50,0,1 );
        h2d_1E_BetaRW[i] = new TH2D("beta_2d_rw_1E_"+bN.at(i),"beta_2d_rw_1E_"+bN.at(i),50,0,1,50,0,1 );
        h2d_BB_BetaRW[i] = new TH2D("beta_2d_rw_BB_"+bN.at(i),"beta_2d_rw_BB_"+bN.at(i),50,0,1,50,0,1 );
        h2d_BE_BetaRW[i] = new TH2D("beta_2d_rw_BE_"+bN.at(i),"beta_2d_rw_BE_"+bN.at(i),50,0,1,50,0,1 );
        h2d_EE_BetaRW[i] = new TH2D("beta_2d_rw_EE_"+bN.at(i),"beta_2d_rw_EE_"+bN.at(i),50,0,1,50,0,1 );
        
        h_1B_ctau[i] = new TH1D("ctau_1B_"+bN.at(i),"ctau_1B_"+bN.at(i),200,0,1000 );
        h_1E_ctau[i] = new TH1D("ctau_1E_"+bN.at(i),"ctau_1E_"+bN.at(i),200,0,1000 );
        h_BB_ctau[i] = new TH1D("ctau_BB_"+bN.at(i),"ctau_BB_"+bN.at(i),200,0,1000 );
        h_BE_ctau[i] = new TH1D("ctau_BE_"+bN.at(i),"ctau_BE_"+bN.at(i),200,0,1000 );
        h_EE_ctau[i] = new TH1D("ctau_EE_"+bN.at(i),"ctau_EE_"+bN.at(i),200,0,1000 );
        h_1B_ctauRW[i] = new TH1D("ctau_rw_1B_"+bN.at(i),"ctau_rw_1B_"+bN.at(i),200,0,1000 );
        h_1E_ctauRW[i] = new TH1D("ctau_rw_1E_"+bN.at(i),"ctau_rw_1E_"+bN.at(i),200,0,1000 );
        h_BB_ctauRW[i] = new TH1D("ctau_rw_BB_"+bN.at(i),"ctau_rw_BB_"+bN.at(i),200,0,1000 );
        h_BE_ctauRW[i] = new TH1D("ctau_rw_BE_"+bN.at(i),"ctau_rw_BE_"+bN.at(i),200,0,1000 );
        h_EE_ctauRW[i] = new TH1D("ctau_rw_EE_"+bN.at(i),"ctau_rw_EE_"+bN.at(i),200,0,1000 );

        h_1B_ctauSL[i] = new TH1D("ctau_sl_1B_"+bN.at(i),"ctau_sl_1B_"+bN.at(i),200,0,1000 );
        h_1E_ctauSL[i] = new TH1D("ctau_sl_1E_"+bN.at(i),"ctau_sl_1E_"+bN.at(i),200,0,1000 );
        h_BB_ctauSL[i] = new TH1D("ctau_sl_BB_"+bN.at(i),"ctau_sl_BB_"+bN.at(i),200,0,1000 );
        h_BE_ctauSL[i] = new TH1D("ctau_sl_BE_"+bN.at(i),"ctau_sl_BE_"+bN.at(i),200,0,1000 );
        h_EE_ctauSL[i] = new TH1D("ctau_sl_EE_"+bN.at(i),"ctau_sl_EE_"+bN.at(i),200,0,1000 );
        h_1B_ctauSLRW[i] = new TH1D("ctau_sl_rw_1B_"+bN.at(i),"ctau_sl_rw_1B_"+bN.at(i),200,0,1000 );
        h_1E_ctauSLRW[i] = new TH1D("ctau_sl_rw_1E_"+bN.at(i),"ctau_sl_rw_1E_"+bN.at(i),200,0,1000 );
        h_BB_ctauSLRW[i] = new TH1D("ctau_sl_rw_BB_"+bN.at(i),"ctau_sl_rw_BB_"+bN.at(i),200,0,1000 );
        h_BE_ctauSLRW[i] = new TH1D("ctau_sl_rw_BE_"+bN.at(i),"ctau_sl_rw_BE_"+bN.at(i),200,0,1000 );
        h_EE_ctauSLRW[i] = new TH1D("ctau_sl_rw_EE_"+bN.at(i),"ctau_sl_rw_EE_"+bN.at(i),200,0,1000 );

        h2d_1B_ctau[i] = new TH2D("ctau_2d_1B_"+bN.at(i),"ctau_2d_1B_"+bN.at(i),50,0,1000,50,0,1000 );
        h2d_1E_ctau[i] = new TH2D("ctau_2d_1E_"+bN.at(i),"ctau_2d_1E_"+bN.at(i),50,0,1000,50,0,1000 );
        h2d_BB_ctau[i] = new TH2D("ctau_2d_BB_"+bN.at(i),"ctau_2d_BB_"+bN.at(i),50,0,1000,50,0,1000 );
        h2d_BE_ctau[i] = new TH2D("ctau_2d_BE_"+bN.at(i),"ctau_2d_BE_"+bN.at(i),50,0,1000,50,0,1000 );
        h2d_EE_ctau[i] = new TH2D("ctau_2d_EE_"+bN.at(i),"ctau_2d_EE_"+bN.at(i),50,0,1000,50,0,1000 );
        h2d_1B_ctauRW[i] = new TH2D("ctau_2d_rw_1B_"+bN.at(i),"ctau_2d_rw_1B_"+bN.at(i),50,0,1000,50,0,1000 );
        h2d_1E_ctauRW[i] = new TH2D("ctau_2d_rw_1E_"+bN.at(i),"ctau_2d_rw_1E_"+bN.at(i),50,0,1000,50,0,1000 );
        h2d_BB_ctauRW[i] = new TH2D("ctau_2d_rw_BB_"+bN.at(i),"ctau_2d_rw_BB_"+bN.at(i),50,0,1000,50,0,1000 );
        h2d_BE_ctauRW[i] = new TH2D("ctau_2d_rw_BE_"+bN.at(i),"ctau_2d_rw_BE_"+bN.at(i),50,0,1000,50,0,1000 );
        h2d_EE_ctauRW[i] = new TH2D("ctau_2d_rw_EE_"+bN.at(i),"ctau_2d_rw_EE_"+bN.at(i),50,0,1000,50,0,1000 );
    }

    std::vector<int> bins = {40,90,183,220,270,310,360,450,540};
    std::vector<std::vector<int> > saveBeta;
    int j=-1;
    for(int i =0; i<541; i++){
        if( std::find(bins.begin(), bins.end(), i) != bins.end()){
            j++;
            std::vector<int> tmpVal; tmpVal.push_back(1); tmpVal.push_back(j); 
            saveBeta.push_back(tmpVal);
        } else{
            std::vector<int> tmpVal; tmpVal.push_back(0); tmpVal.push_back(0); 
            saveBeta.push_back(tmpVal);           
        }
    }
    for(int i=0; i < 541; i++){
        std::cout << i << ", " << saveBeta.at(i)[0] << ", " << saveBeta.at(i)[1] << std::endl;
    }

    std::cout << "filled efficiency arrays, going to loop through " << chain->GetEntries() << " events" << std::endl;

    for (int i_evt=0; i_evt < chain->GetEntries(); i_evt++)
    {
        /* if(i_evt % 1000 == 0){
         timer.Stop();
         std::cout << "event " << i_evt  << " at time " << timer.RealTime() << std::endl;
         timer.Start();
         }
         */
        chain->GetEntry(i_evt);
        if(debug) std::cout << "retrieved event " << i_evt << std::endl;

        toyLLP->SetAllVariables();
        toyLLP->set_Lxyz();

        nEvents+= 1.0;
        if(i_evt == 0) rnd.SetSeed(toyLLP->pT->at(0));
        if(i_evt % 10000 == 0) {
            Long64_t nEntries = chain->GetEntries();
            timestamp_t t1 = get_timestamp();
            double time1 = (t1 - t0) / 1000000.0L;
            // if(time1 > 0.0000031) cout << "time1 " << time1 << endl;
            cout << "nEvent: " << nEvents << "/" << nEntries  << " at time " << time1 << endl;
            t0 = t1;
        }

        double deltar_llps = -99;

        if(toyLLP->eta->size() == 2){
            deltar_llps = DeltaR(toyLLP->phi->at(0), toyLLP->phi->at(1), toyLLP->eta->at(0), toyLLP->eta->at(1));
        } else{
            cout << "there are: " << toyLLP->eta->size() << " llps in this event! That's not equal to 2... oh no!" << endl;
            //nEventsWithout2LLPs++;
            continue;
        }

        //if(deltar_vpi < 2.0) continue; //only if doing 2vx search only!

        if(TMath::Abs(toyLLP->eta->at(0)) > 2.5 && TMath::Abs(toyLLP->eta->at(1)) > 2.5){ continue;} //abcd can use one up to 3.2 (end of calorimeters...)
        else  if(TMath::Abs(toyLLP->eta->at(0)) > 3.2 || TMath::Abs(toyLLP->eta->at(1)) > 3.2){ continue;}
        
        if (debug ) std::cout << "topology eta check is ok! " << std::endl;
        
        std::vector<TVector3> original_decay_pos;
        
        for(unsigned int i=0; i<toyLLP->eta->size(); ++i) {
            TVector3 tmp_pos;
            tmp_pos.SetMagThetaPhi(toyLLP->Lxyz->at(i),toyLLP->theta->at(i),toyLLP->phi->at(i));
            original_decay_pos.push_back(tmp_pos);
        }
        
        bool is1B_Event_rw = is1B_Event(original_decay_pos);
        bool is1E_Event_rw = is1E_Event(original_decay_pos);
        bool isBB_Event_rw = isBB_Event(original_decay_pos);
        bool isBE_Event_rw = isBE_Event(original_decay_pos);
        bool isEE_Event_rw = isEE_Event(original_decay_pos);

        ////////////////////////////////
        //* Varibales initialization *//

        int barreldec = 0;
        int endcapdec = 0;

        double R1=0.;
        double R2=0.;
        double z1=0.;
        double z2=0.;
        double eta1=0.;
        double eta2=0.;
        double ctau = -999;

        if (debug )std::cout << "beginning ctau loop " << std::endl;
        
        for(int k=0; k<540; ++k) {
            ctau = xBinValues[k] + 0.0005;
            if (debug )std::cout << "ctau: " << ctau << std::endl;

            //if(ctau > 10) break;

            std::vector<TVector3> new_decay_pos;
            double new_leading_ctau = 0;
            double new_subleading_ctau = 0;
            for(unsigned int i=0; i<toyLLP->eta->size(); ++i) {

                ////////////////////////////////
                //* Generating v-pion decays *//O

                double bgct = toyLLP->bg->at(i)*ctau*1000.0; //in mm
                double Lxyz = rnd.Exp(bgct);
                if (i==0){
                    new_leading_ctau = Lxyz / toyLLP->bg->at(i);
                    new_subleading_ctau = Lxyz / toyLLP->bg->at(i);
                } else{
                    double tmpct = Lxyz / toyLLP->bg->at(i);
                    if(tmpct > new_leading_ctau){ new_leading_ctau = tmpct;}
                    else {new_subleading_ctau = tmpct;}
                }
                
                TVector3 tmp_pos;
                tmp_pos.SetMagThetaPhi(Lxyz,toyLLP->theta->at(i),toyLLP->phi->at(i));
                new_decay_pos.push_back(tmp_pos);
            }

            
            if(is1B_Event(new_decay_pos)){
                h_1B_Decays->Fill(ctau);
                if(saveBeta[k][0]){
                    h_1B_Beta[saveBeta[k][1]]->Fill(toyLLP->leading_beta);
                    h_1B_BetaSL[saveBeta[k][1]]->Fill(toyLLP->subleading_beta);
                    h2d_1B_Beta[saveBeta[k][1]]->Fill(toyLLP->leading_beta,toyLLP->subleading_beta);
                    h_1B_ctau[saveBeta[k][1]]->Fill(new_leading_ctau);
                    h_1B_ctauSL[saveBeta[k][1]]->Fill(new_subleading_ctau);
                    h2d_1B_ctau[saveBeta[k][1]]->Fill(new_leading_ctau,new_subleading_ctau);
                }
            } else if(is1E_Event(new_decay_pos)){
                h_1E_Decays->Fill(ctau);
                if(saveBeta[k][0]){
                    h_1E_ctau[saveBeta[k][1]]->Fill(new_leading_ctau);
                    h_1E_ctauSL[saveBeta[k][1]]->Fill(new_subleading_ctau);
                    h2d_1E_ctau[saveBeta[k][1]]->Fill(new_leading_ctau,new_subleading_ctau);
                    h_1E_Beta[saveBeta[k][1]]->Fill(toyLLP->leading_beta);
                    h_1E_BetaSL[saveBeta[k][1]]->Fill(toyLLP->subleading_beta);
                    h2d_1E_Beta[saveBeta[k][1]]->Fill(toyLLP->leading_beta,toyLLP->subleading_beta);
                }
            } else if(isBB_Event(new_decay_pos)){
                h_BB_Decays->Fill(ctau);
                if(saveBeta[k][0]){
                    h_BB_ctau[saveBeta[k][1]]->Fill(new_leading_ctau);
                    h_BB_ctauSL[saveBeta[k][1]]->Fill(new_subleading_ctau);
                    h2d_BB_ctau[saveBeta[k][1]]->Fill(new_leading_ctau,new_subleading_ctau);
                    h_BB_Beta[saveBeta[k][1]]->Fill(toyLLP->leading_beta);
                    h_BB_BetaSL[saveBeta[k][1]]->Fill(toyLLP->subleading_beta);
                    h2d_BB_Beta[saveBeta[k][1]]->Fill(toyLLP->leading_beta,toyLLP->subleading_beta);
                }
            } else if(isBE_Event(new_decay_pos)){
                h_BE_Decays->Fill(ctau);
                if(saveBeta[k][0]){
                    h_BE_ctau[saveBeta[k][1]]->Fill(new_leading_ctau);
                    h_BE_ctauSL[saveBeta[k][1]]->Fill(new_subleading_ctau);
                    h2d_BE_ctau[saveBeta[k][1]]->Fill(new_leading_ctau,new_subleading_ctau);
                    h_BE_Beta[saveBeta[k][1]]->Fill(toyLLP->leading_beta);
                    h_BE_BetaSL[saveBeta[k][1]]->Fill(toyLLP->subleading_beta);
                    h2d_BE_Beta[saveBeta[k][1]]->Fill(toyLLP->leading_beta,toyLLP->subleading_beta);
                }
            } else if(isEE_Event(new_decay_pos)){
                h_EE_Decays->Fill(ctau);
                if(saveBeta[k][0]){
                    h_EE_ctau[saveBeta[k][1]]->Fill(new_leading_ctau);
                    h_EE_ctauSL[saveBeta[k][1]]->Fill(new_subleading_ctau);
                    h2d_EE_ctau[saveBeta[k][1]]->Fill(new_leading_ctau,new_subleading_ctau);
                    h_EE_Beta[saveBeta[k][1]]->Fill(toyLLP->leading_beta);
                    h_EE_BetaSL[saveBeta[k][1]]->Fill(toyLLP->subleading_beta);
                    h2d_EE_Beta[saveBeta[k][1]]->Fill(toyLLP->leading_beta,toyLLP->subleading_beta);
                }
            }
            
            double newWeight = toyLLP->newWeight(xBinValues[k]*1000.+0.5,gen_ctau);
            
            if(is1B_Event_rw){
                h_1B_DecaysRW->Fill(ctau, newWeight);
                if(saveBeta[k][0]){
                    h_1B_ctauRW[saveBeta[k][1]]->Fill(toyLLP->leading_ctau,newWeight);
                    h_1B_ctauSLRW[saveBeta[k][1]]->Fill(toyLLP->subleading_ctau,newWeight);
                    h2d_1B_ctauRW[saveBeta[k][1]]->Fill(toyLLP->leading_ctau,toyLLP->subleading_ctau,newWeight);
                    h_1B_BetaRW[saveBeta[k][1]]->Fill(toyLLP->leading_beta,newWeight);
                    h_1B_BetaSLRW[saveBeta[k][1]]->Fill(toyLLP->subleading_beta,newWeight);
                    h2d_1B_BetaRW[saveBeta[k][1]]->Fill(toyLLP->leading_beta,toyLLP->subleading_beta,newWeight);
                }
            } else if(is1E_Event_rw){
                h_1E_DecaysRW->Fill(ctau, newWeight);
                if(saveBeta[k][0]){
                    h_1E_ctauRW[saveBeta[k][1]]->Fill(toyLLP->leading_ctau,newWeight);
                    h_1E_ctauSLRW[saveBeta[k][1]]->Fill(toyLLP->subleading_ctau,newWeight);
                    h2d_1E_ctauRW[saveBeta[k][1]]->Fill(toyLLP->leading_ctau,toyLLP->subleading_ctau,newWeight);
                    h_1E_BetaRW[saveBeta[k][1]]->Fill(toyLLP->leading_beta,newWeight);
                    h_1E_BetaSLRW[saveBeta[k][1]]->Fill(toyLLP->subleading_beta,newWeight);
                    h2d_1E_BetaRW[saveBeta[k][1]]->Fill(toyLLP->leading_beta,toyLLP->subleading_beta,newWeight);
                }
            } else if(isBB_Event_rw){
                h_BB_DecaysRW->Fill(ctau, newWeight);
                if(saveBeta[k][0]){
                    h_BB_ctauRW[saveBeta[k][1]]->Fill(toyLLP->leading_ctau,newWeight);
                    h_BB_ctauSLRW[saveBeta[k][1]]->Fill(toyLLP->subleading_ctau,newWeight);
                    h2d_BB_ctauRW[saveBeta[k][1]]->Fill(toyLLP->leading_ctau,toyLLP->subleading_ctau,newWeight);
                    h_BB_BetaRW[saveBeta[k][1]]->Fill(toyLLP->leading_beta,newWeight);
                    h_BB_BetaSLRW[saveBeta[k][1]]->Fill(toyLLP->subleading_beta,newWeight);
                    h2d_BB_BetaRW[saveBeta[k][1]]->Fill(toyLLP->leading_beta,toyLLP->subleading_beta,newWeight);
                }
            } else if(isBE_Event_rw){
                h_BE_DecaysRW->Fill(ctau, newWeight);
                if(saveBeta[k][0]){
                    h_BE_ctauRW[saveBeta[k][1]]->Fill(toyLLP->leading_ctau,newWeight);
                    h_BE_ctauSLRW[saveBeta[k][1]]->Fill(toyLLP->subleading_ctau,newWeight);
                    h2d_BE_ctauRW[saveBeta[k][1]]->Fill(toyLLP->leading_ctau,toyLLP->subleading_ctau,newWeight);
                    h_BE_BetaRW[saveBeta[k][1]]->Fill(toyLLP->leading_beta,newWeight);
                    h_BE_BetaSLRW[saveBeta[k][1]]->Fill(toyLLP->subleading_beta,newWeight);
                    h2d_BE_BetaRW[saveBeta[k][1]]->Fill(toyLLP->leading_beta,toyLLP->subleading_beta,newWeight);
                }
            } else if(isEE_Event_rw){
                h_EE_DecaysRW->Fill(ctau, newWeight);
                if(saveBeta[k][0]){
                    h_EE_ctauRW[saveBeta[k][1]]->Fill(toyLLP->leading_ctau,newWeight);
                    h_EE_ctauSLRW[saveBeta[k][1]]->Fill(toyLLP->subleading_ctau,newWeight);
                    h2d_EE_ctauRW[saveBeta[k][1]]->Fill(toyLLP->leading_ctau,toyLLP->subleading_ctau,newWeight);
                    h_EE_BetaRW[saveBeta[k][1]]->Fill(toyLLP->leading_beta,newWeight);
                    h_EE_BetaSLRW[saveBeta[k][1]]->Fill(toyLLP->subleading_beta,newWeight);
                    h2d_EE_BetaRW[saveBeta[k][1]]->Fill(toyLLP->leading_beta,toyLLP->subleading_beta,newWeight);
                }
            }
            
            if(debug) std::cout << "looped through particles to see if a vetex was found " << std::endl;
            /*   timestamp_t t3 = get_timestamp();
             double time3 = (t3 - t2) / 1000000.0L;
             if(time3 > 0.0000031) cout << "time3 " << time3 << endl;*/

        } //end loop through ctaus
        //return kTRUE;
        if(debug) std::cout << "done event" << std::endl;
        toyLLP->clearAllVectors();
    } //end loop through events?
/*

    h_BB_Decays->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
    h_BE_Decays->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
    h_EE_Decays->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
    h_1B_Decays->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
    h_1E_Decays->Scale(LUMI*SampleDetails::mediatorXS/nEvents);

    h_BB_DecaysRW->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
    h_BE_DecaysRW->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
    h_EE_DecaysRW->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
    h_1B_DecaysRW->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
    h_1E_DecaysRW->Scale(LUMI*SampleDetails::mediatorXS/nEvents);
*/
    
    outputFile->Write();

}

