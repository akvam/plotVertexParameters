
#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/SystHistograms.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "TVector3.h"

#include "PlottingPackage/PlottingUtils.h"

//#include "CommonUtils/MathUtils.h"

#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLorentzVector.h"

using namespace std;
using namespace plotVertexParameters;

double wrapPhi(double phi);
double DeltaR2(double phi1, double phi2, double eta1, double eta2);
double DeltaR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return(delPhi*delPhi+delEta*delEta);
}
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;
    
}
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

int isSignal; int isData;
double jetSliceWeights[13];
TStopwatch timer;
//



int main(int argc, char **argv){
    
    std::cout << "running program!" << std::endl;
    
    TChain *chain = new TChain("recoTree");
    
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    
    InputFiles::AddFilesToChain(TString(argv[1]), chain);
    
    //Directory the plots will go in
    
    TString plotDirectory = TString(argv[2]);
    TFile *outputFile = new TFile(plotDirectory+"/outputMSSystematics.root","RECREATE");
    
    VertexVariables *msvx = new VertexVariables;
    CommonVariables *commonVar = new CommonVariables;
    LLPVariables    *llpVar = new LLPVariables;
    SystHistograms  *histEtaFull = new SystHistograms;
    
    SystHistograms *histEtaFullLJ = new SystHistograms;
    SystHistograms *histEtaFullSLJ = new SystHistograms;

    SystHistograms *histEta[5];
    for(int i=0;i<5;i++) histEta[i] = new SystHistograms;

    setPrettyCanvasStyle();
    
    std::cout << "set canvas style" << std::endl;
    
    TH1::SetDefaultSumw2();
    
    isSignal = -1; isData = -1;
    
    msvx->setZeroVectors();
    msvx->setBranchAdresses(chain);
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain);
    llpVar->setZeroVectors();
    llpVar->setBranchAdresses(chain);

    histEtaFull->initializeHistograms("full","AllJets");
    
    histEta[0]->initializeHistograms("eta1","AllJets");
    histEta[1]->initializeHistograms("eta2","AllJets");
    histEta[2]->initializeHistograms("eta3","AllJets");
    histEta[3]->initializeHistograms("eta4","AllJets");
    histEta[4]->initializeHistograms("eta5","AllJets");

//
    
    timer.Start();
    
    setJetSliceWeights(jetSliceWeights);
    
    int jetSlice = -1;
    double finalWeight = 0;
    
    for (int l=0;l<chain->GetEntries();l++)
    {
        
        if(l % 100000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        
        chain->GetEntry(l);
        
        if(!commonVar->passJ400) continue;
        //if(MSVertex_eta->size() == 0) continue;
        
        TString inputFile = TString(chain->GetCurrentFile()->GetName());
        
        //TString inputFile = chain->GetFile()->GetName();
        if( l % 100000 == 0 ) std::cout << "running over file: " << inputFile << std::endl;

        if( inputFile.Contains("LongLived") || inputFile.Contains("_LLP") ){ isSignal = 1; isData = 0; }

        if( isSignal == 1 ) finalWeight = 1.0;
        else std::cout << "what on earth are you running on? Input file is " << inputFile << std::endl;

        int nBarrel=0; int nEndcap = 0; int nID = 0;
        int bIndex = -1; int eIndex = -1;
        
        //see if event is a good dijet event
        int indexLJ = -1;
        int indexSLJ = -1;
        double maxPt = 0;
        for ( size_t i_jet=0;i_jet<commonVar->Jet_ET->size();i_jet++){
            double jetPt = commonVar->Jet_pT->at(i_jet);

            if(jetPt > maxPt){
                maxPt = jetPt;
                indexLJ = i_jet;
            }
        }
        if(maxPt < 460.) continue; //410 is not not high enough energy for a dijet event with J400 - 460 has > 98% efficiency!
        maxPt = 0;
        for ( size_t i_jet=0;i_jet<commonVar->Jet_ET->size();i_jet++){
            if( i_jet == indexLJ) continue; //looking for sub-leading jet
            double jetPt = commonVar->Jet_pT->at(i_jet);
            if(jetPt > maxPt){
                maxPt = jetPt;
                indexSLJ = i_jet;
            }
        }
    	/*std::cout << "subleading/leading jet?!? " << indexLJ << ", " << indexSLJ << std::endl;
    	std::cout << "eta/phi size: " << commonVar->Jet_eta->size()<<", " << commonVar->Jet_phi->size() << std::endl;
    	std::cout << "LJ: " << commonVar->Jet_eta->at(indexLJ)<<", " << commonVar->Jet_phi->at(indexLJ) << std::endl;
    	std::cout << "SLJ: " << commonVar->Jet_eta->at(indexSLJ)<<", " << commonVar->Jet_phi->at(indexSLJ) << std::endl;
*/
    	if(indexLJ < 0 || indexSLJ < 0){
        	//std::cout << "no subleading/leading jet?!? " << indexLJ << ", " << indexSLJ << std::endl;
        	continue;
        }
        if(commonVar->Jet_pT->at(indexSLJ) < 75.) continue; //not high enough energy for dijet event again.
                
        if(commonVar->Jet_isGoodLLP->at(indexLJ) == 0 || commonVar->Jet_isGoodLLP->at(indexSLJ) == 0) continue; //one of hte jets is bad! 
        
        if(TMath::Abs(wrapPhi(commonVar->Jet_phi->at(indexLJ) - commonVar->Jet_phi->at(indexSLJ))) < 2.9) continue; //need two back-to-back jets
        if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) > 2.7 || TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) > 2.7) continue; //dont want forward jets
        int i2 = -1; int i1 = -1;
        if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) < 0.7) i1 = 0;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) < 1.0) i1 = 1;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) < 1.5) i1 = 2;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) < 1.7) i1 = 3;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexLJ)) < 2.7) i1 = 4;
        else continue;
        if(TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) < 0.7) i2 = 0;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) < 1.0) i2 = 1;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) < 1.5) i2 = 2;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) < 1.7) i2 = 3;
        else if(TMath::Abs(commonVar->Jet_eta->at(indexSLJ)) < 2.7) i2 = 4;
        else continue;
        
        int pt1 = 0; int pt2 = 0;
        if(commonVar->Jet_nAssocMSeg->at(indexLJ) > 20) pt1 = 1;
        if(commonVar->Jet_nAssocMSeg->at(indexSLJ) > 20) pt2 = 1;


    	double ljeta = commonVar->Jet_eta->at(indexLJ);
    	double ljphi = commonVar->Jet_phi->at(indexLJ);
    	double sljeta = commonVar->Jet_eta->at(indexSLJ);
    	double sljphi = commonVar->Jet_phi->at(indexSLJ);
        
        int nInConeLJ[40]; for(int i=0;i<40;i++) nInConeLJ[i]=0;
        int nInConeSLJ[40]; for(int i=0;i<40;i++) nInConeSLJ[i]=0;
    	double aa = 0.05;

        for(size_t tt = 0; tt < msvx->tracklet_eta->size(); tt++){
        	double eta = msvx->tracklet_eta->at(tt);
        	double phi = msvx->tracklet_phi->at(tt);
        	double dR_lj = DeltaR(phi,ljphi,eta,ljeta);
        	double dR_slj = DeltaR(phi,sljphi,eta,sljeta);
        	for(int i=1;i<41;i++){
        		if(dR_lj < aa * double(i) ) nInConeLJ[i-1]++;
        		if(dR_slj < aa * double(i) ) nInConeSLJ[i-1]++;
        	}
        }
        
       // std::cout << "good dijet event, with eta regions : " << i1 <<" , " << i2 << std::endl;
        
        int nMSVx_LJ=0; double MSVx_nMDT_LJ = 0;
        int nMSVx_SLJ=0; double MSVx_nMDT_SLJ = 0;
        
         //std::cout << "Event has " << msvx->isGood->size() << " vertices " << std::endl;  
        for (size_t ll=0;ll<msvx->eta->size();ll++)
        {
        	//std::cout << "start looking at vertex " << ll << std::endl;
            if(msvx->nMDT->at(ll)==0) continue; //skip dummy vertices (when too many tracklets in cluster/event
            int msvx_nMDT = msvx->nMDT->at(ll);
            double msvx_eta = msvx->eta->at(ll);
            double msvx_phi = msvx->phi->at(ll);
            double msvx_R = msvx->R->at(ll);
            double msvx_z = msvx->z->at(ll);
            //bool   isGood = msvx->isGood->at(ll);

            int isEndcap = 0;
            int isBarrel = 0;
            /*if ( fabs(msvx_eta)<0.8 ){
                isBarrel = 1;
                nBMSVx+=finalWeight;
                //std::cout << "vertex in barrel! " <<std::endl;
            }
            else if ( fabs(msvx_eta)> 1.3 && fabs(msvx_eta) < 2.5 ){
                nEMSVx+=finalWeight;
                isEndcap = 1;// for endcaps
                //std::cout << "Vertex in endcaps!" << std::endl;
            }
            else{
                //std::cout << "Vertex found, but in neither barrel nor endcaps" << std::endl;
                continue;
            }*/
            //
            //std::cout << "in vertex loop: vertex " << ll << ". jet indices: " << indexLJ <<", " << indexSLJ<< std::endl;
        	
        	//std::cout << "LJ: " << ljeta<<", " << ljphi << ", deltaR " << DeltaR(msvx_phi,ljphi,msvx_eta,ljeta) << std::endl;
        	//std::cout << "SLJ: " << sljeta<<", " << sljphi	 << ", deltaR " << DeltaR(msvx_phi,sljphi,msvx_eta,sljeta) << std::endl;
            if(DeltaR(msvx_phi,ljphi,msvx_eta,ljeta) < 0.4  ){
                //std::cout << "vertex match LJ " << std::endl;

                MSVx_nMDT_LJ += msvx_nMDT;
                nMSVx_LJ++;
            }
            if( DeltaR(msvx_phi,sljphi,msvx_eta,sljeta) < 0.4 ){
                //std::cout << "vertex match SLJ " << std::endl;

                MSVx_nMDT_SLJ += msvx_nMDT;
                nMSVx_SLJ++;
            }
            //std::cout << "finished looking at vertex " << ll << std::endl;

        }
       // std::cout << "after vertex loop " << std::endl;
        
        histEtaFull->h_nMSVx->Fill(nMSVx_LJ,finalWeight);        histEtaFull->h_nMSVx->Fill(nMSVx_SLJ,finalWeight);
        histEtaFullLJ->h_nMSVx->Fill(nMSVx_LJ,finalWeight);
        histEtaFullLJ->h_MSVx_nMDT->Fill(MSVx_nMDT_LJ,finalWeight);
        histEtaFullSLJ->h_nMSVx->Fill(nMSVx_SLJ,finalWeight);
        histEtaFullSLJ->h_MSVx_nMDT->Fill(MSVx_nMDT_SLJ,finalWeight);
        histEtaFull->h_nMSVx->Fill(nMSVx_LJ,finalWeight); histEtaFull->h_nMSVx->Fill(nMSVx_SLJ,finalWeight);
        histEtaFull->h_MSVx_nMDT->Fill(MSVx_nMDT_LJ,finalWeight); histEtaFull->h_MSVx_nMDT->Fill(MSVx_nMDT_SLJ,finalWeight);

        histEtaFull->h_eta->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight); histEtaFull->h_eta->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_eta->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_eta->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight);

        histEtaFull->h_phi->Fill(commonVar->Jet_phi->at(indexLJ),finalWeight); histEtaFull->h_phi->Fill(commonVar->Jet_phi->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_phi->Fill(commonVar->Jet_phi->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_phi->Fill(commonVar->Jet_phi->at(indexSLJ),finalWeight);
        
        histEtaFull->h_pT->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight); histEtaFull->h_pT->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_pT->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_pT->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
        
        histEtaFull->h_ET->Fill(commonVar->Jet_ET->at(indexLJ),finalWeight); histEtaFull->h_ET->Fill(commonVar->Jet_ET->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_ET->Fill(commonVar->Jet_ET->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_ET->Fill(commonVar->Jet_ET->at(indexSLJ),finalWeight);

        histEtaFull->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexLJ),finalWeight); histEtaFull->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexSLJ),finalWeight);
        
           histEtaFull->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight); 
           histEtaFull->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
         histEtaFullLJ->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
        
        histEtaFull->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexLJ),finalWeight); histEtaFull->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexSLJ),finalWeight);
        histEtaFullLJ->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexLJ),finalWeight);
        histEtaFullSLJ->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexSLJ),finalWeight);

        
        if(pt1) histEtaFull->h_nMSTracklets->Fill(commonVar->Jet_nMSTracklets->at(indexLJ),finalWeight); 
        if(pt1) histEtaFullLJ->h_nMSTracklets->Fill(commonVar->Jet_nMSTracklets->at(indexLJ),finalWeight);
        if(pt2) histEtaFull->h_nMSTracklets->Fill(commonVar->Jet_nMSTracklets->at(indexSLJ),finalWeight);
        if(pt2) histEtaFullSLJ->h_nMSTracklets->Fill(commonVar->Jet_nMSTracklets->at(indexSLJ),finalWeight);
        

        if(pt1) histEtaFull->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexLJ),finalWeight);
        if(pt2) histEtaFull->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexSLJ),finalWeight);
        if(pt1) histEtaFullLJ->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexLJ),finalWeight);
        if(pt2) histEtaFullSLJ->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexSLJ),finalWeight);
       

        histEtaFull->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ)); histEtaFull->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));
        histEtaFullLJ->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        histEtaFullSLJ->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));
        for(int i=0;i<40;i++){
            double xVal = aa*double(i) + 0.005;
        	histEtaFull->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeLJ[i]);  
        	histEtaFull->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeSLJ[i]);   
        	histEtaFullLJ->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeLJ[i]);  
        	histEtaFullSLJ->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeSLJ[i]);   
        	if(pt1) histEta[i1]->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeLJ[i]);
        	if(pt2) histEta[i2]->h_cone_avgTracklets->Fill(xVal, finalWeight*nInConeSLJ[i]);
        }

        if(pt1) histEta[i1]->h_nMSVx->Fill(nMSVx_LJ,finalWeight);
        if(pt2) histEta[i2]->h_nMSVx->Fill(nMSVx_SLJ,finalWeight);
        if(pt1) histEta[i1]->h_MSVx_nMDT->Fill(MSVx_nMDT_LJ,finalWeight);
        if(pt2) histEta[i2]->h_MSVx_nMDT->Fill(MSVx_nMDT_SLJ,finalWeight);
        
        if(pt1) histEta[i1]->h_eta->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_eta->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight);
        
        if(pt1) histEta[i1]->h_phi->Fill(commonVar->Jet_phi->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_phi->Fill(commonVar->Jet_phi->at(indexSLJ),finalWeight);
        
        
        if(pt1) histEta[i1]->h_pT->Fill(commonVar->Jet_pT->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_pT->Fill(commonVar->Jet_pT->at(indexSLJ),finalWeight);
        
        if(pt1) histEta[i1]->h_ET->Fill(commonVar->Jet_ET->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_ET->Fill(commonVar->Jet_ET->at(indexSLJ),finalWeight);
        
        if(pt1) histEta[i1]->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_logRatio->Fill(commonVar->Jet_logRatio->at(indexSLJ),finalWeight);
        
        
        if(pt1) histEta[i1]->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_nAssocMSeg->Fill(commonVar->Jet_nAssocMSeg->at(indexSLJ),finalWeight);
        
        
        if(pt1) histEta[i1]->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_nMSegCone->Fill(commonVar->Jet_nMSegCone->at(indexSLJ),finalWeight);
        
        
        if(pt1) histEta[i1]->h_nMSTracklets->Fill(commonVar->Jet_nMSTracklets->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_nMSTracklets->Fill(commonVar->Jet_nMSTracklets->at(indexSLJ),finalWeight);
        if(pt1) histEta[i1]->h_nMSTracklets2->Fill(nInConeLJ[3],finalWeight);
        if(pt2) histEta[i2]->h_nMSTracklets2->Fill(nInConeSLJ[3],finalWeight);
        if(pt1) histEta[i1]->h_nMSTracklets3->Fill(nInConeLJ[5],finalWeight);
        if(pt2) histEta[i2]->h_nMSTracklets3->Fill(nInConeSLJ[5],finalWeight);
        if(pt1) histEta[i1]->h_nMSTracklets5->Fill(nInConeLJ[9],finalWeight);
        if(pt2) histEta[i2]->h_nMSTracklets5->Fill(nInConeSLJ[9],finalWeight);
        if(pt1) histEta[i1]->h_nMSTracklets6->Fill(nInConeLJ[11],finalWeight);
        if(pt2) histEta[i2]->h_nMSTracklets6->Fill(nInConeSLJ[11],finalWeight);
        
        
        if(pt1) histEta[i1]->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexLJ),finalWeight);
        if(pt2) histEta[i2]->h_nMuonRoIs->Fill(commonVar->Jet_nMuonRoIs->at(indexSLJ),finalWeight);
        
        
        if(pt1) histEta[i1]->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexLJ));
        if(pt2) histEta[i2]->h_eta_nAssocMSeg->Fill(commonVar->Jet_eta->at(indexSLJ),finalWeight*commonVar->Jet_nAssocMSeg->at(indexSLJ));
        
  
        // ** Fill variable for counting number of events with more than one vertex **

        
        msvx->clearAllVectors();
        commonVar->clearAllVectors();
    }
    
    
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;
    
    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;
    
    histEtaFull->h_eta_nAssocMSeg->Divide(histEtaFull->h_eta);
    histEtaFullLJ->h_eta_nAssocMSeg->Divide(histEtaFullLJ->h_eta);
    histEtaFullSLJ->h_eta_nAssocMSeg->Divide(histEtaFullSLJ->h_eta);
    
    histEtaFull->h_cone_avgTracklets->Scale(1./histEtaFull->h_eta->Integral());
    histEtaFullLJ->h_cone_avgTracklets->Scale(1./histEtaFullLJ->h_eta->Integral());
    histEtaFullSLJ->h_cone_avgTracklets->Scale(1./histEtaFullSLJ->h_eta->Integral());
    
    for(int i=0;i<5;i++){
    	histEta[i]->h_eta_nAssocMSeg->Divide(histEta[i]->h_eta);
    	histEta[i]->h_cone_avgTracklets->Scale(1./histEta[i]->h_eta->Integral());
    }

    
    // ** Make plot **
    //
    
    outputFile->Write();
}
