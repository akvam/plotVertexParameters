void plotMSegs(){
	//0.7 < |η| < 1.0 or 1.5 < |η| < 1.7
	//TFile *_file0 = TFile::Open("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v04/user.hrussell.mc15_13TeV.361024.Pythia8EvtGen_jetjet_JZ4W.EXOT15_DVana.p2452_dv04_hist/user.hrussell.7782684._000002.hist-output.root");
        TChain *reco1 = new TChain("recoTree");

	reco1->Add("/LLPData/Outputs/JZXW_DVAna/AnalysisCode_v04/user.hrussell.mc15_13TeV.361024.Pythia8EvtGen_jetjet_JZ4W.EXOT15_DVana.p2452_dv04_hist/*.root");
	
	//TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/dataTest/myoutput/hist-rootfiles.root");

         TChain *reco2 = new TChain("recoTree");
	reco2->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/DijetSkims/AnalysisCode_v04/user.hrussell.data15_13TeV.00284484.physics_Main.merge.DVAna_DijetSkim.f644_m1518_p2495_dv04_hist/*.root");
	reco2->Add("/LLPData/Outputs/data_outputsFromDiVertAnalysis/DijetSkims/AnalysisCode_v04/user.hrussell.data15_13TeV.00284285.physics_Main.merge.DAOD_EXOT15.f643_m1518_p2495_dv04_hist/*.root");
	    TCanvas* c1 = new TCanvas("c1","c1",800,600);

	TH1F *h_MC = new TH1F("MCMSegs","",50,0,100);
	TH1F *h_Data = new TH1F("DataMSegs","",50,0,100);
	h_MC->Sumw2();h_Data->Sumw2();
	reco1->Draw("CalibJet_nAssocMSeg >> MCMSegs","eventWeight*(CalibJet_ET@.size() > 1 && CalibJet_ET[1] > 75 && CalibJet_ET >= CalibJet_ET[1] && event_passJ400&& TMath::Abs(CalibJet_phi[0] - CalibJet_phi[1]) > 2.14 && (CalibJet_pT[0] + CalibJet_pT[1]) > 200)","");
	reco2->Draw("CalibJet_nAssocMSeg >> DataMSegs"," CalibJet_ET@.size() > 1 && CalibJet_ET[1] > 75 && CalibJet_ET[0] < 800 && CalibJet_ET >= CalibJet_ET[1] && event_passJ400&& TMath::Abs(CalibJet_phi[0] - CalibJet_phi[1]) > 2.14 && (CalibJet_pT[0] + CalibJet_pT[1]) > 200","SAME");
	
	h_MC->Scale(h_Data->Integral()/h_MC->Integral());
	h_MC->Draw();
	h_Data->Draw("SAME");
 h_Data->SetLineColor(kBlack);h_Data->SetMarkerColor(kBlack);h_Data->SetMarkerSize(1.2);h_Data->SetLineWidth(2);h_Data->SetMarkerStyle(20);
h_MC->SetLineColor(kAzure-3);h_MC->SetMarkerColor(kAzure-3);h_MC->SetMarkerSize(1.2);h_MC->SetLineWidth(2);h_MC->SetMarkerStyle(22);
	double max = h_MC->GetMaximum();
	if(h_Data->GetMaximum() > max ) max = h_Data->GetMaximum();
	h_MC->GetYaxis()->SetRangeUser(0.01,max*1.5);
	c1->SetLogy(1);

	c1->Print("SystematicsOutputs/dataAndMC_MSegs.pdf");

	h_MC->Divide(h_Data);
	h_MC->Draw();
	c1->Print("SystematicsOutputs/MCoverData_MSegs.pdf");


	TH1F *h_MCpt = new TH1F("MCpt","",50,0,1000);
	TH1F *h_Datapt = new TH1F("Datapt","",50,0,1000);
	h_MCpt->Sumw2();h_Datapt->Sumw2();
	reco1->Draw("CalibJet_pT >> MCpt","eventWeight*(CalibJet_ET@.size() > 1 && CalibJet_ET[1] > 75 && CalibJet_nAssocMSeg > 20 && CalibJet_ET >= CalibJet_ET[1] && event_passJ400&& TMath::Abs(CalibJet_phi[0] - CalibJet_phi[1]) > 2.14 && (CalibJet_pT[0] + CalibJet_pT[1]) > 200)","");
	reco2->Draw("CalibJet_pT >> Datapt"," CalibJet_ET@.size() > 1 && CalibJet_ET[1] > 75 && CalibJet_ET[0] < 800 && CalibJet_nAssocMSeg > 20 && CalibJet_ET >= CalibJet_ET[1] && event_passJ400&& TMath::Abs(CalibJet_phi[0] - CalibJet_phi[1]) > 2.14 && (CalibJet_pT[0] + CalibJet_pT[1]) > 200","SAME");
	 h_Datapt->SetLineColor(kBlack);h_Datpt->SetMarkerColor(kBlack);h_Datapt->SetMarkerSize(1.2);h_Datapt->SetLineWidth(2);h_Datapt->SetMarkerStyle(20);
h_MCpt->SetLineColor(kAzure-3);h_MCpt->SetMarkerColor(kAzure-3);h_MCpt->SetMarkerSize(1.2);h_MCpt->SetLineWidth(2);h_MCpt->SetMarkerStyle(22);
	h_MCpt->Scale(h_Datapt->Integral()/h_MCpt->Integral());
	h_MCpt->Draw();
	h_Datapt->Draw("SAME");

	 max = h_MCpt->GetMaximum();
	if(h_Datapt->GetMaximum() > max ) max = h_Datapt->GetMaximum();
	h_MCpt->GetYaxis()->SetRangeUser(0.01,max*1.5);
	c1->SetLogy(1);

	c1->Print("SystematicsOutputs/dataAndMC_pt.pdf");

	h_MCpt->Divide(h_Datapt);
	h_MCpt->Draw();
	c1->Print("SystematicsOutputs/MCoverData_pt.pdf");


}
