
#include "Riostream.h"
#include <iostream>

#include "CommonUtils/MathUtils.h"

#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/TriggerVariables.h"
#include "PlottingPackage/TriggerHistograms.h"
#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/EventHistograms.h"

#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>

using namespace std;
using namespace plotVertexParameters;
using namespace PlottingUtils;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
double jetSliceWeights[13];
TStopwatch timer;
int nEvt_passedNoIsoOffIso;
int nEvt_passedIso;
int nEvt_passedNoIsoOffIso_Barrel;
int nEvt_passedIso_Barrel;
int nEvt_passedNoIsoOffIso_EndCaps;
int nEvt_passedIso_EndCaps;
//
double nMSVx;
double nBMSVx;
double nEMSVx;
double nMSVx_passHitQual;
double nMSVx_passJetIso;
double nMSVx_passTrkIso;
double nMSVx_passGVC;

//
int nEvt_MoreThanOneVtx;
int nEvt_MoreThanOneVtx_noiso;



int main(){
    
    std::cout << "running program!" << std::endl;
    
    //TFile *outFile = new TFile("outputHistograms.root","RECREATE");
    
    TChain *chain = new TChain("recoTree");
    
    
    //JZXW slices
    //chain->Add("/LLPData/Outputs/JZxW_outputsFromAnalysisCode/test_JZ0W_100000ev.root");
    //chain->Add("/LLPData/Outputs/JZxW_outputsFromAnalysisCode/test_JZ1W_100000ev.root");
    //chain->Add("/LLPData/Outputs/JZxW_outputsFromAnalysisCode/test_JZ2W_100000ev.root");
    //chain->Add("/LLPData/Outputs/JZxW_outputsFromAnalysisCode/test_JZ3W_100000ev.root");
    /*chain->Add("/LLPData/Outputs/JZxW_outputsFromAnalysisCode/test_JZ4W_100000ev.root");
     chain->Add("/LLPData/Outputs/JZxW_outputsFromAnalysisCode/test_JZ5W_100000ev.root");
     chain->Add("/LLPData/Outputs/JZxW_outputsFromAnalysisCode/test_JZ6W_100000ev.root");
     chain->Add("/LLPData/Outputs/JZxW_outputsFromAnalysisCode/test_JZ7W_100000ev.root");
     chain->Add("/LLPData/Outputs/JZxW_outputsFromAnalysisCode/test_JZ8W_100000ev.root");*/
    
    //Signal
    chain->Add("/afs/cern.ch/work/a/akvam/private/MSDV/plotVertexParameters/run/LongLived_VertRecoTest.root");
    
    std::cout << "added file!" << std::endl;
    
    VertexVariables *variables = new VertexVariables;
    TriggerVariables *trigVar = new TriggerVariables;
    CommonVariables *commonVar = new CommonVariables;
    
    std::vector<EventHistograms*> histograms;
    /*EventHistograms *trigNoIsoHist = new EventHistograms;
    EventHistograms *trigJetIsoHist = new EventHistograms;
    EventHistograms *trigTrkIsoHist = new EventHistograms;
    
    EventHistograms *passJetIsoHist = new EventHistograms;
    EventHistograms *failJetIsoHist = new EventHistograms;*/

    setPrettyCanvasStyle();
    
    TH1::SetDefaultSumw2();

    isSignal = -1;

    variables->setZeroVectors();
    variables->setBranchAdresses(chain, false, true);
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain, false, true, false, true);
    trigVar->setZeroVectors();
    trigVar->setBranchAdresses(chain, false);   

    nEvt_passedNoIsoOffIso=0;
    nEvt_passedIso=0;
    nEvt_passedNoIsoOffIso_Barrel=0;
    nEvt_passedIso_Barrel=0;
    nEvt_passedNoIsoOffIso_EndCaps=0;
    nEvt_passedIso_EndCaps=0;
    nMSVx=0;
    nBMSVx=0;
    nEMSVx=0;
    nMSVx_passHitQual=0;
    nMSVx_passJetIso=0;
    nMSVx_passTrkIso=0;
    nMSVx_passGVC=0;
    nEvt_MoreThanOneVtx=0;
    nEvt_MoreThanOneVtx_noiso=0;
    
    timer.Start();
    
    setJetSliceWeights(jetSliceWeights);
    
    int jetSlice = -1;
    double finalWeight = 0;
    
    for (int l=0;l<chain->GetEntries();l++)
    {

        if(l % 100 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        
        chain->GetEntry(l);
        
        TString inputFile = TString(chain->GetCurrentFile()->GetName());
        
            if( l % 100000 == 0 ) std::cout << "running over file: " << inputFile << std::endl;
            if(inputFile.Contains("JZ0W")) jetSlice = 0;
            if(inputFile.Contains("JZ1W")) jetSlice = 1;
            if(inputFile.Contains("JZ2W")) jetSlice = 2;
            if(inputFile.Contains("JZ3W")) jetSlice = 3;
            if(inputFile.Contains("JZ4W")) jetSlice = 4;
            if(inputFile.Contains("JZ5W")) jetSlice = 5;
            if(inputFile.Contains("JZ6W")) jetSlice = 6;
            if(inputFile.Contains("JZ7W")) jetSlice = 7;
            if(inputFile.Contains("JZ8W")) jetSlice = 8;
            
            if(   inputFile.Contains("JZ")    ) isSignal = 0;
            if(inputFile.Contains("LongLived")) isSignal = 1;

//        if(isSignal == 0 ) finalWeight = jetSliceWeights[jetSlice] * variables->eventWeight;
        else if( isSignal == 1 ) finalWeight = 1.0;
        finalWeight = 1.0;
        
        if(l==0) std::cout << "isSignal: " << isSignal << std::endl;


        int nMSVx_evt=0;
        int nMSVx_noiso=0;
    	bool vxMatched = false;

        for (size_t ll=0;ll<variables->eta->size();ll++)
        {
            double msvx_eta = variables->eta->at(ll);
            double msvx_phi = variables->phi->at(ll);
            double msvx_R = variables->R->at(ll);
            double msvx_z = variables->z->at(ll);
            int msvx_isGood = variables->isGood->at(ll);
            int msvx_isBad = false;
            if(!variables->passesJetIso->at(ll) && !variables->passesTrackIso->at(ll) ) msvx_isBad = true;
            nMSVx+=finalWeight;
            nMSVx_noiso++;
            int isEndcap = 0;
            int isBarrel = 0;
            if ( fabs(msvx_eta)<1.0 ){
                isBarrel = 1;
                nBMSVx+=finalWeight;
                std::cout << "vertex in barrel! " <<std::endl;
            }
            else if ( fabs(msvx_eta)>=1.0 && fabs(msvx_eta)<=2.5 ){
                nEMSVx+=finalWeight;
                isEndcap = 1;// for endcaps
                std::cout << "Vertex in endcaps!" << std::endl;
            }
            else{
                //std::cout << "Vertex found, but in neither barrel nor endcaps" << std::endl;
                continue;
            }
            //

            for(size_t it = 0; it < trigVar->mu_roi_nRoI->size(); it++){

            	double trig_eta = trigVar->mu_roi_eta->at(it);
            	double trig_phi = trigVar->mu_roi_phi->at(it);
            	int isIso = 0;
            	int isNoniso = 0;
            	if(trigVar->mu_roi_nJet->at(it) == 0 && trigVar->mu_roi_nTrk->at(it) == 0) isIso = 1;
            	else if(trigVar->mu_roi_nJet->at(it) > 0 && trigVar->mu_roi_nTrk->at(it) > 0) isNoniso = 1;
            	double t_dr2 = MathUtils::DeltaR2(trig_phi,msvx_phi,trig_eta,msvx_eta);

            	if(t_dr2 < 0.16){
            		if(vxMatched){
            			std::cout << "this vertex matched to two RoI clusters..." << std::endl;
            			continue; //needs to be more sophisticated in the future!
            		}
            		if(variables->passesHitThresholds->at(ll)){
            			if(isIso && msvx_isGood==1){
            				vxMatched = true;
            			}
            			if(isNoniso && msvx_isBad){
                    		vxMatched = true;
            			}
            		}
            	}		
            } //end loop through triggers
        } // end loop through vertices
        

        bool passIso = commonVar->passMuvtx;
        bool passNoiso = false;
        if( commonVar->passMuvtx == false && commonVar->passMuvtx_noiso == true ) passNoiso = true;
       
        
        for(size_t it = 0; it < trigVar->mu_roi_nRoI->size(); it++){
        	if(trigVar->mu_roi_nJet->at(it) == 0 && trigVar->mu_roi_nTrk->at(it) == 0 && passIso){
//        	    trigIsoHist->h_TrigPassed->Fill(1.5);
        		passIso = true;
        	}
        	else if(trigVar->mu_roi_nJet->at(it) > 0 && trigVar->mu_roi_nTrk->at(it) > 0 && passNoiso){
//        		trigIsoHist->h_TrigPassed->Fill(0.5);
        		passNoiso = true;
        	}
        }
        int histNo = -1;
        if(passIso) histNo = 0; if(passNoiso) histNo = 1; 
        //loop and fill jets
/*
        if(passIso || passNoiso){
        
          for(size_t i = 0; i < commonVar->Jet_ET->size(); i++){
        	 histograms->h_Jet_Et[histNo]->Fill(commonVar->Jet_ET->at(i));
        	 histograms->h_Jet_pT[histNo]->Fill(commonVar->Jet_pT->at(i));
        	 histograms->h_Jet_eta[histNo]->Fill(commonVar->Jet_eta->at(i));
        	 histograms->h_Jet_phi[histNo]->Fill(commonVar->Jet_phi->at(i));
        	 histograms->h_Jet_nJetPerEvt[histNo]->Fill(commonVar->Jet_pT->size());
        	 
        	 if(vxMatched){
            	 histograms->h_Jet_Et_vx[histNo]->Fill(commonVar->Jet_ET->at(i));
            	 histograms->h_Jet_pT_vx[histNo]->Fill(commonVar->Jet_pT->at(i));
            	 histograms->h_Jet_eta_vx[histNo]->Fill(commonVar->Jet_eta->at(i));
            	 histograms->h_Jet_phi_vx[histNo]->Fill(commonVar->Jet_phi->at(i));
            	 histograms->h_Jet_nJetPerEvt_vx[histNo]->Fill(commonVar->Jet_pT->size());    		 
        	 }
        	 
          }
          histograms->h_HT[histNo]->Fill(commonVar->eventHT);
          histograms->h_HTMiss[histNo]->Fill(commonVar->eventHTMiss);
          histograms->h_NJets[histNo]->Fill(commonVar->eventNJets);
          histograms->h_Meff[histNo]->Fill(commonVar->eventMeff);
          
          if(vxMatched){
              histograms->h_HT_vx[histNo]->Fill(commonVar->eventHT);
              histograms->h_HTMiss_vx[histNo]->Fill(commonVar->eventHTMiss);
              histograms->h_NJets_vx[histNo]->Fill(commonVar->eventNJets);
              histograms->h_Meff_vx[histNo]->Fill(commonVar->eventMeff);
          }
        
        }//end if one trigger passed.
*/
        
        // ** Fill variable for counting number of events with more than one vertex **
        if ( nMSVx_evt>1)       nEvt_MoreThanOneVtx++;
        if ( nMSVx_noiso>1) nEvt_MoreThanOneVtx_noiso++;

        variables->clearAllVectors();
        
    }
    
    
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;
    
    cout << "" << endl;
    cout << " nEntries                 = " << chain->GetEntries() << endl;
    cout << "" << endl;
    cout << " nMS in total, pre any criteria  (b,e)    = " << nMSVx <<
    ", (" << nBMSVx << ", " << nEMSVx << ") " << endl;
    cout << " nMS passing all criteria      = " << nMSVx_passGVC << endl;
    cout << " nMS passing ~track isolation      = " << nMSVx_passTrkIso << endl;
    cout << " nMS passing jet isolation     = " << nMSVx_passJetIso << endl;
    
    cout << " nMS passing Hit quality = " << nMSVx_passHitQual              << endl;
    //
    cout << " nEvt with > 1 vtx                = " << nEvt_MoreThanOneVtx       << endl;
    cout << " nEvt with > 1 vtx (no isolation) = " << nEvt_MoreThanOneVtx_noiso << endl;
    
    
    // ** Make plot **
    //
/*               
    TString plotDirectory = "run281411_plots";
    
    PlottingUtils::plots("Event_HT"  ,plotDirectory ,"Event H_{T} [GeV]"  ,histograms->h_HT,true,2);
    PlottingUtils::plots("Event_HTMiss"  ,plotDirectory ,"Event H_{T}^{miss} [GeV]"  ,histograms->h_HTMiss,true,2);
    PlottingUtils::plots("Event_Meff"  ,plotDirectory ,"Event M_{eff} [GeV]"  ,histograms->h_Meff,true,2);
    PlottingUtils::plots("Event_NJets"  ,plotDirectory ,"nJets with p_{T} > 30 GeV in event"  ,histograms->h_NJets,true,2);
    
    PlottingUtils::plots("Event_HT_vx"  ,plotDirectory ,"Event H_{T} [GeV]"  ,histograms->h_HT_vx,true,2);
    PlottingUtils::plots("Event_HTMiss_vx"  ,plotDirectory ,"Event H_{T}^{miss} [GeV]"  ,histograms->h_HTMiss_vx,true,2);
    PlottingUtils::plots("Event_Meff_vx"  ,plotDirectory ,"Event M_{eff} [GeV]"  ,histograms->h_Meff_vx,true,2);
    PlottingUtils::plots("Event_NJets_vx"  ,plotDirectory ,"nJets with p_{T} > 30 GeV in event"  ,histograms->h_NJets_vx,true,2);    
   
    PlottingUtils::plots("Jet_pT"         ,plotDirectory ,"Jet p_{T}" ,histograms->h_Jet_pT,true);
    PlottingUtils::plots("Jet_eta"        ,plotDirectory ,"Jet #eta"  ,histograms->h_Jet_eta, false);
    PlottingUtils::plots("Jet_phi"        ,plotDirectory ,"Jet #phi"  ,histograms->h_Jet_phi, false);
    
    PlottingUtils::plots("Jet_pT_vx"         ,plotDirectory ,"Jet p_{T}" ,histograms->h_Jet_pT_vx,true);
    PlottingUtils::plots("Jet_eta_vx"        ,plotDirectory ,"Jet #eta"  ,histograms->h_Jet_eta_vx, false);
    PlottingUtils::plots("Jet_phi_vx"        ,plotDirectory ,"Jet #phi"  ,histograms->h_Jet_phi_vx, false);
        
    PlottingUtils::plots1("MSVertex_TrigMatched", plotDirectory, "trigger",histograms->h_MSVertex_TrigMatched,false);
    PlottingUtils::plots1("TrigPassed", plotDirectory, "trigger",histograms->h_TrigPassed,false);

    std::cout << histograms->h_TrigPassed->GetBinContent(1) << ", " << histograms->h_TrigPassed->GetBinContent(2) << std::endl;
    std::cout << histograms->h_MSVertex_TrigMatched->GetBinContent(1) << ", " << histograms->h_MSVertex_TrigMatched->GetBinContent(2) << std::endl;
    
    histograms->h_MSVertex_TrigMatched->Divide(histograms->h_TrigPassed);
    std::cout << histograms->h_MSVertex_TrigMatched->GetBinContent(1) << ", " << histograms->h_MSVertex_TrigMatched->GetBinContent(2) << std::endl;

    PlottingUtils::plots1("Trig_Vtx_Eff", plotDirectory, "trigger",histograms->h_MSVertex_TrigMatched,false,0.15);
 */
   
}
