//
//  plotParamsFromTTree.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/PlottingUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/CommonVariables.h"
#include "PlottingPackage/LLPVariables.h"
#include "PlottingPackage/HistTools.h"

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>

using namespace std;
using namespace plotVertexParameters;
using namespace PlottingUtils;
//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
double jetSliceWeights[13];
TStopwatch timer;


double wrapPhi(double phi);
double DeltaR(double phi1, double phi2, double eta1, double eta2);
double DeltaR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wrapPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double wrapPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;
    
}


int main(int argc, char **argv){
    
    std::cout << "running program!" << std::endl;
    
    //TFile *outFile = new TFile("outputHistograms.root","RECREATE");
    
    TChain *chain = new TChain("recoTree");
    
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
        
    InputFiles::AddFilesToChain(TString(argv[1]), chain);
        
    std::cout << "Ther are: " << chain->GetEntries() << " events" << std::endl;
    
    setPrettyCanvasStyle();
    
    TH1::SetDefaultSumw2();
    
    timer.Start();
	
    TString plotDirectory = TString(argv[2]);

    TFile *outputFile = new TFile(plotDirectory+"/outputTruthParams.root","RECREATE");
    HistTools* hist = new HistTools;

    hist->addHist("LLPdR",100,0,5);
    hist->addHist("LLPdEta",100,0,5);
    hist->addHist("LLPdPhi",64,0,3.2);

    
    std::cout << "filled hists!" << std::endl;
 
    //Directory the plots will go in

    chain->SetBranchStatus("*",0);
    
    CommonVariables *commonVar = new CommonVariables;
    LLPVariables *llpVar = new LLPVariables;
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain, false, false, false, false ,false);
    llpVar->setZeroVectors();
    llpVar->setBranchAddresses(chain);
    
    timer.Start();
    
    double finalWeight = 0;
    
    for (int i_evt=0;i_evt<chain->GetEntries();i_evt++)
    {
        
        if(i_evt % 100000 == 0){
            timer.Stop();
            std::cout << "event " << i_evt  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }
        
        chain->GetEntry(i_evt);
        
//        finalWeight = commonVar->pileupEventWeight;
        finalWeight = 1; //change this when I've figured out the pileup stuff  
     
        if(llpVar->eta->size() != 2) std::cout << "Have a problem: there are " << llpVar->eta->size() << " LLPs in the event..." << std::endl;
        
        hist->fill("LLPdR", DeltaR(llpVar->phi->at(0),llpVar->phi->at(1),llpVar->eta->at(0),llpVar->eta->at(1)), finalWeight);
        hist->fill("LLPdEta", TMath::Abs(llpVar->eta->at(0)-llpVar->eta->at(1)), finalWeight);
        hist->fill("LLPdPhi", TMath::Abs(wrapPhi(llpVar->phi->at(0)-llpVar->phi->at(1))), finalWeight);
        
        llpVar->clearAllVectors();
        commonVar->clearAllVectors(false, false);
   }//end loop through events 
    std::cout << "plotted hists!" << std::endl;

    outputFile->Write();
    
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;
    
}
