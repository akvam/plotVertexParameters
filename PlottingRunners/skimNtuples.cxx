//
//  skimNtuples.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 05/03/17.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "PlottingPackage/InitializationUtils.h"
#include "PlottingPackage/InputFiles.h"
#include "PlottingPackage/VertexVariables.h"
#include "PlottingPackage/TriggerVariables.h"
#include "PlottingPackage/CommonVariables.h"

#include "TFile.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>

using namespace std;
using namespace plotVertexParameters;

TStopwatch timer;

int main(int argc, char **argv){
    
    std::cout << "running ntuple skimming program!" << std::endl;
    
    //TFile *outFile = new TFile("outputHistograms.root","RECREATE");
    
    TChain *chain = new TChain("recoTree");
    
    std::cout << "Have " << argc << " arguments:" << std::endl;
    std::cout << "Running over files: " << argv[1] << std::endl;
    std::cout << "Results are put in folder: " << argv[2] << std::endl;
    std::cout << "Output file name : " << argv[3] << std::endl;
    
    InputFiles::AddFilesToChain(TString(argv[1]), chain);
    
    //Directory the plots will go in
    
    TString outputDirectory = TString(argv[2]);
    
    TFile *outputFile = new TFile(outputDirectory+"/"+TString(argv[3]),"RECREATE");
    
    std::cout << "added file!, will be saved in directory: " << outputDirectory << std::endl;
    
    chain->SetBranchStatus("*",1);
    
    TString inputFile = chain->GetFile()->GetName();
    
    VertexVariables *vxVar = new VertexVariables;
    TriggerVariables *trigVar = new TriggerVariables;
    CommonVariables *commonVar = new CommonVariables;
    
    vxVar->setZeroVectors(false);
    vxVar->setBranchAdresses(chain, false, true);
    std::cout << "got vertex variables" << std::endl;
    trigVar->setZeroVectors();
    trigVar->setBranchAdresses(chain, false);
    std::cout << "got trigger variables" << std::endl;
    commonVar->setZeroVectors();
    commonVar->setBranchAdresses(chain,true,false,true,true,false);
    std::cout << "got common variables" << std::endl;
    
    std::cout << "" << std::endl;
    std::cout << " Running over " << chain->GetEntries() << " events." << std::endl;
    std::cout << "" << std::endl;
    
    //Create a new file + a clone of old tree in new file
    TTree *newtree = chain->CloneTree(0);
    
    timer.Start();
    
    double finalWeight = 1.0;
    for (int l=0;l<chain->GetEntries();l++)
    {
        
        chain->GetEntry(l);
        //if(l < 1531522) continue;
        
	if(l % 5000000 == 0){
            timer.Stop();
            std::cout << "event " << l  << " at time " << timer.RealTime() << std::endl;
            timer.Start();
        }

    std::cout << "Before all if statements" << std::endl;
    std::cout << "vxVar->eta->size() == ";
    std::cout << vxVar->eta->size() << std::endl;
        
        if(vxVar->eta->size() == 0){
            std::cout << "No problems checking size of eta" << std::endl;
            vxVar->clearAllVectors(true,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(true, true ,false);
            continue;
        }

        if(!commonVar->passMuvtx_noiso){
            std::cout << "No problems checking passMuvtx_noiso" << std::endl;
            vxVar->clearAllVectors(true,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(true, true ,false);
            continue;
        }
        
        if(!commonVar->isQualityEvent(true)) {
            std::cout << "No problems checking quality event" << std::endl;
            vxVar->clearAllVectors(true,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(true, true ,false);
            continue;
        }
        if(!commonVar->hasGoodPV) {
            std::cout << "No problems checking good PV" << std::endl;
            vxVar->clearAllVectors(true,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(true, true ,false);
            continue;
        }
        
        std::vector<double> trigEta, trigPhi;
        std::vector<int> trigRegion;
        
        std::cout << "About to check good triggers" << std::endl;
        trigVar->FindGoodTriggers(trigEta, trigPhi, trigRegion);
        
        if(trigEta.size() == 0){
            //std::cout << "THIS EVENT PASSED MUVTX BUT HAS NO GOOD TRIGGERS!" << std::endl;
            vxVar->clearAllVectors(true,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(true, true, false);
            continue; //no RoI clusters, don't continue (should be impossible - but not anymore, b/c eta cuts)
            
        }
        commonVar->setGoodJetEventFlags();
       if(commonVar->isBadEventJetHigh){
            vxVar->clearAllVectors(true,false);
            trigVar->clearAllVectors(false);
            commonVar->clearAllVectors(true, true ,false);
            continue;
        }
        
        newtree->Fill();
        
        vxVar->clearAllVectors(true,false);
        trigVar->clearAllVectors(false);
        commonVar->clearAllVectors(true, true ,false);
    }
    
    std::cout << "finished all events" << std::endl;
    
    
    newtree->Print();
    newtree->AutoSave();
    //outputFile->Write();

    
    timer.Stop();
    std::cout<<"Time is: "<<int(timer.RealTime())<<std::endl;
 
    delete outputFile;
    
}
