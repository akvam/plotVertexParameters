//
//  skimmingVariableNames.h
//  plotVertexParameters
//
//  Created by Heather Russell on 29/03/17.
//
//

#ifndef plotVertexParameters_skimmingVariableNames_h
#define plotVertexParameters_skimmingVariableNames_h

namespace plotVertexParameters {

double lj_eta;
double slj_eta;
double lj_phi;
double slj_phi;
double lj_pT;
double slj_pT;
double lj_nmsegA;
double slj_nmsegA;
double lj_nmseg2;
double slj_nmseg2;
double lj_nmseg4;
double slj_nmseg4;
double lj_nmseg6;
double slj_nmseg6;
double lj_nmseg8;
double slj_nmseg8;
double lj_nmseg10;
double slj_nmseg10;
double sum_mseg;
std::vector<std::vector<int> > mseg_cone;
uint32_t runNumber;
int eventIterator;
double eventWeight;
int nMDTs;
int nRPCs;
int nTGCs;
std::vector<std::string> *mdtChamber = new std::vector<std::string>;
std::vector<int> *mdtTube = new std::vector<int>;
std::vector<int> *mdtLayer = new std::vector<int>;
std::vector<float> * TrackletFitBestChi2 = new std::vector<float>;
std::vector<float> *mdtEta = new std::vector<float>;
std::vector<float> *mdtPhi = new std::vector<float>;
std::vector<float> *mdtR = new std::vector<float>;
std::vector<float> *mdtz = new std::vector<float>;
std::vector<float> *mdtAdc = new std::vector<float>;
std::vector<float> *mdtTDC = new std::vector<float>;
std::vector<int> *mdtStatus = new std::vector<int>;
std::vector<float> *mdtRadius = new std::vector<float>;
std::vector<float> *mdtRadErr = new std::vector<float>;
std::vector<float> *mdtLoc_x = new std::vector<float>;
std::vector<float> *mdtLoc_y = new std::vector<float>;
std::vector<float> *mdtLoc_z = new std::vector<float>;
std::vector<int> *MLChSecInt = new std::vector<int>;
std::vector<int> *MLChStInt = new std::vector<int>;
std::vector<int> *MLChMLInt = new std::vector<int>;
std::vector<int> *MLCh_nGoodHits = new std::vector<int>;
std::vector<int> *MLCh_nHits = new std::vector<int>;
std::vector<int> *MLCh_nSeeds = new std::vector<int>;
std::vector<int> *MLCh_nSegs = new std::vector<int>;
std::vector<float> *TrackletSeeds_slope = new std::vector<float>;
std::vector<float> *TrackletSeeds_int = new std::vector<float>;
std::vector<float> *TrackletSeeds_mdt1_z = new std::vector<float>;
std::vector<float> *TrackletSeeds_mdt1_R = new std::vector<float>;
std::vector<float> *TrackletSeeds_mdt2_z = new std::vector<float>;
std::vector<float> *TrackletSeeds_mdt2_R = new std::vector<float>;
std::vector<float> *TrackletSeeds_mdt3_z = new std::vector<float>;
std::vector<float> *TrackletSeeds_mdt3_R = new std::vector<float>;
int n3HitTrackletSeeds;
int nTrackletSegs;
int nTrackSegments;
std::vector<int> *TrackSegmentisGood = new std::vector<int>;
std::vector<int> *TrackSegmentStation = new std::vector<int>;
std::vector<float> *TrackSegmentDeltaB = new std::vector<float>;
std::vector<float> *TrackSegmentDeltaAlpha = new std::vector<float>;
std::vector<float> *MSeg_eta = new std::vector<float>;
std::vector<float> *MSeg_phi = new std::vector<float>;
std::vector<float> *MSeg_R = new std::vector<float>;
std::vector<float> *MSeg_x = new std::vector<float>;
std::vector<float> *MSeg_y = new std::vector<float>;
std::vector<float> *MSeg_z = new std::vector<float>;
std::vector<float> *MSeg_alpha = new std::vector<float>;
std::vector<float> *MSegLoc_x = new std::vector<float>;
std::vector<float> *MSegLoc_y = new std::vector<float>;
std::vector<float> *MSegLoc_z = new std::vector<float>;
std::vector<float> *MSegLoc_theta = new std::vector<float>;
std::vector<float> *MSeg_chi2Prob = new std::vector<float>;
std::vector<std::string> *MSeg_chamber = new std::vector<std::string>;
std::vector<float> *Tracklet_eta = new std::vector<float>;
std::vector<float> *Tracklet_phi = new std::vector<float>;
std::vector<float> *Tracklet_R = new std::vector<float>;
std::vector<float> *Tracklet_z = new std::vector<float>;
std::vector<float> *Tracklet_ml1_d12 = new std::vector<float>;
std::vector<float> *Tracklet_ml1_d13 = new std::vector<float>;
std::vector<float> *Tracklet_ml2_d12 = new std::vector<float>;
std::vector<float> *Tracklet_ml2_d13 = new std::vector<float>;
std::vector<int> *Tracklet_ml1_nHitsOnTrack = new std::vector<int>;
std::vector<int> *Tracklet_ml2_nHitsOnTrack = new std::vector<int>;
std::vector<int> *Tracklet_nHitsOnTrack = new std::vector<int>;
std::vector<std::vector<float> > *Tracklet_Hits_ml1_R = new std::vector<std::vector<float> >;
std::vector<std::vector<float> > *Tracklet_Hits_ml1_z = new std::vector<std::vector<float> >;
std::vector<std::vector<float> > *Tracklet_Hits_ml1_eta = new std::vector<std::vector<float> >;
std::vector<std::vector<float> > *Tracklet_Hits_ml1_phi = new std::vector<std::vector<float> >;
std::vector<std::vector<float> > *Tracklet_Hits_ml2_R = new std::vector<std::vector<float> >;
std::vector<std::vector<float> > *Tracklet_Hits_ml2_z = new std::vector<std::vector<float> >;
std::vector<std::vector<float> > *Tracklet_Hits_ml2_eta = new std::vector<std::vector<float> >;
std::vector<std::vector<float> > *Tracklet_Hits_ml2_phi = new std::vector<std::vector<float> >;
std::vector<std::string> *Tracklet_chamber = new std::vector<std::string>;
std::vector<int> *Tracklet_inBarrel = new std::vector<int>;
std::vector<float> *Tracklet_ML2Seg_pos_x = new std::vector<float>;
std::vector<float> *Tracklet_ML2Seg_pos_y = new std::vector<float>;
std::vector<float> *Tracklet_ML2Seg_pos_z = new std::vector<float>;
std::vector<float> *Tracklet_ML2Seg_alpha = new std::vector<float>;
std::vector<float> *Tracklet_ML1Seg_pos_x = new std::vector<float>;
std::vector<float> *Tracklet_ML1Seg_pos_y = new std::vector<float>;
std::vector<float> *Tracklet_ML1Seg_pos_z = new std::vector<float>;
std::vector<float> *Tracklet_ML1Seg_alpha = new std::vector<float>;
std::vector<float> *TSeg_tmp_ndof = new std::vector<float>;
std::vector<float> *TSeg_tmp_chi2Prob = new std::vector<float>;
std::vector<float> *TSeg_tmp_chi2 = new std::vector<float>;
std::vector<float> *TrackSegment_eta = new std::vector<float>;
std::vector<float> *TrackSegment_phi = new std::vector<float>;
std::vector<float> *TrackSegment_R = new std::vector<float>;
std::vector<float> *TrackSegment_z = new std::vector<float>;

std::vector<float> *muon_eta = new std::vector<float>;
std::vector<float> *muon_phi = new std::vector<float>;

std::vector<int> *LLP_inBarrel = new std::vector<int>;
std::vector<int> *LLP_inEndcap = new std::vector<int>;
std::vector<float> *LLP_R = new std::vector<float>;
std::vector<float> *LLP_z = new std::vector<float>;
std::vector<float> *LLP_eta = new std::vector<float>;
std::vector<float> *LLP_phi = new std::vector<float>;
std::vector<float> *LLP_gamma = new std::vector<float>;
std::vector<float> *LLP_deltar = new std::vector<float>;
std::vector<float> *LLP_nTrackSegs = new std::vector<float>;
std::vector<float> *LLP_nTracklets = new std::vector<float>;
std::vector<float> *LLP_nMDT = new std::vector<float>;
std::vector<float> *LLP_nRPC = new std::vector<float>;
std::vector<float> *LLP_nTGC = new std::vector<float>;
std::vector<float> *Cluster_eta = new std::vector<float>;
std::vector<float> *Cluster_phi = new std::vector<float>;
std::vector<int> *Cluster_region = new std::vector<int>;
std::vector<int> *Cluster_syst = new std::vector<int>;
std::vector<int> *Cluster_sf = new std::vector<int>;
std::vector<int> *Vertex_indexLLP = new std::vector<int>;
std::vector<float> *Vertex_LLP_dR = new std::vector<float>;
std::vector<float> *VertexEta = new std::vector<float>;
std::vector<float> *VertexPhi = new std::vector<float>;
std::vector<float> *Vertex_z = new std::vector<float>;
std::vector<float> *Vertex_R = new std::vector<float>;
std::vector<float> *Vertex_chi2prob = new std::vector<float>;
std::vector<int> *Vertex_nMDT = new std::vector<int>;
std::vector<int> *Vertex_nRPC = new std::vector<int>;
std::vector<int> *Vertex_nTGC = new std::vector<int>;
std::vector<int> *Vertex_nHighOccChambers = new std::vector<int>;
std::vector<int> *Vertex_nTracklets = new std::vector<int>;
std::vector<int> *Vertex_pass = new std::vector<int>;
std::vector<int> *Vertex_region = new std::vector<int>;
std::vector<unsigned int> *Vertex_author = new std::vector<unsigned int>;
std::vector<int> *Vertex_sf = new std::vector<int>;
std::vector<int> *Vertex_syst = new std::vector<int>;
std::vector<int> *nSeedsPerSegPair = new std::vector<int>;
std::vector<int> *nTracklet_Systs = new std::vector<int>;
int nTracklets;
int nClusters;
int nVertices;
int progressBar;
int progressBar_BVx;
int progressBar_EVx;
int nGoodSegmentPairs;
int nGoodSegmentPairsSB;
int nGoodSegmentPairsCB;
int nGoodSegmentPairsE;

}
#endif
