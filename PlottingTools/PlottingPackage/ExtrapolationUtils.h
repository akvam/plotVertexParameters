//
//  ExtrapolationUtils.h
//  plotVertexParameters
//
//  Created by Heather Russell on 09/02/16.
//
//

#ifndef __plotVertexParameters__ExtrapolationUtils__
#define __plotVertexParameters__ExtrapolationUtils__

#include <stdio.h>
#include "TVector3.h"
#include "TH1.h"
#include "TRandom3.h"
namespace ExtrapolationUtils{
    int region(TVector3 llp);
    int topology(TVector3 llp1, TVector3 llp2);
    int topologyStealth(TVector3 llp1, TVector3 llp2);
    void fillLifetimeExtrapolations(TRandom3 &rnd, double bg_1,TVector3 llp1,double bg_2,TVector3 llp2, TH1F *decayPositions[6], double weightVBF, double weightVH);
    void fillLifetimeExtrapolations(TRandom3 &rnd, double bg_1,TVector3 llp1,double bg_2,TVector3 llp2, TH1F *decayPositions[4]);
    
    bool isBadTopology(std::vector<TVector3>& llp_locs);
    bool isEID_Event(std::vector<TVector3>& llp_locs);
    bool isBID_Event(std::vector<TVector3>& llp_locs);
    bool is1E_Event(std::vector<TVector3>& llp_locs);
    bool is1B_Event(std::vector<TVector3>& llp_locs);
    bool isEE_Event(std::vector<TVector3>& llp_locs);
    bool isBE_Event(std::vector<TVector3>& llp_locs);
    bool isBB_Event(std::vector<TVector3>& llp_locs);
    int checkDecLoc(TVector3& llp_loc);
    std::pair<int, int> getJetIndices(int region, TVector3& llp_loc, double momentum);
}

#endif /* defined(__plotVertexParameters__ExtrapolationUtils__) */
