//
//  HistTools.h
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#ifndef __plotVertexParameters__HistTools__
#define __plotVertexParameters__HistTools__

#include <stdio.h>
#include "TH1.h"
#include "TH2.h"

struct HistTools {
public:
    std::vector<TH1D* > h_1D; 

    std::vector<TH2D* > h_2D;
    
    void addHist(TString name, int xBin, double xMin, double xMax, int yBin, double yMin, double yMax);
    void addHist(TString name, int xBin, double xMin, double xMax);
    void addHist(TString name, std::vector<TString> suffixes, int nx, Double_t x[]);

    void addGAHists(TString name, TString exp);

    void fill(TString name, double value, double weight);
    void fill(TString name, double value);
    void scale(double sf);
    double getBinContent(TString name, int bin);
    void setBinContent(TString name, int bin, double content);

    void fill(TString name, double valueX, double valueY, double weight);
    
};
#endif /* defined(__plotVertexParameters__HistTools__) */
