//
//  EfficiencyHistograms.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#include "PlottingPackage/EfficiencyHistograms.h"
#include <iostream>
#include "PlottingPackage/PlottingUtils.h"

void EfficiencyHistograms::addHist(TString name, int xBin, double xMin, double xMax){
    TH1D *temp = new TH1D(name,name,xBin,xMin,xMax);
    temp->Sumw2();
    h_numerator.push_back(temp);
    
    TH1D *temp1 = new TH1D(name+"_ALL",name+"_ALL",xBin,xMin,xMax);
    temp1->Sumw2();
    h_numeratorALL.push_back(temp1);
    
    TH1D *temp2 = new TH1D(name+"_denom",name+"_denom",xBin,xMin,xMax);
    temp2->Sumw2();
    h_denominator.push_back(temp2);
}
void EfficiencyHistograms::addHist2(TString name, int xBin, double xMin, double xMax){
    TH1D *temp = new TH1D(name,name,xBin,xMin,xMax);
    temp->Sumw2();
    h_numerator.push_back(temp);
    
    TH1D *temp2 = new TH1D(name+"_denom",name+"_denom",xBin,xMin,xMax);
    temp2->Sumw2();
    h_denominator.push_back(temp2);
}
void EfficiencyHistograms::addHist(TString name, int xBin, double xMin, double xMax, int yBin, double yMin, double yMax){
    TH2D *temp = new TH2D(name,name,xBin,xMin,xMax,yBin,yMin,yMax);
    temp->Sumw2();
    h_numerator2D.push_back(temp);
    TH2D *temp2 = new TH2D(name+"_denom",name+"_denom",xBin,xMin,xMax,yBin,yMin,yMax);
    temp2->Sumw2();
    h_denominator2D.push_back(temp2);
}
void EfficiencyHistograms::addHist(TString name, int nx, Double_t x[],int ny, Double_t y[]){
    TH2D *temp = new TH2D(name,name, nx, x, ny, y);
    temp->Sumw2();
    h_numerator2D.push_back(temp);
    TH2D *temp2 = new TH2D(name+"_denom",name+"_denom", nx, x, ny, y);
    temp2->Sumw2();
    h_denominator2D.push_back(temp2);
}
void EfficiencyHistograms::addHist(TString name, int nx, Double_t x[]){
    TH1D *temp = new TH1D(name,name, nx, x);
    temp->Sumw2();
    h_numerator.push_back(temp);

    TH1D *temp1 = new TH1D(name+"_ALL",name+"_ALL", nx, x);
    temp1->Sumw2();
    h_numeratorALL.push_back(temp1);
    
    TH1D *temp2 = new TH1D(name+"_denom",name+"_denom", nx, x);
    temp2->Sumw2();
    h_denominator.push_back(temp2);
}
void EfficiencyHistograms::addEffHist(TString name, int xBin, double xMin, double xMax){
    TEfficiency *temp = new TEfficiency(name,name,xBin,xMin,xMax);
    t_eff.push_back(temp);
}
/*void EfficiencyHistograms::addEffHist(TString name, int xBin, double xMin, double xMax, int yBin, double yMin, double yMax){
    TEfficiency *temp = new TEfficiency(name,name,xBin,xMin,xMax,yBin,yMin,yMax);
    t_eff2D.push_back(temp);
}*/

void EfficiencyHistograms::fill(TString name, double value, double weight){
    
    bool isFilled = false;
    if(name.Contains("denom")){
        for(auto hist : h_denominator){
            if(TString(hist->GetName()) == name){
                hist->Fill(value,weight);
                isFilled = true;
                break;
            }
        }
    } else if(name.Contains("ALL")){
        for(auto hist : h_numeratorALL){
            if(TString(hist->GetName()) == name){
                hist->Fill(value,weight);
                isFilled = true;
                break;
            }
        }
    } else {
        for(auto hist : h_numerator){
            if(TString(hist->GetName()) == name){
                hist->Fill(value,weight);
                isFilled = true;
                break;
            }
        }
    }
    if( !isFilled ) std::cout << "DIDN'T FIND HISTOGRAM " << name << "!" << std::endl;
    
    return;
}
void EfficiencyHistograms::fill(TString name, double valueX, double valueY, double weight){
    
    bool isFilled = false;
    if(name.Contains("denom")){
        for(auto hist : h_denominator2D){
            if(hist->GetName() == name){
                hist->Fill(valueX,valueY,weight);
                isFilled = true;
                break;
            }
        }
    } else {
        for(auto hist : h_numerator2D){
            if(hist->GetName() == name){
                hist->Fill(valueX,valueY,weight);
                isFilled = true;
                break;
            }
        }
    }
    if( !isFilled ) std::cout << "DIDN'T FIND 2D HISTOGRAM " << name << "!" << std::endl;
    
    return;
}
void EfficiencyHistograms::fillEff(TString name, double value, double weight, bool pass){
    
    bool isFilled = false;
    for(auto hist : t_eff){
            if(TString(hist->GetName()) == name){
                hist->Fill(pass,weight,value);
                isFilled = true;
                break;
            }
    }

    if( !isFilled ) std::cout << "DIDN'T FIND TEFFICIENCY " << name << "!" << std::endl;
    
    return;
}
void EfficiencyHistograms::plotEfficiencies(TString histDir){
    if( h_denominator.size() != h_numerator.size() || h_denominator.size() != h_numeratorALL.size() ){
        std::cout << "not the same number of numerators and denominators, not making efficiencies!" << std::endl;
        return;
    }
    
    for(unsigned int i_hist = 0; i_hist < h_numerator.size(); i_hist++){
        TString histName = h_numerator.at(i_hist)->GetName();
        TH1D *numerators[2]; numerators[0] = h_numeratorALL.at(i_hist); numerators[1] = h_numerator.at(i_hist);
        TString xLabel = "";
        if(histName.Contains("Lxy") ) xLabel = "Lxy [m]";
        else if (histName.Contains("Lz") ) xLabel = "|Lz| [m]";
        
        TString type = findHistType(histName);
        
        if(type != "") PlottingUtils::plotMSEfficiency(histName,histDir,xLabel,numerators,h_denominator.at(i_hist),type);
        else PlottingUtils::plotEfficiency(histName, histDir, xLabel, "Efficiency", numerators[1],h_denominator.at(i_hist),h_denominator.at(i_hist)->GetXaxis()->GetXmin(),h_denominator.at(i_hist)->GetXaxis()->GetXmax());
    }
    
    for(unsigned int i_hist = 0; i_hist < h_numerator2D.size(); i_hist++){
        TString histName = h_numerator2D.at(i_hist)->GetName();
        TString xLabel = "";        TString yLabel = "";
        double xMin=0; double yMin = 0; double xMax = 10; double yMax=10;
        if(histName.Contains("BB") ){ xLabel = "Lxy [m]"; yLabel = "Lxy [m]"; xMin=3;xMax=8;yMin=3;yMax=8;}
        else if (histName.Contains("BE") ){ xLabel = "|Lz| [m]"; yLabel = "Lxy [m]";xMin=5;xMax=15;yMin=3;yMax=8;}
        else if (histName.Contains("EE") ){ xLabel = "|Lz| [m]"; yLabel = "|Lz| [m]";xMin=5;xMax=15;yMin=5;yMax=15;}
        
        PlottingUtils::plotEfficiency(histName,histDir,xLabel,yLabel, h_numerator2D.at(i_hist),
                                      h_denominator2D.at(i_hist),xMin,xMax,yMin,yMax);
    }
    for(unsigned int i_eff = 0; i_eff < t_eff.size(); i_eff++){
        TString name = t_eff.at(i_eff)->GetName();
        TString xLabel = "";
        if(name.Contains("Lxy") ) xLabel = "Lxy [m]";
        else if (name.Contains("Lz") ) xLabel = "|Lz| [m]";
        PlottingUtils::plot(name, histDir, xLabel, "Efficiency", t_eff.at(i_eff)->CreateGraph() ,false, "");
    }
    return;
}
TString EfficiencyHistograms::findHistType(TString histName){
    TString type = "";
    if(histName.Contains("MSVxTrig")) type = "MSVxTrig";
    else if(histName.Contains("MSVx")) type = "MSVx";
    else if(histName.Contains("MSTrig")) type = "MuTrig";
    else if(histName.Contains("CalR")) type = "CalR";
    else if(histName.Contains("IDVx")) type = "IDVx";
    else std::cout << "histogram type unknown for " << histName << std::endl;
    return type;
}
