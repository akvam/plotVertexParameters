//
//  plottingUtils.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 11/10/15.
//
//

#include <stdio.h>
#include "CommonUtils/SortingUtils.h"
#include "CommonUtils/MathUtils.h"
#include "PlottingPackage/PlottingUtils.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "AtlasStyle.h"
#include <iostream>     // std::cout, std::fixed
#include <iomanip>
#include <sstream>
#include "TVector3.h"
#include "TLine.h"
#include "TEfficiency.h"

using namespace PlottingUtils;
using namespace std;

void PlottingUtils::make_jetIso_GVC(CommonVariables* &commonVar, const double eta, const double phi, GVCHistograms* &hists,
        const double dRCut, const double logRCut, const double ETCut, const double finalWeight){
    bool passJetIso=true;
    double highest_E_jet = 0;
    double highest_E_jet_dR = 99;
    std::vector<std::pair<int,int> > jets_by_vertices;
    std::vector<int> dR_bins;
    std::vector<int> Et_bins;
    std::vector<int> logR_bins;

    for ( size_t i_jet=0;i_jet<commonVar->Jet_ET->size();i_jet++){
        double jetEt = commonVar->Jet_ET->at(i_jet);
        if(commonVar->Jet_logRatio->at(i_jet) > logRCut || jetEt <= 30 || !commonVar->Jet_passJVT) continue;

        double jet_dr = MathUtils::DeltaR(commonVar->Jet_phi->at(i_jet),phi,commonVar->Jet_eta->at(i_jet),eta);
        if( jetEt > 30. && ( (fabs(eta) < 1.0 && jet_dr < 0.3) ||
                (fabs(eta) >=1.0 && fabs(eta) < 2.5 && jet_dr  < 0.6) ) ){ passJetIso = false;}
        if(!passJetIso && jetEt > highest_E_jet){
            highest_E_jet = jetEt;
            highest_E_jet_dR = jet_dr;
        }
        int dr_bin = jet_dr*20+1; //x-bin value
        int et_bin = jetEt*0.2+1; //y-bin value
        if( et_bin > 41) et_bin = 41;

        if( jet_dr <= dRCut && commonVar->Jet_logRatio->at(i_jet) <= logRCut ){ Et_bins.push_back(et_bin); }
        if( jetEt >= ETCut && commonVar->Jet_logRatio->at(i_jet) <= logRCut ){ dR_bins.push_back(dr_bin); }
        //if( Jet_ET >= ETCut && jet_dr =< dRCut ){ logR_bins.push_back(); }


        std::pair<int,int> tmp_pair = std::make_pair(et_bin,dr_bin);
        jets_by_vertices.push_back(tmp_pair);
    }

    PlottingUtils::make_2d_gvc_plot(jets_by_vertices,hists->h_JetdR_vs_Et_fPass,0.05,5.,finalWeight);
    hists->h_JetdR_vs_Et_highestE->Fill(highest_E_jet_dR,highest_E_jet,finalWeight);
    //std::cout << "jet Et: " << std::endl;
    PlottingUtils::make_1d_gvc_plot(Et_bins, hists->h_jetIso_vsEt_fPass,5.,finalWeight,true);
    //std::cout << " jet dR: " << std::endl;
    PlottingUtils::make_1d_gvc_plot(dR_bins, hists->h_jetIso_vsdR_fPass,0.05,finalWeight,false);

    return;

}
void PlottingUtils::fillAllBins(const double weight, TH1D* h){
    //std::cout << "this hist has : " << h->GetNbinsX() << " bins " << std::endl;
    for(int i=1; i<=h->GetNbinsX();i++){
        double binx = h->GetXaxis()->GetBinCenter(i);
        h->Fill(binx,weight);
    }
}
void PlottingUtils::make_trkIso_GVC(CommonVariables* &commonVar, const double eta, const double phi, GVCHistograms* &hists,
        const double dRCut, const double sumdRCut, const double sumpTCut, const double pTCut, const double finalWeight){

    TVector3 sumTrackPt; sumTrackPt.SetPtEtaPhi(0,0,0);
    std::vector<std::pair<int,int> > tracks_by_vertices;
    std::vector<std::pair<int,int> > stracks_by_vertices;
    std::vector<int> dR_bins;
    std::vector<int> Pt_bins;
    std::vector<int> dRsum_bins;
    std::vector<int> Ptsum_bins;

    TVector3 sumpT_dR[20];
    TVector3 sumpT_dR_1D; sumpT_dR_1D.SetPtEtaPhi(0,0,0);
    for(int i=0; i< 20; i++){
        sumpT_dR[i].SetPtEtaPhi(0,0,0);
    }

    for ( size_t i_t=0;i_t<commonVar->Track_pT->size();i_t++){
        double trackPt = commonVar->Track_pT->at(i_t);
        if(trackPt < 0) std::cout << "WARINING: trackPt is less than zero: " << trackPt <<std::endl;
        TVector3 tmp_vec; tmp_vec.SetPtEtaPhi(trackPt,commonVar->Track_eta->at(i_t), commonVar->Track_phi->at(i_t));
        double t_dr = MathUtils::DeltaR(commonVar->Track_phi->at(i_t),phi,commonVar->Track_eta->at(i_t),eta);
        if(t_dr < 0.2) sumTrackPt+=tmp_vec;

        for(unsigned int i=0; i < 20; i++){
            if(t_dr < (double)i*0.05){
                sumpT_dR[i] += tmp_vec;
            }
        }

        if(t_dr < sumdRCut){
            sumpT_dR_1D += tmp_vec;
        }

        //add one to bin values because root starts bins from 1.
        int dr_bin = t_dr*20+1; //x-bin value
        int pt_bin = trackPt*2+1; //y-bin value
        if(t_dr  <= dRCut) { Pt_bins.push_back(pt_bin); }
        if(trackPt >= pTCut) { dR_bins.push_back(dr_bin); }

        if( pt_bin > 41) pt_bin = 41;
        std::pair<int,int> tmp_pair = std::make_pair(pt_bin,dr_bin);
        tracks_by_vertices.push_back(tmp_pair);

    }

    Ptsum_bins.push_back( int(sumpT_dR_1D.Perp()*2+1) );

    //std::cout << "sumTrackPt in 0.2 cone: " << sumTrackPt << std::endl;
    for(unsigned int i=0; i < 20; i++){
        int pt_bin = sumpT_dR[i].Perp()+1; //y-bin value
        if( pt_bin > 31) pt_bin = 31;
        int dr_bin = i+1;
        std::pair<int,int> tmp_pair = std::make_pair(pt_bin,dr_bin);
        stracks_by_vertices.push_back(tmp_pair);
        if(sumpT_dR[i].Perp() > 10.){
            dRsum_bins.push_back(dr_bin);
        }
    }

    PlottingUtils::make_2d_gvc_plot(tracks_by_vertices,hists->h_TrackdR_vs_Pt_fPass,0.05,0.5,finalWeight);
    PlottingUtils::make_2d_gvc_plot(stracks_by_vertices,hists->h_TrackdR_vs_sumPt_fPass,0.05,1.0,finalWeight);

    //std::cout << "track sum Pt: " << std::endl;
    PlottingUtils::make_1d_gvc_plot(Ptsum_bins, hists->h_trkSumIso_vsPt_fPass,1.0,finalWeight,true);
    PlottingUtils::make_1d_gvc_plot(dRsum_bins, hists->h_trkSumIso_vsdR_fPass,0.05,finalWeight,false);
    // std::cout << "track Pt: " << std::endl;

    PlottingUtils::make_1d_gvc_plot(Pt_bins, hists->h_trkIso_vsPt_fPass,0.5,finalWeight,true);
    //std::cout << "track dR: " << std::endl;
    PlottingUtils::make_1d_gvc_plot(dR_bins, hists->h_trkIso_vsdR_fPass,0.05,finalWeight,false);

    return;

}

void PlottingUtils::make_2d_gvc_plot(std::vector<std::pair<int,int> > &by_vertices, TH2D* h_GVC_2D, const double xScale, const double yScale, const double finalWeight){
    if(by_vertices.size()>0) {
        SortingUtils::make_unique_list(by_vertices);
        int dr_threshold_bin = 21;
        for(size_t j=0; j<(by_vertices.size());j++){
            if(j>0) dr_threshold_bin = by_vertices.at(j-1).second;
            for(int bin_y = 1; bin_y <= by_vertices.at(j).first; bin_y++){
                for(int bin_x = by_vertices.at(j).second; bin_x < dr_threshold_bin; bin_x++){
                    h_GVC_2D->Fill((double)bin_x*xScale - 0.001, (double)bin_y*yScale-0.001,finalWeight);
                }
            }
        }
    }
    return;
}
void PlottingUtils::make_2d_gvc_hits(std::vector<std::pair<int,int> > &by_vertices, TH2D* h_GVC_2D, const double xScale, const double yScale, const double finalWeight){
    if(by_vertices.size()>0) {
        SortingUtils::make_unique_list(by_vertices);
        int threshold_bin_x = h_GVC_2D->GetNbinsX()+1;
        int threshold_bin_y = h_GVC_2D->GetNbinsY()+1;
        for(size_t j=0; j<(by_vertices.size());j++){
            if(j>0) threshold_bin_y = by_vertices.at(j-1).second;
            for(int bin_y = by_vertices.at(j).first; bin_y <threshold_bin_y; bin_y++){
                for(int bin_x = by_vertices.at(j).second; bin_x < threshold_bin_x; bin_x++){
                    h_GVC_2D->Fill((double)bin_x*xScale - 0.001, (double)bin_y*yScale-0.001,finalWeight);
                }
            }
        }
    }
    return;
}
void PlottingUtils::make_1d_gvc_plot(std::vector<int> &by_vertices, TH1D* h_GVC_1D, const double xScale, const double finalWeight, const bool fillUp){
    if(by_vertices.size() == 0 ){
        if(fillUp){by_vertices.push_back(1);}
        else{by_vertices.push_back(h_GVC_1D->GetNbinsX()+1);}
    }
    if(by_vertices.size()>0) {
        std::sort(by_vertices.begin(),by_vertices.end());
        //std::cout << "by vertices: "; for(int i=0; i < by_vertices.size(); i++){std::cout << by_vertices.at(i) << ", ";} std::cout << std::endl;
        int threshold_bin = h_GVC_1D->GetNbinsX()+1;
        int min_bin = 0; int max_bin = 0;
        if(fillUp){
            min_bin = by_vertices.at(by_vertices.size()-1)+1;
            max_bin = threshold_bin;
        } else {
            min_bin = 1;
            if(by_vertices.at(0) > threshold_bin){ max_bin = threshold_bin;}
            else max_bin = by_vertices.at(0);

        }        
        for(int bin_x = min_bin; bin_x < max_bin; bin_x++){
            double binx = h_GVC_1D->GetBinCenter(bin_x);
            h_GVC_1D->Fill(binx,finalWeight);
        }
    }
    return;
}
void PlottingUtils::make_1d_gvc_plot(int threshold_bin, TH1D* h_GVC_1D, const double finalWeight, const bool fillUp){
    int min_bin = 0; int max_bin = 0;
    if(fillUp){
        min_bin = threshold_bin;
        max_bin = h_GVC_1D->GetNbinsX()+1;
    } else {
        min_bin = 1;
        if(threshold_bin > h_GVC_1D->GetNbinsX()) max_bin = h_GVC_1D->GetNbinsX()+1;
        else max_bin = threshold_bin+1;
    }        
    for(int bin_x = min_bin; bin_x < max_bin; bin_x++){
        double binx = h_GVC_1D->GetBinCenter(bin_x);
        h_GVC_1D->Fill(binx,finalWeight);
    }

    return;
}
void PlottingUtils::plotMSEfficiency(TString name, TString dir, TString Xtitle, TH1D *pass[2],TH1D *total, TString histType){

    if(histType == "MSVx" || histType == "MSVxTrig" || histType == "MuTrig"){
        TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);

        TLatex latex;
        latex.SetNDC();
        latex.SetTextColor(kBlack);
        latex.SetTextSize(0.038);
        latex.SetTextAlign(13);  //align at top

        // ** Get the maximum **

        TString title = pass[1]->GetName();

        TH1D *h_MSVx_Eff = (TH1D*)pass[0]->Clone();
        TH1D *h_good_MSVx_Eff = (TH1D*)pass[1]->Clone();
        for(int j=0; j<2; j++){
            for(int bin=1; bin <= pass[j]->GetNbinsX(); bin++){
                double error = 0; double eff = 0;
                if(total->GetBinContent(bin)>0){
                    eff = pass[j]->GetBinContent(bin) / total->GetBinContent(bin);
                    error = sqrt( eff * (1.0 - eff) / total->GetBinContent(bin));
                }
                if(j==0){
                    h_MSVx_Eff->SetBinContent(bin,eff);
                    h_MSVx_Eff->SetBinError(bin,error);
                } else{
                    h_good_MSVx_Eff->SetBinContent(bin,eff);
                    h_good_MSVx_Eff->SetBinError(bin,error);
                }
            }
        }
        h_MSVx_Eff->GetYaxis()->SetTitle("Efficiency");
        h_good_MSVx_Eff->SetLineColor(kBlack);
        h_MSVx_Eff->SetLineColor(kBlue);
        h_good_MSVx_Eff->SetMarkerColor(kBlack);
        h_good_MSVx_Eff->SetMarkerStyle(21);
        h_good_MSVx_Eff->SetMarkerSize(1.2);

        h_MSVx_Eff->SetMarkerColor(kBlue);
        h_MSVx_Eff->SetMarkerSize(1.2);
        h_MSVx_Eff->SetMarkerStyle(22);

        h_MSVx_Eff->SetMinimum(0.0);
        h_MSVx_Eff->SetMaximum(1.0);

        h_MSVx_Eff->SetName(title+"_ALL_eff");
        h_good_MSVx_Eff->SetName(title+"_eff");

        h_MSVx_Eff->Draw();
        h_good_MSVx_Eff->Draw("SAME");

        latex.DrawLatex(.2,.91,"#font[72]{ATLAS}  Internal");
        //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");
        //
        TLegend *leg = new TLegend(0.18,0.75,0.5,0.85);
        leg->SetFillColor(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.038);
        if(histType == "MSVx"){
            leg->AddEntry(h_MSVx_Eff,"#font[42]{All MS Vertices}","lp");
            leg->AddEntry(h_good_MSVx_Eff,"#font[42]{Good MS Vertices}","lp");
        } else{
            leg->AddEntry(h_MSVx_Eff,"#font[42]{reweighted to data timing}","lp");
            leg->AddEntry(h_good_MSVx_Eff,"#font[42]{nominal MC timing}","lp");       
        }
        leg->Draw();
        //
        c->Print(dir+"/"+name+".png");
        c->Print(dir+"/"+name+".pdf");
        return;
    } else if(histType == "MuTrig"){
        if(name.Contains("B")){ PlottingUtils::plotEfficiency(name, dir, Xtitle, "Muvtx Trigger Efficiency", pass[1],total,0,10);}
        else if(name.Contains("E")){ PlottingUtils::plotEfficiency(name, dir, Xtitle, "Muvtx Trigger Efficiency", pass[1],total,0,15);}
        return;
    } else return;

}
TGraphAsymmErrors* PlottingUtils::plotEfficiency(TString name, TString dir, TString Xtitle, TString Ytitle, TH1D *pass,TH1D *total, const double xMin, const double xMax){
    TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.038);
    latex.SetTextAlign(13);  //align at top

    pass->SetLineColor(kRed+1);
    pass->SetMarkerColor(kRed+1);

    TGraphAsymmErrors *g_eff = new TGraphAsymmErrors(pass,total,"cl=0.683 b(1,1) mode");

    g_eff->Draw("APE");
    g_eff->GetXaxis()->SetRangeUser(xMin,xMax);
    g_eff->GetYaxis()->SetTitle(Ytitle);
    g_eff->GetXaxis()->SetTitle(Xtitle);
    g_eff->SetName((TString)pass->GetName() + "_eff");
    c->Print(dir+"/"+name+"_tgraph.root");
    c->Print(dir+"/"+name+"_tgraph.pdf");
    g_eff->Write();
    return g_eff;

}

void PlottingUtils::plotEfficiency(TString name, TString dir, TString Xtitle, TString Ytitle, TH2D *pass,TH2D *total, const double xMin, const double xMax,const double yMin, const double yMax){
    TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);
    std::cout << "making 2D efficiency plot from: " << name << std::endl;
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.038);
    latex.SetTextAlign(13);  //align at top
    TH2D *h_eff = (TH2D*)pass->Clone();
    h_eff->SetName(TString(h_eff->GetName())+"_eff");
    for(int binX=1; binX < (1+pass->GetNbinsX()); binX++){
        for(int binY=1; binY < (1+pass->GetNbinsY()); binY++){
            //std::cout << binX << ", " << binY << ", " << pass->GetBinContent(binX,binY)  <<", " <<  total->GetBinContent(binX,binY) << std::endl;
            double error = 0; double eff = 0;
            if(total->GetBinContent(binX,binY)>0){
                eff = pass->GetBinContent(binX,binY) / total->GetBinContent(binX,binY);
                error = sqrt( eff * (1.0 - eff) / total->GetBinContent(binX,binY));
            }
            h_eff->SetBinContent(binX,binY,eff);
            h_eff->SetBinError(binX,binY,error);
        }
    }

    h_eff->Draw("COLZ");
    //h_eff->GetXaxis()->SetRangeUser(xMin,xMax);
    //h_eff->GetYaxis()->SetRangeUser(yMin,yMax);
    h_eff->GetYaxis()->SetTitle(Ytitle);
    h_eff->GetXaxis()->SetTitle(Xtitle);

    c->Print(dir+"/"+name+".root");
    c->Print(dir+"/"+name+".pdf");
}

void PlottingUtils::plots(TString name, TString dir, TString Xtitle, TH1D *h[2], bool log_scale, int legStyle)
{
    TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.038);
    latex.SetTextAlign(13);  //align at top

    h[0]->Scale(1./h[0]->GetEntries());

    h[1]->Scale(1./h[1]->GetEntries());

    // ** Get the maximum **
    double h_max=-999;
    if ( h[0]->GetMaximum() > h_max) h_max = h[0]->GetMaximum();
    if ( h[1]->GetMaximum() > h_max) h_max = h[1]->GetMaximum();

    if ( log_scale ){
        c->SetLogy();
        h[0]->SetMinimum(0.5/h[1]->GetEntries());
        h[0]->SetMaximum(h_max*2);
    }else{
        h[0]->SetMinimum(0);
        h[0]->SetMaximum(h_max*1.28);
    }

    TH1 *h0_copy = (TH1D*)h[0]->Clone("h0_copy");
    h0_copy->SetYTitle("a.u.");
    h0_copy->SetXTitle(Xtitle);
    h0_copy->Draw("HISTO");
    h[0]->SetLineColor(kBlack);
    h[0]->SetFillColor(kBlack);
    h[0]->SetFillStyle(3001);
    h[0]->SetMarkerSize(0);
    h[0]->Draw("E2 SAME");
    //
    TH1 *h1_copy = (TH1D*)h[1]->Clone("h1_copy");
    h1_copy->SetLineColor(kBlue);
    h1_copy->Draw("HISTO SAME");
    h[1]->SetLineColor(kBlue);
    h[1]->SetFillColor(kBlue);
    h[1]->SetFillStyle(3001);
    h[1]->SetMarkerSize(0);
    h[1]->Draw("E2 SAME");
    //
    latex.DrawLatex(.43,.91,"#font[72]{ATLAS}  Internal");
    //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");
    //
    TLegend *leg = new TLegend(0.70,0.81,0.92,0.92);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.038);
    if(legStyle==1){
        leg->AddEntry(h0_copy,"#font[42]{|#eta| < 1}","lp");
        leg->AddEntry(h1_copy,"#font[42]{1.0 < |#eta| < 2.5}","lp");
    } else if (legStyle==2){
        leg->AddEntry(h0_copy,"#font[42]{muvtx}","l");
        leg->AddEntry(h1_copy,"#font[42]{muvtx_noiso}","l");
    }
    leg->Draw();
    //
    //c->Print(dir+"/"+name+".eps");
    c->Print(dir+"/"+name+".png");
    c->Print(dir+"/"+name+".pdf");

    //c->Print(dir+"_JetIso/"+name+"_JetIso.eps");
    return;

}
void PlottingUtils::plot(TString name, TString dir, TString Xtitle, TString Ytitle, TH1D *h1, TH1D *h2, TH1D *h3, bool log_scale, TString leg1, TString leg2, TString leg3)
{
    TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);
    SetAtlasStyle ();
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.038);
    latex.SetTextAlign(13);  //align at top

    // ** Get the maximum **
    double h_max=-999; double lowest_max = 1000000;
    if( h1->GetMaximum() > 0 ) lowest_max = h1->GetMaximum();
    if ( h1->GetMaximum() > h_max) h_max = h1->GetMaximum();
    if ( h2->GetMaximum() > h_max) h_max = h2->GetMaximum();
    if ( h3->GetMaximum() > h_max) h_max = h3->GetMaximum();
    if ( h2->GetMaximum() < lowest_max && h2->GetMaximum() > 0) lowest_max = h2->GetMaximum();
    if ( h3->GetMaximum() < lowest_max && h3->GetMaximum() > 0) lowest_max = h3->GetMaximum();

    if ( log_scale ){
        c->SetLogy();
        h1->SetMinimum(lowest_max*0.01);
        h1->SetMaximum(h_max*15.);
        h2->SetMinimum(lowest_max*0.01);
        h2->SetMaximum(h_max*15.);
        h3->SetMinimum(lowest_max*0.01);
        h3->SetMaximum(h_max*15.);
    }else{
        h1->SetMinimum(0);
        h1->SetMaximum(h_max*1.28);
    }

    h1->GetXaxis()->SetTitle(Xtitle);
    h1->GetXaxis()->SetTitleOffset(1.4);
    h1->GetYaxis()->SetTitle(Ytitle);
    h1->GetYaxis()->SetTitleOffset(1.4);

    h1->SetLineColor(kBlack);
    h1->SetMarkerSize(1.2);    h1->SetLineWidth(2);
    h1->SetMarkerStyle(20);
    h1->Draw("HIST");


    h2->SetLineColor(kBlue);
    h2->SetMarkerSize(1.2);    h2->SetLineWidth(2);
    h2->SetMarkerStyle(22);
    h2->Draw("HIST SAME");

    h3->SetLineColor(kGreen);
    h3->SetMarkerSize(1.2);    h3->SetLineWidth(2); h3->SetLineStyle(2);
    h3->SetMarkerStyle(21);
    h3->Draw("HIST SAME");
    //
    latex.DrawLatex(.2,.91,"#font[72]{ATLAS}  Internal");
    //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");
    //

    TLegend *leg = new TLegend(0.50,0.81,0.92,0.92);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.038);

    leg->AddEntry(h1,"#font[42]{"+leg1+"}","l");
    leg->AddEntry(h2,"#font[42]{"+leg2+"}","l");
    leg->AddEntry(h3,"#font[42]{"+leg3+"}","l");

    leg->Draw();

    //
    //c->Print(dir+"/"+name+".eps");
    c->Print(dir+"/"+name+".png");
    c->Print(dir+"/"+name+".pdf");

    //c->Print(dir+"_JetIso/"+name+"_JetIso.eps");
    return;

}
void PlottingUtils::plot(TString name, TString dir, TString Xtitle, TString Ytitle, TH1D *h1, TH1D *h2, bool log_scale, TString leg1, TString leg2)
{
    TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);
    SetAtlasStyle ();
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.038);
    latex.SetTextAlign(13);  //align at top

    TH1D *h1_copy = (TH1D*)h1->Clone("h1_copy");
    TH1D *h2_copy = (TH1D*)h2->Clone("h2_copy");

    
    // ** Get the maximum **
    double h_max=-999; double lowest_max = 1000000;
    if( h1->GetMaximum() > 0 ) lowest_max = h1->GetMaximum();
    if ( h1->GetMaximum() > h_max) h_max = h1->GetMaximum();
    if ( h2->GetMaximum() > h_max) h_max = h2->GetMaximum();
    if ( h2->GetMaximum() < lowest_max && h2->GetMaximum() > 0) lowest_max = h2->GetMaximum();

    if ( log_scale ){
        c->SetLogy();
        h1->SetMinimum(lowest_max*0.01);
        h1->SetMaximum(h_max*15.);
        h2->SetMinimum(lowest_max*0.01);
        h2->SetMaximum(h_max*15.);
    }else{
        h1->SetMinimum(0);
        h1->SetMaximum(h_max*1.28);
    }

    h1->GetXaxis()->SetTitle(Xtitle);
    h1->GetXaxis()->SetTitleOffset(1.4);
    h1->GetYaxis()->SetTitle(Ytitle);
    h1->GetYaxis()->SetTitleOffset(1.4);
    h1->SetFillColor(kBlack);
    h1->SetFillStyle(3001);
    h1->SetMarkerSize(0);
    h1->Draw("E2 SAME");    
    h1_copy->SetLineWidth(2);
    h1_copy->SetLineColor(kBlack);
    h1_copy->Draw("HIST SAME");


    h2->SetFillColor(kBlue);
    h2->SetFillStyle(3001);
    h2->SetMarkerSize(0);
    h2->Draw("E2 SAME");
    h2_copy->SetLineColor(kBlue);
    h2_copy->Draw("HIST SAME");

    //
    latex.DrawLatex(.2,.91,"#font[72]{ATLAS}  Internal");
    //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");
    //

    TLegend *leg = new TLegend(0.50,0.81,0.92,0.92);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.038);

    leg->AddEntry(h1,"#font[42]{"+leg1+"}","l");
    leg->AddEntry(h2,"#font[42]{"+leg2+"}","l");

    leg->Draw();

    //
    //c->Print(dir+"/"+name+".eps");
    c->Print(dir+"/"+name+".png");
    c->Print(dir+"/"+name+".pdf");

    //c->Print(dir+"_JetIso/"+name+"_JetIso.eps");
    return;

}
void PlottingUtils::plot(TString name, TString dir, TString Xtitle, TString Ytitle, TH1D *h1,bool log_scale, TString leg1)
{
    TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);
    SetAtlasStyle ();
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.038);
    latex.SetTextAlign(13);  //align at top

    // ** Get the maximum **
    double h_max=-999; double maxVal = -99;
    if(maxVal != -99) h_max = maxVal;
    else if ( h1->GetMaximum() > h_max) h_max = h1->GetMaximum();

    if ( log_scale ){
        c->SetLogy();
        h1->SetMinimum(.10);
        h1->SetMaximum(h_max*10.);
    }else{
        h1->SetMinimum(0);
        h1->SetMaximum(h_max*1.28);
    }

    h1->GetXaxis()->SetTitle(Xtitle);
    h1->GetXaxis()->SetTitleOffset(1.4);
    h1->GetYaxis()->SetTitle(Ytitle);
    h1->GetYaxis()->SetTitleOffset(1.4);

    h1->SetLineColor(kBlack);
    h1->SetMarkerSize(1.2);    h1->SetLineWidth(2);
    h1->SetMarkerStyle(20);
    h1->Draw("HIST");

    //
    latex.DrawLatex(.2,.91,"#font[72]{ATLAS}  Internal");
    //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");
    //

    TLegend *leg = new TLegend(0.50,0.81,0.92,0.92);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.038);

    leg->AddEntry(h1,"#font[42]{"+leg1+"}","l");

    leg->Draw();

    //
    //c->Print(dir+"/"+name+".eps");
    c->Print(dir+"/"+name+".png");
    c->Print(dir+"/"+name+".pdf");

    //c->Print(dir+"_JetIso/"+name+"_JetIso.eps");
    return;

}
void PlottingUtils::plot(TString name, TString dir, TString Xtitle, TString Ytitle, TGraphAsymmErrors *h1,bool log_scale, TString leg1)
{
    TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);
    SetAtlasStyle ();
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.038);
    latex.SetTextAlign(13);  //align at top

    // ** Get the maximum **
    double h_max=-999; double maxVal = -99;
    if(maxVal != -99) h_max = maxVal;
    else if ( h1->GetMaximum() > h_max) h_max = h1->GetMaximum();

    if ( log_scale ){
        c->SetLogy();
        h1->SetMinimum(.10);
        h1->SetMaximum(h_max*10.);
    }else{
        h1->SetMinimum(0);
        h1->SetMaximum(h_max*1.28);
    }

    h1->GetXaxis()->SetTitle(Xtitle);
    h1->GetXaxis()->SetTitleOffset(1.4);
    h1->GetYaxis()->SetTitle(Ytitle);
    h1->GetYaxis()->SetTitleOffset(1.4);

    h1->SetLineColor(kBlack);
    h1->SetMarkerSize(1.2);    h1->SetLineWidth(2);
    h1->SetMarkerStyle(20);
    h1->Draw("AP");

    //
    latex.DrawLatex(.2,.91,"#font[72]{ATLAS}  Internal");
    //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");
    //

    TLegend *leg = new TLegend(0.50,0.81,0.92,0.92);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.038);

    leg->AddEntry(h1,"#font[42]{"+leg1+"}","l");

    leg->Draw();

    //
    //c->Print(dir+"/"+name+".eps");
    c->Print(dir+"/"+name+".png");
    c->Print(dir+"/"+name+".pdf");

    //c->Print(dir+"_JetIso/"+name+"_JetIso.eps");
    return;

}

void PlottingUtils::plot2DGVC(TString name, TString dir, TString Ytitle, TString Xtitle, TH2D *h, bool change_eff, double scale_factor, int plotType,const double yMin, const double yMax, const double xMin, const double xMax, std::vector<double> &yVals_XP, std::vector<double> &xVals_YP)
{
    TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);

    c->SetRightMargin(0.15);
    h->Scale(1./scale_factor);
    if ( change_eff ){
        unsigned int nBins = h->GetBin(h->GetNbinsX(),h->GetNbinsY());
        for(unsigned int i=1;i < nBins + 1; i++){
            h->SetBinContent(i,1-h->GetBinContent(i));
            if(h->GetBinContent(i) > 1) std::cout << "Bin value too high!!" << std::endl;
        }
    }
    double minVal = 0.95*h->GetMinimum();
    if(h->GetMaximum() > 1) std::cout << "Warning! GVC hist has maximum value of " << h->GetMaximum() << std::endl;
    if(scale_factor>1){
        //c->SetLogz();
        h->GetYaxis()->SetRangeUser(yMin,yMax);
        h->GetXaxis()->SetRangeUser(xMin,xMax);
        minVal = h->GetMinimum();
        h->SetMinimum(minVal);
        h->SetMaximum(1);
    }

    h->SetYTitle(Ytitle);
    h->SetXTitle(Xtitle);
    h->Draw("COLZ");

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.0438);
    latex.SetTextAlign(13);  //align at top
    latex.DrawLatex(.161,.992,"#font[72]{ATLAS}  Internal");
    //latex.DrawLatex(.635,.996,"#sqrt{s} = 13 TeV,  78 pb^{-1}");

    c->Print(dir+"/"+name+".png");
    c->Print(dir+"/"+name+".pdf");
    c->Print(dir+"/"+name+".root");

    c->SetRightMargin(0.05);

    TH2D *hX = (TH2D*)h->Clone();
    TH2D *hY = (TH2D*)h->Clone();

    TLegend *leg = new TLegend(0.19,0.22,0.55,0.42);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.038);

    int colors[5] = {3,-6,-3,-4,-9};
//    int markers[5] = {20,21,22,23,29};
    int lines[5] = {1,2,4,7,8};

    TH1D* tmp[yVals_XP.size()];
    minVal = 1;
    for(size_t i =0; i<yVals_XP.size(); i++){
        double yVal = yVals_XP.at(i);
        stringstream stream;
        stream << fixed << setprecision(0) << yVal;
        string s = stream.str();

        TString histTitle = TString("xProjection_")+TString(std::to_string(yVal));
        int firstybin = hX->GetYaxis()->FindBin(yVal);
        tmp[i] = hX->ProjectionX(histTitle,firstybin,firstybin);
        tmp[i]->GetXaxis()->SetRangeUser(xMin,xMax);
        tmp[i]->GetYaxis()->SetTitle("Acceptance");
        if(i==0) tmp[i]->Draw("HIST");
        else tmp[i]->Draw("HIST SAME");
        if(tmp[i]->GetMinimum() < minVal) minVal = tmp[i]->GetMinimum();
        tmp[i]->SetLineColor(kAzure+colors[i]);tmp[i]->SetLineStyle(lines[i]);tmp[i]->SetLineWidth(2);
        leg->AddEntry(tmp[i],"#font[42]{"+Ytitle+" = "+TString(s)+"}","l");

    }
    tmp[0]->SetMinimum(0.9*minVal);
    leg->Draw();
    c->Print(dir+"/"+name+"_xp.png");
    c->Print(dir+"/"+name+"_xp.pdf");
    c->Print(dir+"/"+name+"_xp.root");

    TH1D* tmp2[yVals_XP.size()];
    TLegend *leg2 = new TLegend(0.55,0.22,0.89,0.42);
    leg2->SetFillColor(0);
    leg2->SetBorderSize(0);
    leg2->SetTextSize(0.038);
    minVal = 1;
    for(size_t i =0; i<xVals_YP.size(); i++){
        double xVal = xVals_YP.at(i);
        stringstream stream;
        stream << fixed << setprecision(2) << xVal;
        string s = stream.str();
        TString histTitle = TString("yProjection_")+TString(std::to_string(xVal));
        int firstxbin = hY->GetXaxis()->FindBin(xVal);
        tmp2[i] = hY->ProjectionY(histTitle,firstxbin,firstxbin);
        tmp2[i]->GetXaxis()->SetRangeUser(yMin,yMax);
        tmp2[i]->GetYaxis()->SetTitle("Acceptance");
        if(i==0) tmp2[i]->Draw("HIST");
        else tmp2[i]->Draw("HIST SAME");
        if(tmp2[i]->GetMinimum() < minVal) minVal = tmp2[i]->GetMinimum();
        tmp2[i]->SetLineColor(kAzure+colors[i]);tmp2[i]->SetLineStyle(lines[i]);tmp2[i]->SetLineWidth(2);
        leg2->AddEntry(tmp2[i],"#font[42]{"+Xtitle+" = "+TString(s)+"}","l");
    }
    tmp2[0]->SetMinimum(0.9*minVal);
    if(name.Contains("CRJet") && minVal < 0.9) tmp2[0]->SetMinimum(0.9);
    leg2->Draw();
    c->Print(dir+"/"+name+"_yp.png");
    c->Print(dir+"/"+name+"_yp.pdf");
    c->Print(dir+"/"+name+"_yp.root");

    //c->Print(dir+"_JetIso/"+name+"_bin0p1x0p1_JetIso.eps");
    //c->Print(dir+name+"_bin0p1x0p1_JetIso_FailingSelection.eps");
    return;

}

void PlottingUtils::plot2D(TString name, TString dir, TString Ytitle, TString Xtitle, TH2D *h)
{
    TCanvas* c = new TCanvas("c_"+name,"c_"+name,800,600);
    c->SetRightMargin(0.12);
    c->SetLeftMargin(0.12);
    c->SetBottomMargin(0.12);
    h->SetYTitle(Ytitle);
    h->SetXTitle(Xtitle);
    h->Draw("COLZ");
    h->GetYaxis()->SetTitleOffset(1.4);
    h->GetXaxis()->SetTitleOffset(1.4);

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.0438);
    latex.SetTextAlign(13);  //align at top
    latex.DrawLatex(.161,.992,"#font[72]{ATLAS}  Internal");
    //latex.DrawLatex(.635,.996,"#sqrt{s} = 13 TeV,  78 pb^{-1}");

    c->Print(dir+"/"+name+".png");
    c->Print(dir+"/"+name+".pdf");
    c->Print(dir+"/"+name+".root");
    //c->Print(dir+"_JetIso/"+name+"_bin0p1x0p1_JetIso.eps");
    //c->Print(dir+name+"_bin0p1x0p1_JetIso_FailingSelection.eps");
    return;

}


void PlottingUtils::plotWithRatio(TString name, TString dir, TString Ytitle, TString Xtitle, TH1D *h1, TH1D *h2, TString leg1, TString leg2,TString ratioTitle, bool logScale=false){

    TCanvas *c = new TCanvas(name, name,800,600);

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.076);
    latex.SetTextAlign(13);

    TH1D *h1_copy = (TH1D*)h1->Clone("h1_copy");
    TH1D *h2_copy = (TH1D*)h2->Clone("h2_copy");

    // ** Evaluate the maximum of the histograms **
    double h_max=-999;
    if (h1->GetMaximum()>h_max)  h_max=h1->GetMaximum();
    if (h2->GetMaximum()>h_max)  h_max=h2->GetMaximum();
    // ** Set maximum and minimum
    if (logScale){
        c->SetLogy();
        h1->SetMaximum(h_max*6.0);
        h1->SetMinimum(0.00001);
    }else{
        h1->SetMaximum(h_max*1.35);
        h1->SetMinimum(0.0);
    }

    // ** First pad
    TPad *pad1 = new TPad("pad1","pad1",0,0.34,1,1);
    pad1->SetBottomMargin(0.04);
    pad1->SetLeftMargin(0.14);
    if (logScale) pad1->SetLogy();
    pad1->Draw();
    pad1->cd(); 

    h1->GetYaxis()->SetLabelSize(0.070);
    h1->GetYaxis()->SetTitleSize(0.070);
    h1->GetYaxis()->SetTitleOffset(0.18);
    h1->GetXaxis()->SetTitleOffset(2.7);
    h1->GetXaxis()->SetLabelOffset(0.5);
    h1->GetYaxis()->SetTitleOffset(0.95);
    h1->SetYTitle(Ytitle);
    h1->SetXTitle(Xtitle);
    h1->SetFillColor(kBlack);
    h1->SetFillStyle(3001);
    h1->SetMarkerSize(0);
    h1->Draw("E2 SAME");
    h1_copy->SetLineColor(kBlack);
    h1_copy->Draw("HISTO SAME");

    h2->SetFillColor(kBlue);
    h2->SetFillStyle(3001);
    h2->SetMarkerSize(0);
    h2->Draw("E2 SAME");
    h2_copy->SetLineColor(kBlue);
    h2_copy->Draw("HISTO SAME");

    TLegend *leg = new TLegend(0.65,0.75,0.9,0.9);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.070);
    leg->AddEntry(h1,"#font[42]{"+leg1+"}","lf");
    leg->AddEntry(h2,"#font[42]{"+leg2+"}","lf");
    leg->Draw();

    latex.DrawLatex(0.18,0.90,"#font[72]{ATLAS} Internal");

    // ** Second pad
    c->cd();

    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
    pad2->SetTopMargin(0.05);
    pad2->SetBottomMargin(0.40);
    pad2->SetLeftMargin(0.14);
    pad2->Draw();
    pad2->cd();

    TH1D *p1_new = (TH1D*)h1->Clone("p1_new");

    p1_new->Divide(h2);
    p1_new->SetMarkerStyle(8);
    p1_new->SetMarkerSize(0.8);
    p1_new->SetMarkerColor(kBlack);
    p1_new->SetMinimum(0.0);
    p1_new->SetMaximum(2.0);
    p1_new->GetXaxis()->SetTitle(Xtitle);
    p1_new->GetXaxis()->SetLabelFont(42);
    p1_new->GetXaxis()->SetLabelSize(0.16);
    p1_new->GetXaxis()->SetLabelOffset(0.05);
    p1_new->GetXaxis()->SetTitleFont(42);
    p1_new->GetXaxis()->SetTitleSize(0.15);
    p1_new->GetXaxis()->SetTitleOffset(1.3);
    p1_new->GetYaxis()->SetNdivisions(505);
    p1_new->GetYaxis()->SetTitle("#font[42]{"+ratioTitle+"}");
    p1_new->GetYaxis()->SetLabelFont(42);
    p1_new->GetYaxis()->SetLabelSize(0.10);
    p1_new->GetYaxis()->SetTitleFont(42);
    p1_new->GetYaxis()->SetTitleSize(0.15);
    p1_new->GetYaxis()->SetTitleOffset(0.44); 
    p1_new->DrawCopy("eP");

    TLine* line = new TLine();
    line->DrawLine(h1->GetXaxis()->GetXmin(),1,h1->GetXaxis()->GetXmax(),1);

    p1_new->Fit("pol0","Q");

    c->cd();

    c->Print(dir+name+"_withRatio.pdf");

}
