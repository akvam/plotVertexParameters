//
//  LimitExtrapolation.cpp
//  plotVertexParameters
//
//  Created by Heather Russell on 03/09/16.
//
//

#include "PlottingPackage/LimitExtrapolation.h"
#include "TMath.h"
#include "TRandom3.h"
#include <iostream>

#define LTSTEP 0.025
#define LTSTEP2 0.25
#define LTSTEP3 0.0025

void LimitExtrapolation::make_x_axis(Float_t &xBinValues[]){
    
    for(int j=0; j<400; j++){
        xBinValues[j] = LTSTEP3*j;
    }
    for(int j=400; j<800; j++){
        xBinValues[j] = LTSTEP*j - 9.;
    }
    for(int j=800; j<1201; j++){
        xBinValues[j] = LTSTEP2*j - 189.;
    }
}

int LimitExtrapolation::region(TVector3 llp){
    if(TMath::Abs(llp.Eta() < 2.7) && llp.Perp() < 33) return 0;
    else if(TMath::Abs(llp.Eta())< 2.5 && llp.Perp() < 275. && TMath::Abs(llp.z()) < 840. ) return 1;
    else if(TMath::Abs(llp.Eta()) < 2.5 && llp.Perp() > 2000 && llp.Perp() < 3500. ) return 2; //HCal
    else if(TMath::Abs(llp.Eta()) < 0.7 && llp.Perp() > 3000. && llp.Perp() < 800. ) return 3;
    else if(TMath::Abs(llp.Eta()) > 1.3 && TMath::Abs(llp.Eta()) < 2.5 && TMath::Abs(llp.z()) > 5000. && TMath::Abs(llp.z()) < 15000. && llp.Perp() < 10000. ) return 4;
       else return -1; //falls in some inbetween region, we're not sensitive to decays here
}
int LimitExtrapolation::topology(TVector3 llp1, TVector3 llp2){
    int reg1 = region(llp1); int reg2 = region(llp2);
    if(reg1 == 0 && reg2 ==0 ) return 0; //prompt
    else if(reg1 == 0 && (reg2 == 1 || reg2 == 2 || reg2 == 3) ) return 1;  //1DV
    else if(reg2 == 0 && (reg1 == 1 || reg1 == 2 || reg1 == 3) ) return 1;	//1DV
    else if((reg2 == 1 || reg2 == 2 || reg2 == 3) && (reg1 == 1 || reg1 == 2 || reg1 == 3) ) return 2; //2DV
    else if((reg1 == 3 || reg1 == 4) && (reg2 == 3 || reg2 == 4) ) return 3; //invisibles
    else return -1;
}
int LimitExtrapolation::topologyStealth(TVector3 llp1, TVector3 llp2){
    int reg1 = region(llp1); int reg2 = region(llp2);
    if(reg1 == 0 && reg2 ==0 ) return 0; //prompt
    else if( (reg1 == 0 && reg2 == 1) || (reg2 == 0 && reg2 == 1) ) return 10;  //1DV+prompt
    else if( reg1 == 1 && reg2 == 1 ) return 11;  //2ID Vertices (also counts for ID+Prompt)
    else if((reg1 == 1 && reg2 == 2) || (reg1 == 2 && reg2 == 1) ) return 21;   //2DV ID/HCal combinations
    else if(reg2 == 2 && reg1 == 2) return 22;   //2DV HCal/HCal combinations
    else if((reg1 == 3 && reg2 == 0) || (reg1 == 0 && reg2 == 3) ) return 30;   //1DV MS prompt combinations
    else if((reg1 == 3 && reg2 == 1) || (reg1 == 1 && reg2 == 3) ) return 31;   //1DV MS prompt combinations
    else if((reg1 == 3 && reg2 == 2) || (reg1 == 2 && reg2 == 3) ) return 32;   //2DV HCal/MS combinations
    else if(reg2 == 3 && reg1 == 3) return 33;  //2 MS vertices
    else if((reg1 == 3 && reg2 == 4) || (reg1 == 4 && reg2 == 3) ) return 34;   //1DV MS+MET combinations
    else if((reg1 == 3 && reg2 == -1) || (reg1 == -1 && reg2 == 3) ) return 35;   //1DV MS+other gaps combinations
    else if(reg1 == 3 || reg2 == 3) std::cout << "region combination not registered: " << reg1 << ", " << reg2 << std::endl;
    else return -1;
}
void LimitExtrapolation::fillLifetimeExtrapolations(TRandom3 &rnd, double bg_1,TVector3 llp1,double bg_2,TVector3 llp2, TH1F *decayPositions[6], double weightVBF, double weightVH){
    for(int i = 0; i < 90; i++){
        double ctau_bin = TMath::Power(10., -6. + i * 0.1) + 0.0000005;
        double Lxyz1 = rnd.Exp(bg_1 *ctau_bin*1000.0); //1000. because ctau bins are in m, and we want the TVector3 in mm
        double Lxyz2 = rnd.Exp(bg_2 *ctau_bin*1000.0);
        llp1.SetMag(Lxyz1);
        llp2.SetMag(Lxyz2);
        int topo = topology(llp1,llp2);
        int r1 = region(llp1); int r2 = region(llp2);
        double ggFXS = 43.92;
        double VBFHXS = 3.748;
        double ZHXS = 0.8696;
        double WHXS = 1.380;
        double ttHXS = 0.5085;
        
        double weightVH2 = weightVH * (ZHXS+WHXS)/ggFXS; // scale by prod cross section difference for ZH vs. ggF
        double weightVBF2 = weightVBF * VBFHXS/ggFXS;
        if( (r1 == 3 && r2 == 4)|| (r2 == 3 && r1 ==4) ) decayPositions[5]->Fill(ctau_bin);
        if(topo != -1 && topo != 3) decayPositions[topo]->Fill(ctau_bin);
        else if(topo == 3){
            decayPositions[topo]->Fill(ctau_bin,weightVH2);
            decayPositions[topo+1]->Fill(ctau_bin,weightVBF2);
        }
    }
    return;
}
void LimitExtrapolation::fillLifetimeExtrapolations(TRandom3 &rnd, double bg_1,TVector3 llp1,double bg_2,TVector3 llp2, TH1F *decayPositions[4]){
    for(int i = 0; i < 90; i++){
        double ctau_bin = TMath::Power(10., -6. + i * 0.1) + 0.0000005;
        double Lxyz1 = rnd.Exp(bg_1 *ctau_bin*1000.0); //1000. because ctau bins are in m, and we want the TVector3 in mm
        double Lxyz2 = rnd.Exp(bg_2 *ctau_bin*1000.0);
        llp1.SetMag(Lxyz1);
        llp2.SetMag(Lxyz2);
        int topo = topologyStealth(llp1,llp2);
        //int r1 = region(llp1); int r2 = region(llp2);
        
        if( topo == 0) decayPositions[0]->Fill(ctau_bin);
        if( topo == 10 || topo == 11 ) decayPositions[1]->Fill(ctau_bin);
        if( topo == 21 || topo == 22 || topo == 31 || topo == 32 || topo == 33) decayPositions[2]->Fill(ctau_bin);
        if( topo >= 30 ) decayPositions[3]->Fill(ctau_bin);
        
        
    }
    return;
}
