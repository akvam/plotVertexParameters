//
//  CommonVariables.cpp
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#include "PlottingPackage/CommonVariables.h"
#include <iostream>
#include "TVector2.h"
#include "TMath.h"
#include "TLorentzVector.h"

double CommonVariables::DelR(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return sqrt(delPhi*delPhi+delEta*delEta);
}
double CommonVariables::DelR2(double phi1, double phi2, double eta1, double eta2){
    double delPhi = wPhi(phi1-phi2);
    double delEta = eta1-eta2;
    return (delPhi*delPhi+delEta*delEta);
}
double CommonVariables::wPhi(double phi){
    while (phi> TMath::Pi()) phi -= TMath::TwoPi();
    while (phi<-TMath::Pi()) phi += TMath::TwoPi();
    return phi;
}
void CommonVariables::setZeroJESVectors(){
    Jet_pT = 0;
    Jet_eta = 0;
    Jet_phi = 0;
}
void CommonVariables::setZeroVectors(){
    backgroundFlags = 0;
    passMuvtx=false;
    passCalRatio=false;
    passMuvtx_noiso=false;
    passJ400 = false;
    eventNumber = 0;
    runNumber = 0;
    lumiBlock = 0;
    eventWeight = 0;
    pileupEventWeight = 0;
    prw_1up = 0;
    prw_1down = 0;
    pdfWeights = 0;
    eventHT=0;
    eventNJets=0;
    eventNJets50=0;
    eventHTMiss = 0;
    eventMHT = 0;
    eventMHT_phi = 0;
    eventMeff=0;
    eventMET=0;
    eventMET_phi=0;

    hasGoodPV = false;
    isGoodLAr = false;
    isGoodTile = false;
    isGoodSCT =false;
    isCompleteEvent = false;
    isBadEventJetLow = false;
    isBadEventJetHigh = false;

    vertexJetPt=0;
    vertexTrackdR=-1;
    vertexJetdR=-1;
    vertexJetdR_pT=0;
    LJetPt=0;
    SLJetPt=0;
    SSLJetPt=0;
    nJetsOutsideVx20i=0;
    nJetsOutsideVx20=0;
    nJetsOutsideVx50i=0;
    nJetsOutsideVx50=0;
    nJetsOutsideVx75i=0;
    nJetsOutsideVx75=0;
    nJetsOutsideVx100i=0;
    nJetsOutsideVx100=0;

    Track_pT=0;
    Track_eta=0;
    Track_phi=0;

    CR_1j30 = 0;
    CR_1j50 = 0;
    CR_1j50_1j20 = 0;
    CR_2j20= 0;
    CR_2j50 = 0;
    CR_1j50_1j150 = 0;
    CR_2j150_low = 0;
    CR_2j150 = 0;
    CR_1j100_1j200 = 0;
    CR_2j200 = 0;
    CR_1j100_1j250 = 0;
    CR_2j250 = 0;
    
    Jet_width=0;
    Jet_passJVT=0;
    Jet_jvt=0;
    Jet_EMF=0;
    Jet_pT=0;
    Jet_E=0;
    Jet_t=0;
    Jet_ET=0;
    Jet_eta=0;
    Jet_phi=0;
    Jet_logRatio=0;
    Jet_indexLLP=0;
    Jet_isGoodLLP=0;
    Jet_isGoodStand=0;

    Jet_nAssocMSeg=0;
    Jet_nMSegCone=0;
    Jet_nMSTracklets=0;
    Jet_nMuonRoIs=0;

}
void CommonVariables::setJESBranchAddresses(TChain *chain){
    chain->SetBranchAddress("CalibJet_pT"                 ,&Jet_pT);
    chain->SetBranchAddress("CalibJet_eta"                ,&Jet_eta);
    chain->SetBranchAddress("CalibJet_phi"                ,&Jet_phi);
}
void CommonVariables::setBranchAdresses(TChain *chain,bool useJets, bool useTruthJets, bool isData, bool useTracks, bool doPDFSystematics, int doLate){
    chain->SetBranchAddress("eventNumber"            ,&eventNumber);
    chain->SetBranchAddress("eventWeight"            ,&eventWeight);
    chain->SetBranchAddress("pileupEventWeight"      ,&pileupEventWeight);
    if(!isData){
        chain->SetBranchAddress("event_Pileup_PRW_DATASF__1up"      ,&prw_1up);
        chain->SetBranchAddress("event_Pileup_PRW_DATASF__1down"      ,&prw_1down);

    }
    if(doPDFSystematics) chain->SetBranchAddress("pdfEventWeights"        ,&pdfWeights);
    chain->SetBranchAddress("event_HT"               ,&eventHT);
    chain->SetBranchAddress("event_NJets"            ,&eventNJets);
    chain->SetBranchAddress("event_HTMiss"           ,&eventHTMiss);
    chain->SetBranchAddress("event_Meff"             ,&eventMeff);
    chain->SetBranchAddress("hasGoodPV"          ,&hasGoodPV);
    chain->SetBranchAddress("backgroundFlags", &backgroundFlags);
    if(isData){
        chain->SetBranchAddress("isGoodSCT"          ,&isGoodSCT);
        chain->SetBranchAddress("isGoodTile"         ,&isGoodTile);
        chain->SetBranchAddress("isGoodLAr"          ,&isGoodLAr);
        chain->SetBranchAddress("isCompleteEvent"    ,&isCompleteEvent);
	chain->SetBranchAddress("lumiBlock"	     ,&lumiBlock);
	chain->SetBranchAddress("runNumber"	     ,&runNumber); 
   }
    if(doLate == 1){
    chain->SetBranchAddress("event_passMuvtxMU6_EMPTY"        ,&passMuvtx);
    chain->SetBranchAddress("event_passMuvtxMU6_noiso_EMPTY"  ,&passMuvtx_noiso);
    } else if(doLate == 2){
        chain->SetBranchAddress("event_passMuvtx_UNISO"        ,&passMuvtx);
        chain->SetBranchAddress("event_passMuvtx_noiso_UNISO"  ,&passMuvtx_noiso);
     }
    else{
        chain->SetBranchAddress("event_passMuvtx"        ,&passMuvtx);
        chain->SetBranchAddress("event_passCalRatio_TAU60"        ,&passCalRatio);
        chain->SetBranchAddress("event_passMuvtx_noiso"  ,&passMuvtx_noiso);
        chain->SetBranchAddress("event_passJ400"         ,&passJ400);
    }
    if(!useTruthJets && useJets){
        chain->SetBranchAddress("CalibJet_jvt"                ,&Jet_jvt);
        chain->SetBranchAddress("CalibJet_width"                ,&Jet_width);
        chain->SetBranchAddress("CalibJet_passJVT"                ,&Jet_passJVT);
        chain->SetBranchAddress("CalibJet_pT"                 ,&Jet_pT);
        chain->SetBranchAddress("CalibJet_ET"                 ,&Jet_ET);
        chain->SetBranchAddress("CalibJet_E"                ,&Jet_E);
        chain->SetBranchAddress("CalibJet_eta"                ,&Jet_eta);
        chain->SetBranchAddress("CalibJet_phi"                ,&Jet_phi);
        chain->SetBranchAddress("CalibJet_logRatio"         ,&Jet_logRatio);
        chain->SetBranchAddress("CalibJet_time"                ,&Jet_t);
        chain->SetBranchAddress("CalibJet_EMF"                ,&Jet_EMF);
        chain->SetBranchAddress("CalibJet_indexLLP"           ,&Jet_indexLLP);
        //chain->SetBranchAddress("CalibJet_isGoodLLP"          ,&Jet_isGoodLLP);
        chain->SetBranchAddress("CalibJet_isGoodStand"        ,&Jet_isGoodStand);
        chain->SetBranchAddress("CalibJet_nAssocMSeg"         ,&Jet_nAssocMSeg);
        //chain->SetBranchAddress("CalibJet_nMSegCone"         ,&Jet_nMSegCone);
        chain->SetBranchAddress("CalibJet_nMuonRoIs"          ,&Jet_nMuonRoIs);
        chain->SetBranchAddress("CalibJet_nMSTracklets"       ,&Jet_nMSTracklets);
    } else if(useTruthJets){
        chain->SetBranchAddress("TruthJet_pT"                 ,&Jet_pT);
        chain->SetBranchAddress("TruthJet_ET"                 ,&Jet_ET);
        chain->SetBranchAddress("TruthJet_eta"                ,&Jet_eta);
        chain->SetBranchAddress("TruthJet_phi"                ,&Jet_phi);
        chain->SetBranchAddress("TruthJet_logRatio"           ,&Jet_logRatio);
        chain->SetBranchAddress("TruthJet_indexLLP"           ,&Jet_indexLLP);
        chain->SetBranchAddress("TruthJet_isGoodLLP"           ,&Jet_isGoodLLP);
        chain->SetBranchAddress("TruthJet_nAssocMSeg"         ,&Jet_nAssocMSeg);
        chain->SetBranchAddress("TruthJet_nMSegCone"         ,&Jet_nMSegCone);
        chain->SetBranchAddress("TruthJet_nMuonRoIs"         ,&Jet_nMuonRoIs);
        chain->SetBranchAddress("TruthJet_nMSTracklets"         ,&Jet_nMSTracklets);
    }
    if(useTracks){
        chain->SetBranchAddress("Track_pT"               ,&Track_pT);
        chain->SetBranchAddress("Track_eta"              ,&Track_eta);
        chain->SetBranchAddress("Track_phi"              ,&Track_phi);
    }
}
void CommonVariables::fixJetET(){
    for( unsigned int j=0; j < Jet_eta->size(); j++){
        Jet_ET->push_back(Jet_E->at(j)/cosh(Jet_eta->at(j)));
    }
}
void CommonVariables::setGoodJetEventFlags(){
    int foundBadJetLow = 0;
    int foundBadJetHigh = 0;
    for( unsigned int j=0; j < Jet_eta->size(); j++){
        if(Jet_pT->at(j) < 20) continue;
        if(!Jet_isGoodStand->at(j)){

            if(Jet_pT->at(j) < 60 && TMath::Abs(Jet_eta->at(j)) < 2.4 && (Jet_passJVT->at(j) == 1) ){
                foundBadJetLow++;
            }
            else if(Jet_pT->at(j) < 60 && TMath::Abs(Jet_eta->at(j)) >= 2.4){
                foundBadJetLow++;
            }
            else if(Jet_pT->at(j) >=60){
                foundBadJetHigh++;
                foundBadJetLow++;
            }
        }
    }
    if(foundBadJetLow){ isBadEventJetLow = true;}
    if(foundBadJetHigh){ isBadEventJetHigh = true;}
    return;

}
bool CommonVariables::isQualityEvent(bool isData){
    //std::cout << isData << " " << (backgroundFlags&(1<<19)) << " " <<  (backgroundFlags&(1<<20)) << " " << hasGoodPV << std::endl; 
    //if( ( (backgroundFlags&(1<<19)) != 0) || ( (backgroundFlags&(1<<20)) != 0) ) return false; //don't use the stupid background flags
    
    if(!isData && hasGoodPV) return true;
    if( hasGoodPV && isGoodSCT && isGoodTile && isGoodLAr && isCompleteEvent ){
        //if( ( (backgroundFlags&(1<<19)) != 0) || ( (backgroundFlags&(1<<20)) != 0) ) return true;
        return true;
    }
    return false;
}
bool CommonVariables::isGoodDijetEvent(){
    if(! std::is_sorted(Jet_pT->begin(), Jet_pT->end(), [](double a, double b) { return a > b; } ) ){
        std::cout << "Jets aren't sorted by pT! Need to do this before proceeding." << std::endl;
        return false;
    } else {
        bool debug = false;
        if(Jet_pT->size() < 2) return false;
        if(debug) std::cout << "has > 1 jet" << std::endl;
        if(eventMHT > 120.) return false;
        if(debug) std::cout << " has MHT <= 120 " << std::endl;
        if( fabs(wPhi(Jet_phi->at(0) - eventMHT_phi ) ) > 3.0 ) return false;
        if(debug) std::cout << "has dPhi LJ, MHT <= 3.0 " << std::endl;
        if( fabs(Jet_t->at(0) )  > 2.5) return false;
        if(debug) std::cout << "jet time <= 2.5 ns " << std::endl;
        for( unsigned int j=0; j < Jet_pT->size(); j++){
            if(Jet_pT->at(j) < 50. ) break;
            if(!Jet_isGoodStand->at(j) && j < 3) return false;
        }
        if(debug) std::cout << "LJ, SLJ, SSLJ all good " << std::endl;
        if(Jet_pT->at(0) < 600.) return false;
        if(debug) std::cout << "leading jet >= 600 GeV" << std::endl;
        if(Jet_pT->at(1) < 60. ) return false;
        if(debug) std::cout << "SLJ >= 60 GeV" << std::endl;
        if( ( (Jet_pT->at(0) - Jet_pT->at(1)) / (Jet_pT->at(0) + Jet_pT->at(1)) ) >= 0.3 ) return false;
        if(debug) std::cout << "Jet pT balance ok" << std::endl;
        if( fabs(wPhi(Jet_phi->at(0) - Jet_phi->at(1))) < 3.0 ) return false;
        if(debug) std::cout << "LJ, SLJ dPhi >= 3.0" << std::endl;
        TLorentzVector jet1;TLorentzVector jet2;
        jet1.SetPtEtaPhiE(Jet_pT->at(0),Jet_eta->at(0),Jet_phi->at(0), Jet_E->at(0));
        jet2.SetPtEtaPhiE(Jet_pT->at(1),Jet_eta->at(1),Jet_phi->at(1), Jet_E->at(1));
        if( (jet1+jet2).Mag2() <= 1100.*1100.) return false;
        if(debug) std::cout << "mjj >= 1.1 TeV" << std::endl;
        //if( 0.5 * fabs((jet1.Rapidity() - jet2.Rapidity() ) ) >= 1.7 ) return false;
        if(0.5*fabs(Jet_eta->at(0) - Jet_eta->at(1)) >= 1.7) return false;
        if(debug) std::cout << "|y*| < 1.7 " << std::endl;
        //if(Jet_logRatio->at(0) < -0.5 || Jet_logRatio->at(1) < -0.1) return false;
        //if(debug) std::cout << "both jets have logRatio > -0.5" << std::endl;
        return true;
    }
    return false;
}
void CommonVariables::getVertexTrackdR(double eta, double phi){
    double mindR = 10;
    for( unsigned int j=0; j < Track_eta->size(); j++){
        if(Track_pT->at(j) < 5. || fabs(Track_eta->at(j)) >= 3.2) continue; //only consider tracks with pT > 5 GeV && |eta| < 3.2
        double deltaR = DelR(Track_phi->at(j), phi, Track_eta->at(j), eta);
        if( deltaR < mindR) mindR = deltaR;
    }
    vertexTrackdR = mindR;       

}
bool CommonVariables::eventIs2j150(){
    int nJets=0;
    for( unsigned int j=0; j < Jet_eta->size(); j++){
        if(Jet_pT->at(j) < 150. || fabs(Jet_eta->at(j)) >= 3.2 || !Jet_isGoodStand->at(j)) continue; 
        //only consider jets with pT > 150 GeV && |eta| < 3.2
        nJets++;
    }
    if(nJets < 2 || isBadEventJetHigh == true) return false;
    return true;
}
bool CommonVariables::eventIs2j150(double eta, double phi){
    int nJets=0;
    for( unsigned int j=0; j < Jet_eta->size(); j++){
        if(Jet_pT->at(j) < 150. || fabs(Jet_eta->at(j)) >= 3.2 || !Jet_isGoodStand->at(j)) continue; 
        //only consider jets with pT > 150 GeV && |eta| < 3.2
        double deltaR2 = DelR2(Jet_phi->at(j), phi, Jet_eta->at(j),eta);    
        if(deltaR2 < 0.7*0.7) continue; //only jets that are > 0.7 away from the vertex!
        nJets++;
    }
    if(nJets < 2 || isBadEventJetHigh == true) return false;
    return true;
}

bool CommonVariables::eventIs2j250(){
    int nJets=0;
    for( unsigned int j=0; j < Jet_eta->size(); j++){
        if(Jet_pT->at(j) < 250. || fabs(Jet_eta->at(j)) >= 3.2 || !Jet_isGoodStand->at(j)) continue;
        //only consider jets with pT > 150 GeV && |eta| < 3.2
        nJets++;
    }
    if(nJets < 2 || isBadEventJetHigh == true) return false;
    return true;
}
bool CommonVariables::eventIs2j250(double eta, double phi){
    int nJets=0;
    for( unsigned int j=0; j < Jet_eta->size(); j++){
        if(Jet_pT->at(j) < 250. || fabs(Jet_eta->at(j)) >= 3.2 || !Jet_isGoodStand->at(j)) continue;
        //only consider jets with pT > 150 GeV && |eta| < 3.2
        double deltaR2 = DelR2(Jet_phi->at(j), phi, Jet_eta->at(j),eta);
        if(deltaR2 < 0.7*0.7) continue; //only jets that are > 0.7 away from the vertex!
        nJets++;
    }
    if(nJets < 2 || isBadEventJetHigh == true) return false;
    return true;
}

void CommonVariables::set2DPlotVars(double eta, double phi){
    double mindR = 10; std::vector<double> jetPts; double vxJetPt=0;
    int n1j20 = 0;
    int n1j30 = 0;
    int n1j50 = 0;
    int n1j150 = 0;
    int n1j50_1j20 = 0;
    int n2j20 = 0;
    int n1j50_1j150 = 0;
    int n2j150 = 0;
    int n2j50 = 0;
    int n2j100 = 0;
    int n1j100_1j200 = 0;
    int n2j200 = 0;
    int n1j100_1j250 = 0;
    int n2j250 = 0;
    for( unsigned int j=0; j < Jet_eta->size(); j++){
        if(Jet_pT->at(j) < 20. || fabs(Jet_eta->at(j)) >= 3.2 || !Jet_isGoodStand->at(j) ) continue; //only consider good jets with pT > 20 GeV && |eta| < 3.2

        double deltaR2 = DelR2(Jet_phi->at(j), phi, Jet_eta->at(j),eta);    

        if(deltaR2 > 0.7*0.7){
            jetPts.push_back(Jet_pT->at(j));
            if(Jet_pT->at(j) > 100.) nJetsOutsideVx100++;
            else if(Jet_pT->at(j) > 75.) nJetsOutsideVx75++;
            else if(Jet_pT->at(j) > 50.) nJetsOutsideVx50++;
            else nJetsOutsideVx20++;
            if(Jet_pT->at(j) > 100.)nJetsOutsideVx100i++;
            if(Jet_pT->at(j) > 75.)nJetsOutsideVx75i++;
            if(Jet_pT->at(j) > 50.)nJetsOutsideVx50i++;
            if(Jet_pT->at(j) > 20.)nJetsOutsideVx20i++;

            if(n1j50 > 0 && Jet_pT->at(j) > 20.) n1j50_1j20++;
            if(n1j20 > 0 && Jet_pT->at(j) > 50.) n1j50_1j20++;
            if(n2j150 > 0 && Jet_pT->at(j) > 50.) n1j50_1j150++;
            if(n1j50 > 0 && Jet_pT->at(j) > 150.) n1j50_1j150++;
            if(n2j100 > 0 && Jet_pT->at(j) > 200.) n1j100_1j200++;
            if(n2j200 > 0 && Jet_pT->at(j) > 100.) n1j100_1j200++;
            if(n2j100 > 0 && Jet_pT->at(j) > 250.) n1j100_1j250++;
            if(n2j250 > 0 && Jet_pT->at(j) > 100.) n1j100_1j250++;

            if(Jet_pT->at(j) > 30.) n1j30++;
            if(Jet_pT->at(j) > 50.) n1j50++;
            if(Jet_pT->at(j) > 20.) n2j20++;
            if(Jet_pT->at(j) > 50.) n2j50++;
            if(Jet_pT->at(j) > 150.) n2j150++;
            if(Jet_pT->at(j) > 100.) n2j100++;
            if(Jet_pT->at(j) > 200.) n2j200++;
            if(Jet_pT->at(j) > 250.) n2j250++;
        }
    }
    std::sort(jetPts.rbegin(), jetPts.rend());
    if(jetPts.size() > 2){
        LJetPt=jetPts.at(0);
        SLJetPt=jetPts.at(1);
        SSLJetPt=jetPts.at(2);
    } else if(jetPts.size() == 2){
        LJetPt= jetPts.at(0);
        SLJetPt= jetPts.at(1);
    } else if(jetPts.size() == 1){
        LJetPt = jetPts.at(0);
    }

    if(n1j30 > 0 && isBadEventJetLow == false) CR_1j30 = 1;
    if(n1j50 > 0 && isBadEventJetLow == false) CR_1j50 = 1;
    if(n1j50_1j20 > 0 && isBadEventJetLow == false) CR_1j50_1j20 = 1;
    if(n2j20 > 1 && isBadEventJetLow == false) CR_2j20 = 1;
    if(n2j50 > 1 && isBadEventJetLow == false) CR_2j50 = 1;
    if(n2j150 > 1 && isBadEventJetLow == false) CR_2j150_low = 1;
    if(n2j150 > 1 && isBadEventJetHigh == false) CR_2j150 = 1;
    if(n1j50_1j150 > 0 && isBadEventJetLow == false) CR_1j50_1j150 = 1;
    if(n1j100_1j200 > 0 && isBadEventJetHigh == false) CR_1j100_1j200 = 1;
    if(n2j200 > 1 && isBadEventJetHigh == false) CR_2j200 = 1;
    if(n1j100_1j250 > 0 && isBadEventJetHigh == false) CR_1j100_1j250 = 1;
    if(n2j250 > 1 && isBadEventJetHigh == false) CR_2j250 = 1;

    return;
}
void CommonVariables::calcMHT(){
    TVector2 HTMiss;
    HTMiss.Set(0.,0.);
    for( unsigned int j =0; j < Jet_eta->size(); j++){
        if(Jet_pT->at(j) < 20.) continue; //only consider jets with pT > 20 GeV
        TVector2 tmp; tmp.SetMagPhi(Jet_pT->at(j), Jet_phi->at(j));
        HTMiss += tmp;
    }
    eventMHT = HTMiss.Mod();
    eventMHT_phi = TVector2::Phi_mpi_pi(HTMiss.Phi());
    return;
}
void CommonVariables::recalculateEvtProperties(){
    double HT = 0; int nJets = 0; TVector2 HTMiss;
    HTMiss.Set(0.,0.);
    int nJets50 = 0;
    for( unsigned int j =0; j < Jet_eta->size(); j++){
        if(Jet_pT->at(j) < 20. || fabs(Jet_eta->at(j)) >= 3.2) continue; //only consider jets with pT > 20 GeV && |eta| < 3.2
        TVector2 tmp; tmp.SetMagPhi(Jet_pT->at(j), Jet_phi->at(j));
        HTMiss += tmp;
        HT += Jet_pT->at(j);
        nJets++;
        if(Jet_pT->at(j) > 50.) nJets50++;
    }
    eventNJets50 = nJets50;
    eventHT = HT;
    eventNJets = nJets;
    eventHTMiss = HTMiss.Mod();
    HTMiss = -1.*HTMiss;
    eventMET = HTMiss.Mod();
    eventMET_phi = TVector2::Phi_mpi_pi(HTMiss.Phi());
    eventMeff = HT + HTMiss.Mod();
}
void CommonVariables::recalculateEvtProperties(double eta, double phi){
    double HT = 0; int nJets = 0; TVector2 HTMiss; int nJets50=0;
    TVector2 HTMiss2; HTMiss2.Set(0.,0.);
    HTMiss.Set(0.,0.); int nMatch= 0; std::vector<double > deltaRs; std::vector<double> jetpts;
    double mindR = 10; double vxJetPt = 0;
    for( unsigned int j =0; j < Jet_eta->size(); j++){
        if(Jet_pT->at(j) < 20. || fabs(Jet_eta->at(j)) >= 3.2) continue; //only consider jets with pT > 20 GeV && |eta| < 3.2
        TVector2 tmp; tmp.SetMagPhi(Jet_pT->at(j), Jet_phi->at(j));

        double deltaR = DelR(Jet_phi->at(j), phi, Jet_eta->at(j),eta);    
        HTMiss2 -= tmp;
        if( TMath::Abs(eta)  < 1.0 && deltaR < 0.3 ){
            if(deltaR < mindR ){
                mindR = deltaR;
                vxJetPt = Jet_pT->at(j);
            }
            deltaRs.push_back(deltaR);
            jetpts.push_back(Jet_pT->at(j));
            nMatch++; 
            continue;
        }
        if( TMath::Abs(eta) >= 1.0 && deltaR < 0.4 ) {
            if(deltaR < mindR ){
                mindR = deltaR;
                vxJetPt = Jet_pT->at(j);
            }
            deltaRs.push_back(deltaR); 
            jetpts.push_back(Jet_pT->at(j));
            nMatch++; 
            continue;
        }
        HTMiss += tmp;
        HT += Jet_pT->at(j);
        nJets++;
        if(Jet_pT->at(j) > 50.) nJets50++;
    }

    if(nMatch > 1){
        std::cout << "this vertex at eta: " << eta << " matched: " << nMatch << " jets" << std::endl;
        for(int i=0; i<deltaRs.size(); i++){std::cout << "jet/vx dR " << i << " = " << deltaRs.at(i) << " with jet pT " << jetpts.at(i) <<  std::endl;}
    }
    eventNJets50 = nJets50;
    eventHT = HT;
    eventNJets = nJets;
    eventHTMiss = HTMiss.Mod();
    eventMET =  HTMiss2.Mod();
    eventMET_phi =  TVector2::Phi_mpi_pi(HTMiss2.Phi());
    eventMeff = HT + HTMiss.Mod();    
    vertexJetPt = vxJetPt;
    return;
}
void CommonVariables::clearAllVectors(bool useTracks = false, bool useJets = true, bool doPDFSystematics = false){

    eventNumber = 0;
    eventWeight = -1;
    pileupEventWeight = -1;
    prw_1up = -1;
    prw_1down = -1;
    eventMHT = 0;
    eventMHT_phi = 0;
    passCalRatio = false;
    passJ400 = false;
    passMuvtx = false;
    passMuvtx_noiso = false;
    hasGoodPV = false;
    isGoodLAr = false;
    isGoodTile = false;
    isGoodSCT = false;
    isCompleteEvent = false;
    isBadEventJetHigh=false;
    isBadEventJetLow=false;
    vertexJetPt=0;
    vertexTrackdR=-1;
    vertexJetdR=-1;
    vertexJetdR_pT=0;
    LJetPt=0;
    SLJetPt=0;
    SSLJetPt=0;
    nJetsOutsideVx20i=0;
    nJetsOutsideVx20=0;
    nJetsOutsideVx50i=0;
    nJetsOutsideVx50=0;
    nJetsOutsideVx75i=0;
    nJetsOutsideVx75=0;
    nJetsOutsideVx100i=0;
    nJetsOutsideVx100=0;
    CR_1j30 = 0;
    CR_1j50 = 0;
    CR_1j50_1j20 = 0;
    CR_2j20= 0;
    CR_2j50 = 0;
    CR_1j50_1j150 = 0;
    CR_2j150_low = 0;
    CR_2j150 = 0;
    CR_1j100_1j200 = 0;
    CR_2j200 = 0;
    CR_1j100_1j250 = 0;
    CR_2j250 = 0;

    if(useTracks){
        Track_pT->clear();
        Track_eta->clear();
        Track_phi->clear();
    }
    if(useJets){
        Jet_width->clear();
        Jet_passJVT->clear();
        Jet_jvt->clear();
        Jet_EMF->clear();
        Jet_pT->clear();
        Jet_ET->clear();
        Jet_E->clear();
        Jet_t->clear();
        Jet_eta->clear();
        Jet_phi->clear();
        Jet_logRatio->clear();
        //Jet_isGoodLLP->clear();
        Jet_isGoodStand->clear();
        Jet_indexLLP->clear();
        Jet_nAssocMSeg->clear();
        //Jet_nMSegCone->clear();
        Jet_nMSTracklets->clear();
        Jet_nMuonRoIs->clear();

    }
    if(doPDFSystematics) pdfWeights->clear();
}
void CommonVariables::clearJESVectors(){
    Jet_pT->clear();
    Jet_eta->clear();
    Jet_phi->clear();
}
