//
//  CommonUtils.h
//  plotVertexParameters
//
//  Created by Heather Russell on 11/12/15.
//
//

#ifndef plotVertexParameters_CommonUtils_h
#define plotVertexParameters_CommonUtils_h
#include "TMath.h"

namespace CommonUtils{
    
    std::vector<double> makeVector(double a, double b, double c, double d){
        std::vector<double> tmp;
        tmp.push_back(a);
        tmp.push_back(b);
        tmp.push_back(c);
        tmp.push_back(d);
        return tmp;
    }
    std::vector<double> makeVector(double a, double b, double c){
        std::vector<double> tmp;
        tmp.push_back(a);
        tmp.push_back(b);
        tmp.push_back(c);
        return tmp;
    }
    std::vector<double> makeVector(double a, double b, double c, double e){
        std::vector<double> tmp;
        tmp.push_back(a);
        tmp.push_back(b);
        tmp.push_back(c);
        tmp.push_back(d);
        tmp.push_back(e);
        return tmp;
    }
};

#endif
