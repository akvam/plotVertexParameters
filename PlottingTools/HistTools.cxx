//
//  HistTools.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#include "PlottingPackage/HistTools.h"
#include <iostream>
#include "PlottingPackage/PlottingUtils.h"

void HistTools::addHist(TString name, int xBin, double xMin, double xMax){
    TH1D *temp = new TH1D(name,name,xBin,xMin,xMax);
    temp->Sumw2();
    h_1D.push_back(temp);
}
void HistTools::addHist(TString name, int xBin, double xMin, double xMax, int yBin, double yMin, double yMax){
    TH2D *temp = new TH2D(name,name,xBin,xMin,xMax,yBin,yMin,yMax);
    temp->Sumw2();
    h_2D.push_back(temp);
}

void HistTools::addGAHists(TString name, TString exp){

    addHist(exp + "_eventHT_"+name,500,0,5000);
    addHist(exp + "_eventHTMiss_"+name,100,0,1000);
    addHist(exp + "_eventMeff_"+name,500,0,5000);
    addHist(exp + "_MSVxEta_"+name,60,-3,3);
    addHist(exp + "_MSVxPhi_"+name,64,-3.2,3.2);
    addHist(exp + "_MSVxIso_"+name, 200,0,10);
    addHist(exp + "_MSVxHits_"+name, 100,0,10000);
    /*addHist(exp + "_MSVxJetdR_"+name,64,-3.2,3.2);
    addHist(exp + "_NJets30_"+name,64,-3.2,3.2);
    if(name.Contains("noiso")){
		addHist(exp + "_MSVxJetPt_"+name,500,0,5000);
	}*/

}
void HistTools::addHist(TString name, std::vector<TString> suffixes, int nx, Double_t x[]){
    for(auto suffix : suffixes){
        TH1D *temp = new TH1D(name+suffix,name+suffix, nx, x);
        temp->Sumw2();
        h_1D.push_back(temp);
    }
}
void HistTools::fill(TString name, double value){

    bool isFilled = false;
    for(auto hist : h_1D){
        if(TString(hist->GetName()) == name){
            hist->Fill(value);
            isFilled = true;
            break;
        }
    }

    if( !isFilled ) std::cout << "DIDN'T FIND HISTOGRAM " << name << "!" << std::endl;

    return;
}
void HistTools::scale(double sf){
    for(auto hist : h_1D){
        hist->Scale(sf);
    }
}
double HistTools::getBinContent(TString name, int bin){

    bool foundHist = false;
    for(auto hist : h_1D){
        if(TString(hist->GetName()) == name){
            return (hist->GetBinContent(bin));
        }
    }

    std::cout << "DIDN'T FIND HISTOGRAM " << name << "!" << std::endl;
    return -1.;
}
void HistTools::setBinContent(TString name, int bin, double content){

    bool foundHist = false;
    for(auto hist : h_1D){
        if(TString(hist->GetName()) == name){
            hist->SetBinContent(bin,content);
            return;
        }
    }

    std::cout << "DIDN'T FIND HISTOGRAM " << name << "!" << std::endl;
    return ;
}

void HistTools::fill(TString name, double value, double weight){

    bool isFilled = false;
    for(auto hist : h_1D){
        if(TString(hist->GetName()) == name){
            hist->Fill(value,weight);
            isFilled = true;
            break;
        }
    }

    if( !isFilled ) std::cout << "DIDN'T FIND HISTOGRAM " << name << "!" << std::endl;

    return;
}
void HistTools::fill(TString name, double valueX, double valueY, double weight){

    bool isFilled = false;
    for(auto hist : h_2D){
        if(hist->GetName() == name){
            hist->Fill(valueX,valueY,weight);
            isFilled = true;
            break;
        }
    }
    if( !isFilled ) std::cout << "DIDN'T FIND 2D HISTOGRAM " << name << "!" << std::endl;

    return;
}
