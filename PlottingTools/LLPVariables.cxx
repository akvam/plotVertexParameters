//
//  LLPVariables.cpp
//  plotVertexParameters
//
//  Created by Heather Russell on 12/10/15.
//
//

#include "PlottingPackage/LLPVariables.h"
#include <iostream>
#include "TVector3.h"
void LLPVariables::setZeroVectors(){

    eta=0;
    phi=0;
    Lz=0;
    Lxy=0;
    Lxyz= new std::vector<double>;
    beta=0;
    gamma=0;
    pT=0;
//    px=0;
//    py=0;
    pz= new std::vector<double>;
    E=0;
    nMSeg = 0;
    bg = new std::vector<double>;
    theta = new std::vector<double>;
    timing = new std::vector<double>;

    leading_ctau=0;
    subleading_ctau=0;
    leading_beta=0;
    subleading_beta=0;
}

void LLPVariables::setBranchAddresses(TChain *chain){

    chain->SetBranchAddress("LLP_beta",&beta);
    chain->SetBranchAddress("LLP_gamma",&gamma);
    chain->SetBranchAddress("LLP_Lxy",&Lxy);
    chain->SetBranchAddress("LLP_Lz",&Lz);
    chain->SetBranchAddress("LLP_eta",&eta);
    chain->SetBranchAddress("LLP_phi",&phi);
    chain->SetBranchAddress("LLP_pT",&pT);
    chain->SetBranchAddress("LLP_E",&E);
    chain->SetBranchAddress("LLP_nMSegs_dR04", &nMSeg);
    chain->SetBranchAddress("LLP_timing",&timing);
    
}
void LLPVariables::setToyBranchAddresses(TChain *chain){
//used in doLimitExtrapolation    
    chain->SetBranchAddress("LLP_E",&E);
    chain->SetBranchAddress("LLP_pT",&pT);
    chain->SetBranchAddress("LLP_eta",&eta);
    chain->SetBranchAddress("LLP_phi",&phi);
    chain->SetBranchAddress("LLP_beta",&beta);
    chain->SetBranchAddress("LLP_gamma",&gamma);
    chain->SetBranchAddress("LLP_timing",&timing);

}
void LLPVariables::SetAllVariables(){

    leading_beta = beta->at(0);
    subleading_beta = beta->at(0);
    leading_ctau = timing->at(0) / gamma->at(0);
    subleading_ctau = timing->at(0) / gamma->at(0);
    
    for(unsigned int i=0; i< eta->size();i++){
        TVector3 tmp; tmp.SetPtEtaPhi(1.0,eta->at(i),phi->at(i));
        theta->push_back(tmp.Theta());
        bg->push_back(beta->at(i)*gamma->at(i));
        double pz_tmp = E->at(i) * ( exp(2.*eta->at(i)) - 1.0 )/ ( exp(2.*eta->at(i)) + 1.0 );
        pz->push_back(pz_tmp);
        if(beta->at(i) > leading_beta){
            leading_beta = beta->at(i);
        } else{
            subleading_beta = beta->at(i);
        }
        double thect = timing->at(i) / gamma->at(i);
        if(thect > leading_ctau){
            leading_ctau = thect;
        } else {
            subleading_ctau = thect;
        }
        
    }
   
   
}
double LLPVariables::newWeight(double new_tau, double gen_tau){
     double theWeight = gen_tau * gen_tau / (new_tau * new_tau);

    //std::cout << "gentau: " << gen_tau << " " << new_tau << " " << theWeight << std::endl;
     
     if(eta->size() != 2){
         std::cout << "ERROR! NOT TWO LONG-LIVED PARTICLES!!" << std::endl;
     }
     
     for(unsigned int i=0; i < eta->size(); i++){
         double llp_tau =  timing->at(i) / gamma->at(i);//c in mm/ns 
         theWeight *= exp(-1*llp_tau / new_tau) / exp(-1*llp_tau / gen_tau);
         //std::cout << llp_tau << " " << exp(-1*llp_tau / new_tau)  << " " << exp(-1*llp_tau / gen_tau) << " " << theWeight << std::endl;
     }
     return theWeight;
}
void LLPVariables::clearAllVectors(){

    eta->clear();
    phi->clear();
    Lz->clear();
    Lxy->clear();
    Lxyz->clear();
    pT->clear();
   // px->clear();
   // py->clear();
    pz->clear();
    bg->clear();
    theta->clear();
    beta->clear();
    gamma->clear();
    E->clear();
    nMSeg->clear();
    timing->clear();
    
}
void LLPVariables::clearAllToyVectors(){
    eta->clear();
    phi->clear();
    E->clear();
    pz->clear();
    pT->clear();
    bg->clear();
    theta->clear();
    beta->clear();
    gamma->clear();
    timing->clear();

}
void LLPVariables::set_pz(){
    for(unsigned int i=0; i< eta->size();i++){
        double pz_tmp = E->at(i) * ( exp(2.*eta->at(i)) - 1.0 )/ ( exp(2.*eta->at(i)) + 1.0 ); 
        pz->push_back(pz_tmp);
    }
}
void LLPVariables::set_Lxyz(){
    for(unsigned int i=0; i< eta->size();i++){
        double lxyztmp = sqrt(Lxy->at(i)*Lxy->at(i) + Lz->at(i)*Lz->at(i));
        Lxyz->push_back(lxyztmp);
    }
}
