//
//  ExtrapolationUtils.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 09/02/16.
//
//

#include "PlottingPackage/ExtrapolationUtils.h"
#include "TMath.h"
#include "TRandom3.h"
#include <iostream>


int ExtrapolationUtils::region(TVector3 llp){
    if(TMath::Abs(llp.Eta() < 2.7) && llp.Perp() < 33) return 0;
    else if(TMath::Abs(llp.Eta())< 2.5 && llp.Perp() < 275. && TMath::Abs(llp.z()) < 840. ) return 1;
    else if(TMath::Abs(llp.Eta()) < 2.5 && llp.Perp() > 2000 && llp.Perp() < 3500. ) return 2;
    else if(TMath::Abs(llp.Eta()) < 1.0 && llp.Perp() > 4000. && llp.Perp() < 7500. ) return 3;
    else if(TMath::Abs(llp.Eta()) > 1.0 && TMath::Abs(llp.Eta()) < 2.5 && TMath::Abs(llp.z()) > 6000. && TMath::Abs(llp.z()) < 14000. && llp.Perp() < 10000. ) return 3;
    else if(TMath::Abs(llp.Eta()) < 2.5 && llp.Perp() > 4000 ) return 4; //this one is MET that isn't covered by any of the 2 MS cases, which also would be MET I believe.
    else if(TMath::Abs(llp.Eta()) > 2.5 && TMath::Abs(llp.z()) > 6000 ) return 4; //this one is MET that isn't covered by any of the 2 MS cases, which also would be MET I believe.
    else return -1; //falls in some inbetween region, we're not sensitive to decays here
}
int ExtrapolationUtils::topology(TVector3 llp1, TVector3 llp2){
    int reg1 = region(llp1); int reg2 = region(llp2);
    if(reg1 == 0 && reg2 ==0 ) return 0; //prompt
    else if(reg1 == 0 && (reg2 == 1 || reg2 == 2 || reg2 == 3) ) return 1;  //1DV
    else if(reg2 == 0 && (reg1 == 1 || reg1 == 2 || reg1 == 3) ) return 1;	//1DV
    else if((reg2 == 1 || reg2 == 2 || reg2 == 3) && (reg1 == 1 || reg1 == 2 || reg1 == 3) ) return 2; //2DV
    else if((reg1 == 3 || reg1 == 4) && (reg2 == 3 || reg2 == 4) ) return 3; //invisibles
    else return -1;
}
int ExtrapolationUtils::topologyStealth(TVector3 llp1, TVector3 llp2){
    int reg1 = region(llp1); int reg2 = region(llp2);
    if(reg1 == 0 && reg2 ==0 ) return 0; //prompt
    else if( (reg1 == 0 && reg2 == 1) || (reg2 == 0 && reg2 == 1) ) return 10;  //1DV+prompt
    else if( reg1 == 1 && reg2 == 1 ) return 11;  //2ID Vertices (also counts for ID+Prompt)    
    else if((reg1 == 1 && reg2 == 2) || (reg1 == 2 && reg2 == 1) ) return 21;   //2DV ID/HCal combinations
    else if(reg2 == 2 && reg1 == 2) return 22;   //2DV HCal/HCal combinations
    else if((reg1 == 3 && reg2 == 0) || (reg1 == 0 && reg2 == 3) ) return 30;   //1DV MS prompt combinations 
    else if((reg1 == 3 && reg2 == 1) || (reg1 == 1 && reg2 == 3) ) return 31;   //1DV MS prompt combinations 
    else if((reg1 == 3 && reg2 == 2) || (reg1 == 2 && reg2 == 3) ) return 32;   //2DV HCal/MS combinations 
    else if(reg2 == 3 && reg1 == 3) return 33;  //2 MS vertices
    else if((reg1 == 3 && reg2 == 4) || (reg1 == 4 && reg2 == 3) ) return 34;   //1DV MS+MET combinations 
    else if((reg1 == 3 && reg2 == -1) || (reg1 == -1 && reg2 == 3) ) return 35;   //1DV MS+other gaps combinations 
    else if(reg1 == 3 || reg2 == 3) std::cout << "region combination not registered: " << reg1 << ", " << reg2 << std::endl;
    else return -1;
}
void ExtrapolationUtils::fillLifetimeExtrapolations(TRandom3 &rnd, double bg_1,TVector3 llp1,double bg_2,TVector3 llp2, TH1F *decayPositions[6], double weightVBF, double weightVH){
    for(int i = 0; i < 90; i++){
        double ctau_bin = TMath::Power(10., -6. + i * 0.1) + 0.0000005;
        double Lxyz1 = rnd.Exp(bg_1 *ctau_bin*1000.0); //1000. because ctau bins are in m, and we want the TVector3 in mm
        double Lxyz2 = rnd.Exp(bg_2 *ctau_bin*1000.0);
        llp1.SetMag(Lxyz1);
        llp2.SetMag(Lxyz2);
        int topo = topology(llp1,llp2);
        int r1 = region(llp1); int r2 = region(llp2);
        double ggFXS = 43.92;
        double VBFHXS = 3.748;
        double ZHXS = 0.8696;
        double WHXS = 1.380;
        double ttHXS = 0.5085;
        
        double weightVH2 = weightVH * (ZHXS+WHXS)/ggFXS; // scale by prod cross section difference for ZH vs. ggF
        double weightVBF2 = weightVBF * VBFHXS/ggFXS;
        if( (r1 == 3 && r2 == 4)|| (r2 == 3 && r1 ==4) ) decayPositions[5]->Fill(ctau_bin);
        if(topo != -1 && topo != 3) decayPositions[topo]->Fill(ctau_bin);
        else if(topo == 3){
         decayPositions[topo]->Fill(ctau_bin,weightVH2);
         decayPositions[topo+1]->Fill(ctau_bin,weightVBF2);
        }
    }
    return;
}
void ExtrapolationUtils::fillLifetimeExtrapolations(TRandom3 &rnd, double bg_1,TVector3 llp1,double bg_2,TVector3 llp2, TH1F *decayPositions[4]){
    for(int i = 0; i < 90; i++){
        double ctau_bin = TMath::Power(10., -6. + i * 0.1) + 0.0000005;
        double Lxyz1 = rnd.Exp(bg_1 *ctau_bin*1000.0); //1000. because ctau bins are in m, and we want the TVector3 in mm
        double Lxyz2 = rnd.Exp(bg_2 *ctau_bin*1000.0);
        llp1.SetMag(Lxyz1);
        llp2.SetMag(Lxyz2);
        int topo = topologyStealth(llp1,llp2);
        //int r1 = region(llp1); int r2 = region(llp2);
        
        if( topo == 0) decayPositions[0]->Fill(ctau_bin);
        if( topo == 10 || topo == 11 ) decayPositions[1]->Fill(ctau_bin);
        if( topo == 21 || topo == 22 || topo == 31 || topo == 32 || topo == 33) decayPositions[2]->Fill(ctau_bin);
        if( topo >= 30 ) decayPositions[3]->Fill(ctau_bin);


    }
    return;
}

int ExtrapolationUtils::checkDecLoc(TVector3& llp_loc){
    double eta_l = TMath::Abs(llp_loc.Eta());
    if(eta_l > 2.5 || llp_loc.Perp() > 10000. || TMath::Abs(llp_loc.z()) > 15000.) return 0;
    else if(eta_l < 0.8 && llp_loc.Perp() < 8000. && llp_loc.Perp() > 3000.) return 1;
    else if(eta_l > 1.3 && eta_l < 2.5 && llp_loc.Perp() < 10000. && TMath::Abs(llp_loc.z()) > 5000. && TMath::Abs(llp_loc.z()) < 15000.) return 2;
    //else if(eta_l < 2.5 && llp_loc.Perp() < 275. && TMath::Abs(llp_loc.z()) < 840. ) return 3;
    //else if(eta_l < 2.9 && llp_loc.Perp() < 3400.) return 5;
    else return 0;
}
bool ExtrapolationUtils::isBB_Event(std::vector<TVector3>& llp_locs){
    if(llp_locs.size() < 2) return false;
    int nBarrel = 0;
    for(unsigned int i_vp=0; i_vp < llp_locs.size(); i_vp++){
        if( checkDecLoc(llp_locs[i_vp]) == 1 ) nBarrel++;
    }
    //double dec_dR = llp_locs[0].DeltaR(llp_locs[1]);
    //if(nBarrel == 2 && dec_dR > 2.0) return true;
    if(nBarrel == 2) return true;
    else return false;
}
bool ExtrapolationUtils::isBE_Event(std::vector<TVector3>& llp_locs){
    if(llp_locs.size() < 2) return false;
    int nBarrel = 0;
    int nEndcap =0;
    for(unsigned int i_vp=0; i_vp < llp_locs.size(); i_vp++){
        if( checkDecLoc(llp_locs[i_vp]) == 1 ) nBarrel++;
        if( checkDecLoc(llp_locs[i_vp]) == 2 ) nEndcap++;
    }
    //double dec_dR = llp_locs[0].DeltaR(llp_locs[1]);
    //if(nBarrel == 1 && nEndcap == 1 && dec_dR > 2.0) return true;
    if(nBarrel == 1 && nEndcap == 1) return true;
    else return false;
}
bool ExtrapolationUtils::isEE_Event(std::vector<TVector3>& llp_locs){
    if(llp_locs.size() < 2) return false;
    int nEndcap = 0;
    for(unsigned int i_vp=0; i_vp < llp_locs.size(); i_vp++){
        if( checkDecLoc(llp_locs[i_vp]) == 2 ) nEndcap++;
    }
    //double dec_dR = llp_locs[0].DeltaR(llp_locs[1]);
    //if(nEndcap == 2 && dec_dR > 2.0) return true;
    if(nEndcap == 2) return true;
    else return false;
}
bool ExtrapolationUtils::isBID_Event(std::vector<TVector3>& llp_locs){
    if(llp_locs.size() < 2) return false;
    int nBarrel=0;
    int nID = 0;
    for(unsigned int i_vp=0; i_vp < llp_locs.size(); i_vp++){
        if( checkDecLoc(llp_locs[i_vp]) == 1 ) nBarrel++;
        if( checkDecLoc(llp_locs[i_vp]) == 3) nID++;
    }
    double dec_dR = llp_locs[0].DeltaR(llp_locs[1]);
    if(nBarrel == 1 && nID == 1 && dec_dR > 2.0) return true;
    else return false;
}
bool ExtrapolationUtils::isEID_Event(std::vector<TVector3>& llp_locs){
    if(llp_locs.size() < 2) return false;
    int nEndcap=0;
    int nID = 0;
    for(unsigned int i_vp=0; i_vp < llp_locs.size(); i_vp++){
        if( checkDecLoc(llp_locs[i_vp]) == 2 ) nEndcap++;
        if( checkDecLoc(llp_locs[i_vp]) == 3) nID++;
    }
    double dec_dR = llp_locs[0].DeltaR(llp_locs[1]);
    if(nEndcap == 1 && nID == 1 && dec_dR > 2.0) return true;
    else return false;
}
bool ExtrapolationUtils::is1B_Event(std::vector<TVector3>& llp_locs){
    if(llp_locs.size() < 2) return false;
    int nBarrel=0;
    int nEndcap=0;
    for(unsigned int i_vp=0; i_vp < llp_locs.size(); i_vp++){
        if( checkDecLoc(llp_locs[i_vp]) == 1 ) nBarrel++;
        else if( checkDecLoc(llp_locs[i_vp]) == 2 ) nEndcap++;
    }
    //double dec_dR = llp_locs[0].DeltaR(llp_locs[1]);
    if(nBarrel == 1 && nEndcap == 0) return true;
    else return false;
}
bool ExtrapolationUtils::is1E_Event(std::vector<TVector3>& llp_locs){
    if(llp_locs.size() < 2) return false;
    int nEndcap=0;
    int nBarrel = 0;
    for(unsigned int i_vp=0; i_vp < llp_locs.size(); i_vp++){
        if( checkDecLoc(llp_locs[i_vp]) == 2 ) nEndcap++;
        else if( checkDecLoc(llp_locs[i_vp]) == 1 ) nBarrel++;
    }
    //double dec_dR = llp_locs[0].DeltaR(llp_locs[1]);
    if(nEndcap == 1 && nBarrel == 0) return true;
    else return false;
}
bool ExtrapolationUtils::isBadTopology(std::vector<TVector3>& llp_locs){

    if(llp_locs.size() < 2) return true;

    for(unsigned int i_vp=0; i_vp < llp_locs.size(); i_vp++){
        if(checkDecLoc(llp_locs[i_vp])==0) return true;
    }
    return false;

}
std::pair<int, int> ExtrapolationUtils::getJetIndices(int region, TVector3& llp_loc, double momentum){
    //momentum = pt if region = 1, else pz if region = 2.
    
    std::pair<int, int > indices;
    
    Double_t ptbins[10] = {0,50,100,150,200,250,300,350,400,500};
    Double_t pzbins[10] = {0,100,200,300,400,500,600,800,1000,1200};
    Double_t Lxybins[10] = {0,0.5,1,1.5,2,2.5,3,3.5,4,5};
    Double_t Lzbins[11] = {0,0.5,1,1.5,2,2.5,3,3.5,4,6,8};
    

    if(region == 1){
        int lbin = 8; int pbin = 8;

        for(unsigned int i=0; i < 9; i++){
            if(llp_loc.Perp()*0.001 >= Lxybins[i] && llp_loc.Perp()*0.001 < Lxybins[i+1]){
                lbin =i;  // bin index starts at 1, but array starts at 0!
                break; //only find it once...
            }
        }
        for(unsigned int i=0; i < 9; i++){
            if(momentum*0.001 >= ptbins[i] && momentum*0.001 < ptbins[i+1]){
                pbin =i;
                break;
            }
        }
        indices = std::make_pair(lbin,pbin);        
    } else if(region==2){
        int lbin = 9; int pbin = 8;

        for(unsigned int i=0; i < 10; i++){
            if(fabs(llp_loc.Z())*0.001 >= Lzbins[i] && fabs(llp_loc.Z())*0.001 < Lzbins[i+1]){
                lbin =i;
                break; //only find it once...
            }
        }
        for(unsigned int i=0; i < 9; i++){
            if(fabs(momentum)*0.001 >= pzbins[i] && fabs(momentum)*0.001 < pzbins[i+1]){
                pbin =i;
                break;
            }
        }
        indices = std::make_pair(lbin,pbin);
    } else {
        indices = std::make_pair(-1,-1);
    }
    return indices;
}

