//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;



int makeCombinedPlots(){
    
	
gROOT->LoadMacro("AtlasUtils.C");
gROOT->LoadMacro("AtlasLabels.C");
gROOT->LoadMacro("AtlasStyle.C");

 TFile *_bkg[7]; int nBKG=7;
 Int_t colorBkg[7] = {kRed-7,kRed-4,kRed+1,kRed+2,kRed+3,kRed+4,kBlack};
 Int_t markerBkg[7] = {20,21,22,23,29,33,34};
 _bkg[0] = new TFile("bkgdJZ/JZ2W/outputGVC.root");
 _bkg[1] = new TFile("bkgdJZ/JZ3W/outputGVC.root");
 _bkg[2] = new TFile("bkgdJZ/JZ4W/outputGVC.root");
 _bkg[3] = new TFile("bkgdJZ/JZ5W/outputGVC.root");
 _bkg[4] = new TFile("bkgdJZ/JZ6W/outputGVC.root");
 _bkg[5] = new TFile("bkgdJZ/JZ7W/outputGVC.root");
 _bkg[6] = new TFile("bkgdJZ/JZ8W/outputGVC.root");
 
 TString bkgNames[7]; bkgNames[0] = "JZ2W";bkgNames[1] = "JZ3W";bkgNames[2] = "JZ4W";
 bkgNames[3] = "JZ5W";bkgNames[4] = "JZ6W"; bkgNames[5] = "JZ7W";bkgNames[6] = "JZ8W";
 
 
 TFile *_data[1]; int nDATA=1;
 Int_t colorData[1] = {kGreen+3};
 Int_t markerData[1] = {31};
 _data[0] = new TFile("dataDijets/run284484/outputGVC.root");
 
 TString dataNames[1]; dataNames[0] = "dijets, run284484";
 
 //_bkg[5] = new TFile("bkgdJZ/JZ7W/outputGVC.root");
 //_bkg[6] = new TFile("bkgdJZ/JZ8W/outputGVC.root");
 TFile *_sig[4]; int nSIG=4;
 _sig[0] = new TFile("signalMC/mH125mS8lt5/outputGVC.root");
 _sig[1] = new TFile("signalMC/mH200mS25lt5/outputGVC.root");
 _sig[2] = new TFile("signalMC/mH600mS150lt9/outputGVC.root");
 _sig[3] = new TFile("signalMC/mg250/outputGVC.root");

 TString sigNames[4]; sigNames[0] = "m_{H},m_{S}=[125,8] GeV"; sigNames[1] = "m_{H},m_{S}=[200,25] GeV";sigNames[2] = "m_{H},m_{S}=[600,150] GeV";sigNames[3] = "m_{#tilde{g}}= 250 GeV";
 Int_t colorSig[4] = {kBlue-3, kAzure+5, kTeal-6,kTeal+2};
 Int_t markerSig[4] = {24,25,26,27};
 
 Int_t colors[10] = {1,2,3,4,6,1,2,3,4,6};
 
 TString names[10];
 names[0] = "divide_MSVx_jetIso_vsEt_fPass_bar_by_MSVx_denom_Et_bar";
 names[1] = "divide_MSVx_jetIso_vsdR_fPass_bar_by_MSVx_denom_dR_bar";
 names[2] = "divide_MSVx_trkIso_vsPt_fPass_bar_by_MSVx_denom_Pt_bar";
 names[3] = "divide_MSVx_trkIso_vsdR_fPass_bar_by_MSVx_denom_dR_bar";
 names[4] = "divide_MSVx_trkSumIso_vsPt_fPass_bar_by_MSVx_denom_sumPt_bar";
 names[5] = "divide_MSVx_jetIso_vsEt_fPass_ec_by_MSVx_denom_Et_ec";
 names[6] = "divide_MSVx_jetIso_vsdR_fPass_ec_by_MSVx_denom_dR_ec";
 names[7] = "divide_MSVx_trkIso_vsPt_fPass_ec_by_MSVx_denom_Pt_ec";
 names[8] = "divide_MSVx_trkIso_vsdR_fPass_ec_by_MSVx_denom_dR_ec";
 names[9] = "divide_MSVx_trkSumIso_vsPt_fPass_ec_by_MSVx_denom_sumPt_ec";
 
 TString titles[10];
 titles[0] = "max jet E_{T}";
 titles[1] = "min jet #Delta R";
 titles[2] = "max track p_{T}";
 titles[3] = "min track #Delta R";
 titles[4] = "max #Sigma track p_{T} in #Delta R = 0.2";
 titles[5] = "max jet E_{T}";
 titles[6] = "min jet #Delta R";
 titles[7] = "max track p_{T}";
 titles[8] = "min track #Delta R";
 titles[9] = "max #Sigma track p_{T} in #Delta R = 0.2";

 SetAtlasStyle();
 gStyle->SetPalette(1);

 bool doSoverB = false;
 
 TCanvas* c = new TCanvas("c_1","c_1",800,600);
 //c->SetLogy();
 for(unsigned int i=0;i<10; i++){
	 TGraphAsymmErrors *g_bkg[7]; TGraphAsymmErrors *g_sig[4]; TGraphAsymmErrors *g_data[1];
	 for(int j=0; j<nBKG; j++){
		 g_bkg[j] = (TGraphAsymmErrors*) _bkg[j]->Get(names[i]);
		 g_bkg[j]->SetMarkerColor(colorBkg[j]); g_bkg[j]->SetLineColor(colorBkg[j]); g_bkg[j]->SetMarkerStyle(markerBkg[j]);
	 }
	 for(int j=0; j<nDATA; j++){
		 g_data[j] = (TGraphAsymmErrors*) _data[j]->Get(names[i]);
		 g_data[j]->SetMarkerColor(colorData[j]); g_data[j]->SetLineColor(colorData[j]); g_data[j]->SetMarkerStyle(markerData[j]);
	 }
	 TGraph *g_new[4];
	 for(int j=0; j<nSIG; j++){
		 g_sig[j] = (TGraphAsymmErrors*) _sig[j]->Get(names[i]);

		 g_sig[j]->SetMarkerColor(colorSig[j]); g_sig[j]->SetLineColor(colorSig[j]); g_sig[j]->SetMarkerStyle(markerSig[j]);

		 Double_t divVal[g_sig[j]->GetN()];
		 double maxDivVal = 0;
		 for(int k=0;k<g_sig[j]->GetN();k++){
			 if(g_bkg[2]->GetY()[k] != 0) {
				 divVal[k] = g_sig[j]->GetY()[k] / g_bkg[2]->GetY()[k];
                 //std::cout << "bin " << k << ", " << g_sig[j]->GetY()[k] << "/" << g_bkg[2]->GetY()[k] << "=" << divVal[k] << std::endl;
				 if(divVal[k] > maxDivVal) maxDivVal = divVal[k];
			 }
			 else divVal[k] = 0;
            // std::cout << "bin " << k << ", " << g_sig[j]->GetY()[k] << "/" << g_bkg[2]->GetY()[k] << "=" << divVal[k] << std::endl;
		 }
		 g_new[j] = new TGraph(g_bkg[2]->GetN(),g_bkg[2]->GetX(),divVal);

		 g_sig[j]->GetYaxis()->SetRangeUser(0.0,1.3);
		 if(doSoverB){
			 g_new[j]->GetYaxis()->SetRangeUser(0.0,maxDivVal*1.5);
		 	 g_new[j]->GetXaxis()->SetTitle(g_sig[j]->GetXaxis()->GetTitle());
		 	 g_new[j]->GetYaxis()->SetTitle("Signal / Background");
		 	 g_new[j]->SetMarkerColor(colorSig[j]); g_new[j]->SetLineColor(colorSig[j]); g_new[j]->SetMarkerStyle(markerSig[j]);
		 }
		 if(!doSoverB){
			 if(i==1 || i == 3 || i == 6 || i == 8)g_sig[j]->GetXaxis()->SetRangeUser(0,1);
			 else if(i==0 || i == 5)g_sig[j]->GetXaxis()->SetRangeUser(20,200);
			 else if(i==2 || i == 7)g_sig[j]->GetXaxis()->SetRangeUser(0,20);
			 else if(i==4 || i == 9)g_sig[j]->GetXaxis()->SetRangeUser(0,30);
			 if(j==0) 	 g_sig[j]->Draw("AP");
			 else g_sig[j]->Draw("P SAME");
		 } else if(doSoverB){
			 if(i==1 || i == 3 || i == 6 || i == 8)g_new[j]->GetXaxis()->SetRangeUser(0,1);
		 	 else if(i==0 || i == 5)g_new[j]->GetXaxis()->SetRangeUser(20,200);
		 	 else if(i==2 || i == 7){
		 		 if(i==2)g_new[j]->GetYaxis()->SetRangeUser(0,4000);
		 		 if(i==0) g_new[j]->GetYaxis()->SetRangeUser(0,9000);
		 		 g_new[j]->GetXaxis()->SetRangeUser(2,20);
		 	}
		 	else if(i==4 || i == 9)g_new[j]->GetXaxis()->SetRangeUser(2,30);
		 	if(j==0) 	 g_new[j]->Draw("AP");
		 	else g_new[j]->Draw("P SAME");
		 }
	 }
	 
	 if(!doSoverB){
	   for(int j=0;j<nBKG;j++){g_bkg[j]->Draw("P SAME");}
	   TLegend *leg = new TLegend(0.63,0.835,0.95,0.935);
	  	 leg->SetFillColor(0);
	  	 leg->SetBorderSize(0);
	  	 leg->SetTextSize(0.03);
	  	 leg-> SetNColumns(3);
	  	 for(int j=0;j<nBKG;j++) leg->AddEntry(g_bkg[j],bkgNames[j],"lp");
	  	 leg->Draw();

	 //for(int j=0;j<nDATA;j++){g_data[j]->Draw("P SAME");}
	 }
	 
	 
	 TLatex latex;
	 latex.SetNDC();
	 latex.SetTextColor(kBlack);
	 latex.SetTextSize(0.04);
	 latex.SetTextAlign(13);  //align at top
	 latex.DrawLatex(.18,.91,"#font[72]{ATLAS}  Internal");
	 //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");
	 
	/*
	 TLegend *legD = new TLegend(0.73,0.835,0.93,0.86);
	 legD->SetFillColor(0);
	 legD->SetBorderSize(0);
	 legD->SetTextSize(0.03);
	 legD->AddEntry(g_data[0],dataNames[0],"lp");
	 legD->Draw();
	 */
	 TLegend *legS = new TLegend(0.38,0.8,0.566,0.935);
	 legS->SetFillColor(0);
	 legS->SetBorderSize(0);
	 legS->SetTextSize(0.03);
	 for(int j=0;j<nSIG;j++){
		 if(doSoverB){
		     legS->AddEntry(g_new[j],sigNames[j],"lp");
		 } else{
			 legS->AddEntry(g_sig[j],sigNames[j],"lp");			 
		 }
	 }
	 legS->Draw();

	 gPad->RedrawAxis();
	 c->Print("combinedPlots/"+names[i]+".pdf");

 }
 
 
 /*
 TLegend *legD = new TLegend(0.5,0.25,0.93,0.5);
 legD->SetFillColor(0);
 legD->SetBorderSize(0);
 legD->SetTextSize(0.03);
 c->SetTopMargin(0.1);
 for(unsigned int i=0;i<10; i++){
	 TGraphAsymmErrors *g_bkg = (TGraphAsymmErrors*) _bkg[2]->Get(names[i]);
	 TGraphAsymmErrors *g_sig1 = (TGraphAsymmErrors*) _sig[0]->Get(names[i]);
	 TGraphAsymmErrors *g_sig2 = (TGraphAsymmErrors*) _sig[1]->Get(names[i]);
	 TGraphAsymmErrors *g_sig3 = (TGraphAsymmErrors*) _sig[2]->Get(names[i]);

	 if(i==1 || i == 3 || i == 6 || i == 8)g_sig1->GetXaxis()->SetRangeUser(0,1);
	 else if(i==0 || i == 5)g_sig1->GetXaxis()->SetRangeUser(0,200);
	 else if(i==2 || i == 7)g_sig1->GetXaxis()->SetRangeUser(0,20);
	 else if(i==4 || i == 9)g_sig1->GetXaxis()->SetRangeUser(0,30);

	 TGraph *g_roc = new TGraph(g_bkg->GetN(),g_bkg->GetY(),g_sig3->GetY());
	 g_roc->SetLineColor(colors[i]);
	 g_roc->SetMarkerColor(colors[i]);
	 g_roc->SetFillColor(kWhite);
	 g_roc->SetMarkerStyle(20+i);
	 g_roc->SetLineWidth(2);
	 g_roc->SetTitle(titles[i]);
	 g_roc->GetXaxis()->SetTitle("Background acceptance");
	 g_roc->GetYaxis()->SetTitle("Signal acceptance");
	 if(i==0 || i == 5){
		 legD->Clear();
		 g_roc->Draw("ALP");
	 }
	 else g_roc->Draw("LP SAME");
	 
	 legD->AddEntry(g_roc,TString(titles[i]),"lp");

	 
	 if(i==4 || i == 9){
		 TLatex latex;
		 latex.SetNDC();
		 latex.SetTextColor(kBlack);
		 latex.SetTextSize(0.05);
		 latex.SetTextAlign(13);  //align at top
		 latex.DrawLatex(.16,.96,"#font[72]{ATLAS}  Internal");
		 TLatex latex2;
		 latex2.SetNDC();
		 latex2.SetTextColor(kBlack);
		 latex2.SetTextSize(0.04);
		 latex2.SetTextAlign(13);  //align at top
		 if(i==4)latex2.DrawLatex(.5,.96,sigNames[2]+", Barrel vertices");
		 else if (i==9)latex2.DrawLatex(.5,.96,sigNames[2]+", Endcap vertices"); 
		 legD->Draw();
		 
		 c->Print("combinedPlots/ROC15_"+names[i]+".pdf");
	 }

	 
 }*/

 return 314;
 
}