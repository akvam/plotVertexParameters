//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"
#include "TLine.h"
using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;

TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    fileName = (TString)fileName(24,fileName.Sizeof() - 47);
    std::cout << fileName << std::endl;
    TString title = "";

    if(fileName.Contains("Chi")){
        title = fileName;
        return title;
    } else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{H},m_{S}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{S}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}

void getFiles(TFile *_sig[30], int &nSIG);
void getFiles(TFile *_sig[30], int &nSIG){

    std::cout << " Stealth SUSY" << std::endl;
    nSIG = 6;
    _sig[0] = new TFile("../OutputPlots/signalMC/mg250/outputMSVxEff_v3.root");
    _sig[1] = new TFile("../OutputPlots/signalMC/mg500/outputMSVxEff_v3.root");
    _sig[2] = new TFile("../OutputPlots/signalMC/mg800/outputMSVxEff_v3.root");
    _sig[3] = new TFile("../OutputPlots/signalMC/mg1200/outputMSVxEff_v3.root");
    _sig[4] = new TFile("../OutputPlots/signalMC/mg1500/outputMSVxEff_v3.root");
    _sig[5] = new TFile("../OutputPlots/signalMC/mg2000/outputMSVxEff_v3.root");
}

int makeLLPJetTurnOnCurves(){

    bool forThesis = false;
    TString outputFolder = "../OutputPlots/notePlots/";

    //gROOT->LoadMacro("AtlasUtils.C");
    //gROOT->LoadMacro("AtlasLabels.C");
    //gROOT->LoadMacro("AtlasStyle.C");


    Int_t colorSig[16] = {kPink-3,kViolet-3,kBlue-3,kAzure+5,kTeal-6,kTeal+2,kAzure+5,kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6, kTeal+2,kBlue-3, kAzure+5, kTeal-6};
    Int_t markerSig[16] = {24,25,26,27,28,30,26,27,24,25,26,27,24,25,26,27};

    Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};

    TString names[12];
    names[0] = "LLPJetEff_pT_eff";
    names[1] = "LLPJetEff_Lxy_eff";
    names[2] = "LLPJetEff_pz_eff";
    names[3] = "LLPJetEff_Lz_eff";
/*    names[4] = "LLPJetEff_pT_eff";
    names[5] = "LLPJetEff_Lxy_eff";
    names[6] = "LLPJetEff_pz_eff";
    names[7] = "LLPJetEff_Lz_eff";
*/ //need to add endcap ones at some point...

    TString titles[12];
    titles[1] = "LLP L_{xy} [m]";
    titles[0] = "LLP p_{T} [GeV]";
    titles[3] = "LLP L_{z} [m]";
    titles[2] = "LLP p_{z} [GeV]";
int nSIG=6;
    TCanvas* c = new TCanvas("c_1","c_1",800,600);
    bool add_ctau = false;
        TFile *_sig[30];
        TString sigNames[30];

        getFiles(_sig, nSIG);
        for(int isig=0; isig<nSIG; isig++){ sigNames[isig] = convertFiletoLabel(_sig[isig]->GetName(), add_ctau);}

        //c->SetLogy();

        for(unsigned int i_plt=0;i_plt < 4; i_plt++){ //loop through GVC plots
            TMultiGraph *mg = new TMultiGraph();
            std::vector<TGraphAsymmErrors *> g_sig;
            TGraph *g_new[16];
            for(int j_sig=0; j_sig<6; j_sig++){
                g_sig.push_back( (TGraphAsymmErrors*) _sig[j_sig]->Get(names[i_plt]));
                g_sig.at(j_sig)->SetMarkerColor(colorSig[j_sig]); g_sig.at(j_sig)->SetLineColor(colorSig[j_sig]);
                g_sig.at(j_sig)->SetMarkerStyle(markerSig[j_sig]);
                mg->Add(g_sig.at(j_sig));

            }

            mg->Draw("AP");
            mg->GetXaxis()->SetTitle(titles[i_plt]);
            mg->GetYaxis()->SetTitle("Efficiency");
            mg->SetMaximum(1.2);

            if(i_plt==1){
                mg->GetXaxis()->SetLimits(0,4);
            } else if(i_plt==0){
                mg->GetXaxis()->SetLimits(0,1000);
            } else if(i_plt==2){
                mg->SetMaximum(1.2);
                mg->GetXaxis()->SetLimits(0,4000);
            } else if(i_plt==3){
                mg->GetXaxis()->SetLimits(0,7);
            }
            c->Update();

            TLatex latex2;
            latex2.SetNDC();
            latex2.SetTextColor(kBlack);
            latex2.SetTextSize(0.035);
            latex2.SetTextAlign(33);  //align at top

            double y_val_label = 0.91;
            if(i_plt < 2){
                if(i_plt==0) latex2.DrawLatex(.9,y_val_label,"#font[52]{Barrel Calorimeter}#font[42]{(|#eta| < 1.5)}");
                else latex2.DrawLatex(.9,y_val_label,"#font[52]{Barrel Calorimeter}#font[42]{(|#eta| < 1.5)}");

            }
            else {
                if(i_plt == 2)latex2.DrawLatex(.9,y_val_label,"#font[52]{Endcap Calorimeter}#font[42]{(1.5 < |#eta| < 3.2)}");
                else latex2.DrawLatex(.9,y_val_label,"#font[52]{Endcap Calorimeter}#font[42]{(1.5 < |#eta| < 3.2)}");
            }

            TLatex latex;
            latex.SetNDC();
            latex.SetTextColor(kBlack);
            latex.SetTextSize(0.04);
            latex.SetTextAlign(13);  //align at top
            if(!forThesis) latex.DrawLatex(.18,.91,"#font[72]{ATLAS}  Internal");

            TLegend *legS = new TLegend(0.18,0.69,0.5,0.87);
            if(i_plt == 1 || i_plt == 3){
                legS->SetX1(0.7);
                legS->SetX2(0.93);
            }
            else if(i_plt == 0 || i_plt == 2){
                legS->SetX1(0.67);
                legS->SetX2(0.9);
                legS->SetY1(0.2);
                legS->SetY2(0.38);
            }
            legS->SetFillStyle(0);
            legS->SetBorderSize(0);
            legS->SetTextSize(0.03);
            legS->SetNColumns(1);
            for(int j=0;j<6;j++) legS->AddEntry(g_sig[j],sigNames[j],"lp");
            legS->Draw();

            gPad->RedrawAxis();
            c->Print(outputFolder+"/"+names[i_plt]+"_v3.pdf");
            c->Clear();
            delete mg;	
        } 
    

    return 314;
}
