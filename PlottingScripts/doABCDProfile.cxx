void doABCDProfile(TString outDir = ""){

    TString expName = "e1";
 
    TCanvas *c1 = new TCanvas("c1","c1",800,600);
    gStyle->SetPalette(1);

    std::vector<TString> histNames = {"1j50_1j150_b","1j100_1j250_ec"};
    std::vector<TString> histNamesSR = {"2j150_low_b","2j250_ec"};

    std::vector<TString>  histLabels = {"#splitline{E_{T,1} > 150 GeV,}{150 GeV > E_{T,2} > 50 GeV}","#splitline{E_{T,1} > 250 GeV,}{250 GeV > E_{T,2} > 100 GeV}"};
    
    TLine *line= new TLine(); line->SetLineColor(kRed);
    TLatex p2;
    p2.SetNDC();
    p2.SetTextSize(0.06);
    p2.SetTextFont(42);
    p2.SetTextColor(kRed);

    for(int i_hist=0;i_hist < 2; i_hist++){
           TH2D *h2 = nullptr;
            h2 = (TH2D*)_file0->Get(expName+"_ClosestdR_vs_nHits_"+histNamesSR.at(i_hist));
            h2->SetDirectory(0);
            
            
            TString histname = expName+"_ClosestdR_vs_nHits_"+histNames.at(i_hist);
            
            TH2D *h = (TH2D*)_file0->Get(histname);
            h->SetDirectory(0);
            
            h->Add(h2,-1.);

            h->GetXaxis()->SetTitle("min(#Delta R(closest jet), #Delta R(closest track))");

            h->GetYaxis()->SetTitle("vertex(nMDT + nTrig hits)");
            
            
            h->GetXaxis()->SetRangeUser(0,5);
            h->GetYaxis()->SetRangeUser(0,7500);            

            TProfile *px = h->ProfileX();
            TProfile *py = h->ProfileY();

            TLatex latexr;
            latexr.SetNDC();
            latexr.SetTextColor(kBlack);
            latexr.SetTextSize(0.04);
            latexr.SetTextAlign(13);  //align at top
            TLatex latex2;
            latex2.SetNDC();
            latex2.SetTextColor(kBlack);
            latex2.SetTextSize(0.035);
            latex2.SetTextAlign(13);  //align at top

            px->Draw();
            px->GetYaxis()->SetTitle("<vertex(nMDT + nTrig hits)>");
            ATLASLabel(0.22,0.89,"Internal");
            if(i_hist==0) latexr.DrawLatex(.6,.87,"Barrel vertices");
            else latexr.DrawLatex(.6,.87,"Endcap vertices");
            latexr.DrawLatex(.6,.92,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}");            
            latex2.DrawLatex(.6,.82,histLabels.at(i_hist));

            c1->Print(outDir+"/"+expName+"_ClosestdR_vs_nHits_"+histNames.at(i_hist)+"_px.pdf");
            
            py->Draw();
            py->GetYaxis()->SetTitle("<min(#Delta R(closest jet), #Delta R(closest track))>");

            ATLASLabel(0.22,0.89,"Internal");
            if(i_hist==0) latexr.DrawLatex(.6,.87,"Barrel vertices");
            else latexr.DrawLatex(.6,.87,"Endcap vertices");
            latexr.DrawLatex(.6,.92,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}");            
            latex2.DrawLatex(.6,.82,histLabels.at(i_hist));

            c1->Print(outDir+"/"+expName+"_ClosestdR_vs_nHits_"+histNames.at(i_hist)+"_py.pdf");

            h = nullptr;
            h2 = nullptr;
        }
    }

