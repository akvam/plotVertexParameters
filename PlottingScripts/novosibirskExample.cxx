#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TPad.h"
#include "TMath.h"
#include <cmath>
#include "TCanvas.h"
#include "../PlottingTools/PlottingPackage/SampleDetails.h"
TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    std::cout << "file name : " << fileName << std::endl;
    TString title = "";
    if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}

double evaluate_novosibirsk( const double x, const double peak, const double width, const double tail )
{
    if (TMath::Abs(tail) < 1.e-7) {
        return TMath::Exp( -0.5 * TMath::Power( ( (x - peak) / width ), 2 ));
    }

    Double_t arg = 1.0 - ( x - peak ) * tail / width;

    if (arg < 1.e-9) {
        return 0.0;
    }
    Double_t log = TMath::Log(arg);

    const Double_t xi = 2.3548200450309494; // 2 Sqrt( Ln(4) )

    Double_t width_zero = ( 2.0 / xi ) * TMath::ASinH( tail * xi * 0.5 );
    Double_t width_zero2 = width_zero * width_zero;
    Double_t exponent = ( -0.5 / (width_zero2) * log * log ) - ( width_zero2 * 0.5 );

    return TMath::Exp(exponent) ;
}
double novosibirsk( double* x, double* par ){
    return par[0]*evaluate_novosibirsk( log10( x[0] ), par[1], par[2], par[3] );
}

void novosibirskExample(){
    TCanvas *c1 = new TCanvas("c1","c1",800,600);


    std::vector<double> percent_difference;

    double lumi =  32864. + 3212.96; //lumi from doLimitExtrapolation, because prodXSs are set to 1.0 in SampleDetails.h and scaling is LUMI*SampleDetails::mediatorXS/nEvents
    //can plot these ones as global number of expected events/BR[H->ss]

    int colors[6] = {kRed-4, kAzure+7, kTeal, kSpring-2, kOrange-3, kViolet+7};
    std::vector<TString> benchmark = {"mH125mS5lt","mH125mS8lt","mH125mS15lt","mH125mS25lt","mH125mS40lt","mH125mS55lt","mH200mS8lt", "mH200mS25lt","mH200mS50lt", "mH400mS50lt", "mH400mS100lt", "mH600mS50lt", "mH600mS150lt", "mH1000mS50lt", "mH1000mS150lt", "mH1000mS400lt"};

    std::vector<double> nEvents_5m = {390000, 393000, 398100, 385000, 399400, 381000, 370000, 386000, 391000, 393000, 392000, 387000, 392000, 386000, 385000, 392000};
    std::vector<double> nEvents_9m = {194000, 197000, 193000, 185000, 197000, 188000, 195000, 196000, 193000, 191000, 196000, 192000, 190000, 183000, 187000, 189000};

    std::vector<double> ctau5;
    std::vector<double> ctau9;
    std::vector<double> off5;
    std::vector<double> off9;
    std::vector<double> est9;

    for(unsigned int i=0; i < benchmark.size(); i++){
        SampleDetails::setGlobalVariables(benchmark.at(i)+"5");
        ctau5.push_back(SampleDetails::sim_ctau);
        off5.push_back(SampleDetails::nObs2Vx/nEvents_5m.at(i));
        SampleDetails::setGlobalVariables(benchmark.at(i)+"9");
        ctau9.push_back(SampleDetails::sim_ctau);
        off9.push_back(SampleDetails::nObs2Vx/nEvents_9m.at(i));
        std::cout << "i : " << i << std::endl;
    }

    for(unsigned int j=3; j < 4; j++){
        double scaling = 1.0;
        TFile* tfile[2];
        TH1D* thist[2];
        TH1D* thist_up[2];
        TH1D* thist_down[2];
        TF1* fn[2];
        TF1* fn_up[2];
        TF1* fn_down[2];
        TGraph* fn3[2];
        TGraphAsymmErrors* g_err[2];
        double maxVal = 0;
        tfile[0] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/outputExtrapolation_"+benchmark.at(j)+"5_dv17_v3.root");
        tfile[1] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/outputExtrapolation_"+benchmark.at(j)+"9_dv17_v3.root");

        //do 5m for novosibirsk example
        for(unsigned int i=0; i<1;i++){
            thist[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx");
            thist_up[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_maxTotal");
            thist_down[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_minTotal");

            double maxValFit = thist[i]->GetMaximum();

            fn[i] = new TF1("fn",novosibirsk,0.05,300,4);
            fn[i]->SetParameter(0,maxValFit);
            fn[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
            fn[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
            fn[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);

            maxValFit = thist_up[i]->GetMaximum();

            fn_up[i] = new TF1("fn_up",novosibirsk,0.05,300,4);
            fn_up[i]->SetParameter(0,maxValFit);
            fn_up[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
            fn_up[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
            fn_up[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);

            maxValFit = thist_down[i]->GetMaximum();
            fn_down[i] = new TF1("fn_down",novosibirsk,0.05,300,4);
            fn_down[i]->SetParameter(0,maxValFit);
            fn_down[i]->SetParameter(1,-0.3); //fn->SetParLimits(1,-1,2);
            fn_down[i]->SetParameter(2,0.4); //fn->SetParLimits(2,0,2);
            fn_down[i]->SetParameter(3,-0.2); //fn->SetParLimits(3,0,20.0);

            thist[i]->Fit( fn[i], "WRV" );
            thist_up[i]->Fit( fn_up[i], "WRV+" );
            thist_down[i]->Fit( fn_down[i], "WRV" );

            Double_t ctau[1000];
            Double_t eff[1000];
            Double_t effUp[1000];
            Double_t effDown[1000];
            int ntot = 0;            
            TGraph *g_res = new TGraph(thist[i]);

            for (Int_t k = 0; k < 1000; k++) {
                Double_t life, nev, nev_up, nev_down;
                g_res->GetPoint(k, life, nev);
                nev = fn[i]->Eval(life);
                nev_up = fn_up[i]->Eval(life) - nev;
                nev_down = nev - fn_down[i]->Eval(life);
                if (nev == 0) {
                    std::cout << "Continue because nev= " << nev << std::endl;
                    continue;
                }
                if(nev_down < 0) nev_down = 0;
                eff[ntot] = nev/scaling;
                ctau[ntot] = life;
                effUp[ntot] = nev_up/scaling; 
                effDown[ntot] = nev_down/scaling;
                ntot++;
                if((nev_up+nev)/scaling > maxVal) maxVal = (nev_up+nev)/scaling;
            }

            fn3[i] = new TGraph(ntot, ctau, eff);
            fn3[i]->SetLineColor(kGreen+2);
            fn3[i]->SetLineStyle(1);
            fn3[i]->SetLineWidth(3);

        }

        c1->SetLogx();
        fn3[0]->Draw("AL");
        thist[0]->GetYaxis()->SetTitleOffset(1.6);
        thist[0]->GetXaxis()->SetRangeUser(0.05,100);
        thist[0]->GetXaxis()->SetTitle("Scalar proper lifetime (c*#tau) [m]");
        thist[0]->GetYaxis()->SetRangeUser(0,maxVal*1.2);
        thist[0]->GetYaxis()->SetTitle("Global expected events");
        thist[0]->SetLineWidth(1);
        thist_up[0]->SetLineWidth(1);
        thist_down[0]->SetLineWidth(1);
        thist[0]->Draw("hist");
        thist_up[0]->Draw("hist same");
        thist_down[0]->Draw("hist same");

        fn3[0]->Draw("c same");
        c1->Update();

        fn_down[0]->SetLineColor(kBlue+1);
        fn_down[0]->SetLineStyle(7);
        fn_down[0]->SetLineWidth(3);
        fn_up[0]->SetLineColor(kRed+1);
        fn_up[0]->SetLineStyle(7);
        fn_up[0]->SetLineWidth(3);

        fn_down[0]->Draw("c same");
        fn_up[0]->Draw("c same");

        TLatex latex;
        latex.SetNDC();
        latex.SetTextColor(kBlack);
        latex.SetTextSize(0.045);
        latex.SetTextAlign(11);  //align at top
        latex.DrawLatex(.2,.82,convertFiletoLabel(benchmark.at(j)));
        latex.SetTextSize(0.035);
        latex.SetTextAlign(11);  //align at top

        TLegend *leg = new TLegend(0.62,0.74,0.9,0.9);
        leg->SetFillStyle(0);
        leg->SetBorderSize(0);
        leg->SetTextSize(0.035);
        leg->AddEntry(thist[0],"Extrapolation result","l");
        leg->AddEntry(fn_up[0],"+1 #sigma fit","l");
        leg->AddEntry(fn3[0],"Nominal fit","l");
        leg->AddEntry(fn_down[0],"-1 #sigma fit","l");
        leg->Draw();

        ATLASLabel(0.2,0.88,"Internal");
        c1->Print("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/NovosibirskExample_"+benchmark.at(j) + "_dv17_v3.pdf");
    }

    return;
}
