void normalizeRows(){
        if(htemp){
	std::cout << "making a plot! " << htemp->GetNbinsY() << ", " << htemp->GetNbinsX() << std::endl;
	for(int i=1;i<htemp->GetNbinsY()+1;i++){
		double integ= htemp->ProjectionX("x",i,i)->Integral();
		std::cout << integ << std::endl;
		if(integ == 0) continue;
		for(int j=1;j<htemp->GetNbinsX()+1;j++){
			int binnum = htemp->GetBin(j,i); 
            float newbinval = htemp->GetBinContent(binnum)/integ;
			htemp->SetBinContent(binnum, newbinval);
		}
	}
	htemp->Draw("COLZ");
        }
}
