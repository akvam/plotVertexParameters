void countEventsInFolder(){
	TChain *ch = new TChain("recoTree");
	ch->Add("*root*");
	uint32_t runNumber = 0;
	ch->SetBranchAddress("runNumber" ,&runNumber); 
	ch->GetEntry(0);
	std::cout << runNumber << ", " << ch->GetEntries() << std::endl;
	delete ch;
	return;
}
