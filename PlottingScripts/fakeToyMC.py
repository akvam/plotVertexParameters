import ROOT
import math

def get_weight(inputs, target_ctau, gen_ctau):
    weight = (gen_ctau*gen_ctau/target_ctau/target_ctau)
    weight *= math.exp(-1*(inputs[0] + inputs[1])*(1./target_ctau - 1./gen_ctau))
    return weight 

gen_ctau = 150.
target_ctau = 5.

gen_cts = [ [ROOT.gRandom.Exp(gen_ctau),ROOT.gRandom.Exp(gen_ctau)] for i in range(0,40000)]
target_cts = [ [ROOT.gRandom.Exp(target_ctau),ROOT.gRandom.Exp(target_ctau)] for i in range(0,40000)]
rw_cts = [ [cts, get_weight(cts,target_ctau, gen_ctau)] for cts in gen_cts]

plots = [ROOT.TH2D("hgen","Generated c#tau (" + str(gen_ctau)+" mm)",10,0,10,10,0,10), 
         ROOT.TH2D("htarget","Regenerated target c#tau ("+str(target_ctau)+" mm)",10,0,10,10,0,10),
         ROOT.TH2D("hrw","Reweighted target c#tau ("+str(target_ctau)+" mm)",10,0,10,10,0,10)]

for x in range(0,len(gen_cts)):
    plots[0].Fill(gen_cts[x][0],gen_cts[x][1])
    plots[1].Fill(target_cts[x][0],target_cts[x][1])
    plots[2].Fill(rw_cts[x][0][0],rw_cts[x][0][1], rw_cts[x][1])

c1 = ROOT.TCanvas("c1","c1",1500,500)
c1.Divide(3,1)

t = ROOT.TLatex()
t.SetNDC(); t.SetTextSize(0.04); t.SetTextFont(41)

for i in range(0,3):
  c1.cd(i+1)
  ROOT.gPad.SetRightMargin(0.110)
  ROOT.gPad.SetLeftMargin(0.15)
  ROOT.gPad.SetTicks(1)
  ROOT.gStyle.SetOptStat(0)
  plots[i].GetXaxis().SetTitle("c#tau_{1} [mm]");  plots[i].GetYaxis().SetTitle("c#tau_{10} [mm]")
  plots[i].Draw("COLZ")
  error = ROOT.Double()
  t.DrawLatex( 0.10, 0.85, "integral = " +str(plots[i].IntegralAndError(1,plots[i].GetNbinsX()+1,1,plots[i].GetNbinsY()+1,error))+" "+str(error) )  

#c1.Print("bad_reweighting_low.pdf")
