//
//  makeRPCScalingPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 05/10/16.
//
//

#include <stdio.h>
#include "TMath.h"
#include "TF1.h"
#include "AtlasStyle.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TFile.h"
#include "TLine.h"
#include "TH1.h"


Double_t effFunc(Double_t *x,Double_t *par);
Double_t effFunc(Double_t *x,Double_t *par)
{
    Double_t efficiency = 0.5*(1.0 - TMath::Erf((x[0] - par[0])/(sqrt(2)*par[1])));
    return efficiency;
}
Double_t ratioFunc(Double_t *x,Double_t *par);
Double_t ratioFunc(Double_t *x,Double_t *par)
{
    Double_t efficiencyUP = 0.5*(1.0 - TMath::Erf((x[0] - par[0])/(sqrt(2)*par[1])));
    Double_t efficiencyDown = 0.5*(1.0 - TMath::Erf((x[0] - par[2])/(sqrt(2)*par[3])));
    return efficiencyUP/efficiencyDown;
}
void makeTimingPlots(TCanvas *&c1, bool forThesis, TString outputDir);
void makeTimingPlots(TCanvas *&c1, bool forThesis, TString outputDir){
    
    TF1 *gausMC = new TF1("gaus","gaus",26,34);
    gausMC->SetLineColor(kRed+1);

    TF1 *gausD = new TF1("gaus","gaus",26,34);
    gausD->SetLineColor(kRed+1);

    //TFile *fD = TFile::Open("/home/hrussell/RPC_timing_2016/RpcTiming_Data_WithHipt_2015.root");
    //TFile *fMC = TFile::Open("/home/hrussell/RPC_timing_2016/RpcTiming_singleMuon_MC_2015_WithHipt.root");
    TFile *fD = TFile::Open("/Users/hrussell/Work/RPCTimingStudy/RpcTiming_Data_WithHipt_2015.root");
    TFile *fMC = TFile::Open("/Users/hrussell/Work/RPCTimingStudy/RpcTiming_singleMuon_MC_2015_WithHipt.root");

    TH1D *hD = (TH1D*)fD->Get("Tabs_all");
    TH1D *hMC = (TH1D*)fMC->Get("Tabs_all");
    
    hD->GetYaxis()->SetTitle("Fraction of muons");
    hD->GetXaxis()->SetTitle("RPC hit time [ticks = 3.125 ns]");
    hD->SetLineColor(kBlack);
    hD->SetFillColor(kOrange-4);
    
    hMC->GetYaxis()->SetTitle("Fraction of muons");
    hMC->GetXaxis()->SetTitle("RPC hit time [ticks = 3.125 ns]");
    hMC->SetLineColor(kBlack);
    hMC->SetFillColor(kOrange-4);
    
    TLine *lWindowS = new TLine(26,0,26,0.6);
    lWindowS->SetLineColor(kBlue+1);
    lWindowS->SetLineStyle(7);
    lWindowS->SetLineWidth(2);
    TLine *lWindowE = new TLine(34,0,34,0.6);
    lWindowE->SetLineColor(kBlue+1);
    lWindowE->SetLineStyle(7);
    lWindowE->SetLineWidth(2);
    
    hD->Draw();
    hD->Fit(gausD,"R");
    printf("Data:\tmu = %f +/- %f,\tsigma = %f +/- %f\n", gausD->GetParameter(1)-0.5, gausD->GetParError(1), gausD->GetParameter(2), gausD->GetParError(2));
    printf("Data:\tmu = %f +/- %f ns,\tsigma = %f +/- %f ns\n", (gausD->GetParameter(1)-0.5)*3.125, gausD->GetParError(1)*3.125, gausD->GetParameter(2)*3.125, gausD->GetParError(2)*3.125);
    printf("Data:\tDelta = %f +/- %f ns", (34 - gausD->GetParameter(1)-0.5)*3.125, gausD->GetParError(1)*3.125);

    hD->Scale(1./hD->Integral());
    hD->Fit(gausD,"RQ");
    hD->SetMaximum(0.6);

    hD->Draw();

    /*TLatex fitparams;
    fitparams.SetTextColor(kBlack);
    fitparams.SetTextSize(0.03);
    fitparams.SetTextAlign(13);
    fitparams.DrawLatex(0.7,0.85,"#mu_{data} = ");*/
    
    TLatex lab;
    lab.SetTextColor(kBlue+1);
    lab.SetTextSize(0.03);
    lab.SetTextAlign(13);
    lab.DrawLatex(35,0.58,"25 ns readout window");
    
    TLatex fitparams;
    fitparams.SetTextColor(kBlack);
    fitparams.SetTextSize(0.035);
    fitparams.SetTextAlign(13);
    fitparams.DrawLatex(40,0.4,"#splitline{#mu = (94.079 #pm 0.001) ns}{#sigma = (2.9548 #pm 0.0004) ns}");
    lWindowS->Draw();
    lWindowE->Draw();
    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.045);
    latex.SetTextAlign(13);  //align at top
    if(!forThesis){
        latex.DrawLatex(.2,.9,"#font[72]{ATLAS}  Internal");
    }
    
    c1->Print(outputDir+"/RPCTiming_data.pdf");
    
    hMC->Draw();
    hMC->Fit(gausMC,"R");
    printf("MC:\tmu = %f +/- %f,\tsigma = %f +/- %f\n", gausMC->GetParameter(1)-0.5, gausMC->GetParError(1), gausMC->GetParameter(2), gausMC->GetParError(2));
    printf("MC:\tmu = %f +/- %f ns,\tsigma = %f +/- %f ns\n", (gausMC->GetParameter(1)-0.5)*3.125, gausMC->GetParError(1)*3.125, gausMC->GetParameter(2)*3.125, gausMC->GetParError(2)*3.125);
    printf("MC:\tDelta = %f +/- %f ns", (34 - gausMC->GetParameter(1)-0.5)*3.125, gausMC->GetParError(1)*3.125);
    
    hMC->Scale(1./hMC->Integral());
    hMC->Fit(gausMC,"RQ");
    hMC->SetMaximum(0.6);
    hMC->Draw();
    if(!forThesis){
        latex.DrawLatex(.2,.9,"#font[72]{ATLAS}  Internal");
    }
    lWindowS->Draw();
    lWindowE->Draw();
    lab.DrawLatex(35,0.58,"25 ns readout window");
    fitparams.DrawLatex(40,0.4,"#splitline{#mu = (92.998 #pm 0.004) ns}{#sigma = (2.084 #pm 0.002) ns}");
    c1->Print(outputDir+"/RPCTiming_MC.pdf");
    
    
}
void makeRPCScalingPlots(bool forThesis, TString outputDir){

    TCanvas *c1 = new TCanvas("c1","c1",750,600);
    
    makeTimingPlots(c1, forThesis, outputDir);
    
    //double deltaMC = 9.97; double sigmaMC = 2.129;
    //double deltaD = 8.909; double sigmaD = 3.070;
    double deltaMC = 13.262; double sigmaMC = 2.084;
    double deltaD = 12.171; double sigmaD = 2.9548;
    
    Double_t params[4] = {deltaD, sigmaD, deltaMC,sigmaMC};
    Double_t xVal[1] = {10};
    std::cout << "ratio value at dT = 10, dT = 20, dT = 25: ";
    std::cout << ratioFunc(xVal,params) << ", ";
    xVal[0] = 20;
    std::cout << ratioFunc(xVal,params) << ", ";
    xVal[0] = 25;
    std::cout << ratioFunc(xVal,params) << ", " << std::endl;
    
    TLegend *leg = new TLegend(0.25,0.2,0.5,0.35);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.03);
    
    TF1 *func_mc = new TF1("MC",effFunc,0,20,2);
    func_mc->SetParameters(deltaMC, sigmaMC);
    func_mc->SetMaximum(1.1); func_mc->SetMinimum(0);
    func_mc->GetXaxis()->SetRangeUser(0,20);
    func_mc->SetLineColor(kBlue+1);
    
    func_mc->GetXaxis()->SetTitle("#Delta t [ns]");
    func_mc->GetYaxis()->SetTitle("Efficiency");
    leg->AddEntry(func_mc,"MC simulation","l");

    
    TF1 *func_d = new TF1("data",effFunc,0,20,2);
    func_d->SetParameters(deltaD, sigmaD);
    func_d->SetLineColor(kBlack);
    leg->AddEntry(func_d,"Data","l");

    TF1 *ratio = new TF1("ratio", ratioFunc, 0, 20, 4);
    ratio->SetParameters(deltaD, sigmaD, deltaMC, sigmaMC);
    ratio->SetLineColor(kRed+1);
    //leg->AddEntry(ratio,"Data/MC","l");

    
    func_mc->Draw();
    func_d->Draw("SAME");
    //ratio->Draw("SAME");
    
    leg->Draw();

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.045);
    latex.SetTextAlign(13);  //align at top
    if(!forThesis){
        latex.DrawLatex(.2,.4,"#font[72]{ATLAS}  Internal");
    }
    
    c1->Print(outputDir+"/RPCScaling_2015_note.pdf");
    c1->Print(outputDir+"/RPCScaling_2015_note.root");
    
}