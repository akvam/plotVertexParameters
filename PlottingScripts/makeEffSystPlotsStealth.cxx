void makeEffSystPlotsStealth(){
    SetAtlasStyle();

    std::vector<TString> samples = {"mg250", "mg500", "mg800" ,"mg1200", "mg1500", "mg2000"};
    std::vector<TString> benchmark;
    std::vector<string> massLine;
    TString model = "stealth";

    benchmark = {"mg250", "mg500", "mg800", "mg1200", "mg1500", "mg2000"};
    massLine = {"250","500","800","1200","1500","2000"};


    ofstream file;
    file.open("SystematicsTables_"+model+"_v3.tex",ios::out);

    std::vector<std::pair<double, double> > msvx_b;
    std::vector<std::pair<double, double> > msvx_e;
    std::vector<std::pair<double, double> > mstrig_b;
    std::vector<std::pair<double, double> > mstrig_e;
    std::vector<std::pair<double, double> > llpjet_b_b;
    std::vector<std::pair<double, double> > llpjet_b_e;
    std::vector<std::pair<double, double> > llpjet_e_b;
    std::vector<std::pair<double, double> > llpjet_e_e;
    std::vector<std::pair<double, double> > trigsf_b;
    std::vector<std::pair<double, double> > trigsf_e;
    std::vector<std::pair<double, double> > ttmp;

    std::pair<double, double> tmp;

    for(int i=0; i<6; i++){
        msvx_b.push_back(plotPURatios_test(samples.at(i),"MSVx_Barrel_Lxy",true));
        msvx_e.push_back(plotPURatios_test(samples.at(i),"MSVx_Endcap_Lz",true));
        mstrig_b.push_back(plotPURatios_test(samples.at(i),"MSTrig_1B_Lxy",true));
        mstrig_e.push_back(plotPURatios_test(samples.at(i),"MSTrig_1E_Lz",true));
        llpjet_b_b.push_back(plotPURatios_test(samples.at(i),"LLPJetEffB_Lxy",true));
        llpjet_b_e.push_back(plotPURatios_test(samples.at(i),"LLPJetEffB_Lz",true));
        llpjet_e_b.push_back(plotPURatios_test(samples.at(i),"LLPJetEffE_Lxy",true));
        llpjet_e_e.push_back(plotPURatios_test(samples.at(i),"LLPJetEffE_Lz",true));
    }

    for(unsigned int i=0; i <msvx_b.size(); i++){
        std::cout << msvx_b.at(i).first << " " << msvx_e.at(i).first << " " << mstrig_b.at(i).first << " " << mstrig_e.at(i).first;
        std::cout << " " << llpjet_b_b.at(i).first << " " << llpjet_b_e.at(i).first << " " << llpjet_e_b.at(i).first << " " << llpjet_e_e.at(i).first << std::endl;
        std::cout << msvx_b.at(i).second << " " << msvx_e.at(i).second << " " << mstrig_b.at(i).second << " " << mstrig_e.at(i).second;
        std::cout << " " << llpjet_b_b.at(i).second << " " << llpjet_b_e.at(i).second << " " << llpjet_e_b.at(i).second << " " << llpjet_e_e.at(i).second << std::endl;
    }

    for(int i=0; i<6; i++){

        msvx_b.push_back(plotPDFRatios_test_oneBin(samples.at(i),"MSVx_Barrel_Lxy"));
        msvx_e.push_back(plotPDFRatios_test_oneBin(samples.at(i),"MSVx_Endcap_Lz"));
        mstrig_b.push_back(plotPDFRatios_test_oneBin(samples.at(i),"MSTrig_1B_Lxy"));
        mstrig_e.push_back(plotPDFRatios_test_oneBin(samples.at(i),"MSTrig_1E_Lz"));
        llpjet_b_b.push_back(plotPDFRatios_test_oneBin(samples.at(i),"LLPJetEffB_Lxy"));
        llpjet_b_e.push_back(plotPDFRatios_test_oneBin(samples.at(i),"LLPJetEffB_Lz"));
        llpjet_e_b.push_back(plotPDFRatios_test_oneBin(samples.at(i),"LLPJetEffE_Lxy"));
        llpjet_e_e.push_back(plotPDFRatios_test_oneBin(samples.at(i),"LLPJetEffE_Lz"));

    }



    /*
tmp = plotPURatios_test("mg250","MSVx_Barrel_Lxy",false);
tmp = plotPURatios_test("mg250","MSVx_Endcap_Lz",false);
tmp = plotPURatios_test("mg250","MSTrig_1B_Lxy",false);
tmp = plotPURatios_test("mg250","MSTrig_1E_Lz",false);

tmp = plotPURatios_test("mg500","MSVx_Barrel_Lxy",false);
tmp = plotPURatios_test("mg500","MSVx_Endcap_Lz",false);
tmp = plotPURatios_test("mg500","MSTrig_1B_Lxy",false);
tmp = plotPURatios_test("mg500","MSTrig_1E_Lz",false);

tmp = plotPURatios_test("mg800","MSVx_Barrel_Lxy",false);
tmp = plotPURatios_test("mg800","MSVx_Endcap_Lz",false);
tmp = plotPURatios_test("mg800","MSTrig_1B_Lxy",false);
tmp = plotPURatios_test("mg800","MSTrig_1E_Lz",false);

tmp = plotPURatios_test("mg1200","MSVx_Barrel_Lxy",false);
tmp = plotPURatios_test("mg1200","MSVx_Endcap_Lz",false);
tmp = plotPURatios_test("mg1200","MSTrig_1B_Lxy",false);
tmp = plotPURatios_test("mg1200","MSTrig_1E_Lz",false);

tmp = plotPURatios_test("mg1500","MSVx_Barrel_Lxy",false);
tmp = plotPURatios_test("mg1500","MSVx_Endcap_Lz",false);
tmp = plotPURatios_test("mg1500","MSTrig_1B_Lxy",false);
tmp = plotPURatios_test("mg1500","MSTrig_1E_Lz",false);

tmp = plotPURatios_test("mg2000","MSVx_Barrel_Lxy",false);
tmp = plotPURatios_test("mg2000","MSVx_Endcap_Lz",false);
tmp = plotPURatios_test("mg2000","MSTrig_1B_Lxy",false);
tmp = plotPURatios_test("mg2000","MSTrig_1E_Lz",false);


plotPDFRatios_test("mg250","MSVx_Barrel_Lxy");
plotPDFRatios_test("mg250","MSVx_Endcap_Lz");
plotPDFRatios_test("mg250","MSTrig_1B_Lxy");
plotPDFRatios_test("mg250","MSTrig_1E_Lz");
plotPDFRatios_test("mg500","MSVx_Barrel_Lxy");
plotPDFRatios_test("mg500","MSVx_Endcap_Lz");
plotPDFRatios_test("mg500","MSTrig_1B_Lxy");
plotPDFRatios_test("mg500","MSTrig_1E_Lz");
plotPDFRatios_test("mg800","MSVx_Barrel_Lxy");
plotPDFRatios_test("mg800","MSVx_Endcap_Lz");
plotPDFRatios_test("mg800","MSTrig_1B_Lxy");
plotPDFRatios_test("mg800","MSTrig_1E_Lz");
plotPDFRatios_test("mg1200","MSVx_Barrel_Lxy");
plotPDFRatios_test("mg1200","MSVx_Endcap_Lz");
plotPDFRatios_test("mg1200","MSTrig_1B_Lxy");
plotPDFRatios_test("mg1200","MSTrig_1E_Lz");
plotPDFRatios_test("mg1500","MSVx_Barrel_Lxy");
plotPDFRatios_test("mg1500","MSVx_Endcap_Lz");
plotPDFRatios_test("mg1500","MSTrig_1B_Lxy");
plotPDFRatios_test("mg1500","MSTrig_1E_Lz");
plotPDFRatios_test("mg2000","MSVx_Barrel_Lxy");
plotPDFRatios_test("mg2000","MSVx_Endcap_Lz");
plotPDFRatios_test("mg2000","MSTrig_1B_Lxy");
plotPDFRatios_test("mg2000","MSTrig_1E_Lz");*/

    for(unsigned int i=0; i <msvx_b.size(); i++){
        std::cout << msvx_b.at(i).first << " " << msvx_e.at(i).first << " " << mstrig_b.at(i).first << " " << mstrig_e.at(i).first;
        std::cout << " " << llpjet_b_b.at(i).first << " " << llpjet_b_e.at(i).first << " " << llpjet_e_b.at(i).first << " " << llpjet_e_e.at(i).first << std::endl;
        std::cout << msvx_b.at(i).second << " " << msvx_e.at(i).second << " " << mstrig_b.at(i).second << " " << mstrig_e.at(i).second;
        std::cout << " " << llpjet_b_b.at(i).second << " " << llpjet_b_e.at(i).second << " " << llpjet_e_b.at(i).second << " " << llpjet_e_e.at(i).second << std::endl;
    }


    for(int i=0; i<samples.size(); i++){

        findScaleFactors(samples.at(i),"MSTrig");
        ttmp = findScaleFactors_oneBin(samples.at(i),"MSTrig");
        trigsf_b.push_back(ttmp.at(0));
        trigsf_e.push_back(ttmp.at(1));
    }
    std::cout << "Trigger scale factor systematics " << std::endl;
    for(unsigned int i=0; i <trigsf_b.size(); i++){
        std::cout << trigsf_b.at(i).first  << " " << trigsf_e.at(i).first << std::endl;
        std::cout << trigsf_b.at(i).second << " "<< trigsf_e.at(i).second << std::endl;
    }

    std::cout << "JES Systematics, LLP jets" << std::endl;
    for(unsigned int i=0; i <samples.size(); i++){
        llpjet_b_b.push_back(plotJESRatios(massLine.at(i),"LLPJetEffB_Lxy",true));
        llpjet_b_e.push_back(plotJESRatios(massLine.at(i),"LLPJetEffB_Lz",true));
        llpjet_e_b.push_back(plotJESRatios(massLine.at(i),"LLPJetEffE_Lxy",true));
        llpjet_e_e.push_back(plotJESRatios(massLine.at(i),"LLPJetEffE_Lz",true));
    }
    for(unsigned int i=2*samples.size(); i <3*samples.size(); i++){
        std::cout << llpjet_b_b.at(i).first << " " << llpjet_b_e.at(i).first << " " << llpjet_e_b.at(i).first << " " << llpjet_e_e.at(i).first << std::endl;
        std::cout << llpjet_b_b.at(i).second << " " << llpjet_b_e.at(i).second << " " << llpjet_e_b.at(i).second << " " << llpjet_e_e.at(i).second << std::endl;

    }

    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    file << "\\begin{tabular}{c|cccc}" << endl;

    file << "\\toprule" << endl;
    file << "{$m_{\\tilde{g}}$} & Trigger reconstruction & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;

    double sc1 = 1000.; double sc2 = 10.;
    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt(pow(trigsf_b.at(i).first,2) + pow( mstrig_b.at(i).first,2) + pow(mstrig_b.at(i+samples.size()).first,2));
        double totdown = TMath::Sqrt(pow(trigsf_b.at(i).second,2) + pow( mstrig_b.at(i).second,2) + pow(mstrig_b.at(i+samples.size()).second,2));
        std::vector<double> uncertainties = {trigsf_b.at(i).first, trigsf_b.at(i).second, mstrig_b.at(i).first, mstrig_b.at(i).second, mstrig_b.at(i+samples.size()).first,mstrig_b.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
            if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
            else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\pm " << unints.at(4) << "\\%$ & ";
        } else{
            file << "+" << unints.at(4) << ", -" << unints.at(5) << "\\% & ";
        }
        if(unints.at(6) == unints.at(7)){
            file << "$\\mathbf{\\pm " << unints.at(6) << "\\%}$ \\\\" << endl;
        } else{
            file << "\\bf{+" << unints.at(6) << ", -" << unints.at(7) << "\\% } \\\\ " << endl;
        }
    }

    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{MS barrel trigger systematics.}\\label{tab:MSBTrigSyst_}" << endl;
    file << "\\end{table}" << endl;
    file << " " << endl;
    file << " " << endl;
    file << " " << endl;

    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    if(model == "higgs" || model == "stealth")file << "\\begin{tabular}{c|cccc}" << endl;
    else if( model == "scalar")file << "\\begin{tabular}{cc|cccc}" << endl;

    file << "\\toprule" << endl;
    file << "{$m_{\\tilde{g}}$} & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;

    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt( pow( msvx_b.at(i).first,2) + pow(msvx_b.at(i+samples.size()).first,2));
        double totdown = TMath::Sqrt(pow( msvx_b.at(i).second,2) + pow(msvx_b.at(i+samples.size()).second,2));
        std::vector<double> uncertainties = {msvx_b.at(i).first, msvx_b.at(i).second, msvx_b.at(i+samples.size()).first,msvx_b.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
            if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
            else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\mathbf{\\pm " << unints.at(4) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(4) << ", -" << unints.at(5) << "\\% } \\\\ " << endl;
        }
    }
    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{MS barrel vertex systematics.}\\label{tab:MSBVxSyst_}" << endl;
    file << "\\end{table}" << endl;
    file << " " << endl;
    file << " " << endl;
    file << " " << endl;

    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    if(model == "higgs" || model == "stealth")file << "\\begin{tabular}{c|cccc}" << endl;
    else if( model == "scalar")file << "\\begin{tabular}{cc|cccc}" << endl;

    file << "\\toprule" << endl;
    file << "{$m_{\\tilde{g}}$} & Trigger reconstruction & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;

    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt(pow(trigsf_e.at(i).first,2) + pow( mstrig_e.at(i).first,2) + pow(mstrig_e.at(i+samples.size()).first,2));
        double totdown = TMath::Sqrt(pow(trigsf_e.at(i).second,2) + pow( mstrig_e.at(i).second,2) + pow(mstrig_e.at(i+samples.size()).second,2));
        std::vector<double> uncertainties = {trigsf_e.at(i).first, trigsf_e.at(i).second, mstrig_e.at(i).first, mstrig_e.at(i).second, mstrig_e.at(i+samples.size()).first,mstrig_e.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
            if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
            else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\pm " << unints.at(4) << "\\%$ & ";
        } else{
            file << "+" << unints.at(4) << ", -" << unints.at(5) << "\\% & ";
        }
        if(unints.at(6) == unints.at(7)){
            file << "$\\mathbf{\\pm " << unints.at(6) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(6) << ", -" << unints.at(7) << "\\% } \\\\ " << endl;
        }
    }

    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{MS endcap trigger systematics.}\\label{tab:MSETrigSyst_}" << endl;
    file << "\\end{table}" << endl;

    file << " " << endl;
    file << " " << endl;
    file << " " << endl;

    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    if(model == "higgs" || model == "stealth")file << "\\begin{tabular}{c|cccc}" << endl;
    else if( model == "scalar")file << "\\begin{tabular}{cc|cccc}" << endl;

    file << "\\toprule" << endl;
    file << "{$m_{\\tilde{g}}$} & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;

    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt( pow( msvx_e.at(i).first,2) + pow(msvx_e.at(i+samples.size()).first,2));
        double totdown = TMath::Sqrt(pow( msvx_e.at(i).second,2) + pow(msvx_e.at(i+samples.size()).second,2));
        std::vector<double> uncertainties = {msvx_e.at(i).first, msvx_e.at(i).second, msvx_e.at(i+samples.size()).first,msvx_e.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
            if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
            else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\mathbf{\\pm " << unints.at(4) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(4) << ", -" << unints.at(5) << "\\% } \\\\ " << endl;
        }
    }
    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{MS endcap vertex systematics.}\\label{tab:MSEVxSyst_}" << endl;
    file << "\\end{table}" << endl;

    file << " " << endl;
    file << " " << endl;
    file << " " << endl;

    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    file << "\\begin{tabular}{c|cccc}" << endl;

    file << "\\toprule" << endl;
    file << "{$m_{\\tilde{g}}$} & JES & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;

    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt(pow(llpjet_b_b.at(i).first,2) + pow( llpjet_b_b.at(i+samples.size()).first,2) + pow(llpjet_b_b.at(i+2*samples.size()).first,2));
        double totdown = TMath::Sqrt(pow(llpjet_b_b.at(i).second,2) + pow( llpjet_b_b.at(i+samples.size()).second,2) + pow(llpjet_b_b.at(i+2*samples.size()).second,2));
        std::vector<double> uncertainties = {llpjet_b_b.at(i+2*samples.size()).first, llpjet_b_b.at(i+2*samples.size()).second, llpjet_b_b.at(i).first, llpjet_b_b.at(i).second, llpjet_b_b.at(i+samples.size()).first,llpjet_b_b.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
            if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
            else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\pm " << unints.at(4) << "\\%$ & ";
        } else{
            file << "+" << unints.at(4) << ", -" << unints.at(5) << "\\% & ";
        }
        if(unints.at(6) == unints.at(7)){
            file << "$\\mathbf{\\pm " << unints.at(6) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(6) << ", -" << unints.at(7) << "\\% } \\\\ " << endl;
        }
    }

    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{Summary of systematic uncertainties on the event-level probability for two jets with $E_{T} > 150$~GeV, in events events with one barrel MS vertex and a second LLP with $|\\eta| < 1.5$}\\label{tab:LLPBVxBJetSyst_}" << endl;
    file << "\\end{table}" << endl;
    file << " " << endl;
    file << " " << endl;
    file << " " << endl;


    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    file << "\\begin{tabular}{c|cccc}" << endl;

    file << "\\toprule" << endl;
    file << "{$m_{\\tilde{g}}$} & JES & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;

    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt(pow(llpjet_b_e.at(i).first,2) + pow( llpjet_b_e.at(i+samples.size()).first,2) + pow(llpjet_b_e.at(i+2*samples.size()).first,2));
        double totdown = TMath::Sqrt(pow(llpjet_b_e.at(i).second,2) + pow( llpjet_b_e.at(i+samples.size()).second,2) + pow(llpjet_b_e.at(i+2*samples.size()).second,2));
        std::vector<double> uncertainties = {llpjet_b_e.at(i+2*samples.size()).first, llpjet_b_e.at(i+2*samples.size()).second, llpjet_b_e.at(i).first, llpjet_b_e.at(i).second, llpjet_b_e.at(i+samples.size()).first,llpjet_b_e.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
            if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
            else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\pm " << unints.at(4) << "\\%$ & ";
        } else{
            file << "+" << unints.at(4) << ", -" << unints.at(5) << "\\% & ";
        }
        if(unints.at(6) == unints.at(7)){
            file << "$\\mathbf{\\pm " << unints.at(6) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(6) << ", -" << unints.at(7) << "\\% } \\\\ " << endl;
        }
    }

    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{Summary of systematic uncertainties on the event-level probability for two jets with $E_{T} > 150$~GeV, in events events with one barrel MS vertex and a second LLP with $|\\eta| > 1.5$}\\label{tab:LLPBVxEJetSyst_}" << endl;
    file << "\\end{table}" << endl;
    file << " " << endl;
    file << " " << endl;
    file << " " << endl;

    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    file << "\\begin{tabular}{c|cccc}" << endl;

    file << "\\toprule" << endl;
    file << "{$m_{\\tilde{g}}$} & JES & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;

    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt(pow(llpjet_e_b.at(i).first,2) + pow( llpjet_e_b.at(i+samples.size()).first,2) + pow(llpjet_e_b.at(i+2*samples.size()).first,2));
        double totdown = TMath::Sqrt(pow(llpjet_e_b.at(i).second,2) + pow( llpjet_e_b.at(i+samples.size()).second,2) + pow(llpjet_e_b.at(i+2*samples.size()).second,2));
        std::vector<double> uncertainties = {llpjet_e_b.at(i+2*samples.size()).first, llpjet_e_b.at(i+2*samples.size()).second, llpjet_e_b.at(i).first, llpjet_e_b.at(i).second, llpjet_e_b.at(i+samples.size()).first,llpjet_e_b.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
            if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
            else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\pm " << unints.at(4) << "\\%$ & ";
        } else{
            file << "+" << unints.at(4) << ", -" << unints.at(5) << "\\% & ";
        }
        if(unints.at(6) == unints.at(7)){
            file << "$\\mathbf{\\pm " << unints.at(6) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(6) << ", -" << unints.at(7) << "\\% } \\\\ " << endl;
        }
    }

    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{Summary of systematic uncertainties on the event-level probability for two jets with $E_{T} > 250$~GeV, in events events with one endcap MS vertex and a second LLP with $|\\eta| < 1.5$}\\label{tab:LLPEVxBJetSyst_}" << endl;
    file << "\\end{table}" << endl;
    file << " " << endl;
    file << " " << endl;
    file << " " << endl;

    file << "\\begin{table}" << endl;
    file << "\\centering" << endl;
    file << "\\begin{tabular}{c|cccc}" << endl;

    file << "\\toprule" << endl;
    file << "{$m_{\\tilde{g}}$} & JES & Pileup & PDF & Total \\\\" << endl;
    file << "\\midrule" << endl;

    for(unsigned int i=0; i<benchmark.size(); i++){
        double totup = TMath::Sqrt(pow(llpjet_e_e.at(i).first,2) + pow( llpjet_e_e.at(i+samples.size()).first,2) + pow(llpjet_e_e.at(i+2*samples.size()).first,2));
        double totdown = TMath::Sqrt(pow(llpjet_e_e.at(i).second,2) + pow( llpjet_e_e.at(i+samples.size()).second,2) + pow(llpjet_e_e.at(i+2*samples.size()).second,2));
        std::vector<double> uncertainties = {llpjet_e_e.at(i+2*samples.size()).first, llpjet_e_e.at(i+2*samples.size()).second, llpjet_e_e.at(i).first, llpjet_e_e.at(i).second, llpjet_e_e.at(i+samples.size()).first,llpjet_e_e.at(i+samples.size()).second,totup,totdown};
        std::vector<double> unints;
        file << massLine.at(i) << " & ";
        for(unsigned int j=0; j < uncertainties.size(); j++){
            if(uncertainties.at(j) < 0.00095) unints.push_back(TMath::Nint(uncertainties.at(j)*10000.)/100.);
            else unints.push_back(TMath::Nint(uncertainties.at(j)*sc1)/sc2);
        }
        if(unints.at(0) == unints.at(1)){
            file << "$\\pm " << unints.at(0) << "\\%$ & ";
        } else{
            file << "+" << unints.at(0) << ", -" << unints.at(1) << "\\% & ";
        }
        if(unints.at(2) == unints.at(3)){
            file << "$\\pm " << unints.at(2) << "\\%$ & ";
        } else{
            file << "+" << unints.at(2) << ", -" << unints.at(3) << "\\% & ";
        }
        if(unints.at(4) == unints.at(5)){
            file << "$\\pm " << unints.at(4) << "\\%$ & ";
        } else{
            file << "+" << unints.at(4) << ", -" << unints.at(5) << "\\% & ";
        }
        if(unints.at(6) == unints.at(7)){
            file << "$\\mathbf{\\pm " << unints.at(6) << "\\%}$ \\\\ " << endl;
        } else{
            file << "\\bf{+" << unints.at(6) << ", -" << unints.at(7) << "\\% } \\\\ " << endl;
        }
    }

    file << "\\bottomrule" << endl;
    file << "\\end{tabular}" << endl;
    file << "\\caption{Summary of systematic uncertainties on the event-level probability for two jets with $E_{T} > 250$~GeV, in events events with one endcap MS vertex and a second LLP with $|\\eta| > 1.5$}\\label{tab:LLPEVxEJetSyst_}" << endl;
    file << "\\end{table}" << endl;
    file << " " << endl;
    file << " " << endl;
    file << " " << endl;


}
