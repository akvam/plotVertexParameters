
void makeMSSystematicsPlots(){
	//0.7 < |η| < 1.0 or 1.5 < |η| < 1.7

	    TCanvas* c1 = new TCanvas("c1","c1",800,600);
		  SetAtlasStyle();

	//c1->SetRightMargin(0.05);
	//c1->SetTopMargin(0.07);
	//c1->SetLeftMargin(0.15);
	//c1->SetBottomMargin(0.15);
	TPad *pad1 = new TPad("pad1","pad1",0,0.32,1,1);
		pad1->SetTopMargin(0.075);
		pad1->SetBottomMargin(0.04);
		pad1->SetLeftMargin(0.14);
		pad1->SetLogy();
		pad1->Draw();
	
	  TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.32);
	  pad2->SetTopMargin(0.05);
	  pad2->SetBottomMargin(0.40);
	  pad2->SetLeftMargin(0.14);
	  pad2->Draw();


	  
	TFile *_file0 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/dijetData/outputMSSystematics.root");

	TFile *_file1 = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/bkgdJZ/JZAll/outputMSSystematics.root");


	TString plotDir = "SystematicsOutputs/";
	
	for (TObject* keyAsObj : *(_file1->GetListOfKeys())){
    		auto key = dynamic_cast<TKey*>(keyAsObj);

  		pad1->cd(); 
  	    gStyle->SetPadTickX(2);
  	    gStyle->SetPadTickY(2);
    	TH1D *h_MC = (TH1D*) key->ReadObj();
		TString histName = TString(h_MC->GetName());
		
		TH1D *h_Data = (TH1D*)_file0->Get(histName);
		std::cout << "Key name: " << key->GetName() << " Type: " << key->GetClassName() << ", entries data/mc: " << h_Data->GetEntries()  <<"/" <<h_MC->GetEntries() <<  std::endl;

		if(h_MC->GetEntries() == 0 || h_Data->GetEntries() == 0) continue;
		if(! histName.Contains("full_pT_nMSTracklets") ) continue; //this one is messed up right now
		
		pad1->SetLogy(0); h_Data->SetMinimum(0);
		h_Data->SetLineColor(kBlack);h_Data->SetMarkerColor(kBlack);h_Data->SetMarkerSize(1.2);h_Data->SetLineWidth(2);h_Data->SetMarkerStyle(20);
 		h_MC->SetLineColor(kBlue+2);h_MC->SetMarkerColor(kBlue+2);h_MC->SetMarkerSize(1.2);h_MC->SetLineWidth(2);h_MC->SetMarkerStyle(4);	
 		h_Data->GetYaxis()->SetTitle("Number of jets");
 				
		if(histName.Contains("pT_n")){ h_Data->GetXaxis()->SetTitle("jet p_{T} [GeV]");h_Data->GetYaxis()->SetTitle("# in cone");}

		double maxVal = h_MC->GetMaximum(); if(h_Data->GetMaximum() > maxVal) maxVal = h_Data->GetMaximum();
		h_Data->SetMaximum(1.3*maxVal);
		h_Data->SetTitle("");

		TString append = "";

		if(histName.Contains("SLJ")) append = ", sub-leading jets"; 
		else if(histName.Contains("LJ")) append = ", leading jets"; 
		else if(histName.Contains("AllJets")) append = ", leading and sub-leading jets"; 
		TString title = "";
		if(histName.Contains("full")) title = TString("|#eta| < 2.7") + append;
		else if(histName.Contains("eta1")) title = TString("|#eta| < 0.7") + append;
		else if(histName.Contains("eta2")) title = TString("0.7 < |#eta| < 1.0") + append;
		else if(histName.Contains("eta3")) title = TString("1.0 < |#eta| < 1.5") + append;
		else if(histName.Contains("eta4")) title = TString("1.5 < |#eta| < 1.7") + append;
		else if(histName.Contains("eta5")) title = TString("1.7 < |#eta| < 2.7") + append;
		


		  h_Data->GetYaxis()->SetLabelSize(0.070);
		  h_Data->GetYaxis()->SetTitleSize(0.065);
		  h_Data->GetYaxis()->SetTitleOffset(0.18);
		  h_Data->GetXaxis()->SetTitleOffset(2.7);
		  h_Data->GetXaxis()->SetLabelOffset(0.5);
		  h_Data->GetYaxis()->SetTitleOffset(0.95);
		  
		h_Data->Draw();
		h_MC->Draw("SAME");

		  
		
		 TLatex latex2;
		 latex2.SetNDC();
		 latex2.SetTextColor(kBlack);
		 latex2.SetTextSize(0.06);
		 latex2.SetTextAlign(31);  //align at bottom
		 latex2.DrawLatex(0.95,.94,title);
			 
		//c1->Print(plotDir+histName+".pdf");
		 TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
		   l.SetNDC();
		   l.SetTextFont(72);
		   l.SetTextSize(0.06);
		   l.SetTextColor(kBlack);
		   l.DrawLatex(.72,.84,"ATLAS");
		     TLatex p; 
		     p.SetNDC();
		     p.SetTextSize(0.06);
		     p.SetTextFont(42);
		     p.SetTextColor(kBlack);
		     p.DrawLatex(0.72+0.105,0.84,"Internal");
		
		 c1->cd();

		  pad2->cd();
		  gStyle->SetPadTickX(1);
		  gStyle->SetPadTickY(1);
		  
		  TH1D *p1_new = (TH1D*)h_Data->Clone("data_clone");
		  
		p1_new->Divide(h_MC);

		
		p1_new->SetMarkerStyle(8);
		  p1_new->SetMarkerSize(0.8);
		  p1_new->SetMarkerColor(kBlack);
		  p1_new->SetMinimum(0.0);
		  p1_new->SetMaximum(2.0);
		  //p1_new->GetXaxis()->SetTitle("to do");
		  p1_new->GetXaxis()->SetLabelFont(42);
		  p1_new->GetXaxis()->SetLabelSize(0.16);
		  p1_new->GetXaxis()->SetLabelOffset(0.05);
		  p1_new->GetXaxis()->SetTitleFont(42);
		  p1_new->GetXaxis()->SetTitleSize(0.14);
		  p1_new->GetXaxis()->SetTitleOffset(1.3);
		  p1_new->GetYaxis()->SetNdivisions(505);
		  p1_new->GetYaxis()->SetTitle("#font[42]{Data/MC}");
		  p1_new->GetYaxis()->SetLabelFont(42);
		  p1_new->GetYaxis()->SetLabelSize(0.15);
		  p1_new->GetYaxis()->SetTitleFont(42);
		  p1_new->GetYaxis()->SetTitleSize(0.13);
		  p1_new->GetYaxis()->SetTitleOffset(0.44); 
		  p1_new->DrawCopy("eP");
		  
		  TLine* line = new TLine();
		  line->DrawLine(p1_new->GetXaxis()->GetXmin(),1,p1_new->GetXaxis()->GetXmax(),1);
		
		TString fitVal = "";
		
		if (histName.Contains("Tracklet") && !histName.Contains("eta")){
		  TF1 *f1 = new TF1("fit1", "pol0", 0, 12);
		  p1_new->Fit("fit1","R");
		  p1_new->GetXaxis()->SetRangeUser(0,12);
		  fitVal =  TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(0) );
		  
		} else if(histName.Contains("MDT")){
		  TF1 *f1 = new TF1("fit1", "pol0", 300, 3000);
		  p1_new->Fit("fit1","R");
		  p1_new->GetXaxis()->SetRangeUser(300,3000); 
		  fitVal =  TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(0) );
		} else if(histName.Contains("pT_")){
			  TF1 *f1 = new TF1("fit1", "pol0", 0, 2000);
			  p1_new->Fit("fit1","R");
			  p1_new->GetXaxis()->SetRangeUser(0,2000); 
			  fitVal =  TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("fit1")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("fit1")->GetParError(0) );
		}  else {
			p1_new->Fit("pol0","Q");
			//std::cout << h_Data->GetFunction("pol0")->GetParameter(0) << ", " << h_Data->GetFunction("pol0")->GetParError(0) << std::endl;
			fitVal = TString("Fit: " ) + TString(std::to_string( p1_new->GetFunction("pol0")->GetParameter(0) ) ) + "+/-" + std::to_string( p1_new->GetFunction("pol0")->GetParError(0) );		
		}
		std::cout << fitVal << std::endl; 
		pad2->SetLogy(0);

		//latex2.DrawLatex(.15,.98,title + ", Data/MC");
		
		 TLatex latex;
		 latex.SetNDC();
		 latex.SetTextColor(kBlack);
		 latex.SetTextSize(0.1);
		 latex.SetTextAlign(13);  //align at top
		 latex.DrawLatex(.25,.92,fitVal);

		c1->Print(plotDir+"ratio_"+histName+".pdf");
	}


}
