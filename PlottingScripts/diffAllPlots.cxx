void diffAllPlots(){
    //0.7 < |η| < 1.0 or 1.5 < |η| < 1.7

    TCanvas* c1 = new TCanvas("c1","c1",800,600);
    SetAtlasStyle();

    c1->SetRightMargin(0.13);
    c1->SetTopMargin(0.07);
    c1->SetLeftMargin(0.15);
    c1->SetBottomMargin(0.15);

    TString plotDir = "../OutputPlots/data/backgroundFlagsTesting/";
    
    for (TObject* keyAsObj : *(_file1->GetListOfKeys())){
        auto key = dynamic_cast<TKey*>(keyAsObj);
        if((TString)key->GetClassName() != "TH2D" && (TString)key->GetClassName() != "TH1D") continue;

        TH1D *h_less = (TH1D*) key->ReadObj();
        TString histName = TString(h_less->GetName());
        
        TH1D *h_more = (TH1D*)_file0->Get(histName);
        std::cout << "Key name: " << key->GetName() << " Type: " << key->GetClassName() << ", entries data/MC: " << h_more->GetEntries()  <<"/" <<h_less->GetEntries() <<  std::endl;

        if(h_less->GetEntries() == 0 || h_more->GetEntries() == 0) continue;
        
        h_less->Divide(h_more);
        
        if((TString)key->GetClassName() == "TH2D") h_less->Draw("COLZ");
        else h_less->Draw();

        c1->Print(plotDir+"diff_"+histName+".pdf");
    }


}
