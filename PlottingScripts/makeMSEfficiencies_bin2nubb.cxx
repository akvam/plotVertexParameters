//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "../AtlasLabels.h"
#include "../AtlasStyle.h"
#include "../AtlasUtils.h"

using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;

TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau){
	fileName = (TString)fileName(0,fileName.Sizeof() - 18);
	TString title = "";

	if(fileName.Contains("Chi")){
		title = fileName;
		return title;
	} else if(fileName.Contains("mS")){
		TString lt = "";
		TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
		TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
		if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
		if(mH == "125") title = (TString) "m_{H},m_{S}=[125,"+mS+"] GeV" + lt;
		else title = (TString) "m_{#Phi},m_{S}=["+mH+","+mS+"] GeV" + lt;
		return title;
	} else if(fileName.Contains("mg")){
		TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
		title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
	}
	return title;
}


int makeMSEfficiencies_bin2nubb(){


	gROOT->LoadMacro("AtlasUtils.C");
	gROOT->LoadMacro("AtlasLabels.C");
	gROOT->LoadMacro("AtlasStyle.C");

	TString type = "HChiChinubbbin2";
	int nSIG=4;
	bool add_ctau = false;

	bool doHChiChi = true;
	bool doStealth = false;
	bool doHSS_lt = false;
	bool doH100 = false;
	bool doH125 = false;
	bool doH200 = false;
	bool doH400_600 = false;
	bool doWChiChi = false;

	//_bkg[5] = new TFile("bkgdJZ/JZ7W/outputGVC.root");
	//_bkg[6] = new TFile("bkgdJZ/JZ8W/outputGVC.root");
	TFile *_sig[30];

	if(doHChiChi){
		_sig[0] = new TFile("HChiChi_cbs_mH125mChi1000/outputMSEff.root");
		_sig[1] = new TFile("HChiChi_cbs_mH125mChi10/outputMSEff.root");
		_sig[2] = new TFile("HChiChi_cbs_mH125mChi30/outputMSEff.root");
		_sig[3] = new TFile("HChiChi_cbs_mH125mChi50/outputMSEff.root");

		_sig[4] = new TFile("HChiChi_lcb_mH125mChi100/outputMSEff.root");
		_sig[5] = new TFile("HChiChi_lcb_mH125mChi10/outputMSEff.root");
		_sig[6] = new TFile("HChiChi_lcb_mH125mChi30/outputMSEff.root");
		_sig[7] = new TFile("HChiChi_lcb_mH125mChi50/outputMSEff.root");

		_sig[8] = new TFile("HChiChi_nubb_mH125mChi100_bin2/outputMSEff.root");
		_sig[9] = new TFile("HChiChi_nubb_mH125mChi10_bin2/outputMSEff.root");
		_sig[10] = new TFile("HChiChi_nubb_mH125mChi30_bin2/outputMSEff.root");
		_sig[11] = new TFile("HChiChi_nubb_mH125mChi50_bin2/outputMSEff.root");

		_sig[12] = new TFile("HChiChi_tatanu_mH125mChi100/outputMSEff.root");
		_sig[13] = new TFile("HChiChi_tatanu_mH125mChi10/outputMSEff.root");
		_sig[14] = new TFile("HChiChi_tatanu_mH125mChi30/outputMSEff.root");
		_sig[15] = new TFile("HChiChi_tatanu_mH125mChi50/outputMSEff.root");
	} else if(doStealth){
		_sig[0] = new TFile("mg250/outputMSEff.root");
		_sig[1] = new TFile("mg500/outputMSEff.root");
		_sig[2] = new TFile("mg800/outputMSEff.root");
		_sig[3] = new TFile("mg1200/outputMSEff.root");
		_sig[4] = new TFile("mg1500/outputMSEff.root");
		_sig[5] = new TFile("mg2000/outputMSEff.root");
	} else if(doHSS_lt){
		_sig[0] = new TFile("mH125mS8lt5/outputMSEff.root");
		_sig[1] = new TFile("mH125mS8lt9/outputMSEff.root");
		_sig[2] = new TFile("mH125mS15lt5/outputMSEff.root");
		_sig[3] = new TFile("mH125mS15lt9/outputMSEff.root");
		_sig[4] = new TFile("mH125mS25lt5/outputMSEff.root");
		_sig[5] = new TFile("mH125mS25lt9/outputMSEff.root");
		_sig[6] = new TFile("mH125mS40lt5/outputMSEff.root");
		_sig[7] = new TFile("mH125mS40lt9/outputMSEff.root");
		_sig[8] = new TFile("mH125mS55lt5/outputMSEff.root");
		_sig[9] = new TFile("mH125mS55lt9/outputMSEff.root");

		_sig[10] = new TFile("mH200mS8lt5/outputMSEff.root");
		_sig[11] = new TFile("mH200mS8lt9/outputMSEff.root");
		_sig[12] = new TFile("mH200mS25lt5/outputMSEff.root");
		_sig[13] = new TFile("mH200mS25lt9/outputMSEff.root");
		_sig[14] = new TFile("mH200mS50lt5/outputMSEff.root");
		_sig[15] = new TFile("mH200mS50lt9/outputMSEff.root");

		_sig[16] = new TFile("mH400mS50lt5/outputMSEff.root");
		_sig[17] = new TFile("mH400mS50lt9/outputMSEff.root");
		_sig[18] = new TFile("mH400mS100lt5/outputMSEff.root");
		_sig[19] = new TFile("mH400mS100lt9/outputMSEff.root");

		_sig[20] = new TFile("mH600mS50lt5/outputMSEff.root");
		_sig[21] = new TFile("mH600mS50lt9/outputMSEff.root");
		_sig[22] = new TFile("mH600mS150lt5/outputMSEff.root");
		_sig[23] = new TFile("mH600mS150lt9/outputMSEff.root");
		
	}  else if(doH100){
		_sig[0] = new TFile("mH100mS8lt5/outputMSEff.root");
		_sig[1] = new TFile("mH100mS25lt5/outputMSEff.root");
	} else if(doH125){
		_sig[0] = new TFile("mH125mS8lt5/outputMSEff.root");
		_sig[1] = new TFile("mH125mS15lt5/outputMSEff.root");
		_sig[2] = new TFile("mH125mS25lt5/outputMSEff.root");
		_sig[3] = new TFile("mH125mS40lt5/outputMSEff.root");
		_sig[4] = new TFile("mH125mS55lt5/outputMSEff.root");
	} else if(doH200){
		_sig[0] = new TFile("mH200mS8lt5/outputMSEff.root");
		_sig[1] = new TFile("mH200mS25lt5/outputMSEff.root");
		_sig[2] = new TFile("mH200mS50lt5/outputMSEff.root");
	} else if(doH400_600){
		_sig[0] = new TFile("mH400mS50lt5/outputMSEff.root");
		_sig[1] = new TFile("mH400mS100lt5/outputMSEff.root");
		_sig[2] = new TFile("mH600mS50lt5/outputMSEff.root");
		_sig[3] = new TFile("mH600mS150lt5/outputMSEff.root");
	} else if(doWChiChi){
		_sig[0] = new TFile("WChiChi_ltb_mChi1000/outputMSEff.root");
		_sig[1] = new TFile("WChiChi_ltb_mChi1500/outputMSEff.root");
		_sig[2] = new TFile("WChiChi_ltb_mChi500/outputMSEff.root");
		_sig[3] = new TFile("WChiChi_nubb_mChi1000/outputMSEff.root");
		_sig[4] = new TFile("WChiChi_nubb_mChi1500/outputMSEff.root");
		_sig[5] = new TFile("WChiChi_nubb_mChi500/outputMSEff.root");
		_sig[6] = new TFile("WChiChi_tatanu_mChi1000/outputMSEff.root");
		_sig[7] = new TFile("WChiChi_tatanu_mChi1500/outputMSEff.root");
		_sig[8] = new TFile("WChiChi_tatanu_mChi500/outputMSEff.root");
		_sig[9] = new TFile("WChiChi_tbs_mChi1000/outputMSEff.root");
		_sig[10] = new TFile("WChiChi_tbs_mChi1500/outputMSEff.root");
		_sig[11] = new TFile("WChiChi_tbs_mChi500/outputMSEff.root");
	}

	TString sigNames[30]; 
	for(int i=8; i<nSIG+8; i++){ sigNames[i] = convertFiletoLabel(_sig[i]->GetName(), add_ctau);}

	Int_t colorSig[16] = {kBlue-3, kAzure+5, kTeal-6,kTeal+2,kViolet-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};
	Int_t markerSig[16] = {24,25,26,27,28,25,26,27,24,25,26,27,24,25,26,27};

	Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};

	TString names[12];
	names[0] = "MSVx_Barrel_Lxy_eff";
	names[1] = "MSVx_1B_Lxy_eff";
	names[2] = "MSVx_BB_Lxy_eff";
	names[3] = "MSVx_BE_Lxy_eff";
	names[4] = "MSVx_Endcap_Lz_eff";
	names[5] = "MSVx_1E_Lz_eff";
	names[6] = "MSVx_EE_Lz_eff";
	names[7] = "MSVx_EB_Lz_eff";

	names[8] = "MSTrig_1B_Lxy_eff";
	names[9] = "MSTrig_BID_Lxy_eff";
	names[10] = "MSTrig_1E_Lz_eff";
	names[11] = "MSTrig_EID_Lz_eff";

	SetAtlasStyle();
	gStyle->SetPalette(1);

	std::cout << "running on : " << _sig[0]->GetName() << std::endl;

	TCanvas* c = new TCanvas("c_1","c_1",800,600);
	//c->SetLogy();

	//vertex reco
	for(unsigned int i=0;i<8; i++){
		
		std::cout << "plot: " << names[i] << std::endl;
		
		TH1D* h_sig[30]; 

		TLegend *legS = new TLegend(0.38,0.8,0.566,0.935);
		legS->SetFillColor(0);
		legS->SetBorderSize(0);
		legS->SetTextSize(0.03);

		for(int j=8; j<nSIG+8; j++){
			h_sig[j] = (TH1D*) _sig[j]->Get(names[i]);
			if(!h_sig[j] ) continue;
			h_sig[j]->SetMarkerColor(colorSig[j]);
			h_sig[j]->SetLineColor(colorSig[j]); 
			h_sig[j]->SetMarkerStyle(markerSig[j]);
			
			if(names[i].Contains("Lxy")) h_sig[j]->GetXaxis()->SetTitle("Lxy [m]");
			else if(names[i].Contains("Lz")) h_sig[j]->GetXaxis()->SetTitle("|Lz| [m]");
			h_sig[j]->GetYaxis()->SetRangeUser(0,1);
			h_sig[j]->GetYaxis()->SetTitle("MS vertex reconstruction efficiency");

			legS->AddEntry(h_sig[j],sigNames[j],"lp");			 


			if(j == 8){ 	 h_sig[j]->Draw("");}
			else h_sig[j]->Draw("SAME");

			if(j == 11){
				TLatex latex;
				latex.SetNDC();
				latex.SetTextColor(kBlack);
				latex.SetTextSize(0.04);
				latex.SetTextAlign(13);  //align at top
				latex.DrawLatex(.18,.91,"#font[72]{ATLAS}  Internal");
				legS->Draw();
				gPad->RedrawAxis();

				c->Print("combinedPlots/"+names[i]+std::to_string(j)+"_"+type+".pdf");
				legS->Clear(); 
			}

		}
	}
	//trigger reco
	for(unsigned int i=8;i<12; i++){
		TGraphAsymmErrors* h_sig[30]; 
		TMultiGraph *mg = new TMultiGraph();

		TLegend *legS = new TLegend(0.38,0.8,0.566,0.935);
		legS->SetFillColor(0);
		legS->SetBorderSize(0);
		legS->SetTextSize(0.03);

		for(int j=8; j<nSIG+8; j++){
			h_sig[j] = new TGraphAsymmErrors();
			h_sig[j] = (TGraphAsymmErrors*) _sig[j]->Get(names[i]);

			if(!h_sig[j] ) continue;
			h_sig[j]->SetMarkerColor(colorSig[j]); h_sig[j]->SetLineColor(colorSig[j]); h_sig[j]->SetMarkerStyle(markerSig[j]);

			legS->AddEntry(h_sig[j],sigNames[j],"lp");			 
			mg->Add(h_sig[j]);

			if(j == 11){
				mg->Draw("AP");

				if(names[i].Contains("Lxy")){
					mg->GetXaxis()->SetLimits(0,8);
					mg->SetMaximum(1);
					mg->GetXaxis()->SetTitle("Lxy [m]");
				}
				else if(names[i].Contains("Lz")){
					mg->GetXaxis()->SetLimits(0,15);
					mg->SetMaximum(1);
					mg->GetXaxis()->SetTitle("|Lz| [m]");
				}
				mg->GetYaxis()->SetTitle("Muvtx trigger efficiency");

				TLatex latex;
				latex.SetNDC();
				latex.SetTextColor(kBlack);
				latex.SetTextSize(0.04);
				latex.SetTextAlign(13);  //align at top
				latex.DrawLatex(.18,.91,"#font[72]{ATLAS}  Internal");
				legS->Draw();
				gPad->RedrawAxis();
				c->Print("combinedPlots/"+names[i]+std::to_string(j)+"_"+type+".pdf");
				
				legS->Clear(); 
				mg->Clear();
			}

		}

		//latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");

		/*
	 TLegend *legD = new TLegend(0.73,0.835,0.93,0.86);
	 legD->SetFillColor(0);
	 legD->SetBorderSize(0);
	 legD->SetTextSize(0.03);
	 legD->AddEntry(g_data[0],dataNames[0],"lp");
	 legD->Draw();
		 */



	}

	return 314;

}
