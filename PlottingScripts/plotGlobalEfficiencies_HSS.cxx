#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TPad.h"
#include "TMath.h"
#include <cmath>

double evaluate_novosibirsk( const double x, const double peak, const double width, const double tail )
{
    if (TMath::Abs(tail) < 1.e-12) {
        return TMath::Exp( -0.5 * TMath::Power( ( (x - peak) / width ), 2 ));
    }
    
    Double_t arg = 1.0 - ( x - peak ) * tail / width;
    
    if (arg < 1.e-12) {
        return 0.0;
    }
    Double_t log = TMath::Log(arg);
    
    const Double_t xi = 2.3548200450309494; // 2 Sqrt( Ln(4) )
    
    Double_t width_zero = ( 2.0 / xi ) * TMath::ASinH( tail * xi * 0.5 );
    Double_t width_zero2 = width_zero * width_zero;
    Double_t exponent = ( -0.5 / (width_zero2) * log * log ) - ( width_zero2 * 0.5 );
    
    return TMath::Exp(exponent) ;
}
double novosibirsk( double* x, double* par ){
    return par[0]*evaluate_novosibirsk( log10( x[0] ), par[1], par[2], par[3] );
}
void plotGlobalEfficiencies_HSS(){
    std::vector<TString> benchmark = {"mH125mS5lt5", "mH125mS8lt5", "mH125mS15lt5", "mH125mS25lt5", "mH125mS40lt5", "mH125mS55lt5"};
    //std::vector<double> scalings = {66.2841*396000., 1.54623*391000., 0.084412*389000., 0.00476892*396000.,  0.000794191*394000., 0.0000554713*390000.};
    //can plot these ones as global number of expected events/BR[H->ss]
    
    TCanvas *c1 = new TCanvas("c1","c1",800,600);

    TFile* tfile[6];
    TH1D* thist[6];
    TH1D* thist_up[6];
    TH1D* thist_down[6];
    TH1D* thist2[6];
    TF1* fn[6];
    TF1* fn_up[6];
    TF1* fn_down[6];
    TGraph* fn3[6];
    TGraphAsymmErrors* g_err[6];
    double maxVals[6] = {0,0,0,0,0,0};
    double maxVal = 0;
    
    int colors[6] = {kViolet+7, kAzure+7, kTeal, kSpring-2, kOrange-3, kRed-4};
    
    for(int i=0; i<5; i++){
        tfile[i] = TFile::Open("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/outputExtrapolation_"+benchmark.at(i)+"_dv17_v3.root");
        thist[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx");
        thist_up[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_maxTotal");
        thist_down[i] = (TH1D*) tfile[i]->Get("Expected_Tot2Vx_minTotal");
        //thist[i]->Scale(1./scalings.at(i));
        char name1[10]; sprintf(name1,"fn_%d",i);
        fn[i] = new TF1(name1,novosibirsk,0.05,300,4);
        fn[i]->SetParameter(0,thist[i]->GetMaximum());
        fn[i]->SetParameter(1,-0.306313); //fn->SetParLimits(1,-1,2);
        fn[i]->SetParameter(2,0.380037); //fn->SetParLimits(2,0,2);
        fn[i]->SetParameter(3,-0.173861); //fn->SetParLimits(3,0,20.0);
         
        thist[i]->Fit( fn[i], "WRV" );

        sprintf(name1,"fnUp_%d",i);
        fn_up[i] = new TF1(name1,novosibirsk,0.05,300,4);
        fn_up[i]->SetParameter(0,thist_up[i]->GetMaximum());
        fn_up[i]->SetParameter(1,-0.306313); //fn->SetParLimits(1,-1,2);
        fn_up[i]->SetParameter(2,0.380037); //fn->SetParLimits(2,0,2);
        fn_up[i]->SetParameter(3,-0.173861); //fn->SetParLimits(3,0,20.0);
         
        thist_up[i]->Fit( fn_up[i], "WRV" );

        sprintf(name1,"fnDown_%d",i);
        fn_down[i] = new TF1(name1,novosibirsk,0.05,300,4);
        fn_down[i]->SetParameter(0,thist_down[i]->GetMaximum());
        fn_down[i]->SetParameter(1,-0.306313); //fn->SetParLimits(1,-1,2);
        fn_down[i]->SetParameter(2,0.5); //fn->SetParLimits(2,0,2);
        fn_down[i]->SetParameter(3,-0.173861); //fn->SetParLimits(3,0,20.0);
         
        thist_down[i]->Fit( fn_down[i], "WRV" );

        
        Double_t ctau[1000];
        Double_t eff[1000];
        Double_t effUp[1000];
        Double_t effDown[1000];
        int ntot = 0;
        
        TGraph *g_res = new TGraph(thist[i]);

        for (Int_t j = 0; j < 1000; j++) {
          Double_t life, nev, nev_up, nev_down;
          g_res->GetPoint(j, life, nev);
          nev = fn[i]->Eval(life);
          nev_up = fn_up[i]->Eval(life) - nev;
          nev_down = nev - fn_down[i]->Eval(life);
          if (nev == 0) {
            std::cout << "Continue because nev= " << nev << std::endl;
           continue;
          }
          if(nev_down< 0) nev_down = 0;
          if(nev > maxVals[i]) maxVals[i] = nev;
          eff[ntot] = nev;
          ctau[ntot] = life;
          effUp[ntot] = nev_up; 
          effDown[ntot] = nev_down;
          ntot++;
            if(nev > maxVal) maxVal = nev;
         }
        
        fn3[i] = new TGraph(ntot, ctau, eff);
        fn3[i]->SetLineColor(colors[i]);
        fn3[i]->SetLineStyle(i+1);
        fn3[i]->SetLineWidth(3);
        
        g_err[i] = new TGraphAsymmErrors(ntot, ctau, eff);
        for(int j=0;j<ntot; j++){

            g_err[i]->SetPointEYlow(j, effDown[j]);
            g_err[i]->SetPointEYhigh(j, effUp[j]);
        }
        g_err[i]->SetFillColorAlpha(colors[i], 0.15);
        fn3[i]->SetFillColorAlpha(colors[i], 0.15);
        g_err[i]->SetLineWidth(0);
    }
//    fn3[5]->SetLineColor(98);
    c1->SetLogx();
    fn3[1]->Draw("AL");
    fn3[1]->GetYaxis()->SetTitleOffset(1.6);
    fn3[1]->GetXaxis()->SetLimits(0.05,100);
    fn3[1]->GetXaxis()->SetTitle("Scalar proper lifetime (c*#tau) [m]");
    fn3[1]->GetYaxis()->SetRangeUser(0,2500);
    fn3[1]->GetYaxis()->SetTitle("Expected events in 36.1 fb^{-1} / BR[H #rightarrow ss]");
    fn3[1]->Draw("AL");
    
    c1->Update();
    g_err[1]->Draw("c3 SAME");
    for(int i=0; i<5; i++){
        if( i == 1) continue;
        g_err[i]->Draw("c3 SAME");
        fn3[i]->Draw("LSAME");
    }

    TLegend *leg = new TLegend(0.18,0.66,0.5,0.91);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->AddEntry(fn3[0],"m_{s} = 5 GeV","lf");
    leg->AddEntry(fn3[1],"m_{s} = 8 GeV","lf");
    leg->AddEntry(fn3[2],"m_{s} = 15 GeV","lf");
    leg->AddEntry(fn3[3],"m_{s} = 25 GeV","lf");
    leg->AddEntry(fn3[4],"m_{s} = 40 GeV","lf");
//    leg->AddEntry(fn3[5],"m_{s} = 55 GeV","lf");
    leg->Draw();

    TLatex label;
    label.SetNDC();
    label.SetTextSize(0.05);
    label.DrawLatex(0.665,0.83,"m_{H} = 125 GeV");
    ATLASLabel(0.66,0.88,"Internal");

    c1->Print("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/GlobalEfficiencies_Combined_hss_dv17_v3.pdf");
    c1->Print("/afs/cern.ch/work/h/hrussell/WorkareaRun2/plotVertexParameters/OutputPlots/signalMC/extrapolation/GlobalEfficiencies_Combined_hss_dv17_v3.root");

    return;
}
