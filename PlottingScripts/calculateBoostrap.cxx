#include "../PlottingTools/PlottingPackage/SampleDetails.h"
#include <stdio.h>

#include "Riostream.h"
#include <iostream>
#include "TH1D.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include <utility>
#include "TRandom3.h"
#include <numeric>

TRandom3 rnd;

std::pair<double,double> getBootstrapOutput(std::vector<double> outputs){

    double sum = std::accumulate(outputs.begin(), outputs.end(), 0.0);
    double mean = sum / outputs.size();

    std::vector<double> diff(outputs.size());
    std::transform(outputs.begin(), outputs.end(), diff.begin(),
            std::bind2nd(std::minus<double>(), mean));
    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / outputs.size());

    return std::make_pair(mean, stdev);

}
std::vector<double> runBootstrap(std::vector<double> &abcd_values){

    int copies = 0;
    std::vector<double> abcds = {0,0,0,0};
    for(unsigned int i=0; i< abcd_values.size(); i++){
        for(unsigned int j=0; j< int(abcd_values.at(i)); j++){
            copies = rnd.Poisson(1.0);
            abcds.at(i) += double(copies);
        } 
    }
    std::vector<double> ratios = {(abcds.at(0)/abcds.at(1) - abcds.at(2)/abcds.at(3)),(abcds.at(1)/abcds.at(0) - abcds.at(3)/abcds.at(2)),
            (abcds.at(0)/abcds.at(3) - abcds.at(1)/abcds.at(3)),(abcds.at(2)/abcds.at(0) - abcds.at(3)/abcds.at(1))};

    return ratios;
}

void calculateBootstrap(std::vector<double> abcd_values){
    TCanvas *c1 = new TCanvas("c1","c1",800,600);

    TH1D* h_ratios[4];
    h_ratios[0] = new TH1D("ab_cd","ab_cd",200,0,10);
    h_ratios[1] = new TH1D("ba_dc","ba_dc",200,0,10);
    h_ratios[2] = new TH1D("ac_bd","ac_bd",200,0,10);
    h_ratios[3] = new TH1D("ca_db","ca_db",200,0,10);

    std::vector<double> stdev;
    std::vector<std::vector<double>> outputs;
    std::vector<double > tmp = {};

    for(int i=0;i<4;i++){outputs.push_back(tmp);}
    for(int i_bs=0;i_bs<20000;i_bs++){
        if(i_bs%1000 == 0) std::cout << "on iteration: " << i_bs << std::endl;
        std::vector<double> bs = runBootstrap(abcd_values);
        for(int i=0;i<4;i++){
            if(bs.at(i) > -999999999 && !isinf(bs.at(i))){
                outputs.at(i).push_back(bs.at(i));
            }
        }
    }

    std::vector<std::pair<double, double>> result; 
    std::vector<TString> histnames = {"ab_cd","ba_dc","ac_bd","ca_db"};
    
    for(int i=0; i< 4; i++){
        result.push_back(getBootstrapOutput(outputs.at(i)));
        std::cout << i+" mean: " << result.at(i).first << ", standard deviation: " << result.at(i).second << std::endl;
        for(int j=0; j < outputs.at(i).size(); j++){h_ratios[i]->Fill(outputs.at(i).at(j) / result.at(i).second );}
        h_ratios[i]->Draw();
        c1->Print(histnames.at(i)+".pdf");
    }
}

int main(){
    std::vector<double> input_abcds = {308, 3, 4000,40};
    std::cout << "Warning, i think this code does nonsense..." << std::endl;
    calculateBootstrap(input_abcds);
}
