//
//  makeCombinedPlots.cxx
//  plotVertexParameters
//
//  Created by Heather Russell on 08/12/15.
//
//

#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"
#include "TLine.h"
using namespace std;

//const bool ATLASLabel = true;
//double wrapPhi(double phi);

//double MathUtils::DeltaR2(double phi1, double phi2, double eta1, double eta2);
int isSignal;
TStopwatch timer;

TString convertFiletoLabel(TString fileName_in, bool add_ctau);
TString convertFiletoLabel(TString fileName_in, bool add_ctau){
    TString fileName = (TString)fileName_in(24,fileName_in.Sizeof() - 40);
    std::cout << fileName << std::endl;
    TString title = "";

    if(fileName.Contains("Chi")){
        title = fileName;
        return title;
    } else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{H},m_{S}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{S}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}

void getFiles(TFile *_sig[30], int mod, int &nSIG, TString &type);
void getFiles(TFile *_sig[30], int mod, int &nSIG, TString &type){
    if(mod == 1){
        nSIG = 4; type = "mixed";
        _sig[0] = new TFile("../OutputPlots/signalMC/mH125mS8lt5/outputGVC.root");
        _sig[1] = new TFile("../OutputPlots/signalMC/mH200mS25lt5/outputGVC.root");
        _sig[2] = new TFile("../OutputPlots/signalMC/mH600mS150lt9/outputGVC.root");
        _sig[3] = new TFile("../OutputPlots/signalMC/mg250/outputGVC.root");
    }
    else if(mod == 2){
        nSIG = 4; type = "HChiChi";
        _sig[3] = new TFile("../OutputPlots/signalMC/HChiChi_cbs_mH125mChi100/outputGVC.root");
        _sig[0] = new TFile("../OutputPlots/signalMC/HChiChi_cbs_mH125mChi10/outputGVC.root");
        _sig[1] = new TFile("../OutputPlots/signalMC/HChiChi_cbs_mH125mChi30/outputGVC.root");
        _sig[2] = new TFile("../OutputPlots/signalMC/HChiChi_cbs_mH125mChi50/outputGVC.root");

        _sig[7] = new TFile("../OutputPlots/signalMC/HChiChi_lcb_mH125mChi100/outputGVC.root");
        _sig[4] = new TFile("../OutputPlots/signalMC/HChiChi_lcb_mH125mChi10/outputGVC.root");
        _sig[5] = new TFile("../OutputPlots/signalMC/HChiChi_lcb_mH125mChi30/outputGVC.root");
        _sig[6] = new TFile("../OutputPlots/signalMC/HChiChi_lcb_mH125mChi50/outputGVC.root");

        _sig[11] = new TFile("../OutputPlots/signalMC/HChiChi_nubb_mH125mChi100/outputGVC.root");
        _sig[8] = new TFile("../OutputPlots/signalMC/HChiChi_nubb_mH125mChi10/outputGVC.root");
        _sig[9] = new TFile("../OutputPlots/signalMC/HChiChi_nubb_mH125mChi30/outputGVC.root");
        _sig[10] = new TFile("../OutputPlots/signalMC/HChiChi_nubb_mH125mChi50/outputGVC.root");

        _sig[15] = new TFile("../OutputPlots/signalMC/HChiChi_tatanu_mH125mChi100/outputGVC.root");
        _sig[12] = new TFile("../OutputPlots/signalMC/HChiChi_tatanu_mH125mChi10/outputGVC.root");
        _sig[13] = new TFile("../OutputPlots/signalMC/HChiChi_tatanu_mH125mChi30/outputGVC.root");
        _sig[14] = new TFile("../OutputPlots/signalMC/HChiChi_tatanu_mH125mChi50/outputGVC.root");
    } else if(mod ==4){
        std::cout << " Stealth SUSY" << std::endl;
        nSIG = 6; type = "stealth";
        _sig[0] = new TFile("../OutputPlots/signalMC/mg250/outputGVC.root");
        _sig[1] = new TFile("../OutputPlots/signalMC/mg500/outputGVC.root");
        _sig[2] = new TFile("../OutputPlots/signalMC/mg800/outputGVC.root");
        _sig[3] = new TFile("../OutputPlots/signalMC/mg1200/outputGVC.root");
        _sig[4] = new TFile("../OutputPlots/signalMC/mg1500/outputGVC.root");
        _sig[5] = new TFile("../OutputPlots/signalMC/mg2000/outputGVC.root");
    } else if(mod ==3){
        nSIG = 0; type = "skip";
        _sig[0] = new TFile("../OutputPlots/signalMC/mH125mS8lt5/outputGVC.root");
        _sig[1] = new TFile("../OutputPlots/signalMC/mH125mS8lt9/outputGVC.root");
        _sig[2] = new TFile("../OutputPlots/signalMC/mH125mS15lt5/outputGVC.root");
        _sig[3] = new TFile("../OutputPlots/signalMC/mH125mS15lt9/outputGVC.root");
        _sig[4] = new TFile("../OutputPlots/signalMC/mH125mS25lt5/outputGVC.root");
        _sig[5] = new TFile("../OutputPlots/signalMC/mH125mS25lt9/outputGVC.root");
        _sig[6] = new TFile("../OutputPlots/signalMC/mH125mS40lt5/outputGVC.root");
        _sig[7] = new TFile("../OutputPlots/signalMC/mH125mS40lt9/outputGVC.root");
        _sig[8] = new TFile("../OutputPlots/signalMC/mH125mS55lt5/outputGVC.root");
        _sig[9] = new TFile("../OutputPlots/signalMC/mH125mS55lt9/outputGVC.root");

        _sig[10] = new TFile("../OutputPlots/signalMC/mH200mS8lt5/outputGVC.root");
        _sig[11] = new TFile("../OutputPlots/signalMC/mH200mS8lt9/outputGVC.root");
        _sig[12] = new TFile("../OutputPlots/signalMC/mH200mS25lt5/outputGVC.root");
        _sig[13] = new TFile("../OutputPlots/signalMC/mH200mS25lt9/outputGVC.root");
        _sig[14] = new TFile("../OutputPlots/signalMC/mH200mS50lt5/outputGVC.root");
        _sig[15] = new TFile("../OutputPlots/signalMC/mH200mS50lt9/outputGVC.root");

        _sig[16] = new TFile("../OutputPlots/signalMC/mH400mS50lt5/outputGVC.root");
        _sig[17] = new TFile("../OutputPlots/signalMC/mH400mS50lt9/outputGVC.root");
        _sig[18] = new TFile("../OutputPlots/signalMC/mH400mS100lt5/outputGVC.root");
        _sig[19] = new TFile("../OutputPlots/signalMC/mH400mS100lt9/outputGVC.root");

        _sig[20] = new TFile("../OutputPlots/signalMC/mH600mS50lt5/outputGVC.root");
        _sig[21] = new TFile("../OutputPlots/signalMC/mH600mS50lt9/outputGVC.root");
        _sig[22] = new TFile("../OutputPlots/signalMC/mH600mS150lt5/outputGVC.root");
        _sig[23] = new TFile("../OutputPlots/signalMC/mH600mS150lt9/outputGVC.root");

    }  else if(mod ==5){
        nSIG = 2; type = "mH100";
        _sig[0] = new TFile("../OutputPlots/signalMC/mH100mS8lt5/outputGVC.root");
        _sig[1] = new TFile("../OutputPlots/signalMC/mH100mS25lt5/outputGVC.root");
    } else if(mod ==6){
        nSIG = 5; type = "mH125";
        _sig[0] = new TFile("../OutputPlots/signalMC/mH125mS8lt5/outputGVC.root");
        _sig[1] = new TFile("../OutputPlots/signalMC/mH125mS15lt5/outputGVC.root");
        _sig[2] = new TFile("../OutputPlots/signalMC/mH125mS25lt5/outputGVC.root");
        _sig[3] = new TFile("../OutputPlots/signalMC/mH125mS40lt5/outputGVC.root");
        _sig[4] = new TFile("../OutputPlots/signalMC/mH125mS55lt5/outputGVC.root");
    } else if(mod ==7){
        nSIG = 3; type = "mH200";
        _sig[0] = new TFile("../OutputPlots/signalMC/mH200mS8lt5/outputGVC.root");
        _sig[1] = new TFile("../OutputPlots/signalMC/mH200mS25lt5/outputGVC.root");
        _sig[2] = new TFile("../OutputPlots/signalMC/mH200mS50lt5/outputGVC.root");
    } else if(mod ==8){
        nSIG = 4; type = "HighMass";
        _sig[0] = new TFile("../OutputPlots/signalMC/mH400mS50lt5/outputGVC.root");
        _sig[1] = new TFile("../OutputPlots/signalMC/mH400mS100lt5/outputGVC.root");
        _sig[2] = new TFile("../OutputPlots/signalMC/mH600mS50lt5/outputGVC.root");
        _sig[3] = new TFile("../OutputPlots/signalMC/mH600mS150lt5/outputGVC.root");
} else if(mod == 9){
	nSIG= 3; type = "mH1000";
        _sig[0] = new TFile("../OutputPlots/signalMC/mH1000mS50lt5/outputGVC.root");
        _sig[1] = new TFile("../OutputPlots/signalMC/mH1000mS150lt5/outputGVC.root");
        _sig[2] = new TFile("../OutputPlots/signalMC/mH1000mS400lt5/outputGVC.root");
    } else if(mod ==10){
        nSIG = 3; type = "WChiChi";
        _sig[0] = new TFile("../OutputPlots/signalMC/WChiChi_ltb_mChi1000/outputGVC.root");
        _sig[1] = new TFile("../OutputPlots/signalMC/WChiChi_ltb_mChi1500/outputGVC.root");
        _sig[2] = new TFile("../OutputPlots/signalMC/WChiChi_ltb_mChi500/outputGVC.root");
        _sig[3] = new TFile("../OutputPlots/signalMC/WChiChi_nubb_mChi1000/outputGVC.root");
        _sig[4] = new TFile("../OutputPlots/signalMC/WChiChi_nubb_mChi1500/outputGVC.root");
        _sig[5] = new TFile("../OutputPlots/signalMC/WChiChi_nubb_mChi500/outputGVC.root");
        _sig[6] = new TFile("../OutputPlots/signalMC/WChiChi_tatanu_mChi1000/outputGVC.root");
        _sig[7] = new TFile("../OutputPlots/signalMC/WChiChi_tatanu_mChi1500/outputGVC.root");
        _sig[8] = new TFile("../OutputPlots/signalMC/WChiChi_tatanu_mChi500/outputGVC.root");
        _sig[9] = new TFile("../OutputPlots/signalMC/WChiChi_tbs_mChi1000/outputGVC.root");
        _sig[10] = new TFile("../OutputPlots/signalMC/WChiChi_tbs_mChi1500/outputGVC.root");
        _sig[11] = new TFile("../OutputPlots/signalMC/WChiChi_tbs_mChi500/outputGVC.root");
    }
}

int makeGVCPlots_JZAll(bool doSoverB = false){

    bool forThesis = false;
    TString outputFolder = "../OutputPlots/GVC/";
    if(forThesis) outputFolder = "../OutputPlots/forThesis/GVC/";

    //gROOT->LoadMacro("AtlasUtils.C");
    //gROOT->LoadMacro("AtlasLabels.C");
    //gROOT->LoadMacro("AtlasStyle.C");

    TFile *_bkg[11]; int nBKG=1;
    Int_t colorBkg[11] = {kBlack,kRed-9, kRed-7,kRed-6,kRed-4,kRed-3,kRed-2,kRed+1,kRed+2,kRed+3,kRed+4};
    Int_t markerBkg[11] = {24,21,22,23,29,33,34,21,22,23,29};
    _bkg[0] = new TFile("../OutputPlots/bkgdJZ/JZAll/outputGVC.root");
    _bkg[1] = new TFile("../OutputPlots/bkgdJZ/JZ3W/outputGVC.root");
    _bkg[2] = new TFile("../OutputPlots/bkgdJZ/JZ4W/outputGVC.root");
    _bkg[3] = new TFile("../OutputPlots/bkgdJZ/JZ5W/outputGVC.root");
    _bkg[4] = new TFile("../OutputPlots/bkgdJZ/JZ6W/outputGVC.root");
    _bkg[5] = new TFile("../OutputPlots/bkgdJZ/JZ7W/outputGVC.root");
    _bkg[6] = new TFile("../OutputPlots/bkgdJZ/JZ8W/outputGVC.root");
    _bkg[7] = new TFile("../OutputPlots/bkgdJZ/JZ9W/outputGVC.root");
    _bkg[8] = new TFile("../OutputPlots/bkgdJZ/JZ10W/outputGVC.root");
    _bkg[9] = new TFile("../OutputPlots/bkgdJZ/JZ11W/outputGVC.root");
    _bkg[10] = new TFile("../OutputPlots/bkgdJZ/JZ12W/outputGVC.root");


    TString bkgNames[11]; 
    bkgNames[0] = "Pythia multi-jets";
    //bkgNames[0] = "JZ3W - JZ12W";
    bkgNames[1] = "JZ3W";
    bkgNames[2] = "JZ4W";
    bkgNames[3] = "JZ5W";
    bkgNames[4] = "JZ6W";
    bkgNames[5] = "JZ7W";
    bkgNames[6] = "JZ8W";
    bkgNames[7] = "JZ9W";
    bkgNames[8] = "JZ10W";
    bkgNames[9] = "JZ11W";
    bkgNames[10] = "JZ12W";

    TString type = "error";
    int nSIG=-1;
    bool add_ctau = false;

    bool doMixed = false; //1
    bool doHChiChi = false; //2
    bool doStealth = true;//3
    bool doHSS_lt = false;//4
    bool doH100 = false;//5
    bool doH125 = false;//6
    bool doH200 = false;//7
    bool doH400_600 = false;//8
    bool doWChiChi = false;//9



    Int_t colorSig[16] = {kPink-5,kViolet-3,kBlue-3,kAzure+5,kTeal-6,kTeal+2,kAzure+5,kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6, kTeal+2,kBlue-3, kAzure+5, kTeal-6};
    Int_t markerSig[16] = {24,25,26,27,28,30,26,27,24,25,26,27,24,25,26,27};

    Int_t colors[20] = {1,2,3,4,6,1,2,3,4,6,1,2,3,4,6,1,2,3,4,6};

    TString names[18];
    names[0] = "MSVx_jetIso_vsEt_fPass_bar_eff";
    names[1] = "MSVx_jetIso_vsdR_fPass_bar_eff";
    names[2] = "MSVx_trkIso_vsPt_fPass_bar_eff";
    names[3] = "MSVx_trkIso_vsdR_fPass_bar_eff";
    names[4] = "MSVx_trkSumIso_vsPt_fPass_bar_eff";
    names[5] = "MSVx_trkSumIso_vsdR_fPass_bar_eff";
    names[6] = "MSVx_jetIso_vsEt_fPass_ec_eff";
    names[7] = "MSVx_jetIso_vsdR_fPass_ec_eff";
    names[8] = "MSVx_trkIso_vsPt_fPass_ec_eff";
    names[9] = "MSVx_trkIso_vsdR_fPass_ec_eff";
    names[10] = "MSVx_trkSumIso_vsPt_fPass_ec_eff";
    names[11] = "MSVx_trkSumIso_vsdR_fPass_ec_eff";

    names[12] = "MSVx_nMDT_fPass_bar_eff";
    names[13] = "MSVx_nMDT_up_fPass_bar_eff";
    names[14] = "MSVx_nRPC_fPass_bar_eff";
    names[15] = "MSVx_nMDT_fPass_ec_eff";
    names[16] = "MSVx_nMDT_up_fPass_ec_eff";
    names[17] = "MSVx_nTGC_fPass_ec_eff";

    TString titles[18];
    titles[0] = "max jet E_{T}";
    titles[1] = "min jet #Delta R";
    titles[2] = "max track p_{T}";
    titles[3] = "min track #Delta R";
    titles[4] = "max #Sigma track p_{T} in #Delta R = 0.2";
    titles[5] = "min #Delta R with #Sigma track p_{T} > 10 GeV";
    titles[6] = "max jet E_{T}";
    titles[7] = "min jet #Delta R";
    titles[8] = "max track p_{T}";
    titles[9] = "min track #Delta R";
    titles[10] = "max #Sigma track p_{T} in #Delta R = 0.2";
    titles[11] = "min #Delta R with #Sigma track p_{T} > 10 GeV";
    titles[12] = "min MDT hits";
    titles[13] = "max MDT hits";
    titles[14] = "min RPC hits";
    titles[15] = "min MDT hits";
    titles[16] = "max MDT hits";
    titles[17] = "min TGC hits";

    //SetAtlasStyle();
    //gStyle->SetPalette(1);

    TCanvas* c = new TCanvas("c_1","c_1",800,600);

    for(int iFile=4;iFile<10;iFile++){ //loop through types of histograms (stealth, HSS, etc)
        TFile *_sig[30];
        TString sigNames[30];
	if(iFile==4) continue;
        getFiles(_sig, iFile, nSIG, type);
        std::cout << "file is now " << nSIG << " of type: " << type << std::endl;
        for(int isig=0; isig<nSIG; isig++){ sigNames[isig] = convertFiletoLabel(_sig[isig]->GetName(), add_ctau);}

        //c->SetLogy();

        for(unsigned int i_plt=0;i_plt<18; i_plt++){ //loop through GVC plots
            std::cout << "now running plot: " << names[i_plt] << std::endl; 
            TMultiGraph *mg = new TMultiGraph();
            std::vector<TGraphAsymmErrors *> g_bkg; std::vector<TGraphAsymmErrors *> g_sig;
            for(int j_bkg=0; j_bkg<nBKG; j_bkg++){
                std::cout << "getting background plot: " << j_bkg;
                g_bkg.push_back((TGraphAsymmErrors*) _bkg[j_bkg]->Get(names[i_plt]));
                g_bkg.at(j_bkg)->SetMarkerColor(colorBkg[j_bkg]); g_bkg.at(j_bkg)->SetLineColor(colorBkg[j_bkg]);
                g_bkg.at(j_bkg)->SetMarkerStyle(markerBkg[j_bkg]);
                mg->Add(g_bkg.at(j_bkg));
            }
            std::cout << std::endl;
            TGraph *g_new[16];
            for(int j_sig=0; j_sig<nSIG; j_sig++){
                std::cout << "getting signal plot: " << j_sig;
                g_sig.push_back( (TGraphAsymmErrors*) _sig[j_sig]->Get(names[i_plt]));
                g_sig.at(j_sig)->SetMarkerColor(colorSig[j_sig]); g_sig.at(j_sig)->SetLineColor(colorSig[j_sig]);
                g_sig.at(j_sig)->SetMarkerStyle(markerSig[j_sig]);
                mg->Add(g_sig.at(j_sig));

                //g_sig[j]->Divide(g_sig[j]->GetHistogram(),g_bkg[2]->GetHistogram());
                std::cout << " signal number " << j_sig << ",  with name " << sigNames[j_sig] << std::endl;
                if(doSoverB){
                    Double_t divVal[g_sig.at(j_sig)->GetN()];
                    Double_t errorVal_up[g_sig.at(j_sig)->GetN()];
                    Double_t errorVal_down[g_sig.at(j_sig)->GetN()];

                    double maxDivVal = 0;

                    for(int k=0;k<g_sig.at(j_sig)->GetN();k++){
                        if(g_bkg.at(0)->GetY()[k] != 0) {
                            divVal[k] = g_sig.at(j_sig)->GetY()[k] / g_bkg[0]->GetY()[k];
                            errorVal_up[k] = sqrt( pow((g_sig.at(j_sig)->GetErrorYhigh(k)/ g_sig.at(j_sig)->GetY()[k]),2) + pow(g_bkg.at(j_sig)->GetErrorYhigh(k)/ g_bkg.at(0)->GetY()[k],2))*divVal[k];
                            errorVal_down[k] = sqrt( pow( (g_sig.at(j_sig)->GetErrorYlow(k)/ g_sig.at(j_sig)->GetY()[k]),2) + pow( g_bkg.at(j_sig)->GetErrorYlow(k)/g_bkg.at(0)->GetY()[k],2))*divVal[k];
                            //std::cout << "bin " << k << ", " << g_sig[j]->GetY()[k] << "/" << g_bkg[2]->GetY()[k] << "=" << divVal[k] << std::endl;
                            if(divVal[k] > maxDivVal) maxDivVal = divVal[k];
                        }
                        else divVal[k] = 0;
                        //std::cout << "bin " << k << ", " << g_sig[j]->GetY()[k] << "/" << g_bkg[0]->GetY()[k] << "=" << divVal[k] << std::endl;
                    }
                    g_new[j_sig] = new TGraphAsymmErrors(g_bkg[0]->GetN(),g_bkg[0]->GetX(),divVal,0,0,errorVal_up, errorVal_down);

                    g_new[j_sig]->GetYaxis()->SetRangeUser(0.0,maxDivVal*1.5);
                    g_new[j_sig]->SetMinimum(0.0);
                    g_new[j_sig]->SetMaximum(maxDivVal*1.5);
                    g_new[j_sig]->GetXaxis()->SetTitle(g_sig.at(j_sig)->GetXaxis()->GetTitle());
                    g_new[j_sig]->GetYaxis()->SetTitle("Signal / Background");
                    g_new[j_sig]->SetMarkerColor(colorSig[j_sig]); g_new[j_sig]->SetLineColor(colorSig[j_sig]);
                    g_new[j_sig]->SetMarkerStyle(markerSig[j_sig]);

                    if(i_plt==1 || i_plt == 3 || i_plt == 5 || i_plt ==7 || i_plt == 9 || i_plt == 11){ g_new[j_sig]->GetXaxis()->SetLimits(0,1);}
                    else if(i_plt==0 || i_plt == 6){ g_new[j_sig]->GetXaxis()->SetLimits(20,200);}
                    else if(i_plt==2 || i_plt == 8){
                        //if(i==2) g_new[j]->GetYaxis()->SetRangeUser(0,4000);
                        //if(i==0) g_new[j]->GetYaxis()->SetRangeUser(0,9000);
                        g_new[j_sig]->GetXaxis()->SetLimits(2,20);
                    }
                    else if(i_plt==4 || i_plt == 10){ g_new[j_sig]->GetXaxis()->SetLimits(2,30);}
                    else if(i_plt > 11){
                        g_new[j_sig]->GetXaxis()->SetRangeUser(200,5000);
                        g_new[j_sig]->GetXaxis()->SetLimits(200,5000);
                    }

                    if(j_sig==0) g_new[j_sig]->Draw("AP");
                    else g_new[j_sig]->Draw("P SAME");
                }
            }
            std::cout << std::endl;
            //for(int j=0;j<nBKG;j++){}
            //for(int j=0;j<nDATA;j++){g_data[j]->Draw("P SAME");}


            //latex.DrawLatex(.43,.86,"#sqrt{s} = 13 TeV, 78 pb^{-1}");

            /* TLegend *leg = new TLegend(0.63,0.835,0.95,0.935);
             leg->SetFillStyle(0);
             leg->SetBorderSize(0);
             leg->SetTextSize(0.03);
             leg-> SetNColumns(3);
             for(int j=0;j<nBKG;j++) leg->AddEntry(g_bkg[j],bkgNames[j],"lp");
             leg->Draw();

             TLegend *legD = new TLegend(0.73,0.835,0.93,0.86);
             legD->SetFillStyle(0);
             legD->SetBorderSize(0);
             legD->SetTextSize(0.03);
             legD->AddEntry(g_data[0],dataNames[0],"lp");
             legD->Draw();
             */
            if(doSoverB){
                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.04);
                latex.SetTextAlign(13);  //align at top
                if(!forThesis) latex.DrawLatex(.18,.91,"#font[72]{ATLAS}  Internal");
                TLegend *legS = new TLegend(0.48,0.835,0.95,0.93);
                legS->SetFillStyle(0);
                legS->SetBorderSize(0);
                legS->SetTextSize(0.03);
                legS->SetNColumns(2);
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(11);  //align at top
                double y_val_label = 0.87;
                if(i_plt < 7){
                    latex2.DrawLatex(.2,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");
                }
                else {
                    latex2.DrawLatex(.2,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");
                }

                for(int j=0;j<nSIG;j++) legS->AddEntry(g_new[j],sigNames[j],"lp");
                legS->Draw();

                gPad->RedrawAxis();
                c->Print(outputFolder+"JZAll_"+names[i_plt]+"_"+type+".pdf");
                //c->Clear();

            } else {

                mg->Draw("AP");
                mg->GetXaxis()->SetTitle(g_sig[0]->GetXaxis()->GetTitle());
                mg->GetYaxis()->SetTitle("Acceptance");
                if(i_plt==1 || i_plt == 3 || i_plt == 5 || i_plt == 7 || i_plt == 9 || i_plt == 11){
                    mg->GetXaxis()->SetLimits(0,1);
                }
                else if(i_plt==0 || i_plt == 6)mg->GetXaxis()->SetLimits(20,200);
                else if(i_plt==2 || i_plt == 8)mg->GetXaxis()->SetLimits(0,20);
                else if(i_plt==4 || i_plt == 10)mg->GetXaxis()->SetLimits(0,30);
                else if (i_plt > 11) mg->GetXaxis()->SetLimits(200,5000);

                mg->SetMaximum(1.4);
                c->Update();

                TLine cutVal;
                //cutVal.SetNDC();
                cutVal.SetLineColor(kGray+1);
                cutVal.SetLineWidth(2);
                cutVal.SetLineStyle(7);
                cutVal.SetY1(0); cutVal.SetY2(1);
                if(i_plt == 0 || i_plt == 6){ cutVal.SetX1(30); cutVal.SetX2(30);}
                else if(i_plt == 1 || i_plt == 3){ cutVal.SetX1(0.3); cutVal.SetX2(0.3);}
                else if(i_plt == 7 || i_plt == 9){ cutVal.SetX1(0.6); cutVal.SetX2(0.6);}
                else if(i_plt == 5 || i_plt == 11){ cutVal.SetX1(0.2); cutVal.SetX2(0.2);}
                else if(i_plt == 2 || i_plt == 8){ cutVal.SetX1(5); cutVal.SetX2(5);}
                else if(i_plt == 4 || i_plt == 10){ cutVal.SetX1(10); cutVal.SetX2(10);}
                else if(i_plt == 12 || i_plt == 15){cutVal.SetX1(300); cutVal.SetX2(300);}
                else if(i_plt == 13 || i_plt == 16){cutVal.SetX1(3000); cutVal.SetX2(3000);}
                else if(i_plt == 14 || i_plt == 17){cutVal.SetX1(250); cutVal.SetX2(250);}

                cutVal.Draw();	
                TLatex latex2;
                latex2.SetNDC();
                latex2.SetTextColor(kBlack);
                latex2.SetTextSize(0.035);
                latex2.SetTextAlign(11);  //align at top

                double y_val_label = 0.85;
                if(i_plt < 7){
                    latex2.DrawLatex(.185,y_val_label,"#font[52]{Barrel }#font[42]{(|#eta| < 0.8)}");
                }
                else {
                    latex2.DrawLatex(.158,y_val_label,"#font[52]{Endcaps }#font[42]{(1.3 < |#eta| < 2.5)}");
                }

                g_bkg[0]->Draw("P SAME");
                TLatex latex;
                latex.SetNDC();
                latex.SetTextColor(kBlack);
                latex.SetTextSize(0.04);
                latex.SetTextAlign(13);  //align at top
                if(!forThesis) latex.DrawLatex(.18,.92,"#font[72]{ATLAS}  Internal");

                TLegend *leg = new TLegend(0.6 ,0.8,0.8,0.82);
                //if(nBKG == 1){ leg->SetX1(0.2); leg->SetX2(0.46); leg->SetY1(0.82); leg->SetY2(0.86);} 
                leg->SetFillStyle(0);
                leg->SetBorderSize(0);
                leg->SetTextSize(0.03);
                //leg-> SetNColumns(1);
                leg->AddEntry(g_bkg[0],bkgNames[0],"lp");
                TLegend *legB = new TLegend(0.18,0.815,0.46,0.825);
                legB->SetFillStyle(0);
                legB->SetBorderSize(0);
                legB->SetTextSize(0.03);
                legB->SetNColumns(3);
                for(int j=1;j<nBKG;j++){
                    legB->AddEntry(g_bkg[j],bkgNames[j],"lp");
                }
                legB->Draw();
                leg->Draw();

                TLegend *legS = new TLegend(0.48,0.835,0.95,0.93);
                if(iFile != 4) legS->SetX1(0.4);
                legS->SetFillStyle(0);
                legS->SetBorderSize(0);
                legS->SetTextSize(0.03);
                legS->SetNColumns(2);
                for(int j=0;j<nSIG;j++) legS->AddEntry(g_sig[j],sigNames[j],"lp");
                legS->Draw();

                gPad->RedrawAxis();
                c->Print(outputFolder+"separateSB/JZAll_"+names[i_plt]+"_"+type+"_onlyAll.pdf");
                c->Clear();
                delete mg;	
            } //end else if not SoverB
        } // end loop through hists
    } //end loop through different models
    /*

     TLegend *legD = new TLegend(0.5,0.25,0.93,0.5);
     legD->SetFillStyle(0);
     legD->SetBorderSize(0);
     legD->SetTextSize(0.03);
     c->SetTopMargin(0.1);
     for(unsigned int i=0;i<10; i++){
     TGraphAsymmErrors *g_bkg = (TGraphAsymmErrors*) _bkg[0]->Get(names[i]);
     TGraphAsymmErrors *g_sig1 = (TGraphAsymmErrors*) _sig[0]->Get(names[i]);
     TGraphAsymmErrors *g_sig2 = (TGraphAsymmErrors*) _sig[1]->Get(names[i]);
     TGraphAsymmErrors *g_sig3 = (TGraphAsymmErrors*) _sig[2]->Get(names[i]);

     if(i==1 || i == 3 || i == 6 || i == 8)g_sig1->GetXaxis()->SetRangeUser(0,1);
     else if(i==0 || i == 5)g_sig1->GetXaxis()->SetRangeUser(0,200);
     else if(i==2 || i == 7)g_sig1->GetXaxis()->SetRangeUser(0,20);
     else if(i==4 || i == 9)g_sig1->GetXaxis()->SetRangeUser(0,30);

     TGraph *g_roc = new TGraph(g_bkg->GetN(),g_bkg->GetY(),g_sig1->GetY());
     g_roc->SetLineColor(colors[i]);
     g_roc->SetMarkerColor(colors[i]);
     g_roc->SetFillColor(kWhite);
     g_roc->SetMarkerStyle(20+i);
     g_roc->SetLineWidth(2);
     g_roc->SetTitle(titles[i]);
     g_roc->GetXaxis()->SetTitle("Background acceptance");
     g_roc->GetYaxis()->SetTitle("Signal acceptance");
     if(i==0 || i == 5){
     legD->Clear();
     g_roc->Draw("ALP");
     }
     else g_roc->Draw("LP SAME");

     legD->AddEntry(g_roc,TString(titles[i]),"lp");


     if(i==4 || i == 9){
     TLatex latex;
     latex.SetNDC();
     latex.SetTextColor(kBlack);
     latex.SetTextSize(0.05);
     latex.SetTextAlign(13);  //align at top
     latex.DrawLatex(.16,.96,"#font[72]{ATLAS}  Internal");
     TLatex latex2;
     latex2.SetNDC();
     latex2.SetTextColor(kBlack);
     latex2.SetTextSize(0.04);
     latex2.SetTextAlign(13);  //align at top
     if(i==4)latex2.DrawLatex(.5,.96,sigNames[0]+", Barrel vertices");
     else if (i==9)latex2.DrawLatex(.5,.96,sigNames[0]+", Endcap vertices"); 
     legD->Draw();

     c->Print("combinedPlots/ROC15_1258"+names[i]+".pdf");
     }


     }*/

    return 314;

}
