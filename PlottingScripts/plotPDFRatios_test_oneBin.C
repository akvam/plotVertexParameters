TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){
    
    std::cout << "file name : " << fileName << std::endl;
    TString title = "";
    
    if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}
std::pair<double, double> plotPDFRatios_test_oneBin(TString sample, TString type) {

    TFile* file[1];

    TH1D*     h1[2];
    TH1D*     err_n[101];
    TH1D*     err_d[101];
    if(type.Contains("Trig")){
        file[0] = new TFile("../OutputPlots/signalMC/"+sample+"/outputMSTrigEff_pdfSyst_v3.root");
    } else if (type.Contains("Vx")){
        file[0] = new TFile("../OutputPlots/signalMC/"+sample+"/outputMSVxEff_pdfSyst_v3.root");

    } else if(type.Contains("Jet")){
        file[0] = new TFile("../OutputPlots/signalMC/"+sample+"/outputJetEffSyst_v3.root");
    }
    //file[0] = new TFile("mergedeffOutput_mg500.v034_PDF-MSTW2008lo68cl.root");
    TString outDir = "../OutputPlots/signalMC/"+sample+"/";

    TCanvas *c1 = new TCanvas("c1","c1",800,600);

    SetAtlasStyle();

    TLatex latex;
    latex.SetNDC();
    latex.SetTextColor(kBlack);
    latex.SetTextSize(0.04);
    latex.SetTextAlign(13);  //align at top
    SetAtlasStyle();

    char err_num[40];
    char err_den[40];

    h1[0] = (TH1D*)file[0]->Get(type+"_0");
    h1[1] = (TH1D*)file[0]->Get(type+"_0_denom");

    err_n[0] = (TH1D*)file[0]->Get(type+"_0")->Clone();
    err_d[0] =(TH1D*)file[0]->Get(type+"_0_denom")->Clone();

    /*char name[40];
    for(int i=0;i<60;i++){
        sprintf(name,"%s%d","Bin_differenes_",i);
        h_diffs[i] =  new TH1D(name,name,50,0.5,1.5);
    }*/
    bool isBarrel = (type.Contains("Barrel") || type.Contains("1B") || type.Contains("Lxy") || type.Contains("B")) ? true : false;
    std::cout <<" is barrel? " << isBarrel << std::endl;


    int rebinFactor = h1[0]->GetNbinsX();
    
    h1[0]->Rebin(rebinFactor);
    h1[1]->Rebin(rebinFactor);
    

    err_n[0]->Rebin(rebinFactor);
    err_d[0]->Rebin(rebinFactor);
    
    h1[0]->Divide(h1[0],h1[1],1.0,1.0,"B");
    for(int i=1; i<101; i++){

        sprintf(err_num,"%s%d", TString(type+"_").Data(),i);
        sprintf(err_den,"%s%d%s",TString(type+"_").Data(),i, "_denom");

        err_n[i] = (TH1D*)file[0]->Get(err_num); 
        err_d[i] = (TH1D*)file[0]->Get(err_den);
        err_n[i]->Rebin(rebinFactor);
        err_d[i]->Rebin(rebinFactor);
        
        err_n[i]->Divide(err_n[i],err_d[i],1.0,1.0,"B");

        //up_m[i]->Draw("SAME");
        //down_m[i]->Draw("SAME");

    }
     std::cout <<" got pdf hists" << std::endl;
    for(int jbin=0;jbin<h1[0]->GetNbinsX();jbin++){
        double delta_bin2 = 0;
        double delta_bin2_err = 0;
        double bin_0 = h1[0]->GetBinContent(jbin+1);
        double bin_0_e = h1[0]->GetBinError(jbin+1);
        if(bin_0 == 0) continue;

        for(int i=1;i<101;i++){
            double b_up = err_n[i]->GetBinContent(jbin+1);
            double b_up_e = err_n[i]->GetBinError(jbin+1);
            double d_bup = pow( (b_up - bin_0),2);
            delta_bin2 += d_bup;
            delta_bin2_err += 4*pow(b_up - bin_0,2) * (b_up_e*b_up_e + bin_0_e*bin_0_e);
        }
        float binval_up = 1. + 0.1*sqrt(delta_bin2)/bin_0;
        float binval_down = 1. - 0.1*sqrt(delta_bin2)/bin_0;
        delta_bin2_err = 0.5*sqrt(delta_bin2_err)/sqrt(delta_bin2);
        float binerr_up = sqrt(delta_bin2_err*delta_bin2_err/delta_bin2+bin_0_e*bin_0_e/bin_0/bin_0) * 0.1 *sqrt(delta_bin2)/bin_0;
        float binerr_down = sqrt(delta_bin2_err*delta_bin2_err/delta_bin2+bin_0_e*bin_0_e/bin_0/bin_0) * 0.1 *sqrt(delta_bin2)/bin_0;
        //cout << binval_up << ", " << bin_0 << ", " << binval_down << endl;

        if(bin_0){
            err_n[0]->SetBinContent(jbin+1,binval_up);
            err_n[0]->SetBinError(jbin+1,binerr_up);
            err_d[0]->SetBinContent(jbin+1,binval_down);
            err_d[0]->SetBinError(jbin+1,binerr_down);
        } else{
            err_n[0]->SetBinContent(jbin+1,0);
            err_n[0]->SetBinError(jbin+1,0);
            err_d[0]->SetBinContent(jbin+1,0);
            err_d[0]->SetBinError(jbin+1,0);
        }
    }

    //up_m[0]->Draw("SAME");
    //down_m[0]->Draw("SAME");

    for(int i=1;i<51;i++){
        err_n[2*i]->SetMarkerColor(i+50); err_n[2*i]->SetLineColor(i+50); err_n[2*i]->SetMarkerStyle(20); err_n[2*i]->SetMarkerSize(1.2);
        err_n[2*i-1]->SetMarkerColor(i+50); err_n[2*i-1]->SetLineColor(i+50); err_n[2*i-1]->SetMarkerStyle(20); err_n[2*i-1]->SetMarkerSize(1.2);
    }

    err_n[0]->SetMarkerColor(kBlack); err_n[0]->SetLineColor(kBlack); err_n[0]->SetMarkerStyle(20); err_n[0]->SetMarkerSize(1.2);
    err_d[0]->SetMarkerColor(kBlack); err_d[0]->SetLineColor(kBlack); err_d[0]->SetMarkerStyle(20); err_d[0]->SetMarkerSize(1.2);

    for(int i=1; i<101; i++){
        err_n[i]->Divide(err_n[i],h1[0]);//,1.0,1.0,"B");
        /*for(int i_bin=1;i_bin < 32;i_bin++){
            h_diffs[i_bin]->Fill(err_n[i]->GetBinContent(i_bin));
        } */
    }

    c1->Clear();

    /*TF1 *fit1 = new TF1("fit1", "gaus");
 for(int i_bin=11;i_bin < 32;i_bin++){
     if(h_diffs[i_bin]->GetEntries() == 0) continue;
     h_diffs[i_bin]->Draw();

     h_diffs[i_bin]->Fit(fit1);
     c1->Print(TString(h_diffs[i_bin]->GetName())+".pdf");
     double eup = h_diffs[i_bin]->GetFunction("fit1")->GetParameter(1) + 3.0*h_diffs[i_bin]->GetFunction("fit1")->GetParameter(2);
     double edown = h_diffs[i_bin]->GetFunction("fit1")->GetParameter(1) - 3.0*h_diffs[i_bin]->GetFunction("fit1")->GetParameter(2);
     error_up.push_back(eup);
     error_down.push_back(edown);
     up_m[0]->SetBinContent(i_bin, eup);
     down_m[0]->SetBinContent(i_bin, edown);
 }*/
    err_n[1]->GetYaxis()->SetRangeUser(0.85,1.3);
    if(type.Contains("Trig")){
        err_n[1]->GetYaxis()->SetTitle("MS Trig Eff (PDF error set/centre)");
    } else if (type.Contains("Vx")){
        err_n[1]->GetYaxis()->SetTitle("MS Vertex Reco Eff (PDF error set/centre)");

    }else {
        err_n[1]->GetYaxis()->SetTitle("Jet Reco Eff (PDF error set/centre)");
    }
    if(isBarrel){    
        err_n[1]->GetXaxis()->SetTitle("LLP L_{xy} [m]");
        err_n[1]->GetXaxis()->SetRangeUser(3,8);
    } else {
        err_n[1]->GetXaxis()->SetTitle("LLP L_{z} [m]");
        err_n[1]->GetXaxis()->SetRangeUser(5,15);        
    }

    cout << err_n[0]->GetBinContent(10) << endl;

    err_n[1]->Draw("HIST");  
    for(int i=2;i<51;i++){
        if(err_n[i]->GetMaximum() <0.95) std::cout << i << std::endl;
        err_n[i]->Draw("SAMEHIST");  
    }
    err_n[0]->Draw("SAMEHIST");
    err_d[0]->Draw("SAMEHIST");
    ATLASLabel(0.2,0.88,"Internal");
    gPad->RedrawAxis();

    TLegend *leg = new TLegend(0.18,0.74,0.4,0.86);
    leg->SetFillStyle(0);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.04);
    leg->AddEntry(err_n[0],"Central value #pm #sigma","l");
    leg->Draw();

    latex.DrawLatex(0.5,0.88,convertFiletoLabel(sample));

    c1->Print(outDir+type+"_pdf_errors_1bin_v3.root");
    c1->Print(outDir+type+"_pdf_errors_1bin_v3.pdf");

    if(isBarrel){    
        err_d[0]->GetXaxis()->SetTitle("LLP L_{xy} [m]");
        err_d[0]->GetXaxis()->SetRangeUser(3,8);
    } else {
        err_d[0]->GetXaxis()->SetTitle("LLP L_{z} [m]");
        err_d[0]->GetXaxis()->SetRangeUser(5,15);        
    }    
    if(type.Contains("Trig")){
        err_d[0]->GetYaxis()->SetTitle("MS Trig Eff (PDF total error/centre)");
    } else if (type.Contains("Vx")){
        err_d[0]->GetYaxis()->SetTitle("MS Vertex Reco Eff (PDF total error/centre)");
    } else {
        err_d[0]->GetYaxis()->SetTitle("Jet Reco Eff (PDF total error/centre)");    
    }  

    err_n[0]->SetMarkerColor(kOrange+7); err_n[0]->SetLineColor(kOrange+7); err_n[0]->SetMarkerStyle(21); err_n[0]->SetMarkerSize(1.2);
    err_d[0]->SetMarkerColor(kBlue-4); err_d[0]->SetLineColor(kBlue-4); err_d[0]->SetMarkerStyle(20); err_d[0]->SetMarkerSize(1.2);

    err_d[0]->GetYaxis()->SetRangeUser(0.85,1.15);

    TF1 *fitdown = new TF1("fitdown", "pol0");
    TF1 *fitup = new TF1("fitup", "pol0");
    err_d[0]->Fit(fitdown);
    err_n[0]->Fit(fitup);

    err_d[0]->Draw();
    err_n[0]->Draw("SAME");

    ATLASLabel(0.2,0.88,"Internal");

    char buffer [100];
    int nc = sprintf(buffer, "Fit up: %1.3f +/- %1.3f", err_n[0]->GetFunction("fitup")->GetParameter(0), err_n[0]->GetFunction("fitup")->GetParError(0));
    TString fitVal =  TString(buffer);
    nc = sprintf(buffer, "Fit down: %1.3f +/- %1.3f", err_d[0]->GetFunction("fitdown")->GetParameter(0), err_d[0]->GetFunction("fitdown")->GetParError(0));

    TString fitVal2 =  TString(buffer);

    std::cout << fitVal << std::endl; 
    std::cout << fitVal2 << std::endl; 

    double error_up = 0; double error_down = 0;
    
    double original_error_up = err_n[0]->GetBinContent(1) - 1;
    double original_error_down =  1 - err_d[0]->GetBinContent(1);
    
    if(original_error_up < 0 && original_error_down < 0){
        error_up = TMath::Abs(original_error_down);
        error_down = TMath::Abs(original_error_up);
    } else if(original_error_up < 0 && original_error_down >= 0){
        if(TMath::Abs(original_error_up) > original_error_down) error_down = TMath::Abs(original_error_up);
        else error_down = original_error_down;
    } else if(original_error_down < 0 && original_error_up >= 0){
        if(TMath::Abs(original_error_down) > original_error_up) error_up = TMath::Abs(original_error_down);
        else error_up = original_error_up;
    } else if(original_error_up > 0 && original_error_down > 0){
        error_up = original_error_up;
        error_down = original_error_down;
    }
    std::pair<double, double> errors = std::make_pair(error_up, error_down);

    std::cout << error_up << std::endl;
    std::cout <<  error_down << std::endl;
    
    //latex2.DrawLatex(.15,.98,title + ", Data/MC");

    latex.DrawLatex(.2,0.37, fitVal);
    latex.DrawLatex(.2,0.3, fitVal2);

    leg->Clear();
    leg->AddEntry(err_n[0],"Central value + #sigma","lp");
    leg->AddEntry(err_d[0],"Central value - #sigma","lp");
    leg->Draw();
    latex.DrawLatex(0.5,0.88,convertFiletoLabel(sample));
    gPad->RedrawAxis();

    c1->Print(outDir+type+"_pdf_errors_fits_1bin_v3.root");
    c1->Print(outDir+type+"_pdf_errors_fits_1bin_v3.pdf");

    return errors;
    
}



