#include "../PlottingTools/PlottingPackage/InputFiles.h"
#include <stdio.h>

#include "Riostream.h"
#include <iostream>

#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStopwatch.h"
#include "TMultiGraph.h"
#include <utility>
#include "TFile.h"
#include <TVector3.h>
#include "TLine.h"
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"


TString convertFiletoLabel(TString fileName, bool add_ctau);
TString convertFiletoLabel(TString fileName, bool add_ctau = false){

    TString title = "";

    if(fileName.Contains("HChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        if(channel == "nubb") channel = "#nu b#bar{b}";
        TString mchi = (TString)fileName(fileName.First("5")+5,fileName.Sizeof());
        title = (TString) "H #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    } else if(fileName.Contains("WChi")){
        TString channel = (TString)fileName(fileName.First("_")+1,fileName.First("m")-9);
        TString mchi = (TString)fileName(fileName.First("m")+4,fileName.Sizeof());
        title = (TString) "W/Z #rightarrow #chi#chi(#chi#rightarrow"+channel+"), m_{#chi} = "+mchi+" GeV";
        return title;
    }else if(fileName.Contains("mS")){
        TString lt = "";
        TString mS = (TString) fileName(fileName.First("S")+1,fileName.First("l")-fileName.First("S")-1);
        TString mH = (TString) fileName(fileName.First("H")+1,fileName.First("S")-fileName.First("H")-2);
        if( add_ctau ) lt = (TString) " c#tau_{lab} = " + fileName(fileName.First("t")+1,fileName.Sizeof()-fileName.First("t")-1) + "m";
        if(mH == "125") title = (TString) "m_{H},m_{s}=[125,"+mS+"] GeV" + lt;
        else title = (TString) "m_{#Phi},m_{s}=["+mH+","+mS+"] GeV" + lt;
        return title;
    } else if(fileName.Contains("mg")){
        TString mg = (TString)fileName(fileName.First("g")+1,fileName.Sizeof()-fileName.First("g")-1);
        title = (TString) "m_{#tilde{g}} = "+mg+" GeV";
    }
    return title;
}

int plotNTracklets(){

        std::vector<TString> samples = {"mg250", "mg500", "mg800" ,"mg1200", "mg1500", "mg2000"};
	TH1D *nT[6];
   TString sigNames[6];

    for(int i=0; i<6; i++){ sigNames[i] = convertFiletoLabel(samples.at(i), false);}

    Int_t colorSig[16] = { kPink-3,kViolet-3,kBlue-3, kAzure+5, kTeal-6,kTeal+2, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2,kBlue-3, kAzure+5, kTeal-6,kTeal+2};

    Int_t markerSig[16] = {24,25,26,27,28,30,26,27,24,25,26,27,24,25,26,27};
        TLegend *legS = new TLegend(0.72,0.65,0.99,0.92);
        legS->SetFillStyle(0);
        legS->SetBorderSize(0);
        legS->SetTextSize(0.03);

	TChain *chain[6];
	for(int i=0;i<6;i++){
		chain[i] = new TChain("recoTree");
		InputFiles::AddFilesToChain(samples.at(i), chain[i]);
	}

        char name[40];
    	char histDrawString[100];
	double maxVal = 0;
	for(int i=0;i<6;i++){
         sprintf(name,"%s%d","nTracklets_",i);
	   sprintf(histDrawString,"%s%d","MSVertex_nMDT>>nTracklets_",i);
	  nT[i] = new TH1D(name, name, 100,0,4000);
	  
  	  chain[i]->Draw(TString(histDrawString),"pileupEventWeight"); 

	  nT[i]->SetLineColor(colorSig[i]);
	  nT[i]->SetMarkerColor(colorSig[i]);
	  nT[i]->SetMarkerStyle(markerSig[i]);
	  nT[i]->SetLineStyle(i+1);

            legS->AddEntry(nT[i],sigNames[i],"lp");
	nT[i]->Scale(1./nT[i]->Integral());
if(nT[i]->GetMaximum() > maxVal) maxVal = nT[i]->GetMaximum();

	}

	nT[0]->GetYaxis()->SetRangeUser(0.0001,1.0);
	nT[0]->Draw();
	nT[0]->GetYaxis()->SetTitle("Fraction of events");
	nT[0]->GetXaxis()->SetTitle("Number of MS tracklets");
	for(int i=1;i<6; i++){
	nT[i]->Draw("SAME");
        }
	legS->Draw();
    ATLASLabel(0.2,0.88,"Internal");
    gPad->RedrawAxis();
	return 9;
}
